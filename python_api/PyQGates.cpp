/** @file python_api/PyQGates.cpp

@brief Python API quantum gate classes

@copyright This file is part of the LibKet library (Python API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#include "PyQGates.hpp"

#define PYBIND11_INIT_GATE( GATE, MODULE )                              \
  py::class_<GATE, PyQBase, std::shared_ptr<GATE>>(MODULE, #GATE)       \
  .def(py::init<>())                                                    \
  .def("apply",    &GATE::apply,      "Apply #GATE gate to expression") \
  .def("dag",      &GATE::dag,        "Return adjoint of #GATE gate")   \
  .def("print",    &GATE::print,      "Print expression")               \
  .def("subexpr",  &GATE::subexpr,    "Return sub-expression")          \
  .def("__call__", &GATE::operator(), "Apply #GATE gate to expression") \
  .def("__repr__", &GATE::print,      "Print expression")

#define PYBIND11_INIT_GATE_ALIAS( ALIAS, GATE, MODULE ) \
  MODULE.def(ALIAS, []() { return GATE(); });

#define PYBIND11_UNARY_GATE( GATE, MODULE )                             \
  py::class_<GATE, PyQBase, std::shared_ptr<GATE>>(MODULE, #GATE)       \
  .def(py::init<>())                                                    \
  .def(py::init<std::shared_ptr<PyQObject>>())                          \
  .def("apply",    &GATE::apply,      "Apply gate to expression")       \
  .def("dag",      &GATE::dag,        "Return adjoint gate")            \
  .def("print",    &GATE::print,      "Print expression")               \
  .def("subexpr",  &GATE::subexpr,    "Return sub-expression")          \
  .def("__call__", &GATE::operator(), "Apply gate to expression")       \
  .def("__repr__", &GATE::print,      "Print expression")

#define PYBIND11_UNARY_GATE_ALIAS( ALIAS, GATE, MODULE )                \
  MODULE.def(ALIAS, []()                                                \
                    { return GATE(); });                                \
  MODULE.def(ALIAS, [](std::shared_ptr<PyQObject> obj)                  \
                    { return GATE(obj); });

#define PYBIND11_BINARY_GATE( GATE, MODULE )                            \
  py::class_<GATE, PyQBase, std::shared_ptr<GATE>>(MODULE, #GATE)       \
  .def(py::init<>())                                                    \
  .def(py::init<std::shared_ptr<PyQObject>,                             \
       std::shared_ptr<PyQObject>>())                                   \
  .def("apply",    &GATE::apply,      "Apply gate to expression")       \
  .def("dag",      &GATE::dag,        "Return adjoint gate")            \
  .def("print",    &GATE::print,      "Print expression")               \
  .def("subexpr",  &GATE::subexpr,    "Return sub-expression")          \
  .def("__call__", &GATE::operator(), "Apply gate to expression")       \
  .def("__repr__", &GATE::print,      "Print expression")

#define PYBIND11_BINARY_GATE_ALIAS( ALIAS, GATE, MODULE )               \
  MODULE.def(ALIAS, []()                                                \
                    { return GATE(); });                                \
  MODULE.def(ALIAS, [](std::shared_ptr<PyQObject> obj0,                 \
                       std::shared_ptr<PyQObject> obj1)                 \
                    { return GATE(obj0, obj1); });

#define PYBIND11_TERNARY_GATE( GATE, MODULE )                           \
  py::class_<GATE, PyQBase, std::shared_ptr<GATE>>(MODULE, #GATE)       \
  .def(py::init<>())                                                    \
  .def(py::init<std::shared_ptr<PyQObject>,                             \
       std::shared_ptr<PyQObject>,                                      \
       std::shared_ptr<PyQObject>>())                                   \
  .def("apply",    &GATE::apply,      "Apply gate to expression")       \
  .def("dag",      &GATE::dag,        "Return adjoint gate")            \
  .def("print",    &GATE::print,      "Print expression")               \
  .def("subexpr",  &GATE::subexpr,    "Return sub-expression")          \
  .def("__call__", &GATE::operator(), "Apply gate to expression")       \
  .def("__repr__", &GATE::print,      "Print expression")

#define PYBIND11_TERNARY_GATE_ALIAS( ALIAS, GATE, MODULE )              \
  MODULE.def(ALIAS, []()                                                \
                    { return GATE(); },                                 \
                    "Gate alias");                                      \
  MODULE.def(ALIAS, [](std::shared_ptr<PyQObject> obj0,                 \
                       std::shared_ptr<PyQObject> obj1,                 \
                       std::shared_ptr<PyQObject> obj2)                 \
                    { return GATE(obj0, obj1, obj2); });

namespace py = pybind11;

/**
   @brief Creates the LibKet quantum gate Python module

   @ingroup py_gates
*/
void pybind11_init_gate(py::module &m) {

  m.attr("__name__") = "pylibket.gate";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";

  // PyQGateBarrier
  PYBIND11_UNARY_GATE( PyQGateBarrier, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "barrier", PyQGateBarrier, m )
    ;
  
  // PyQGateCCNOT
  PYBIND11_TERNARY_GATE( PyQGateCCNOT, m )
    ;
  PYBIND11_TERNARY_GATE_ALIAS( "ccnot", PyQGateCCNOT, m )
    ;
  
  // PyQGateCNOT
  PYBIND11_BINARY_GATE( PyQGateCNOT, m )
    ;
  
  PYBIND11_BINARY_GATE_ALIAS( "cnot", PyQGateCNOT, m );
  
  m.def("cx", []() { return PyQGateCNOT(); });
  m.def("cx", [](std::shared_ptr<PyQObject> ctrl,                   
                 std::shared_ptr<PyQObject> obj) { return PyQGateCNOT(ctrl, obj); });
  

  // PyQGateCPhase
  PYBIND11_BINARY_GATE( PyQGateCPhase, m )
    
    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGateCPhase::angle)
    .def_readwrite("tol",   &PyQGateCPhase::tol)
    ;

  PYBIND11_BINARY_GATE_ALIAS( "cphase", PyQGateCPhase, m );

  m.def("cphase", [](const std::string& angle)     { return PyQGateCPhase(angle); });
  m.def("cphase", [](const std::string& angle,
                     const std::string& tol)       { return PyQGateCPhase(angle, tol); });
  m.def("cphase", [](std::shared_ptr<PyQObject> ctrl,                   
                     std::shared_ptr<PyQObject> obj,
                     const std::string& angle)     { return PyQGateCPhase(ctrl, obj, angle); });
  m.def("cphase", [](std::shared_ptr<PyQObject> ctrl,                   
                     std::shared_ptr<PyQObject> obj,
                     const std::string& angle,
                     const std::string& tol)       { return PyQGateCPhase(ctrl, obj, angle, tol); });
  
  // PyQGateCPhasedag
  PYBIND11_BINARY_GATE( PyQGateCPhasedag, m )
    
    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGateCPhasedag::angle)
    .def_readwrite("tol",   &PyQGateCPhasedag::tol)
    ;

  PYBIND11_BINARY_GATE_ALIAS( "cphasedag", PyQGateCPhasedag, m );

  m.def("cphasedag", [](const std::string& angle)     { return PyQGateCPhasedag(angle); });
  m.def("cphasedag", [](const std::string& angle,
                        const std::string& tol)       { return PyQGateCPhasedag(angle, tol); });
  m.def("cphasedag", [](std::shared_ptr<PyQObject> ctrl,                   
                        std::shared_ptr<PyQObject> obj,
                        const std::string& angle)     { return PyQGateCPhasedag(ctrl, obj, angle); });
  m.def("cphasedag", [](std::shared_ptr<PyQObject> ctrl,                   
                        std::shared_ptr<PyQObject> obj,
                        const std::string& angle,
                        const std::string& tol)       { return PyQGateCPhasedag(ctrl, obj, angle, tol); });
  
  // PyQGateCPhaseK
  PYBIND11_BINARY_GATE( PyQGateCPhaseK, m )
    
    .def(py::init<std::size_t>())
    .def(py::init<std::size_t, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, std::size_t>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, std::size_t, const std::string&>())
    
    .def_readwrite("k",   &PyQGateCPhaseK::k)
    .def_readwrite("tol", &PyQGateCPhaseK::tol)
    ;

  PYBIND11_BINARY_GATE_ALIAS( "cphasek", PyQGateCPhaseK, m );
  
  m.def("cphasek", [](std::size_t k)                { return PyQGateCPhaseK(k); });
  m.def("cphasek", [](std::size_t k,
                      const std::string& tol)       { return PyQGateCPhaseK(k, tol); });
  m.def("cphasek", [](std::shared_ptr<PyQObject> ctrl,                   
                      std::shared_ptr<PyQObject> obj,
                      std::size_t k)                { return PyQGateCPhaseK(ctrl, obj, k); });
  m.def("cphasek", [](std::shared_ptr<PyQObject> ctrl,                   
                      std::shared_ptr<PyQObject> obj,
                      std::size_t k,
                      const std::string& tol)       { return PyQGateCPhaseK(ctrl, obj, k, tol); });
  
  // PyQGateCPhaseKdag
  PYBIND11_BINARY_GATE( PyQGateCPhaseKdag, m )
    
    .def(py::init<std::size_t>())
    .def(py::init<std::size_t, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, std::size_t>())
    .def(py::init<std::shared_ptr<PyQObject>, std::shared_ptr<PyQObject>, std::size_t, const std::string&>())
    
    .def_readwrite("k",   &PyQGateCPhaseKdag::k)
    .def_readwrite("tol", &PyQGateCPhaseKdag::tol)
    ;
  
  PYBIND11_BINARY_GATE_ALIAS( "cphasekdag", PyQGateCPhaseKdag, m );
  
  m.def("cphasekdag", [](std::size_t k)                { return PyQGateCPhaseKdag(k); });
  m.def("cphasekdag", [](std::size_t k,
                         const std::string& tol)       { return PyQGateCPhaseKdag(k, tol); });
  m.def("cphasekdag", [](std::shared_ptr<PyQObject> ctrl,                   
                         std::shared_ptr<PyQObject> obj,
                         std::size_t k)               { return PyQGateCPhaseKdag(ctrl, obj, k); });
  m.def("cphasekdag", [](std::shared_ptr<PyQObject> ctrl,                   
                         std::shared_ptr<PyQObject> obj,
                         std::size_t k,
                         const std::string& tol)       { return PyQGateCPhaseKdag(ctrl, obj, k, tol); });
  
  // PyQGateCY
  PYBIND11_BINARY_GATE( PyQGateCY, m )    
    ;
  PYBIND11_BINARY_GATE_ALIAS( "cy", PyQGateCY, m )
    ;
  
  // PyQGateCZ
  PYBIND11_BINARY_GATE( PyQGateCZ, m )
    ;
  PYBIND11_BINARY_GATE_ALIAS( "cz", PyQGateCZ, m )
    ;

  // PyQGateHadamard
  PYBIND11_UNARY_GATE( PyQGateHadamard, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "h", PyQGateHadamard, m )
    ;

  m.def("hadamard", []() { return PyQGateHadamard(); });
  m.def("hadamard", [](std::shared_ptr<PyQObject> obj) { return PyQGateHadamard(obj); });
  
  // PyQGateIdentity
  PYBIND11_UNARY_GATE( PyQGateIdentity, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "i", PyQGateIdentity, m )
    ;
  m.def("identity", []() { return PyQGateIdentity(); });
  m.def("identity", [](std::shared_ptr<PyQObject> obj) { return PyQGateIdentity(obj); });

  // PyQGateInit
  PYBIND11_INIT_GATE( PyQGateInit, m )
    ;
  PYBIND11_INIT_GATE_ALIAS( "init", PyQGateInit, m )
    ;
  
  // PyQGateMeasure
  PYBIND11_UNARY_GATE( PyQGateMeasure, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "measure", PyQGateMeasure, m )
    ;
  
  // PyQGateMeasure_X
  PYBIND11_UNARY_GATE( PyQGateMeasure_X, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "measure_x", PyQGateMeasure_X, m )    
    ;

  // PyQGateMeasure_Y
  PYBIND11_UNARY_GATE( PyQGateMeasure_Y, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "measure_y", PyQGateMeasure_Y, m )
    ;
  
  // PyQGateMeasure_Z
  PYBIND11_UNARY_GATE( PyQGateMeasure_Z, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "measure_z", PyQGateMeasure_Z, m )    
    ;
  
  // PyQGatePauli_X
  PYBIND11_UNARY_GATE( PyQGatePauli_X, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "pauli_x", PyQGatePauli_X, m )
    ;

  // PyQGatePauli_Y
  PYBIND11_UNARY_GATE( PyQGatePauli_Y, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "pauli_y", PyQGatePauli_Y, m )
    ;

  // PyQGatePauli_Z
  PYBIND11_UNARY_GATE( PyQGatePauli_Z, m )
    ;
  PYBIND11_UNARY_GATE_ALIAS( "pauli_z", PyQGatePauli_Z, m )
    ;
  
  // PyQGatePhase
  PYBIND11_UNARY_GATE( PyQGatePhase, m )
    
    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGatePhase::angle)
    .def_readwrite("tol",   &PyQGatePhase::tol)
    ;

  PYBIND11_UNARY_GATE_ALIAS( "phase", PyQGatePhase, m );
  
  m.def("phase", [](const std::string& angle)     { return PyQGatePhase(angle); });
  m.def("phase", [](const std::string& angle,
                    const std::string& tol)       { return PyQGatePhase(angle, tol); });
  m.def("phase", [](std::shared_ptr<PyQObject> obj,
                    const std::string& angle)     { return PyQGatePhase(obj, angle); });
  m.def("phase", [](std::shared_ptr<PyQObject> obj,
                    const std::string& angle,
                    const std::string& tol)       { return PyQGatePhase(obj, angle, tol); });
  
  // PyQGatePrep_X
  PYBIND11_UNARY_GATE( PyQGatePrep_X, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "prep_x", PyQGatePrep_X, m )
  ;

  // PyQGatePrep_Y
  PYBIND11_UNARY_GATE( PyQGatePrep_Y, m )    
  ;
  PYBIND11_UNARY_GATE_ALIAS( "prep_y", PyQGatePrep_Y, m )
  ;
  
  // PyQGatePrep_Z
  PYBIND11_UNARY_GATE( PyQGatePrep_Z, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "prep_z", PyQGatePrep_Z, m )
  ;
  
  // PyQGateReset
  PYBIND11_UNARY_GATE( PyQGateReset, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "reset", PyQGateReset, m )
  ;
  
  // PyQGateRotate_MX90
  PYBIND11_UNARY_GATE( PyQGateRotate_MX90, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "rotate_mx90", PyQGateRotate_MX90, m )
  ;
  
  // PyQGateRotate_MY90
  PYBIND11_UNARY_GATE( PyQGateRotate_MY90, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "rotate_my90", PyQGateRotate_MY90, m )
  ;
  
  // PyQGateRotate_X
  PYBIND11_UNARY_GATE( PyQGateRotate_X, m )
    
    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&, const std::string&>())

    .def_readwrite("angle", &PyQGateRotate_X::angle)
    .def_readwrite("tol",   &PyQGateRotate_X::tol)
    ;

  PYBIND11_UNARY_GATE_ALIAS( "rotate_x", PyQGateRotate_X, m );
  
  m.def("rotate_x", [](const std::string& angle)     { return PyQGateRotate_X(angle); });
  m  .def("rotate_x", [](const std::string& angle,
                         const std::string& tol)       { return PyQGateRotate_X(angle, tol); });
  m.def("rotate_x", [](std::shared_ptr<PyQObject> obj,
                       const std::string& angle)     { return PyQGateRotate_X(obj, angle); });
  m.def("rotate_x", [](std::shared_ptr<PyQObject> obj,
                       const std::string& angle,
                       const std::string& tol)       { return PyQGateRotate_X(obj, angle, tol); });
  
  // PyQGateRotate_Xdag
  PYBIND11_UNARY_GATE( PyQGateRotate_Xdag, m )
    
    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGateRotate_Xdag::angle)
    .def_readwrite("tol",   &PyQGateRotate_Xdag::tol)
    ;

  PYBIND11_UNARY_GATE_ALIAS( "rotate_xdag", PyQGateRotate_Xdag, m );

  m.def("rotate_xdag", [](const std::string& angle)     { return PyQGateRotate_Xdag(angle); });
  m.def("rotate_xdag", [](const std::string& angle,
                          const std::string& tol)       { return PyQGateRotate_Xdag(angle, tol); });
  m.def("rotate_xdag", [](std::shared_ptr<PyQObject> obj,
                          const std::string& angle)     { return PyQGateRotate_Xdag(obj, angle); });
  m.def("rotate_xdag", [](std::shared_ptr<PyQObject> obj,
                          const std::string& angle,
                          const std::string& tol)       { return PyQGateRotate_Xdag(obj, angle, tol); });
  
  // PyQGateRotate_Y
  PYBIND11_UNARY_GATE( PyQGateRotate_Y, m )
    
    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGateRotate_Y::angle)
    .def_readwrite("tol",   &PyQGateRotate_Y::tol)
    ;

  PYBIND11_UNARY_GATE_ALIAS( "rotate_y", PyQGateRotate_Y, m );
  
  m.def("rotate_y", [](const std::string& angle)     { return PyQGateRotate_Y(angle); });
  m.def("rotate_y", [](const std::string& angle,
                       const std::string& tol)       { return PyQGateRotate_Y(angle, tol); });
  m.def("rotate_y", [](std::shared_ptr<PyQObject> obj,
                       const std::string& angle)     { return PyQGateRotate_Y(obj, angle); });
  m.def("rotate_y", [](std::shared_ptr<PyQObject> obj,
                       const std::string& angle,
                       const std::string& tol)       { return PyQGateRotate_Y(obj, angle, tol); });
  
  // PyQGateRotate_Ydag
  PYBIND11_UNARY_GATE( PyQGateRotate_Ydag, m )

    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGateRotate_Ydag::angle)
    .def_readwrite("tol",   &PyQGateRotate_Ydag::tol)
    ;

  PYBIND11_UNARY_GATE_ALIAS( "rotate_ydag", PyQGateRotate_Ydag, m );
  
  m.def("rotate_ydag", [](const std::string& angle)     { return PyQGateRotate_Ydag(angle); });
  m.def("rotate_ydag", [](const std::string& angle,
                          const std::string& tol)       { return PyQGateRotate_Ydag(angle, tol); });
  m.def("rotate_ydag", [](std::shared_ptr<PyQObject> obj,
                          const std::string& angle)     { return PyQGateRotate_Ydag(obj, angle); });
  m.def("rotate_ydag", [](std::shared_ptr<PyQObject> obj,
                          const std::string& angle,
                          const std::string& tol)       { return PyQGateRotate_Ydag(obj, angle, tol); });
  
  // PyQGateRotate_Z
  PYBIND11_UNARY_GATE( PyQGateRotate_Z, m )

    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGateRotate_Z::angle)
    .def_readwrite("tol",   &PyQGateRotate_Z::tol)
    ;

  PYBIND11_UNARY_GATE_ALIAS( "rotate_z", PyQGateRotate_Z, m );
  
  m.def("rotate_z", [](const std::string& angle)     { return PyQGateRotate_Z(angle); });
  m.def("rotate_z", [](const std::string& angle,
                       const std::string& tol)       { return PyQGateRotate_Z(angle, tol); });
  m.def("rotate_z", [](std::shared_ptr<PyQObject> obj,
                       const std::string& angle)     { return PyQGateRotate_Z(obj, angle); });
  m.def("rotate_z", [](std::shared_ptr<PyQObject> obj,
                       const std::string& angle,
                       const std::string& tol)       { return PyQGateRotate_Z(obj, angle, tol); });

  // PyQGateRotate_Zdag
  PYBIND11_UNARY_GATE( PyQGateRotate_Zdag, m )
    
    .def(py::init<const std::string&>())
    .def(py::init<const std::string&, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&>())
    .def(py::init<std::shared_ptr<PyQObject>, const std::string&, const std::string&>())
    
    .def_readwrite("angle", &PyQGateRotate_Zdag::angle)
    .def_readwrite("tol",   &PyQGateRotate_Zdag::tol)
    ;

  PYBIND11_UNARY_GATE_ALIAS( "rotate_zdag", PyQGateRotate_Zdag, m );
  
  m.def("rotate_zdag", [](const std::string& angle)     { return PyQGateRotate_Zdag(angle); });
  m.def("rotate_zdag", [](const std::string& angle,
                          const std::string& tol)       { return PyQGateRotate_Zdag(angle, tol); });
  m.def("rotate_zdag", [](std::shared_ptr<PyQObject> obj,
                          const std::string& angle)     { return PyQGateRotate_Zdag(obj, angle); });
  m.def("rotate_zdag", [](std::shared_ptr<PyQObject> obj,
                          const std::string& angle,
                          const std::string& tol)       { return PyQGateRotate_Zdag(obj, angle, tol); });
  
  // PyQGateRotate_X90
  PYBIND11_UNARY_GATE( PyQGateRotate_X90, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "rotate_x90", PyQGateRotate_X90, m )
  ;

  // PyQGateRotate_Y90
  PYBIND11_UNARY_GATE( PyQGateRotate_Y90, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "rotate_y90", PyQGateRotate_Y90, m )
  ;
  
  // PyQGateS
  PYBIND11_UNARY_GATE( PyQGateS, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "s", PyQGateS, m )
  ;
  
  // PyQGateSdag
  PYBIND11_UNARY_GATE( PyQGateSdag, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "sdag", PyQGateSdag, m )
  ;
    
  // PyQGateT
  PYBIND11_UNARY_GATE( PyQGateT, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "t", PyQGateT, m )
  ;

  // PyQGateTdag
  PYBIND11_UNARY_GATE( PyQGateTdag, m )
  ;
  PYBIND11_UNARY_GATE_ALIAS( "tdag", PyQGateTdag, m )
  ;
  
  // PyQGateSwap
  PYBIND11_BINARY_GATE( PyQGateSwap, m )    
  ;
  PYBIND11_BINARY_GATE_ALIAS( "swap", PyQGateSwap, m )
  ;
}
