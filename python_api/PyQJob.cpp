/** @file python_api/PyQJob.cpp

@brief Python API quantum job execution class

@copyright This file is part of the LibKet library (Python API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#include "PyQJob.hpp"

namespace py = pybind11;

/**
   @brief Creates the LibKet job execution Python module

   @ingroup py_api
*/
void pybind11_init_job(py::module &m) {

  m.attr("__name__") = "pylibket.job";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";
  
  py::class_<PyQJob, PyQBase, std::shared_ptr<PyQJob>>(m, "PyQJob");
}
