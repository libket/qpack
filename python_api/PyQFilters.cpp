/** @file python_api/PyQFilters.cpp

@brief Python API quantum filter classes

@copyright This file is part of the LibKet library (Python API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#include "PyQFilters.hpp"

#define PYBIND11_FILTER( FILTER, MODULE )                               \
  py::class_<FILTER, PyQBase, std::shared_ptr<FILTER>>(MODULE, #FILTER) \
  .def(py::init<>())                                                    \
  .def(py::init<std::shared_ptr<PyQObject>>())                          \
  .def("apply",    &FILTER::apply,      "Apply filter to expression")     \
  .def("print",    &FILTER::print,      "Print expression")             \
  .def("subexpr",  &FILTER::subexpr,    "Return sub-expression")        \
  .def("__call__", &FILTER::operator(), "Apply filter to expression")     \
  .def("__repr__", &FILTER::print,      "Print expression")

#define PYBIND11_FILTER_ALIAS( ALIAS, FILTER, MODULE )                  \
  MODULE.def(ALIAS, []()                                                \
                    { return FILTER(); });                              \
  MODULE.def(ALIAS, [](std::shared_ptr<PyQObject> obj)                  \
                    { return FILTER(obj); });

namespace py = pybind11;

/**
   @brief Creates the LibKet quantum filter Python module

   @ingroup py_filters
*/
void pybind11_init_filter(py::module &m) {

  m.attr("__name__") = "pylibket.filter";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";

  // PyQFilterSelect
  PYBIND11_FILTER( PyQFilterSelect, m )

    .def(py::init<const std::vector<std::size_t>&>())
    .def(py::init<const std::vector<std::size_t>&, std::shared_ptr<PyQObject>>())
    
    .def_readwrite("sel", &PyQFilterSelect::sel)
    ;

  PYBIND11_FILTER_ALIAS( "sel", PyQFilterSelect, m );
  
  m.def("sel", [](const std::vector<std::size_t>& sel) { return PyQFilterSelect(sel); });
  m.def("sel", [](const std::vector<std::size_t>& sel,
                  std::shared_ptr<PyQObject> obj)      { return PyQFilterSelect(sel, obj); });
  
  // PyQFilterSelectAll
  PYBIND11_FILTER( PyQFilterSelectAll, m )
    ;
  PYBIND11_FILTER_ALIAS( "all", PyQFilterSelectAll, m )
    ;
         
  // PyQFilterShift
  PYBIND11_FILTER( PyQFilterShift, m )

    .def(py::init<long int>())
    .def(py::init<long int, std::shared_ptr<PyQObject>>())
    
    .def_readwrite("offset", &PyQFilterShift::offset)
    ;

  PYBIND11_FILTER_ALIAS( "shift", PyQFilterShift, m );
  
  m.def("shift", [](long int offset)                { return PyQFilterShift(offset); });
  m.def("shift", [](long int offset,
                    std::shared_ptr<PyQObject> obj) { return PyQFilterShift(offset, obj); });
  
  // PyQFilterTag
  PYBIND11_FILTER( PyQFilterTag, m )
    
    .def(py::init<std::size_t>())
    .def(py::init<std::size_t, std::shared_ptr<PyQObject>>())
    
    .def_readwrite("tag", &PyQFilterTag::tag)
    ;

  PYBIND11_FILTER_ALIAS( "tag", PyQFilterTag, m );
  
  m.def("tag", [](std::size_t tag)                { return PyQFilterTag(tag); });
  m.def("tag", [](std::size_t tag,
                  std::shared_ptr<PyQObject> obj) { return PyQFilterTag(tag, obj); });
  
  // PyQFilterGotoTag
  PYBIND11_FILTER( PyQFilterGotoTag, m )
    
    .def(py::init<std::size_t>())
    .def(py::init<std::size_t, std::shared_ptr<PyQObject>>())
    
    .def_readwrite("tag", &PyQFilterGotoTag::tag)
    ;

  PYBIND11_FILTER_ALIAS( "gototag", PyQFilterGotoTag, m );
  
  m.def("gototag", [](std::size_t tag)                { return PyQFilterGotoTag(tag); });
  m.def("gototag", [](std::size_t tag,
                      std::shared_ptr<PyQObject> obj) { return PyQFilterGotoTag(tag, obj); });
  
  // PyQRange
  PYBIND11_FILTER( PyQRange, m )
    
    .def(py::init<std::size_t, std::size_t>())
    .def(py::init<std::size_t, std::size_t, std::shared_ptr<PyQObject>>())
    
    .def_readwrite("begin", &PyQRange::begin)
    .def_readwrite("end", &PyQRange::end)
    ;

  PYBIND11_FILTER_ALIAS( "range", PyQRange, m );

  m.def("qureg", [](std::size_t begin, std::size_t end) { return PyQRange(begin, end); });
  m.def("qureg", [](std::size_t begin, std::size_t end,
                    std::shared_ptr<PyQObject> obj)     { return PyQRange(begin, end, obj); });

  // PyQRegister
  PYBIND11_FILTER( PyQRegister, m )
    
    .def(py::init<std::size_t, std::size_t>())
    .def(py::init<std::size_t, std::size_t, std::shared_ptr<PyQObject>>())
    
    .def_readwrite("begin", &PyQRegister::begin)
    .def_readwrite("qubits", &PyQRegister::qubits)
    ;

  PYBIND11_FILTER_ALIAS( "qureg", PyQRegister, m );

  m.def("qureg", [](std::size_t begin, std::size_t qubits) { return PyQRegister(begin, qubits); });
  m.def("qureg", [](std::size_t begin, std::size_t qubits,
                    std::shared_ptr<PyQObject> obj)        { return PyQRegister(begin, qubits, obj); });
  
  // PyQBit
  PYBIND11_FILTER( PyQBit, m )
    
    .def(py::init<std::size_t>())
    .def(py::init<std::size_t, std::shared_ptr<PyQObject>>())
    
    .def_readwrite("id", &PyQBit::id)
    ;

  PYBIND11_FILTER_ALIAS( "qubit", PyQBit, m );
  
  m.def("qubit", [](std::size_t id)                 { return PyQBit(id); });
  m.def("qubit", [](std::size_t id,
                    std::shared_ptr<PyQObject> obj) { return PyQBit(id, obj); });
}
