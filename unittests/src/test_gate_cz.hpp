/** @file test_gate_cz.hpp
 *
 *  @brief LibKet::gates::cz() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cz)
{
  TEST_API_CONSISTENCY( cz );
  TEST_API_CONSISTENCY( CZ );
  TEST_API_CONSISTENCY( czdag );
  TEST_API_CONSISTENCY( CZdag );
  
  TEST_API_CONSISTENCY( QCZ() );

  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCZ", cz );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCZ", CZ );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCZ", czdag );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCZ", CZdag );

  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCZ", QCZ() );
}
