/** @file test_gate_reset.hpp
 *
 *  @brief LibKet::gates::reset() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_reset)
{
  TEST_API_CONSISTENCY( reset );
  TEST_API_CONSISTENCY( RESET );

  TEST_API_CONSISTENCY( QReset() );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QReset", reset );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QReset", RESET );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QReset", QReset() );
}
