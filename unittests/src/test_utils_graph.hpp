/** @file test_utils_graph.hpp
 *
 *  @brief LibKet::utils::graph unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet;
using namespace LibKet::utils;

TEST_FIXTURE(Fixture, utils_graph)
{
  // Empty graph
  try {
    auto g = graph<>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Graph with single edge
  try {
    auto g = graph<>::edge<1,2>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {1,2} }");
    CHECK_EQUAL(g.size(),    1);
    CHECK_EQUAL(g.from<0>(), 1);
    CHECK_EQUAL(g.to<0>(),   2);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Graph with multiple edges
  try {
    auto g = graph<>::edge<1,2>::edge<2,3>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {1,2} {2,3} }");
    CHECK_EQUAL(g.size(),    2);
    CHECK_EQUAL(g.from<0>(), 1);
    CHECK_EQUAL(g.to<0>(),   2);
    CHECK_EQUAL(g.from<1>(), 2);
    CHECK_EQUAL(g.to<1>(),   3);    
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Subgraph
  try {
    auto g = graph<>::edge<1,2>::edge<2,3>::edge<3,4>::edge<4,5>().subgraph<1,3>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {2,3} {4,5} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Full graph with zero vertices
  try {
    auto g = make_full_graph<0>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Full graph with one vertex
  try {
    auto g = make_full_graph<1>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Full graph with two vertices
  try {
    auto g = make_full_graph<2>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Full graph with three vertices
  try {
    auto g = make_full_graph<3>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {0,2} {1,2} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Full graph with four vertices
  try {
    auto g = make_full_graph<4>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {0,2} {0,3} {1,2} {1,3} {2,3} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Full graph with five vertices
  try {
    auto g = make_full_graph<5>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {0,2} {0,3} {0,4} {1,2} {1,3} {1,4} {2,3} {2,4} {3,4} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Full graph with six vertices
  try {
    auto g = make_full_graph<6>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {0,2} {0,3} {0,4} {0,5} {1,2} {1,3} {1,4} {1,5} {2,3} {2,4} {2,5} {3,4} {3,5} {4,5} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Regular graph with zero vertices
  try {
    auto g = make_regular_graph<0>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Regular graph with one vertex
  try {
    auto g = make_regular_graph<1>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Regular graph with two vertices
  try {
    auto g = make_regular_graph<2>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Regular graph with three vertices
  try {
    auto g = make_regular_graph<3>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {1,2} {2,0} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Regular graph with four vertices
  try {
    auto g = make_regular_graph<4>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {0,2} {1,2} {1,3} {2,3} {3,0} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
  
  // Regular graph with five vertices
  try {
    auto g = make_regular_graph<5>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {0,2} {1,2} {1,3} {2,3} {2,4} {3,4} {3,0} {4,0} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // Regular graph with six vertices
  try {
    auto g = make_regular_graph<6>();
    std::stringstream ss; ss << g;
    CHECK_EQUAL(ss.str(), "{ {0,1} {0,2} {1,2} {1,3} {2,3} {2,4} {3,4} {3,5} {4,5} {4,0} {5,0} }");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }  
}
