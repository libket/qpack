/** @file test_gate_phase.hpp
 *
 *  @brief LibKet::gates::phase() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_phase)
{
  TEST_API_CONSISTENCY( phase<>, QConst(3.141) );
  TEST_API_CONSISTENCY( PHASE<>, QConst(3.141) );
  TEST_API_CONSISTENCY( ph<>,    QConst(3.141) );
  TEST_API_CONSISTENCY( PH<>,    QConst(3.141) );

  TEST_API_CONSISTENCY( phasedag<>, QConst(3.141) );
  TEST_API_CONSISTENCY( PHASEdag<>, QConst(3.141) );
  TEST_API_CONSISTENCY( phdag<>,    QConst(3.141) );
  TEST_API_CONSISTENCY( PHdag<>,    QConst(3.141) );

  TEST_API_CONSISTENCY( QPhase<QConst_t(3.141)>() );

  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", phase<>, QConst(3.141) );
  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", PHASE<>, QConst(3.141) );
  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", ph<>,    QConst(3.141) );
  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", PH<>,    QConst(3.141) );

  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", phasedag<>, QConst(3.141) );
  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", PHASEdag<>, QConst(3.141) );
  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", phdag<>,    QConst(3.141) );
  TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", PHdag<>,    QConst(3.141) );
    
  //TEST_UNARY_GATE( "QPhase<" QCONST_T "(3.141),QConst1_t(0)>", QPhase<QConst_t(3.141)>() );
    
  try {
    // phase(QConst(3.141), phase(QConst(-3.141)));
    auto expr = phase(QConst(3.141), phase(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPhase<" QCONST_T "(3.141),QConst1_t(0)>\n| filter = QFilter\n|   "
                "expr = UnaryQGate\n|          |   gate = QPhase<" QCONST_T "(-3.141),QConst1_t(0)>\n|         "
                " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // phase(QConst(-3.141), phase(QConst(3.141)));
    auto expr = phase(QConst(-3.141), phase(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPhase<" QCONST_T "(-3.141),QConst1_t(0)>\n| filter = QFilter\n|   "
                "expr = UnaryQGate\n|          |   gate = QPhase<" QCONST_T "(3.141),QConst1_t(0)>\n|         "
                " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // phase(QConst(3.141), phase(QConst(-3.141), init()));
    auto expr = phase(QConst(3.141), phase(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPhase<" QCONST_T "(3.141),QConst1_t(0)>\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QPhase<" QCONST_T "(-3.141),QConst1_t(0)>\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // phase(QConst(-3.141), phase(QConst(3.141), init()));
    auto expr = phase(QConst(-3.141), phase(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPhase<" QCONST_T "(-3.141),QConst1_t(0)>\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QPhase<" QCONST_T "(3.141),QConst1_t(0)>\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
