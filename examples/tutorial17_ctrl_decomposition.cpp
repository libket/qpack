/** @file examples/tutorial17_ctrl_decomposition.cpp

    @brief C++ tutorial-17: Quantum expression with controlled unitary gate decomposition

    This tutorial illustrates the basic usage of the built-in
    decomposition of a controlled 2x2 unitary gate into native gates.

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

struct U2_ftor {
  inline auto operator()() const noexcept
  {
    return arma::Col<double>{ 1.0/sqrt(2.0), -1.0/sqrt(2.0),                               
                              1.0/sqrt(2.0),  1.0/sqrt(2.0)};
  }
};

int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 9;

  // Create quantum expression
  auto expr = measure(
                      cnot(sel<0,1,2>(),
                                   sel<3,4,5>(init())
                                   )
                      );
  
   // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
