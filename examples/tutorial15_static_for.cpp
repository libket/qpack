/** @file examples/tutorial15_static_for.cpp

    @brief C++ tutorial-15: Quantum expression with custom static-for loop

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

template<index_t start, index_t end, index_t step, index_t index>
struct body
{  
  template<typename  Expr>
  inline constexpr auto operator()(Expr&& expr) noexcept
  {
    return crk<index>(sel<index-1>(
                                   gototag<0>()
                                   ),
                      sel<index>(
                                 gototag<0>(expr)
                                 )
                      );
  }
};

int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 6;
  
  // Create simple quantum expression:
  // controlled k-rotation between any two qubits
  auto expr = measure(
                      all(
                          utils::static_for<1,5,2,body>(
                                                        tag<0>(init())
                                                        )
                          )
                      );
  
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
