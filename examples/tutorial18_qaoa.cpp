/** @file examples/tutorial18_qaoa.cpp

    @brief C++ tutorial-18: Quantum Approximate Optimization Algorithm

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller, Koen Mesman
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

template<index_t start, index_t end, index_t step, index_t index>
struct body
{  
  template<typename  Expr, typename Graph, typename Beta, typename Theta>
  inline constexpr auto operator()(Expr&& expr, Graph, Beta, Theta) noexcept
  {
    return rx(Beta{},
              cnot(sel<Graph::template from<index>()>(
                                                      gototag<0>()
                                                      ),
                   sel<Graph::template to<index>()>(
                                                    gototag<0>(expr)
                                                    )
                   )
              );
  }
};



int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 6;

  // Create compile-time graph
  constexpr const utils::graph<>
    ::edge<0,1>
    ::edge<2,3>
    ::edge<4,5> g;
  
  QInfo << g << std::endl;

  auto beta = QVar(0.5);
  auto theta = QVar(0.8);

  QInfo << beta << "," << theta << std::endl;
  
  // Create simple quantum expression:
  // controlled-not between any two qubits given as edges of the graph g
  auto expr = measure(
                      all(
                          utils::static_for<0,g.size()-1,1,body>(
                                                               tag<0>(init()),
                                                               g,
                                                               beta,
                                                               theta
                                                               )
                          )
                      );

  QInfo << vars(expr).front() << std::endl;

  exit(0);
  
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
