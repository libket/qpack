/** @file examples/tutorial05.cpp

    @brief C++ tutorial-05: Quantum expressions and quantum filters

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  // Set number of qubits
  const std::size_t nqubits = 6;

  auto expr = tag<0>(x(sel<0>(h(all()))));

  show<99>(expr);
  
  /*
  // Create simple quantum expression
  auto expr = measure(
                      qftdag<>(
                               qft<>(
                                     gototag<0>(
                                                h(
                                                  sel<1,2,3,4>(
                                                               gototag<0>(
                                                                          x(
                                                                            sel<0,1,2>(
                                                                                       tag<0>(
                                                                                              init()
                                                                                              )
                                                                                       )
                                                                            )
                                                                          )
                                                               )
                                                  )
                                                )
                                     )
                               )
                      ); 
  */
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
