/** @file examples/tutorial12_hook.cpp

    @brief C++ tutorial-12: Quantum expression with hook gate

    This tutorial illustrates the usage of the hook gate

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

namespace LibKet {
  // Create a generic functor from a LibKet expression
  QFunctor_alias(expr_ftor, measure(h(init())) );

  // Create a specialized functor from a string cQASM code
  QFunctor_alias(str_ftor,  std::string("h q[#]\ni q[#]\n") );  
}

int main(int argc, char *argv[])
{
  // Set number of qubits
  const std::size_t nqubits = 6;
  
  // Create quantum gate from expression functor
  auto expr = hook< expr_ftor >();
  show<99>(expr);

  // Create quantum gate from string functor (only cQASM)
  auto expr_cQASM = hook< str_ftor >( init() );
  show<99>(expr_cQASM);
    
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
