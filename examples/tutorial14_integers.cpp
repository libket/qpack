/** @file examples/tutorial14_integers.cpp

    @brief C++ tutorial-14: Quantum integer data type

    This tutorial illustrates the usage of the QInt type

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 6;
  
  QBitArray<nqubits,QEndianness::lsb> a(5);
  QBitArray<nqubits,QEndianness::msb> b(-5);

  QInfo << static_cast<int>(a) << std::endl;
  
  // Create simple quantum expression
  auto expr = init();
  
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
