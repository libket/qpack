/** @file examples/tutorial09.cpp

    @brief C++ tutorial-09: Quantum expressions and quantum filters

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  // Set number of qubits
  const std::size_t nqubits = 6;
  
  // Create quantum expression
  auto expr = measure(
                      all(
                          x(
                            gototag<1>(
                                       h(
                                         gototag<0>(
                                                    tag<1>(
                                                           sel<2,3>(
                                                                    all(
                                                                        tag<0>(
                                                                               sel<0,1>(
                                                                                        init()
                                                                                        )
                                                                               )
                                                                        )
                                                                    )
                                                           )
                                                    )
                                         )
                                       )
                            )
                          )
                      );
  
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
