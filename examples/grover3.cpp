/** @file examples/tutorial19_grover3.cpp

    @brief C++ tutorial-19: Gover's algorithm with 3 qubits

    This tutorial illustrates Grover's algorithm with 3 qubits

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{  
  // Set number of qubits
  const size_t nqubits = 3;

  // Create expression for the oracle operator for binary 010 (decimal 2)
  auto oracle = [](auto expr)
  { return x(
             sel_<0>(
                     x(h(
                         sel_<2>(
                                 ccnot(sel_<0>(),
                                       sel_<1>(),
                                       sel_<2>(
                                               h(x(
                                                   sel_<2>(
                                                           x(
                                                             sel_<0>(expr)
                                                             )
                                                           )
                                                   ) )
                                               )
                                       )
                                 )
                         ) )
                     )
             );
  };
  
  // Create expression for the diffusion operator
  auto diffusion = [](auto expr)
  { return h(
             x(
               all(
                   h(sel_<2>(
                             ccnot(sel_<0>(),
                                   sel_<1>(),
                                   sel_<2>(
                                           h(
                                             sel_<2>(
                                                     x(
                                                       h(
                                                         all(expr)
                                                         )
                                                       )
                                                     )
                                             )
                                           )
                                   )
                             )
                     )
                   )
               )
             );
  };
  
  // Create expression for Grover's algorithm
  auto expr = measure(diffusion(oracle(h(init()))));
  
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
