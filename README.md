![Image](docs/_static/QPack_logo_color.png?raw=true)

# QPack - A cross-platform quantum benchmark-suite

As the technology of quantum computers improves, the need to evaluate their performance also becomes an important tool for indexing and comparing of quantum performance. Current benchmarking proposals either focus on gate-level evaluation, are centered around a single performance metric, or only evaluate in-house devices. This gives rise to the need for a holistic, application-oriented, and hardware-agnostic benchmarking tool that can provide fair and varied insight into quantum computer performance. This thesis continues the development of the QPack benchmark, which collects quantum computer data by running noisy intermediate-scale quantum (NISQ)-era applications and transforms this data into an overall performance score, which is decomposed into four subscores. These scores are quantitative metrics of quantum performance that allow for easy and quick comparisons between different quantum computers. 

The QPack benchmark is an application-oriented cross-platform benchmarking suite for quantum computers and simulators, which makes use of scalable Quantum Approximate Optimization Algorithm and Variational Quantum Eigensolver applications. Using a varied set of benchmark applications, an insight into how well a quantum computer or its simulator performs on a general NISQ-era application can be quantitatively made. QPack is built on top of the cross-platform library [LibKet](https://gitlab.com/libket/LibKet), which allows for a single expression of a quantum circuit and execution on multiple devices.

The latest version of the QPack benchmark and results collected from a variety of quantum computers or simulators form IBM, Rigetti, IonQ, Cirq, and QuEST can be found in this repository.


## Citing QPack

For more information on the **QPack** benchmark, please refer to our [publication](https://arxiv.org/abs/2205.12142). If you use **QPack** in your research please cite it as follows

```bibtex
@misc{https://doi.org/10.48550/arxiv.2205.12142,
  doi       = {10.48550/ARXIV.2205.12142},
  url       = {https://arxiv.org/abs/2205.12142},
  author    = {Donkers, Huub and Mesman, Koen and Al-Ars, Zaid and MÃ¶ller, Matthias},
  keywords  = {Quantum Physics, Emerging Technologies (cs.ET), Quantum Computing, Benchmarking, Application-level, Cross-platform, NISQ, QAOA, VQE},  
  title     = {QPack Scores: Quantitative performance metrics for application-oriented quantum computer benchmarking},  
  publisher = {arXiv},  
  year      = {2022},  
  copyright = {Creative Commons Attribution 4.0 International}
}

```
