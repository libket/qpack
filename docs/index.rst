.. _LibKet Index:

.. image:: _static/LibKet_logo_color.png
    :alt: LibKet

Welcome to LibKet
=================

LibKet (pronounced lib-ket) is a lightweight `expression
template <https://en.wikipedia.org/wiki/Expression_templates>`_ library
that allows you to develop quantum algorithms as backend-agnostic
generic expressions and execute them on different quantum simulator
and hardware backends without changing your code.

Supported quantum hardware backends

- `IBM Quantum Experience <https://quantum-computing.ibm.com>`_
- `QuTech Quantum Inspire <https://quantuminspire.com>`_
- `Rigetti Aspen <https://qcs.rigetti.com>`_

Supported quantum simulator backends

- `Atos Quantum Learning Machine <https://atos.net/en/solutions/quantum-learning-machine>`_
- `Google Cirq <https://quantumai.google/cirq>`_
- `IBM Qiskit <https://qiskit.org>`_
- `QE-Lab OpenQL <https://openql.readthedocs.io>`_
- `QuEST - Quantum Exact Simulation Toolkit <https://quest.qtechtheory.org>`_
- `QuTech QX simulator <https://qutech.nl/qx-quantum-computer-simulator/>`_
- `Rigetti PyQuil <http://docs.rigetti.com/>`_ 

All you need to get started is a C++14 (or better) compiler and,
optionally, `Python 3.x <https://www.python.org>`_ to execute quantum
algorithms directly from within LibKet. The following code snippet
shows how to generate the quantum algorithm for an :math:`n`-qubit QFT
circuit (`expr` in line 11) and execute it on the `Quantum Inspire
<https://quantuminspire.com>`_ cloud platform using 6-qubits (lines
15-16):

.. code-block:: cpp
    :linenos:
    :emphasize-lines: 11,15,16

    // Include main header file
    #include <LibKet.hpp>

    // Import namespaces
    using namespace LibKet;
    using namespace LibKet::circuits;
    using namespace LibKet::filters;
    using namespace LibKet::gates;
    
    // Create generic quantum expression
    auto expr = qft(init());
    
    // Execute QFT<6> on Quantum-Inspire platform
    try {
      QDevice<QDeviceType::qi_26_simulator, 6> qi; qi(expr);
      utils::json result = qi.eval();
      
      QInfo << "job ID     : " << qi.get<QResultType::id>(result)               << std::endl;
      QInfo << "time stamp : " << qi.get<QResultType::timestamp>(result)        << std::endl;
      QInfo << "duration   : " << qi.get<QResultType::duration>(result).count() << std::endl;
      QInfo << "best       : " << qi.get<QResultType::best>(result)             << std::endl;
      QInfo << "histogram  : " << qi.get<QResultType::histogram>(result)        << std::endl;
      
    } catch(const std::exception &e) {
      QWarn << e.what() << std::endl;
    }

.. _LibKet User Documentation:

User Documentation
------------------

.. toctree::
   :maxdepth: 2

   getting_started
   components
   release_notes
   api_documentation
   faq

.. Hiding - Indices and tables
   :ref:`genindex`
   :ref:`modindex`
   :ref:`search`
