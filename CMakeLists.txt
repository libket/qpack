########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# CMakeLists.txt accepts the following command line parameters
#
# LIBKET_BUILD_COVERAGE
# LIBKET_BUILD_C_API
# LIBKET_BUILD_DOCS
# LIBKET_BUILD_PYTHON_API
# LIBKET_BUILD_EXAMPLES
# LIBKET_BUILD_UNITTESTS
#
# LIBKET_BUILTIN_OPENQL
# LIBKET_BUILTIN_QUEST
# LIBKET_BUILTIN_QX
# LIBKET_BUILTIN_UNITTESTS
#
# LIBKET_L2R_EVALUATION    (enable left-to-right evaluation)
# LIBKET_GEN_PROFILING     (enable generation of profiling data)
# LIBKET_OPTIMIZE_GATES    (enable optimization of gates, e.g. H(H(q0)) = I(q0))
# LIBKET_PROF_COMPILE      (enable profiling of compilation)
# LIBKET_USE_PCH           (enable use of precompiled headers)
#
# LIBKET_WITH_AQASM        (enable support for Atos QASM)
# LIBKET_WITH_CIRQ         (enable support for Cirq used by Google)
# LIBKET_WITH_CQASM        (enable support for Common QASM used by QuTech's QX simulator)
# LIBKET_WITH_MPI          (enable support for MPI)
# LIBKET_WITH_OPENMP       (enable support for OpenMP)
# LIBKET_WITH_OPENQASM     (enable support for OpenQASM used by Microsofts Qiskit framework)
# LIBKET_WITH_OPENQL       (enable support for OpenQL used by QuTech's OpenQL simulator)
# LIBKET_WITH_QASM         (enable support for Qasm2circ LaTeX export)
# LIBKET_WITH_QUEST        (enable support for Quantum exact simulation toolkit by University of Oxford, UK)
# LIBKET_WITH_QUIL         (enable support for Quantum instruction set architecture used by Rigetti)
# LIBKET_WITH_QX           (enable support for QuTech's QX simulator)
#
########################################################################

########################################################################
# Force CMake version 3.1 or above
########################################################################
cmake_minimum_required (VERSION 3.1)

########################################################################
# Set RPATH on MacOSX
########################################################################
if (APPLE)
  set(CMAKE_MACOSX_RPATH ON)
  SET(CMAKE_SKIP_BUILD_RPATH FALSE)
  SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
  SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
  SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
  LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
  IF ("${isSystemDir}" STREQUAL "-1")
    SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
  ENDIF ()
endif()

########################################################################
# Use solution folders for Visual Studio
########################################################################
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  # Set default build type to RelWithDebInfo
  set(CMAKE_BUILD_TYPE Release CACHE STRING
    "Type of build (None Debug Release RelWithDebInfo MinSizeRel)" FORCE)
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "RelWithDebInfo" "MinSizeRel")
endif()

########################################################################
# We do not support in-source build
########################################################################
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
  message(FATAL_ERROR "In-source builds are not permitted.\nPlease make a separate folder for building, otherwise type \nmake\nthat will create a ./build folder and will compile in that folder. Cmake has created garbage files/dirs (you may manually remove them):\nCMakeCache.txt CMakeFiles")
endif()

########################################################################
#
# Project: LibKet
#
########################################################################

########################################################################
# Project name
########################################################################
project(LibKet C CXX)

########################################################################
# LibKet version
########################################################################
set(LIBKET_MAJOR_VERSION 2021)
set(LIBKET_MINOR_VERSION 02)
set(LIBKET_REVISION_VERSION 795)
set(LIBKET_VERSION "${LIBKET_MAJOR_VERSION}.${LIBKET_MINOR_VERSION}-${LIBKET_REVISION_VERSION}")

message( "  _   _       _   _      __  "    )
message( " | | | |     (_) | |__   \\ \\ "  )
message( " | | | |     | | | '_ \\   \\ \\" )
message( " | | | |___  | | | |_) |  / /"    )
message( " |_| |_____| |_| |_.__/  /_/ "    )
message( "     Version ${LIBKET_VERSION}"   )

########################################################################
# Option list
########################################################################
option(LIBKET_BUILD_COVERAGE    "Build with coverage"       OFF)
option(LIBKET_BUILD_C_API       "Build C API"               ON )
option(LIBKET_BUILD_DOCS        "Build documentation"       ON )
option(LIBKET_BUILD_PYTHON_API  "Build Python API"          ON )
option(LIBKET_BUILD_EXAMPLES    "Build examples"            ON )
option(LIBKET_BUILD_UNITTESTS   "Build unittests"           ON )

option(LIBKET_BUILTIN_OPENQL    "Use built-in OpenQL"       ON )
option(LIBKET_BUILTIN_QUEST     "Use built-in QuEST"        ON )
option(LIBKET_BUILTIN_QX        "Use built-in QX"           ON )
option(LIBKET_BUILTIN_UNITTESTS "Use built-in UnitTests++"  ON )

option(LIBKET_L2R_EVALUATION    "Left-to-right evaluation"  OFF)
option(LIBKET_GEN_PROFILING     "Generate profiling data"   OFF)
option(LIBKET_OPTIMIZE_GATES    "Optimize gates"            ON )
option(LIBKET_PROF_COMPILE      "Profile compilation"       OFF)

if(NOT ${CMAKE_VERSION} VERSION_LESS "3.16.0")
  option(LIBKET_USE_PCH         "Use precompiled headers"   ON )
else()
  option(LIBKET_USE_PCH         "Use precompiled headers"   OFF)
endif()

option(LIBKET_WITH_AQASM        "With Atos QASM"            OFF)
option(LIBKET_WITH_CIRQ         "With common CIRQ"          OFF)
option(LIBKET_WITH_CQASM        "With common QASM"          OFF)
option(LIBKET_WITH_MPI          "With MPI"                  OFF)
option(LIBKET_WITH_OPENMP       "With OpenMP"               OFF)
option(LIBKET_WITH_OPENQASM     "With OpenQASM"             OFF)
option(LIBKET_WITH_OPENQL       "With OpenQL"               OFF)
option(LIBKET_WITH_QASM         "With QASM2Circ"            OFF)
option(LIBKET_WITH_QUEST        "With QuEST"                OFF)
option(LIBKET_WITH_QUIL         "With Quil"                 OFF)
option(LIBKET_WITH_QX           "With QX"                   OFF)

########################################################################
# LibKet data types
########################################################################
if(NOT LIBKET_COEFF_TYPE)
  set(LIBKET_COEFF_TYPE "double" CACHE STRING
   "Real type (float, double, long double)" FORCE)
endif()
set_property(CACHE LIBKET_COEFF_TYPE PROPERTY STRINGS
  "float" "double" "long double")

if(NOT LIBKET_INDEX_TYPE)
  set(LIBKET_INDEX_TYPE "int" CACHE STRING
   "Index type (short, int, long, long long)" FORCE)
endif()
set_property(CACHE LIBKET_INDEX_TYPE PROPERTY STRINGS
  "short" "int" "long" "long long")

if(LIBKET_COEFF_TYPE STREQUAL "float")
  set(LIBKET_CONST_PREC "8")
elseif(LIBKET_COEFF_TYPE STREQUAL "double")
  set(LIBKET_CONST_PREC "16")
elseif(LIBKET_COEFF_TYPE STREQUAL "long double")
  set(LIBKET_CONST_PREC "32")
else()
  set(LIBKET_CONST_PREC "64")
endif()

########################################################################
# C++ standard
########################################################################
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

########################################################################
# Append path to additional modules
########################################################################
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")

########################################################################
# Python embedded interpreter and libraries
########################################################################
if (CMAKE_VERSION VERSION_LESS 3.12.0)
  find_package(PythonInterp 3 QUIET)
  find_package(PythonLibs 3 QUIET)
  if(PYTHONLIBS_FOUND AND NOT PYTHON_VERSION_MAJOR VERSION_LESS "3")
    include_directories(${PYTHON_INCLUDE_DIRS})
    list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES ${PYTHON_LIBRARIES})
    list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES ${PYTHON_LIBRARIES})
    set(LIBKET_WITH_PYTHON ON)
  endif()
else ()
  find_package(Python3 COMPONENTS Interpreter Development QUIET)
  if(Python3_Development_FOUND)
    include_directories(${Python3_INCLUDE_DIRS})
    list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES ${Python3_LIBRARIES})
    list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES ${Python3_LIBRARIES})
    set(LIBKET_WITH_PYTHON ON)
  endif()
endif()

########################################################################
# Threads
########################################################################
find_package(Threads QUIET REQUIRED)
if(Threads_FOUND)
  list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES Threads::Threads)
  list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES Threads::Threads)
endif()

########################################################################
# Doxygen
########################################################################
find_package(Doxygen QUIET)
if(DOXYGEN_FOUND AND LIBKET_BUILD_DOCS)
  set(DOXYGEN_OUTPUT_DIR ${PROJECT_BINARY_DIR}/docs/doxygen)
  set(DOXYGEN_TAGFILE ${PROJECT_BINARY_DIR}/docs/doxygen/libket-doxygen.tag.xml)  
  configure_file(${PROJECT_SOURCE_DIR}/docs/Doxyfile.in ${PROJECT_BINARY_DIR}/docs/doxygen/Doxyfile @ONLY)
  add_custom_target(Doxygen
    COMMAND
    ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/docs/doxygen/Doxyfile
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    COMMENT "Generating documentation with Doxygen"
    )

else()
  add_custom_target(Doxygen
    ${CMAKE_COMMAND} -E cmake_echo_color --cyan "Please install doxygen, and run cmake . \\&\\& make Doxygen to generate the Doxygen documentation."
    )
endif()

########################################################################
# Sphinx
########################################################################
find_package(Sphinx QUIET)
if(SPHINX_FOUND AND LIBKET_BUILD_DOCS)
  set(SPHINX_SOURCE ${PROJECT_SOURCE_DIR}/docs)
  set(SPHINX_BUILD ${PROJECT_BINARY_DIR}/docs/sphinx)
  set(SPHINX_INDEX_FILE ${SPHINX_BUILD}/index.html)
  add_custom_target(Sphinx
    COMMAND
    ${SPHINX_EXECUTABLE} -b html
    -Dbreathe_projects.LibKet=${PROJECT_BINARY_DIR}/docs/doxygen/xml
    ${SPHINX_SOURCE} ${SPHINX_BUILD}
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    COMMENT "Generating documentation with Sphinx"
    )
  add_dependencies(Sphinx Doxygen)
else()
  add_custom_target(Sphinx
    ${CMAKE_COMMAND} -E cmake_echo_color --cyan "Please install sphinx, and run cmake . \\&\\& make Sphinx to generate the Sphinx documentation."
    )
endif()

########################################################################
# Documentation
########################################################################
add_custom_target(docs
  DEPENDS Doxygen
  DEPENDS Sphinx)

########################################################################
# Dependencies
########################################################################
include(deps)

########################################################################
# Armadillo
########################################################################
add_subdirectory("${armadillo_SOURCE_DIR}")
include_directories("${armadillo_SOURCE_DIR}/include")
link_directories("${armadillo_BINARY_DIR}")
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   armadillo)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES armadillo)

########################################################################
# OptimLib
########################################################################
if (WIN32)
  include_directories("${PROJECT_SOURCE_DIR}/external/optim_windows/header_only_version")
else()
  if(NOT (EXISTS "${optim_SOURCE_DIR}/header_only_version"))
    execute_process(
      COMMAND ./configure --header-only-version
      WORKING_DIRECTORY "${optim_SOURCE_DIR}"
      )
  endif()
  include_directories("${optim_SOURCE_DIR}/header_only_version")
endif()

########################################################################
# Parsing Expression Grammar Template Library
########################################################################
include_directories("${pegtl_SOURCE_DIR}/include")

########################################################################
# Universal Number Arithmetic
########################################################################
include_directories("${universal_SOURCE_DIR}")

########################################################################
# Enable MPI support
########################################################################
if(LIBKET_WITH_MPI)
  find_package(MPI REQUIRED)
  include_directories("${MPI_INCLUDE_PATH}")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MPI_CXX_LIBRARIES}")
endif(LIBKET_WITH_MPI)

########################################################################
# Enable OpenMP support
########################################################################
if (LIBKET_WITH_OPENMP)
  # Apple explicitly disabled OpenMP support in their compilers that
  # are shipped with XCode but there is an easy workaround as
  # described at https://mac.r-project.org/openmp/
  if (CMAKE_C_COMPILER_ID STREQUAL "AppleClang" OR CMAKE_C_COMPILER_ID STREQUAL "Clang" AND ${CMAKE_SYSTEM_NAME} MATCHES "Darwin" OR
      CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND ${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    find_path(OpenMP_C_INCLUDE_DIR
      NAMES "omp.h" PATHS /usr/local /opt /opt/local /opt/homebrew PATH_SUFFICES include)
    find_path(OpenMP_CXX_INCLUDE_DIR
      NAMES "omp.h" PATHS /usr/local /opt /opt/local /opt/homebrew PATH_SUFFICES include)
    find_library(OpenMP_libomp_LIBRARY
      NAMES "omp" PATHS /usr/local /opt /opt/local /opt/homebrew PATH_SUFFICES lib)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Xclang -fopenmp -I${OpenMP_C_INCLUDE_DIR}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Xclang -fopenmp -I${OpenMP_CXX_INCLUDE_DIR}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_libomp_LIBRARY}")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${OpenMP_libomp_LIBRARY}")
  else() 
    find_package(OpenMP QUIET REQUIRED)
    list(APPEND CMAKE_C_FLAGS ${OpenMP_C_FLAGS})
    list(APPEND CMAKE_CXX_FLAGS ${OpenMP_CXX_FLAGS})
  endif()
endif()

########################################################################
# Enable Atos QASM support
########################################################################
if (LIBKET_WITH_AQASM)
  include(AQASM)
endif()

########################################################################
# Enable Cirq support
########################################################################
if (LIBKET_WITH_CIRQ)
  include(Cirq)
endif()

########################################################################
# Enable common QASM support
########################################################################
if (LIBKET_WITH_CQASM)
  include(cQASM)
endif()

########################################################################
# Enable OpenQASM support
########################################################################
if (LIBKET_WITH_OPENQASM)
  include(OpenQASM)
endif()

########################################################################
# Enable OpenQL support
########################################################################
if (LIBKET_WITH_OPENQL)
  include(OpenQL)
endif()

########################################################################
# Enable QASM2circ
########################################################################
if (LIBKET_WITH_QASM)
  include(QASM)
endif()

########################################################################
# Enable QuEST support
########################################################################
if (LIBKET_WITH_QUEST)
  include(QuEST)
endif()

########################################################################
# Enable Quil support
########################################################################
if (LIBKET_WITH_QUIL)
  include(Quil)
endif()

########################################################################
# Enable QX support
########################################################################
if (LIBKET_WITH_QX)
  include(QX)
endif()

########################################################################
# Enable Testing support
########################################################################
include(CTest)
enable_testing()

########################################################################
# Increase maximum template instantiation depth
########################################################################
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang")
  list(APPEND LIBKET_CXX_FLAGS "-ftemplate-depth=100000")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  list(APPEND LIBKET_CXX_FLAGS "-ftemplate-depth=100000")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  list(APPEND LIBKET_CXX_FLAGS "-ftemplate-depth=100000")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  # using Intel C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  # using Visual Studio C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PGI")
  # using Portland Group C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PathScale")
  # using PathScale C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "SunPro")
  # using Oracle Solaris Studio C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "XL")
  # using IBM XL C++
endif()

########################################################################
# Enable profiled compilation
########################################################################
if (LIBKET_PROF_COMPILE)
  if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang")
    list(APPEND LIBKET_CXX_FLAGS "-ftime-report")
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    list(APPEND LIBKET_CXX_FLAGS "-ftime-trace -ftime-report")
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    list(APPEND LIBKET_CXX_FLAGS "-ftime-report")
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    # using Intel C++
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
    # using Visual Studio C++
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PGI")
    # using Portland Group C++
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PathScale")
    # using PathScale C++
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "SunPro")
    # using Oracle Solaris Studio C++
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "XL")
    # using IBM XL C++
  endif()
endif()

########################################################################
# Include LIBKET_C_FLAGS and LIBKET_CXX_FLAGS
########################################################################
list(APPEND CMAKE_C_FLAGS   ${LIBKET_C_FLAGS})
list(APPEND CMAKE_CXX_FLAGS ${LIBKET_CXX_FLAGS})
string(REPLACE ";" " " CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}")
string(REPLACE ";" " " CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

########################################################################
# Summary
########################################################################
message("")
message("LIBKET version: ${LIBKET_VERSION}")
message("")
message("Configuration:")
message("Build type.........................: ${CMAKE_BUILD_TYPE}")
message("Build shared libraries.............: ${BUILD_SHARED_LIBS}")
message("Build directory....................: ${PROJECT_BINARY_DIR}")
message("Source directory...................: ${PROJECT_SOURCE_DIR}")
message("Install directory..................: ${CMAKE_INSTALL_PREFIX}")

message("")
message("AR command.........................: ${CMAKE_AR}")
message("RANLIB command.....................: ${CMAKE_RANLIB}")

if(CMAKE_C_COMPILER)
  message("")
  message("C compiler.........................: ${CMAKE_C_COMPILER}")
  message("C compiler flags ..................: ${CMAKE_C_FLAGS}")
  message("C compiler flags (debug)...........: ${CMAKE_C_FLAGS_DEBUG}")
  message("C compiler flags (release).........: ${CMAKE_C_FLAGS_RELEASE}")
  message("C compiler flags (release+debug)...: ${CMAKE_C_FLAGS_RELWITHDEBINFO}")
endif()

if(CMAKE_CXX_COMPILER)
  message("")
  message("CXX compiler.......................: ${CMAKE_CXX_COMPILER}")
  message("CXX standard.......................: ${CMAKE_CXX_STANDARD}")
  message("CXX compiler flags ................: ${CMAKE_CXX_FLAGS}")
  message("CXX compiler flags (debug).........: ${CMAKE_CXX_FLAGS_DEBUG}")
  message("CXX compiler flags (release).......: ${CMAKE_CXX_FLAGS_RELEASE}")
  message("CXX compiler flags (release+debug).: ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
endif()

message("")
message("EXE linker flags...................: ${CMAKE_EXE_LINKER_FLAGS}")
message("EXE linker flags (debug)...........: ${CMAKE_EXE_LINKER_FLAGS_DEBUG}")
message("EXE linker flags (release).........: ${CMAKE_EXE_LINKER_FLAGS_RELEASE}")
message("EXE linker flags (release+debug)...: ${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO}")

if(DOXYGEN_FOUND)
  message("")
  message("Doxygen............................: ${DOXYGEN_EXECUTABLE}")

  if(DOXYGEN_DOT_FOUND)
    message("Dot................................: ${DOXYGEN_DOT_EXECUTABLE}")
  endif()
endif()

if(SPHINX_FOUND)
  message("")
  message("Sphinx.............................: ${SPHINX_EXECUTABLE}")
endif()

if(PYTHONLIBS_FOUND)
  message("")
  message("PYTHON_VERSION_STRING..............: ${PYTHON_VERSION_STRING}")
  message("PYTHON_INCLUDE_DIRS................: ${PYTHON_INCLUDE_DIRS}")
  message("PYTHON_LIBRARIES...................: ${PYTHON_LIBRARIES}")
endif()

if(Python3_Development_FOUND)
  message("")
  message("Python3_VERSION....................: ${Python3_VERSION}")
  message("Python3_INCLUDE_DIRS...............: ${Python3_INCLUDE_DIRS}")
  message("Python3_LIBRARIES..................: ${Python3_LIBRARIES}")
endif()

message("")
message("Options:")
message("LIBKET_BUILD_COVERAGE..............: ${LIBKET_BUILD_COVERAGE}")
message("LIBKET_BUILD_C_API.................: ${LIBKET_BUILD_C_API}")
message("LIBKET_BUILD_DOCS..................: ${LIBKET_BUILD_DOCS}")
message("LIBKET_BUILD_PYTHON_API............: ${LIBKET_BUILD_PYTHON_API}")
message("LIBKET_BUILD_EXAMPLES..............: ${LIBKET_BUILD_EXAMPLES}")
message("LIBKET_BUILD_UNITTESTS.............: ${LIBKET_BUILD_UNITTESTS}")
message("")
message("LIBKET_BUILTIN_OPENQL..............: ${LIBKET_BUILTIN_OPENQL}")
message("LIBKET_BUILTIN_QUEST...............: ${LIBKET_BUILTIN_QUEST}")
message("LIBKET_BUILTIN_QX..................: ${LIBKET_BUILTIN_QX}")
message("LIBKET_BUILTIN_UNITTESTS...........: ${LIBKET_BUILTIN_UNITTESTS}")
message("")
message("LIBKET_L2R_EVALUATION..............: ${LIBKET_L2R_EVALUATION}")
message("LIBKET_GEN_PROFILING...............: ${LIBKET_GEN_PROFILING}")
message("LIBKET_OPTIMIZE_GATES..............: ${LIBKET_OPTIMIZE_GATES}")
message("LIBKET_PROF_COMPILE................: ${LIBKET_PROF_COMPILE}")
message("LIBKET_USE_PCH.....................: ${LIBKET_USE_PCH}")
message("")
message("Features:")
message("LIBKET_WITH_AQASM..................: ${LIBKET_WITH_AQASM}")
message("LIBKET_WITH_CIRQ...................: ${LIBKET_WITH_CIRQ}")
message("LIBKET_WITH_CQASM..................: ${LIBKET_WITH_CQASM}")
message("LIBKET_WITH_OPENMP.................: ${LIBKET_WITH_OPENMP}")
message("LIBKET_WITH_OPENQASM...............: ${LIBKET_WITH_OPENQASM}")
message("LIBKET_WITH_OPENQL.................: ${LIBKET_WITH_OPENQL}")
message("LIBKET_WITH_QASM...................: ${LIBKET_WITH_QASM}")
message("LIBKET_WITH_QUEST..................: ${LIBKET_WITH_QUEST}")
message("LIBKET_WITH_QUIL...................: ${LIBKET_WITH_QUIL}")
message("LIBKET_WITH_QX.....................: ${LIBKET_WITH_QX}")
message("")

########################################################################
# Load macros
########################################################################
include("${PROJECT_SOURCE_DIR}/cmake/add_executables.cmake")
include("${PROJECT_SOURCE_DIR}/cmake/add_tests.cmake")

########################################################################
# Generate configuration
########################################################################
configure_file("${PROJECT_SOURCE_DIR}/libket/QConfig.h.in"
               "${PROJECT_BINARY_DIR}/libket/QConfig.h")
include_directories(${PROJECT_BINARY_DIR}/libket)

########################################################################
# Add kernel directory
########################################################################
include_directories(libket)
include_directories(external)

add_subdirectory(libket)

file (GLOB_RECURSE LIBKET_HEADERS
  ${PROJECT_SOURCE_DIR}/libket/*.h)
source_group("Headers" FILES ${LIBKET_HEADERS})

########################################################################
# Build with coverage
########################################################################
if(LIBKET_BUILD_COVERAGE AND CMAKE_COMPILER_IS_GNUCXX)
  include(CodeCoverage)
  APPEND_COVERAGE_COMPILER_FLAGS()
endif()

########################################################################
# Add C API
########################################################################
if (LIBKET_BUILD_C_API)
  include_directories(c_api)
  add_subdirectory(c_api)
endif()

########################################################################
# Add Python API
########################################################################
if (LIBKET_BUILD_PYTHON_API)
  include_directories(python_api)
  add_subdirectory(python_api)
endif()

########################################################################
# Add Examples
########################################################################
if (LIBKET_BUILD_EXAMPLES)
  add_subdirectory(examples)

  if (LIBKET_BUILD_C_API)
    add_subdirectory(c_examples)
  endif()
endif()

########################################################################
# Add Qpack
########################################################################
add_subdirectory(Qpack)

########################################################################
# Add UnitTests
########################################################################
if (LIBKET_BUILD_UNITTESTS)
  add_subdirectory(unittests)
endif()

########################################################################
# Configure just-in-time compiler
########################################################################
include("${PROJECT_SOURCE_DIR}/cmake/genJITCompiler.cmake")
genJITCompiler()

########################################################################
# Add custom targets for building Python virtual environments
########################################################################
include("${PROJECT_SOURCE_DIR}/cmake/genPythonVEnv.cmake")
genPythonVEnv()
