/** @file c_api/QCircuits.cpp

@brief C API quantum circuit classes implementation

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller

@ingroup c_api
*/

#include <QCircuits.h>
