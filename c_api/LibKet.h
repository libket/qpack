/** @file c_api/LibKet.h

    @brief C API main header file

    @copyright This file is part of the LibKet library (C API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup c_api LibKet C API
 */
#pragma once
#ifndef C_API_LIBKET_H
#define C_API_LIBKET_H

#include <QBase.h>
#include <QJob.h>
#include <QStream.h>

#include <QFilters.h>
#include <QGates.h>
#include <QCircuits.h>

#endif // C_API_LIBKET_H
