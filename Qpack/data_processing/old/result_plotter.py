import json
import matplotlib.pyplot as plt
from math import log10, acos, atan, pi
from matplotlib.ticker import MaxNLocator
from statistics import mean
from qpu_names import qpu_names
from os import path
import numpy as np
from scipy.optimize import minimize
from matplotlib import gridspec

def geo_mean(iterable):
    a = np.array(iterable)
    b = a[a != 0]
    return b.prod()**(1.0/len(a))

from os import listdir
from os.path import isfile, join

#Find available qpu's and problems
#data_dir = "/home/huub/LibKet/qpack_output/mock_data"
data_dir = "/home/huub/LibKet/qpack_output/old"
qpus = []
problems = ['MCP', 'DSP', 'MIS', 'TSP', 'RH', 'IC'] #['MCP','RH', 'IC'] # ['MCP', 'DSP', 'MIS', 'TSP', 'RH', 'IC']
qpu_problems = {}
onlyfiles = [f for f in listdir(data_dir) if isfile(join(data_dir, f))]

for file in onlyfiles:
    split = file.split('_')
    qpu = split[1]
    problem = split[2]
    if(not(qpu in qpus)):
        qpus.append(qpu)
        qpu_problems[qpu] = []
    if(not(problem in qpu_problems[qpu]) and problem in problems):
        qpu_problems[qpu].append(problem)

print(f"QPUs: {qpus}")
print(f"Problems: {qpu_problems}")

qpus = ['3010008', '7000000', '6010003']
#qpus = ['3010002', '3010008', '7000000', '9000001', '6010003']
#qpus = ['10000001', '4010001']
#qpus = ['402001B']
#qpus = ['AAA', 'BBA', 'CCA', 'DDA']

qpack_speed = {}
qpack_accuracy = {}
qpack_scalability = {}
qpack_capacity = {}
problem_names = {}

#qpus = ['7000000']
#Get QueST data
file = open("QuEST_Scores.json")
QuEST_data = json.load(file)

qpus.sort()

for problem in problems:
    
    #Create figures
    fig, ((ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9)) = plt.subplots(3, 3)
    fig.set_size_inches(8,8)
    legend = []
    qpu_speed = {}
    qpu_accuracy = {}
    qpu_scalability = {}
    qpu_capacity = {}

    minSize = 100
    maxSize = 0
    
    for qpu in qpus:
        
        speed_score = 0
        scale_score = 0
        accuracy_score = 0
        capacity_score = 0
        numDatapoints = 0
        graph_sizes = [] 
        qjob_average = []
        cjob_average = []
        numQubits = []
        depth = []
        numParams = []
        score_average = []
        score_optimal = []
        opt_iterations = []
        ttime = []
        qtime = []
        ctime = []
        part_speed = []
        part_acc = []

        sizes = []
        for file in onlyfiles:
            split = file.split('_')
            _qpu = split[1]
            _problem = split[2]
            size = split[3][1:]
            if(_qpu == qpu and _problem == problem and not(size in sizes)):
                sizes.append(int(size))

        sizes.sort()        

        #print(f'{problem} {qpu}: {sizes}')        
        
        first_flag = True
        second_flag = True
        cap_flag = True

        for size in range(4,13): #sizes:
            qjob_average_mean = []
            cjob_average_mean = []
            numQubits_mean = []
            depth_mean = []
            numParams_mean = []
            score_mean = []
            score_optimal_mean = []
            opt_iterations_mean = []
            ttime_mean = []
            qtime_mean = []
            ctime_mean = []
            graph_sizes.append(int(size))

            #Load json data    
            data_path = str(f"{data_dir}/Result_{qpu}_{problem}_N{size}_P3_S10000.json")
            #print(data_path)
            if(not(path.exists(data_path))):
                #print("File not found!")
                data_path = str(f"{data_dir}/Result_{qpu}_{problem}_N{size}_P3_S4096.json") #break;
            
            file = open(data_path)
            measurements = json.load(file)

            #print(measurements)
            
            for rep_data in measurements.values():  
                qjob_average_mean.append(mean(rep_data['Job durations [ms]']))
                cjob_average_mean.append(mean(rep_data['Optimizer durations [ms]']))
                numQubits_mean.append(rep_data['Qubits'])
                depth_mean.append(rep_data['Depth'])
                numParams_mean.append(len(rep_data['Optimal params']))
                score_mean.append(rep_data['Score'])
                score_optimal_mean.append(rep_data['Optimal Score'])
                opt_iterations_mean.append(rep_data['Optimizer iterations'])
                ttime_mean.append(rep_data['Total Algorithm duration [s]'])
                qtime_mean.append(rep_data['Total Quantum duration [s]'])
                ctime_mean.append(rep_data['Total Classic duration [s]'])
            
            #Raw data collection
            qjob_average.append(mean(qjob_average_mean))
            cjob_average.append(mean(cjob_average_mean))
            numQubits.append(mean(numQubits_mean))
            depth.append(mean(depth_mean))
            numParams.append(mean(numParams_mean))
            score_average.append(min(score_mean))
            score_optimal.append(mean(score_optimal_mean))
            opt_iterations.append(mean(opt_iterations_mean))
            ttime.append(mean(ttime_mean))
            qtime.append(mean(qtime_mean))
            ctime.append(mean(ctime_mean))  
            numDatapoints += 1
            
            #Speed score
            shots = 4096
            p_speed_score = shots*mean(depth_mean) / (mean(qjob_average_mean)/1000)
            speed_score += p_speed_score
            part_speed.append(p_speed_score)
            
            #Accuracy score
            QuEST_score = QuEST_data[problem]['score'][QuEST_data[problem]['sizes'].index(int(size))]
            abs_error = QuEST_score-min(score_mean)+10e-10
            rel_error = abs_error/QuEST_score
            accuracy_score += rel_error
            part_acc.append(rel_error)

            '''    
            #Scalability score
            if(first_flag or second_flag):
                r_0 = 1
                r_1 = 1
                if(not(first_flag)):
                    second_flag = False
                first_flag = False

            else:
                n_0

                s_0 = (qjob_average[-1] - qjob_average[-2])/ (numQubits[-1] - numQubits[-2])
                s_1 = (qjob_average[-2] - qjob_average[-3])/ (numQubits[-2] - numQubits[-3])

                r_0 = 0.5 - atan(s_0)/pi
                r_1 = 0.5 - atan(s_1)/pi
            
            if(r_0 == 0):
                r_scale = 1
            else:    
                r_scale = r_0/r_1
            
            scale_score += r_scale
            '''

            #Capacity score
            threshold = 0.2
            if(rel_error < threshold): # and cap_flag):
                capacity_score = numQubits[-1]       
            else:
                cap_flag = False    
        
            if size < minSize:                
                minSize = size

            if size > maxSize:   
                maxSize = size
        
        if(numDatapoints > 0):
            
            #print(scale_score)
            #print(numDatapoints)

            #print(f'{str(qpu_names[int(qpu, base=16)])}, {problem}: {part_speed}')
            speed_score = log10(speed_score / numDatapoints)

            #Accuracy score mapping
            a_acc = 20/pi
            b_acc = 3
            accuracy_score = a_acc*(pi/2 - atan(b_acc*(accuracy_score / numDatapoints)))
            #print(graph_sizes)
            #print(qjob_average)
            #print(f'{str(qpu_names[int(qpu, base=16)])}, {problem}: {numDatapoints}')
            
            #Compute scalability score:
            if(numDatapoints > 1):

                #Normalise qjobtimes
                qjob_array = np.array(qjob_average)
                if(np.amax(qjob_array) != np.amin(qjob_array)):
                    qjob_array[-1] += 0.001
                norm_in = (qjob_array - np.amin(qjob_array))/(np.amax(qjob_array)-np.amin(qjob_array))

                def min_func(a):
                    x = np.array(graph_sizes)
                    y = x**a
                    norm_y = (y - np.amin(y))/(np.amax(y)-np.amin(y))   
                    sq_diff = (norm_in-norm_y)**2
                    err = sum(sq_diff)
                    return err


                fit = minimize(min_func, 1, method='Nelder-Mead')
                
                a_scale = 20/pi
                b_scale = 0.2
                scale_score = a_scale*(pi/2 - atan(b_scale*fit.x[0]))

            else:
                scale_score = 0        

            #scale_score = 10*scale_score**2


            
            #print(qjob_array)
            #print(qjob_norm)
            #print(numDatapoints)
            #print(f'{str(qpu_names[int(qpu, base=16)])}, {problem}: {scale_score}')

            qpu_speed[qpu] = speed_score
            qpu_accuracy[qpu] = accuracy_score
            qpu_scalability[qpu] = scale_score 
            qpu_capacity[qpu] = capacity_score
            
            legend.append(str(qpu_names[int(qpu, base=16)]))
            '''
            zipped = sorted(zip(graph_sizes, qjob_average, cjob_average, numQubits, depth, numParams, score_average, opt_iterations, ttime, qtime, ctime))
            
            ax1.plot(list(item[0] for item in zipped), list(item[1] for item in zipped), marker = 'o', markersize=3)
            ax2.plot(list(item[0] for item in zipped), list(item[2] for item in zipped), marker = 'o', markersize=3)
            ax3.plot(list(item[3] for item in zipped), list(item[4] for item in zipped), marker = 'o', markersize=3)
            ax4.plot(list(item[0] for item in zipped), list(item[5] for item in zipped), marker = 'o', markersize=3)
            ax5.plot(list(item[0] for item in zipped), list(item[6] for item in zipped), marker = 'o', markersize=3)
            ax6.plot(list(item[0] for item in zipped), list(item[7] for item in zipped), marker = 'o', markersize=3)
            ax7.plot(list(item[0] for item in zipped), list(item[8] for item in zipped), marker = 'o', markersize=3)
            ax8.plot(list(item[0] for item in zipped), list(item[9] for item in zipped), marker = 'o', markersize=3)
            ax9.plot(list(item[0] for item in zipped), list(item[10] for item in zipped), marker = 'o', markersize=3)
            '''
            zipped = sorted(zip(graph_sizes, qjob_average, cjob_average, numQubits, part_speed, part_acc, score_average, opt_iterations, ttime, qtime, ctime))
            
            ax1.plot(list(item[0] for item in zipped), list(item[1] for item in zipped), marker = 'o', markersize=3)
            ax2.plot(list(item[0] for item in zipped), list(item[2] for item in zipped), marker = 'o', markersize=3)
            ax3.plot(list(item[3] for item in zipped), list(item[4] for item in zipped), marker = 'o', markersize=3)
            ax4.plot(list(item[0] for item in zipped), list(item[5] for item in zipped), marker = 'o', markersize=3)
            ax5.plot(list(item[0] for item in zipped), list(item[6] for item in zipped), marker = 'o', markersize=3)
            ax6.plot(list(item[0] for item in zipped), list(item[7] for item in zipped), marker = 'o', markersize=3)
            ax7.plot(list(item[0] for item in zipped), list(item[8] for item in zipped), marker = 'o', markersize=3)
            ax8.plot(list(item[0] for item in zipped), list(item[9] for item in zipped), marker = 'o', markersize=3)
            ax9.plot(list(item[0] for item in zipped), list(item[10] for item in zipped), marker = 'o', markersize=3)
    

    try: zipped
    except NameError: zipped = None
    if(zipped is not None):
        start = QuEST_data[problem]['sizes'].index(minSize)
        end = QuEST_data[problem]['sizes'].index(maxSize)
        ax5.plot(QuEST_data[problem]['sizes'][start:end+1], QuEST_data[problem]['score'][start:end+1], color='red', marker = '', label="baseline")
    
    qpack_speed[problem] = qpu_speed
    qpack_accuracy[problem] = qpu_accuracy
    qpack_scalability[problem] = qpu_scalability 
    qpack_capacity[problem] = qpu_capacity

    # Force x-axis integers
    ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax3.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax4.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax5.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax6.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax7.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax8.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax9.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    #y-axis scale
    ax1.set_yscale("log")
    ax2.set_yscale("log")
    ax7.set_yscale("log")
    ax8.set_yscale("log")
    ax9.set_yscale("log")
     
    # Adding title
    fig.suptitle(str(problem))
    ax1.set_title('Average qjob runtime')
    ax1.set_xlabel("Problem size")
    ax1.set_ylabel("Runtime [ms]")

    ax2.set_title('Average cjob runtime')
    ax2.set_xlabel("Problem size")
    ax2.set_ylabel("Runtime [ms]")

    ax3.set_title('Speed')
    ax3.set_xlabel("Problem size")
    ax3.set_ylabel("Layers [1/s]")
    
    ax4.set_title('Error')
    ax4.set_xlabel("Problem size")
    ax4.set_ylabel("Relative Error")
    
    ax5.set_title('Expectation Value')
    ax5.set_xlabel("Problem size")
    ax5.set_ylabel("Score")
    ax5.legend()
    
    ax6.set_title('Optimizer iterations')
    ax6.set_xlabel("Problem size")
    ax6.set_ylabel("Iterations")
        
    ax7.set_title('Total runtime')
    ax7.set_xlabel("Problem size")
    ax7.set_ylabel("Runtime [s]")
    
    ax8.set_title('Total Quantum runtime')
    ax8.set_xlabel("Problem size")
    ax8.set_ylabel("Runtime [s]")
    
    ax9.set_title('Total Classic runtime')
    ax9.set_xlabel("Problem size")
    ax9.set_ylabel("Runtime [s]")
    
    #Add legend
    fig.legend(legend, loc='upper center', bbox_to_anchor=(0.5, 0.01), fancybox=True, shadow=True, ncol=4)
    
    ax1.get_xaxis().tick_bottom()
    ax1.get_yaxis().tick_left()

    fig.tight_layout()
    
    plt.savefig('plots/'+str(problem)+'.png', bbox_inches='tight')

#print(qpack_speed) 


#problems.append('Overall')
qpu_radar = []
#qpus.remove('7000000') #qpus = ['7000000']
for qpu in qpus:   
    
    ###Bar charts##################################################      
    X = np.arange(len(qpu_problems[qpu])+1)
    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    problem_legend = qpu_problems[qpu].copy()
    problem_legend.append('Overall')
    ax.set_yticks(X, problem_legend)
    ax.set_title(str(qpu_names[int(qpu, base=16)])) 
    
    plot_speed = []
    plot_accuracy = []
    plot_scale = []
    plot_cap = []
    for problem in qpu_problems[qpu]:
        plot_speed.append(qpack_speed[problem][qpu])
        plot_accuracy.append(qpack_accuracy[problem][qpu])
        plot_scale.append(qpack_scalability[problem][qpu])
        plot_cap.append(qpack_capacity[problem][qpu])
    
    #Overal score is geometric mean of datapoints
    speed_total = mean(plot_speed)
    accuracy_total = mean(plot_accuracy)
    scale_total = mean(plot_scale)
    cap_total = mean(plot_cap)    
    
    plot_speed.append(speed_total)
    plot_accuracy.append(accuracy_total)
    plot_scale.append(scale_total)
    plot_cap.append(cap_total)
    
    qpu_radar.append([speed_total, accuracy_total, scale_total, cap_total])
       
    speed_bar = ax.barh(X + 0.20, plot_speed, 0.20)
    acc_bar = ax.barh(X + 0.00, plot_accuracy, 0.20)
    scale_bar = ax.barh(X - 0.20, plot_scale, 0.20)  
    cap_bar = ax.barh(X - 0.40, plot_cap, 0.20)  
    ax.legend(['Runtime', 'Accuracy', 'Scalability', 'Capacity'])

    ax.bar_label(speed_bar, fmt='%.2f',size= 10)     
    ax.bar_label(acc_bar, fmt='%.2f', size= 10) 
    ax.bar_label(scale_bar, fmt='%.2f', size= 10)
    ax.bar_label(cap_bar, fmt='%.2f', size= 10)
    ax.set_xlabel('Benchmark Score')
    
    plt.savefig('plots/benchmark_'+str(qpu_names[int(qpu, base=16)])+'.png', bbox_inches='tight')
 

###Radar charts################################################## 
categories = ['Runtime', 'Accuracy', 'Scalability', 'Capacity']  
categories = [*categories, categories[0]]
label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(categories))

fig = plt.figure(figsize=(5, 8))
gs  = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
ax1 = plt.subplot(gs[0], projection = 'polar')
ax2 = plt.subplot(gs[1])
#plt.figure(figsize=(6, 6))
#plt.subplot(polar=True)

max_score = 0
for i, qpu in enumerate(qpus): 
    total_score = 0.5*(qpu_radar[i][0]+qpu_radar[i][2])*(qpu_radar[i][1]+qpu_radar[i][3])
    ax1.plot(label_loc, [*qpu_radar[i], qpu_radar[i][0]], label=str(qpu_names[int(qpu, base=16)]))
    ax1.fill(label_loc, [*qpu_radar[i], qpu_radar[i][0]], alpha=0.25)
    ax2.bar_label(ax2.bar(i, total_score, 0.35), fmt='%.2f',size= 10)
    if total_score > max_score:
        max_score = total_score

fig.suptitle('QPU benchmark comparison', size=15)
ax1.set_thetagrids(np.degrees(label_loc), labels=categories)
ax1.legend(prop={'size': 9}, bbox_to_anchor=(1.2, 1.1), loc='upper right')

ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
ax2.set_xticklabels([' ', *[qpu_names[int(qpu, base=16)] for qpu in qpus]], rotation=45, ha="right", rotation_mode="anchor")
ax2.set_ylabel('Benchmark Score')
ax2.set_xlabel('Quantum Backend')
ax2.set_ylim([0, 1.25*max_score])

fig.tight_layout()

plt.savefig('plots/benchmark_result.png', bbox_inches='tight')