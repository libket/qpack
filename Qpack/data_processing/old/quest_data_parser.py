import json
import matplotlib.pyplot as plt
from statistics import mean
from os import path
from os import listdir
from os.path import isfile, join

#Find available qpu's and problems
data_dir = "/home/huub/LibKet/qpack_output_old/QuEST_baseline"
qpus = []
problems = []
onlyfiles = [f for f in listdir(data_dir) if isfile(join(data_dir, f))]

for file in onlyfiles:
    split = file.split('_')
    qpu = split[1]
    problem = split[2]
    if(not(qpu in qpus)):
        qpus.append(qpu)
    if(not(problem in problems)):
        problems.append(problem)

#Data file
QuEST_dict = {}

for problem in problems:
    
    score_average = []
    score_optimal = []
    sizes = []
    
    for file in onlyfiles:
        split = file.split('_')
        _qpu = split[1]
        _problem = split[2]
        size = split[3][1:]
        if(_qpu == "7000000" and _problem == problem and not(size in sizes)):
            sizes.append(int(size))

    sizes.sort()        
    
    graph_sizes = []
    for size in sizes:

        score_mean = []
        score_optimal_mean = []
        graph_sizes.append(int(size))

        #Load json data    
        data_path = str(f"{data_dir}/Result_7000000_{problem}_N{size}_P3_S10000.json")
        if(not(path.exists(data_path))):
            print("File not found!")
            break;
        
        file = open(data_path)
        measurements = json.load(file)

        #print(measurements)
        
        for rep_data in measurements.values():  
            score_mean.append(rep_data['Score'])
            score_optimal_mean.append(rep_data['Optimal Score'])
        
        #Raw data collection
        score_average.append(min(score_mean))
        score_optimal.append(mean(score_optimal_mean))  
        
    QuEST_dict[problem] = {'sizes' : graph_sizes, 'score' : score_average, 'score_opt' : score_optimal}
                     
#axs = ((ax1, ax2, ax3), (ax4, ax5, ax6))
fig, axs = plt.subplots(2, 3)
fig.set_size_inches(8,8)

for i, r_ax in enumerate(axs):
    for j, ax in enumerate(r_ax):
        problem = problems[i*3+j]
        ax.set_title(f'{problem} score')
        ax.set_xlabel("Nodes")
        ax.set_ylabel("Score")
        ax.plot(QuEST_dict[problem]['sizes'], QuEST_dict[problem]['score'], marker = 'o')
        ax.plot(QuEST_dict[problem]['sizes'], QuEST_dict[problem]['score_opt'], marker = 'o')

#Add legend
fig.suptitle("QuEST Simulator score results")
fig.legend(["VQA", "Optimal"], loc='upper center', bbox_to_anchor=(0.5, 0.01), fancybox=True, shadow=True, ncol=5)

fig.tight_layout()

plt.savefig('plots/QuEST_scores.png', bbox_inches='tight')
json.dump(QuEST_dict,  open("QuEST_Scores.json", "w"), indent=4)
