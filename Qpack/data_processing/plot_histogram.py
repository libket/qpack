#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 09:26:28 2022

@author: huub
"""

import matplotlib.pyplot as plt
from math import log2, sqrt
from hist import counts, P, shots, problem

qubits = '0' + str(int(log2(len(counts)))) + 'b'

states = [format(i, qubits) for i in range(len(counts))]

sort_counts = sorted(zip(counts, states), reverse=True)

nonzeros = 0
for count in counts:
    if count > 0:
        nonzeros +=1

num_plot_states = nonzeros

sort_best = sorted(zip([sort_counts[i][1] for i in range(num_plot_states)], [sort_counts[i][0] for i in range(num_plot_states)]))

fig = plt.figure(figsize=(9,2))
ax = fig.add_axes([0,0,1,1])
ax.bar([sort_best[i][0] for i in range(num_plot_states)], [sort_best[i][1] for i in range(num_plot_states)])
plt.xticks(rotation=90, ha='center', rotation_mode="default", fontsize=8)
plt.xlabel("State", fontsize=11)
plt.ylabel("Counts", fontsize=11)
plt.title(f"State distribution {problem} for {shots} shots and P={P}", fontsize=14)

plt.savefig(f"/home/huub/qpack/Qpack/data_processing/plots/hists/Example_{problem}_P{P}.pdf", format="pdf", bbox_inches="tight")
plt.show()

N = int(sqrt(int(log2(len(counts)))))
for i in range(N):
    print(sort_counts[0][1][i*N:i*N+N])

print(" ")    
for i in range(N):
    print(sort_counts[1][1][i*N:i*N+N])
    
print(" ")    
for i in range(N):
    print(sort_counts[2][1][i*N:i*N+N])