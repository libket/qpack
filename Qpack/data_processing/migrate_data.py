import json
from os import listdir, makedirs
from os.path import isfile, join

#Custom JSON encoder, lists have no line breaks
class MyJSONEncoder(json.JSONEncoder):

  def iterencode(self, o, _one_shot=False):
    list_lvl = 0
    for s in super(MyJSONEncoder, self).iterencode(o, _one_shot=_one_shot):
      if s.startswith('['):
        list_lvl += 1
        s = s.replace('\n', '').rstrip().replace(" ", "")
      elif 0 < list_lvl:
        s = s.replace('\n', '').rstrip().replace(" ", "")
        if len(s) > 1:
            s = s[0] + " " + s[1:]
        if s and s[-1] == ',':
          s = s[:-1] + self.item_separator
        elif s and s[-1] == ':':
          s = s[:-1] + self.key_separator
      if s.endswith(']'):
        list_lvl -= 1
      yield s

#Find available qpu's and problems
#data_dir = "/home/huub/LibKet/qpack_output/mock_data"
data_dir = "/home/huub/LibKet/qpack_output_old"
new_data_dir = "/home/huub/LibKet/qpack_output/benchmark"
onlyfiles = [f for f in listdir(data_dir) if isfile(join(data_dir, f))]

#Create dict to store data
data_structure = {}

'''
#Loop over all data files
for file in onlyfiles:
    split = file.split('_')
    qpu = split[1]
    problem = split[2]
    size = split[3][1:]
    if(len(size) < 2):
        size = "0"+size
    
    #create directories if non-existant   
    makedirs(f'{new_data_dir}/{qpu}/{problem}/N{size}', exist_ok = True)   
    
    #Open data file
    with open(f'{data_dir}/{file}') as json_file:
        data = json.load(json_file)
        
        #Filter and add to JSON file in new data format
        for key, value in data.items(): 
            if value["Score"] is None:     
                continue          
            value["Expectation Value"] = value.pop("Score")
            value["Optimal Expectation Value"] = value.pop("Optimal Score")
            value["Optimizer params"] = value.pop("Optimal params")
            value.pop("Average job duration [ms]")
            value["QPU"] = qpu
            value["Problem"] = problem
            value["Size"] = int(size)
            value["P"] = 0 if problem in ['RH', 'IC'] else 3
            value["Shots"] = 4096

            #Store new result file
            with open(f'{new_data_dir}/{qpu}/{problem}/N{size}/dataset{int(key):02d}.json', 'w') as outfile:
                json.dump(value, outfile, indent=2, cls=MyJSONEncoder)
'''