# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 21:57:05 2022

@author: huub
"""

#Name lib:
qpu_names = {     'Constant1'                                 : 0xAAA,
                  'Constant2'                                 : 0xAAB,
                  'Constant3'                                 : 0xAAC,
                  'Linear1'                                   : 0xBBA,
                  'Linear2'                                   : 0xBBB,
                  'Linear3'                                   : 0xBBC,
                  'Quadratic1'                                : 0xCCA, 
                  'Quadratic2'                                : 0xCCB, 
                  'Quadratic3'                                : 0xCCC,   
                  'Exponential1'                              : 0xDDA, 
                  'Exponential2'                              : 0xDDB, 
                  'Exponential3'                              : 0xDDC,                  
                  'generic'                                   : 0x00000000, 
                  'pyquil_visualizer'                         : 0x00000001, 
                  'qasm2tex_visualizer'                       : 0x00000002, 
                  'qiskit_visualizer'                         : 0x00000003,
                  'atos_qlm_feynman_simulator'                : 0x01000001,
                  'atos_qlm_linalg_simulator'                 : 0x01000002, 
                  'atos_qlm_stabs_simulator'                  : 0x01000003, 
                  'atos_qlm_mps_simulator'                    : 0x01000004, 
                  'qi_26_simulator'                           : 0x02000001, 
                  'qi_34_simulator'                           : 0x02000002, 
                  'qi_spin2'                                  : 0x02010001, 
                  'qi_starmon5'                               : 0x02010002,
                  'qiskit_almaden_simulator'                  : 0x03000001, 
                  'qiskit_armonk_simulator'                   : 0x03000002, 
                  'qiskit_athens_simulator'                   : 0x03000003, 
                  'qiskit_belem_simulator'                    : 0x03000004, 
                  'qiskit_boeblingen_simulator'               : 0x03000005, 
                  'qiskit_bogota_simulator'                   : 0x03000006,
                  'qiskit_brooklyn_simulator'                 : 0x03000007, 
                  'qiskit_burlington_simulator'               : 0x03000008, 
                  'qiskit_cairo_simulator'                    : 0x03000009, 
                  'qiskit_cambridge_simulator'                : 0x0300000A, 
                  'qiskit_casablanca_simulator'               : 0x0300000B, 
                  'qiskit_dublin_simulator'                   : 0x0300000C, 
                  'qiskit_essex_simulator'                    : 0x0300000D, 
                  'qiskit_guadalupe_simulator'                : 0x0300000E, 
                  'qiskit_hanoi_simulator'                    : 0x0300000F, 
                  'Jakarta simulator'                         : 0x03000010, 
                  'qiskit_johannesburg_simulator'             : 0x03000011, 
                  'qiskit_kolkata_simulator'                  : 0x03000012,
                  'Lagos simulator'                           : 0x03000013, 
                  'qiskit_lima_simulator'                     : 0x03000014, 
                  'qiskit_london_simulator'                   : 0x03000015, 
                  'qiskit_manhattan_simulator'                : 0x03000016,
                  'Manila simulator'                          : 0x03000017, 
                  'qiskit_melbourne_simulator'                : 0x03000018,
                  'qiskit_montreal_simulator'                 : 0x03000019, 
                  'qiskit_mumbai_simulator'                   : 0x0300001A,
                  'Nairobi simulator'                         : 0x0300001B, 
                  'qiskit_ourense_simulator'                  : 0x0300001C, 
                  'qiskit_paris_simulator'                    : 0x0300001D,
                  'qiskit_peekskill_simulator'                : 0x0300001E, 
                  'qiskit_poughkeepsie_simulator'             : 0x0300001F,
                  'Quito simulator'                           : 0x03000020, 
                  'qiskit_rochester_simulator'                : 0x03000021,
                  'qiskit_rome_simulator'                     : 0x03000022,
                  'qiskit_rueschlikon_simulator'              : 0x03000023, 
                  'qiskit_santiago_simulator'                 : 0x03000024,
                  'qiskit_singapore_simulator'                : 0x03000025, 
                  'qiskit_sydney_simulator'                   : 0x03000026,
                  'qiskit_tenerife_simulator'                 : 0x03000027,
                  'qiskit_tokyo_simulator'                    : 0x03000028, 
                  'qiskit_toronto_simulator'                  : 0x03000029, 
                  'qiskit_valencia_simulator'                 : 0x0300002A, 
                  'qiskit_vigo_simulator'                     : 0x0300002B,
                  'qiskit_yorktown_simulator'                 : 0x0300002C, 
                  'qiskit_washington_simulator'               : 0x0300002D, 
                  'Perth simulator'                           : 0x0300002E, 
                  'qiskit_pulse_simulator'                    : 0x03010001, 
                  'Qiskit QASM Simulator'                     : 0x03010002, 
                  'qiskit_statevector_simulator'              : 0x03010003, 
                  'qiskit_unitary_simulator'                  : 0x03010004, 
                  'qiskit_aer_density_matrix_simulator'       : 0x03010005,
                  'qiskit_aer_extended_stabilizer_simulator'  : 0x03010006,
                  'qiskit_aer_matrix_product_state_simulator' : 0x03010007, 
                  'Qiskit Aer Simulator'                      : 0x03010008, 
                  'qiskit_aer_stabilizer_simulator'           : 0x03010009,
                  'qiskit_aer_statevector_simulator'          : 0x0301000A, 
                  'qiskit_aer_superop_simulator'              : 0x0301000B, 
                  'qiskit_aer_unitary_simulator'              : 0x0301000C, 
                  'ibmq_almaden_simulator'                    : 0x04000001, 
                  'ibmq_armonk_simulator'                     : 0x04000002, 
                  'ibmq_athens_simulator'                     : 0x04000003, 
                  'ibmq_belem_simulator'                      : 0x04000004,
                  'ibmq_boeblingen_simulator'                 : 0x04000005, 
                  'ibmq_bogota_simulator'                     : 0x04000006, 
                  'ibmq_brooklyn_simulator'                   : 0x04000007, 
                  'ibmq_burlington_simulator'                 : 0x04000008, 
                  'ibmq_cairo_simulator'                      : 0x04000009, 
                  'ibmq_cambridge_simulator'                  : 0x0400000A, 
                  'ibmq_casablanca_simulator'                 : 0x0400000B, 
                  'ibmq_dublin_simulator'                     : 0x0400000C, 
                  'ibmq_essex_simulator'                      : 0x0400000D, 
                  'ibmq_guadalupe_simulator'                  : 0x0400000E, 
                  'ibmq_hanoi_simulator'                      : 0x0400000F, 
                  'ibmq_jakarta_simulator'                    : 0x04000010, 
                  'ibmq_johannesburg_simulator'               : 0x04000011, 
                  'ibmq_kolkata_simulator'                    : 0x04000012, 
                  'ibmq_lagos_simulator'                      : 0x04000013, 
                  'ibmq_lima_simulator'                       : 0x04000014,
                  'ibmq_london_simulator'                     : 0x04000015, 
                  'ibmq_manhattan_simulator'                  : 0x04000016, 
                  'ibmq_manila_simulator'                     : 0x04000017, 
                  'ibmq_melbourne_simulator'                  : 0x04000018,    
                  'ibmq_montreal_simulator'                   : 0x04000019,
                  'ibmq_mumbai_simulator'                     : 0x0400001A, 
                  'ibmq_nairobi_simulator'                    : 0x0400001B,
                  'ibmq_ourense_simulator'                    : 0x0400001C, 
                  'ibmq_paris_simulator'                      : 0x0400001D, 
                  'ibmq_peekskill_simulator'                  : 0x0400001E, 
                  'ibmq_poughkeepsie_simulator'               : 0x0400001F, 
                  'ibmq_quito_simulator'                      : 0x04000020,
                  'ibmq_rochester_simulator'                  : 0x04000021,
                  'ibmq_rome_simulator'                       : 0x04000022, 
                  'ibmq_rueschlikon_simulator'                : 0x04000023,
                  'ibmq_santiago_simulator'                   : 0x04000024,
                  'ibmq_singapore_simulator'                  : 0x04000025, 
                  'ibmq_sydney_simulator'                     : 0x04000026, 
                  'ibmq_tenerife_simulator'                   : 0x04000027, 
                  'ibmq_tokyo_simulator'                      : 0x04000028, 
                  'ibmq_toronto_simulator'                    : 0x04000029, 
                  'ibmq_valencia_simulator'                   : 0x0400002A, 
                  'ibmq_vigo_simulator'                       : 0x0400002B, 
                  'ibmq_yorktown_simulator'                   : 0x0400002C, 
                  'ibmq_washington_simulator'                 : 0x0400002D,
                  'ibmq_perth_simulator'                      : 0x0400002E, 
                  'IBMQ QASM Simulator'                       : 0x04010001, 
                  'ibmq_almaden'                              : 0x04020001, 
                  'ibmq_armonk'                               : 0x04020002, 
                  'ibmq_athens'                               : 0x04020003,
                  'ibmq_belem'                                : 0x04020004, 
                  'ibmq_boeblingen'                           : 0x04020005, 
                  'ibmq_bogota'                               : 0x04020006, 
                  'ibmq_brooklyn'                             : 0x04020007, 
                  'ibmq_cairo'                                : 0x04020008,
                  'ibmq_burlington'                           : 0x04020009,
                  'ibmq_cambridge'                            : 0x0402000A, 
                  'ibmq_casablanca'                           : 0x0402000B, 
                  'ibmq_dublin'                               : 0x0402000C, 
                  'ibmq_essex'                                : 0x0402000D,
                  'ibmq_guadalupe'                            : 0x0402000E, 
                  'ibmq_hanoi'                                : 0x0402000F, 
                  'IBMQ Jakarta'                              : 0x04020010,
                  'ibmq_johannesburg'                         : 0x04020011, 
                  'ibmq_kolkata'                              : 0x04020012, 
                  'IBMQ Lagos'                                : 0x04020013,
                  'ibmq_lima'                                 : 0x04020014,
                  'ibmq_london'                               : 0x04020015,
                  'ibmq_manhattan'                            : 0x04020016, 
                  'IBMQ Manila'                               : 0x04020017, 
                  'ibmq_melbourne'                            : 0x04020018,
                  'ibmq_montreal'                             : 0x04020019,
                  'ibmq_mumbai'                               : 0x0402001A,
                  'IBMQ Nairobi'                              : 0x0402001B, 
                  'ibmq_ourense'                              : 0x0402001C, 
                  'ibmq_paris'                                : 0x0402001D, 
                  'ibmq_peekskill'                            : 0x0402001E,
                  'ibmq_poughkeepsie'                         : 0x0402001F,
                  'IBMQ Quito'                                : 0x04020020,
                  'ibmq_rochester'                            : 0x04020021, 
                  'ibmq_rome'                                 : 0x04020022, 
                  'ibmq_rueschlikon'                          : 0x04020023,
                  'ibmq_santiago'                             : 0x04020024, 
                  'ibmq_singapore'                            : 0x04020025,
                  'ibmq_sydney'                               : 0x04020026, 
                  'ibmq_tenerife'                             : 0x04020027,
                  'ibmq_tokyo'                                : 0x04020028,
                  'ibmq_toronto'                              : 0x04020029,
                  'ibmq_valencia'                             : 0x0402002A,
                  'ibmq_vigo'                                 : 0x0402002B,
                  'ibmq_yorktown'                             : 0x0402002C,
                  'ibmq_washington'                           : 0x0402002D,
                  'IBMQ Perth'                                : 0x0402002E,
                  'openql_cc_light_compiler'                  : 0x05000001,
                  'openql_cc_light17_compiler'                : 0x05000002, 
                  'openql_qx_compiler'                        : 0x05000003, 
                  'rigetti_aspen_8_simulator'                 : 0x06000001, 
                  'rigetti_aspen_9_simulator'                 : 0x06000002, 
                  'rigetti_aspen_10_simulator'                : 0x06000003, 
                  'rigetti_aspen_M1_simulator'                : 0x06000004, 
                  'rigetti_9q_simulator'                      : 0x06010001,
                  'rigetti_9q_square_simulator'               : 0x06010002,
                  #'Rigetti 16Q QVM '                         : 0x06010003, # (local)
                  'Rigetti 16Q L QVM'                         : 0x06010013, # (local opt)
                  'Rigetti 16Q R QVM'                         : 0x06010030, # (remote)
                  'rigetti_16q_square_simulator'              : 0x06010004,
                  'rigetti_aspen_8'                           : 0x06020001, 
                  'rigetti_aspen_9'                           : 0x06020002, 
                  'rigetti_aspen_10'                          : 0x06020003, 
                  'Rigetti Aspen-M-1'                         : 0x06020004, 
                  'QuEST Simulator'                           : 0x07000000,
                  'QX'                                        : 0x08000000, 
                  'Cirq Simulator'                            : 0x09000001, 
                  'cirq_bristlecone_simulator'                : 0x09000002, 
                  'cirq_foxtail_simulator'                    : 0x09000003,
                  'cirq_sycamore_simulator'                   : 0x09000004, 
                  'cirq_sycamore23_simulator'                 : 0x09000005,
                  'Ionq Simulator'                            : 0x10000001, 
                  'ionq_qpu'                                  : 0x10000002}  
qpu_names = {value: key for key, value in qpu_names.items()}  
