import json
from os import listdir, remove, makedirs
import matplotlib.pyplot as plt
from math import log10, atan, pi
from matplotlib.ticker import MaxNLocator
from statistics import mean
from qpu_names import qpu_names
import numpy as np
from scipy.optimize import minimize
from matplotlib import gridspec
from pathlib import Path

problem_names = {'MCP' : 'QAOA: MaxCut problem',
                 'DSP' : 'QAOA: Dominating set problem',
                 'MIS' : 'QAOA: Maximal independent set problem',
                 'TSP' : 'QAOA: Traveling salesperson problem',
                 'RH' : 'VQE: Random Hamiltonian',
                 'IC' : 'VQE: Ising chain'}

def loadBenchmarkData(data_dir, max_size = 1000, subset=['MCP', 'DSP', 'MIS', 'TSP', 'RH', 'IC']):
    '''
    @brief Function to load in QPack benchmark data from selected data path
    
    @params data_dir : Path to data directory of QPack benchmark files
    
    @return Dictionary of relevant benchmark parameters used to compute 
            benchmark scores
    '''
        
    benchmark_data = {}
    for qpu in listdir(f'{data_dir}'):
        benchmark_data[qpu] = {}
        for problem in listdir(f'{data_dir}/{qpu}'):

            if problem not in subset:
                continue
            
            benchmark_data[qpu][problem] = {}
            for size in listdir(f'{data_dir}/{qpu}/{problem}'):

                if(int(size[1:]) > max_size):
                    continue

                benchmark_data[qpu][problem][size] = {}
                
                benchmark_data[qpu][problem][size]['qjob average'] = 0
                benchmark_data[qpu][problem][size]['exec average'] = 0
                benchmark_data[qpu][problem][size]['cjob average'] = 0
                benchmark_data[qpu][problem][size]['exp val'] = 100
                benchmark_data[qpu][problem][size]['opt iterations'] = 0
                benchmark_data[qpu][problem][size]['total alg time'] = 0
                benchmark_data[qpu][problem][size]['total q time'] = 0
                benchmark_data[qpu][problem][size]['total c time'] = 0
                
                numReps = 0
                for rep in listdir(f'{data_dir}/{qpu}/{problem}/{size}'):
                    file = open(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')
                    data = json.load(file)

                    '''
                    if not data['Optimizer params'] or not data['Expectation Value']:
                        print(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')
                        #remove(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')
                        continue
                    '''    
                    #Add queue zeroes if missing
                    if 'Queue durations [ms]' not in data:
                        #print("None!")
                        data['Queue durations [ms]'] = [0.0]  
                    #print(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')

                    #Filter missing queue data
                    #test_list = [10, 15, 28, 45, 654, 545, -1, 546, -10000, 1247, -1000]
                    missing_idx = []
                    for index, queue_time in  enumerate(data['Queue durations [ms]']): #enumerate(test_list):
                        if queue_time < 0:
                            missing_idx.append(index)
                    
                    #if len(missing_idx) > 0:
                        #print(data['Queue durations [ms]'])
                        #print(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')
                        #print(list(reversed(missing_idx)))                            
                    
                    for idx in reversed(missing_idx):  
                        #queue_rm = data['Queue durations [ms]'][idx]
                        #job_rm = data['QJob durations [ms]'][idx]
                        #print(f"Removing queue: {queue_rm} and job: {job_rm} ")

                        data['Queue durations [ms]'].pop(idx)
                        data['QJob durations [ms]'].pop(idx)

                   # if len(missing_idx) > 0:    
                        #print(data['Queue durations [ms]'])
                

                    benchmark_data[qpu][problem][size]['qjob average'] += mean(data['QJob durations [ms]']) - mean(data['Queue durations [ms]']) #mean(data['Job durations [ms]'])
                    benchmark_data[qpu][problem][size]['exec average'] += mean(data['Circuit execution durations [ms]'])
                    benchmark_data[qpu][problem][size]['cjob average'] += mean(data['Optimizer durations [ms]']) #mean(data['Circuit execution durations [ms]']) 
                    benchmark_data[qpu][problem][size]['qubits'] = data['Qubits']
                    benchmark_data[qpu][problem][size]['depth'] = data['Depth']
                    benchmark_data[qpu][problem][size]['shots'] = data['Shots']
                    benchmark_data[qpu][problem][size]['num params'] = len(data['Optimizer params'])
                    if data['Expectation Value'] is None:
                        print(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')
                    if data['Expectation Value'] < benchmark_data[qpu][problem][size]['exp val']:
                        benchmark_data[qpu][problem][size]['exp val'] = data['Expectation Value']
                    benchmark_data[qpu][problem][size]['opt exp val'] = data['Optimal Expectation Value']
                    benchmark_data[qpu][problem][size]['opt iterations'] += data['Optimizer iterations']
                    benchmark_data[qpu][problem][size]['total alg time'] += data['Total Algorithm duration [s]']
                    benchmark_data[qpu][problem][size]['total q time'] += data['Total Quantum duration [s]']
                    benchmark_data[qpu][problem][size]['total c time'] += data['Total Classic duration [s]']
                    numReps += 1
                    
                if numReps > 0:
                    benchmark_data[qpu][problem][size]['qjob average'] /= numReps
                    benchmark_data[qpu][problem][size]['exec average'] /= numReps
                    benchmark_data[qpu][problem][size]['cjob average'] /= numReps
                    #benchmark_data[qpu][problem][size]['exp val'] /= numReps
                    benchmark_data[qpu][problem][size]['opt iterations'] /= numReps
                    benchmark_data[qpu][problem][size]['total alg time'] /= numReps
                    benchmark_data[qpu][problem][size]['total q time'] /= numReps
                    benchmark_data[qpu][problem][size]['total q time'] /= numReps

                
    return benchmark_data

def plotData(QPack_data, qpus = [], reduce=False, subdir=None):
    '''
    @brief Plots raw data metrics from QPack benchmark for each problem
    '''
    
    #Get all available QPU data if no subset is provided
    if not qpus:
        qpus = list(QPack_data.keys())

    #Set problems to process
    problems = []
    for qpu in qpus:
        for problem in list(QPack_data[qpu].keys()):
            if problem not in problems:
                problems.append(problem)
    
    #Get QueST data
    file = open("QuEST_Scores.json")
    QuEST_data = json.load(file)
    
    #Loop over all problems
    for problem in problems:
    
        #Create figures
        if reduce:
            fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3)
            fig.set_size_inches(8, 5)
        else:
            fig, ((ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9)) = plt.subplots(3, 3)
            fig.set_size_inches(8,8)
        
        
        legend = []
        
        #Set maximum and minimum problem size variables
        minSize = float('inf')
        maxSize = 0
        
        #Loop over all selected qpus
        for qpu in qpus:
            
            #If a qpu has not evaluated a problem, skip this qpu
            if problem not in list(QPack_data[qpu].keys()):
                continue
            
            #Add current QPU to legend
            legend.append(str(qpu_names[int(qpu, base=16)]))
            
            #Get problem sizes for this qpu
            sizes = list(QPack_data[qpu][problem].keys())        

            #Retrieve data from JSON object
            #print(f'{str(qpu_names[int(qpu, base=16)])}: {str(problem)}')
            qjob_average = [QPack_data[qpu][problem][size]['qjob average'] for size in sizes]
            exec_average = [QPack_data[qpu][problem][size]['exec average'] for size in sizes]
            cjob_average = [QPack_data[qpu][problem][size]['cjob average'] for size in sizes]
            numQubits = [QPack_data[qpu][problem][size]['qubits'] for size in sizes]
            part_speed =  [QPack_data[qpu][problem][size]['depth']*QPack_data[qpu][problem][size]['shots']/(QPack_data[qpu][problem][size]['qjob average']/1000) for size in sizes]
            part_acc = [(QuEST_data[problem]['exp val'][QuEST_data[problem]['sizes'].index(int(size[1:]))]-QPack_data[qpu][problem][size]['exp val'])/QuEST_data[problem]['exp val'][QuEST_data[problem]['sizes'].index(int(size[1:]))] for size in sizes]
            exp_val_average = [QPack_data[qpu][problem][size]['exp val'] for size in sizes] 
            opt_iterations = [QPack_data[qpu][problem][size]['opt iterations'] for size in sizes]
            ttime = [QPack_data[qpu][problem][size]['total alg time'] for size in sizes]
            qtime = [QPack_data[qpu][problem][size]['total q time'] for size in sizes]
            ctime = [QPack_data[qpu][problem][size]['total c time'] for size in sizes]   
            
            int_sizes = [int(size[1:]) for size in sizes]
            
            #Sort data and plot on axes
            zipped = sorted(zip(int_sizes, qjob_average, exec_average, cjob_average, numQubits, part_speed, part_acc, exp_val_average, opt_iterations, ttime, qtime, ctime))
            
            ax1.plot(list(item[0] for item in zipped), list(item[1] for item in zipped), marker = 'o', markersize=3)
            ax2.plot(list(item[0] for item in zipped), list(item[2] for item in zipped), marker = 'o', markersize=3)
            ax3.plot(list(item[0] for item in zipped), list(item[3] for item in zipped), marker = 'o', markersize=3)
            ax4.plot(list(item[0] for item in zipped), list(item[7] for item in zipped), marker = 'o', markersize=3)
            ax5.plot(list(item[0] for item in zipped), list(item[6] for item in zipped), marker = 'o', markersize=3)
            ax6.plot(list(item[0] for item in zipped), list(item[8] for item in zipped), marker = 'o', markersize=3)

            #print(f'{str(qpu_names[int(qpu, base=16)])}: {str(problem)}')
            #print(f'Sizes: {list(item[0] for item in zipped)}')
            #print(f'QJob: {list(item[1] for item in zipped)}')
            #print(f'Exec: {list(item[2] for item in zipped)}')
            #print(f'Exec %: {mean(list(item[2] for item in zipped))/mean(list(item[1] for item in zipped))}')
            
            if not reduce:
                ax7.plot(list(item[0] for item in zipped), list(item[9] for item in zipped), marker = 'o', markersize=3)
                ax8.plot(list(item[0] for item in zipped), list(item[10] for item in zipped), marker = 'o', markersize=3)
                ax9.plot(list(item[0] for item in zipped), list(item[11] for item in zipped), marker = 'o', markersize=3)
            
            #Find smallest and largest problem size
            if zipped[0][0] < minSize:
                minSize = zipped[0][0]
                
            if zipped[-1][0] > maxSize:
                maxSize = zipped[-1][0]
        
        #Plot QuEST baseline
        quest_zip = sorted(zip(QuEST_data[problem]['sizes'], QuEST_data[problem]['exp val']))
        quest_sizes = list(item[0] for item in quest_zip)
        quest_exps = list(item[1] for item in quest_zip)
        start = quest_sizes.index(minSize)
        end = quest_sizes.index(maxSize)
        ax4.plot(quest_sizes[start:end+1], quest_exps[start:end+1] , color='red', marker = '', label="baseline")

        # Force x-axis integers
        ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax3.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax4.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax5.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax6.xaxis.set_major_locator(MaxNLocator(integer=True))
        if not reduce:
            ax7.xaxis.set_major_locator(MaxNLocator(integer=True))
            ax8.xaxis.set_major_locator(MaxNLocator(integer=True))
            ax9.xaxis.set_major_locator(MaxNLocator(integer=True))
        
        #y-axis scale
        ax1.set_yscale("log")
        ax2.set_yscale("log")
        ax3.set_yscale("log")
        if not reduce:
            ax7.set_yscale("log")
            ax8.set_yscale("log")
            ax9.set_yscale("log")
         
        # Add titles and lables
        fig.suptitle(problem_names[str(problem)])
        ax1.set_title('Average QJob \n duration')
        ax1.set_xlabel("Problem size")
        ax1.set_ylabel("Runtime [ms]")

        ax2.set_title('Average circuit \n execution duration')
        ax2.set_xlabel("Problem size")
        ax2.set_ylabel("Runtime [ms]")
    
        ax3.set_title('Average optimizer \n duration')
        ax3.set_xlabel("Problem size")
        ax3.set_ylabel("Runtime [ms]")
    
        #ax3.set_title('Speed')
        #ax3.set_xlabel("Qubits")
        #ax3.set_ylabel("Layers [1/s]")

        ax4.set_title('Output state')
        ax4.set_xlabel("Problem size")
        ax4.set_ylabel("Expectation value")
        ax4.legend()
        
        ax5.set_title('Error')
        ax5.set_xlabel("Problem size")
        ax5.set_ylabel("Relative Error")
        
        ax6.set_title('Optimizer iterations')
        ax6.set_xlabel("Problem size")
        ax6.set_ylabel("Iterations")
        
        if not reduce:            
            ax7.set_title('Total runtime')
            ax7.set_xlabel("Problem size")
            ax7.set_ylabel("Runtime [s]")
            
            ax8.set_title('Total Quantum runtime')
            ax8.set_xlabel("Problem size")
            ax8.set_ylabel("Runtime [s]")
            
            ax9.set_title('Total Classic runtime')
            ax9.set_xlabel("Problem size")
            ax9.set_ylabel("Runtime [s]")
        
        #Add legend
        fig.legend(legend, loc='upper center', bbox_to_anchor=(0.5, 0.01), fancybox=True, shadow=True, ncol=4)
        
        #Set tick marks
        ax1.get_xaxis().tick_bottom()
        ax1.get_yaxis().tick_left()
        
        #Save result in plots directory
        fig.tight_layout()
        if subdir:
            plt.savefig(f'plots/{subdir}/{str(problem)}.pdf', bbox_inches='tight')
        else:
            plt.savefig(f'plots/{str(problem)}.pdf', bbox_inches='tight')

        plt.close()   

def computeScores(QPack_data, qpus=[]):
    '''
    @brief Computes sub-scores for selected QPUs
    
    @param Qpack_data : Dataset containing all relevant benchmark data
                        per QPU per problem
    
    @param qpus : Optional subset of qpus

    @return QPack Scores for each QPU and problem set

    '''

    #Get all available QPU data if no subset is provided
    if not qpus:
        qpus = list(QPack_data.keys())
        
    #Get QueST data
    file = open("QuEST_Scores.json")
    QuEST_data = json.load(file)
    
    #Create struct to store score resutls for all QPUs    
    scores = {}
    
    for qpu in qpus:
        scores[qpu] = {}
        
        problems = list(QPack_data[qpu].keys())

        for problem in problems:
            
            scores[qpu][problem] = {}
            #Get problem sizes for this qpu
            sizes = list(QPack_data[qpu][problem].keys())    
            
            #Runtime sub-score
            ave_troughput = mean([QPack_data[qpu][problem][size]['depth']*QPack_data[qpu][problem][size]['shots']/(QPack_data[qpu][problem][size]['qjob average']/1000) for size in sizes])
            scores[qpu][problem]['runtime'] = log10(ave_troughput)
            
            #Accuracy sub-score
            rel_errors = []
            for size in sizes:
                QuEST_score = QuEST_data[problem]['exp val'][QuEST_data[problem]['sizes'].index(int(size[1:]))]
                abs_error = QuEST_score-QPack_data[qpu][problem][size]['exp val']+10e-10
                rel_errors.append(abs_error/QuEST_score)
            ave_error = mean(rel_errors)  
           
            #Accuracy score mapping
            a_acc = 10
            b_acc = 5
            scores[qpu][problem]['accuracy'] = a_acc*(pi/2 - atan(b_acc*(ave_error)))
            
            #Scalability sub-score            
            qjob_averages = [QPack_data[qpu][problem][size]['qjob average'] for size in sizes]
            int_sizes = [int(size[1:]) for size in sizes]
            zipped = sorted(zip(int_sizes, qjob_averages, rel_errors))
            
            if(len(sizes) > 1):
                
                #Normalise qjobtimes
                qjob_array = np.array([item[1] for item in zipped])
                if(np.amax(qjob_array) != np.amin(qjob_array)):
                    qjob_array[-1] += 0.001
                norm_in = (qjob_array - np.amin(qjob_array))/(np.amax(qjob_array)-np.amin(qjob_array))

                def min_func(a):
                    x = np.array([item[0] for item in zipped])
                    y = x**a
                    norm_y = (y - np.amin(y))/(np.amax(y)-np.amin(y))   
                    sq_diff = (norm_in-norm_y)**2
                    err = sum(sq_diff)
                    return err
                
                fit = minimize(min_func, 1, method='Nelder-Mead')
                
                a_scale = 10
                b_scale = 0.75
                scores[qpu][problem]['scalability'] = a_scale*(pi/2 - atan(b_scale*fit.x[0]))

            else:
                scores[qpu][problem]['scalability'] = 0  
                
            #Capacity score
            max_size = 0
            threshold = 0.25
            for i in range(len(sizes)):
                if(zipped[i][2] < threshold and zipped[i][0] > max_size):
                    max_size = zipped[i][0]
            
            if(max_size):
                scores[qpu][problem]['capacity'] = QPack_data[qpu][problem][f'N{max_size:02d}']['qubits']   
            else:     
                scores[qpu][problem]['capacity'] = 0

        '''        
        plt.plot(norm_in, label = str(qpu_names[int(qpu, base=16)]))
        plt.title(problem)
        plt.legend()
        plt.savefig(f'plots/test.png', bbox_inches='tight') 
        '''
                
        #Overall sub-scores
        scores[qpu]['Combined'] = {}
        scores[qpu]['Combined']['runtime'] = mean([scores[qpu][problem]['runtime'] for problem in problems])
        scores[qpu]['Combined']['accuracy'] = mean([scores[qpu][problem]['accuracy'] for problem in problems])
        scores[qpu]['Combined']['scalability'] = mean([scores[qpu][problem]['scalability'] for problem in problems])
        scores[qpu]['Combined']['capacity'] = mean([scores[qpu][problem]['capacity'] for problem in problems])        
        
    return scores    

def plotQPUScores(qpu_scores, qpus=[], subdir=None, xlimit=None):
    '''
    @brief Creates plot for all QPU benchmark sub-scores

    @param qpu_scores : Dictionary containing QPack scores for each QPU
    
    @param qpus : Optional subset of qpus
    '''

    #Get all available QPU data if no subset is provided
    if not qpus:
        qpus = list(qpu_scores.keys())
    
    #Loop over all selected qpus
    for qpu in qpus: 
        
        #Get problems for this QPU
        problems = list(qpu_scores[qpu].keys())
        
        #Set bar axis         
        X = np.arange(len(problems))
        fig = plt.figure(figsize=(3,6))
        ax = fig.add_axes([0,0,1,1])
        ax.set_yticks(X, problems)
        ax.tick_params(axis='x', labelsize=12)
        ax.tick_params(axis='y', labelsize=12)
        ax.set_title(str(qpu_names[int(qpu, base=16)])+'\n\n\n', fontsize=14) 
        
        #Create plotting data lists
        plot_speed = []
        plot_accuracy = []
        plot_scale = []
        plot_cap = []
        for problem in problems:
            plot_speed.append(qpu_scores[qpu][problem]['runtime'])
            plot_accuracy.append(qpu_scores[qpu][problem]['accuracy'])
            plot_scale.append(qpu_scores[qpu][problem]['scalability'])
            plot_cap.append(qpu_scores[qpu][problem]['capacity'])
        
        #Plot all scores with some offset                   
        speed_bar = ax.barh(X + 0.20, plot_speed, 0.20)
        acc_bar = ax.barh(X + 0.00, plot_accuracy, 0.20)
        scale_bar = ax.barh(X - 0.20, plot_scale, 0.20)  
        cap_bar = ax.barh(X - 0.40, plot_cap, 0.20)  
        ax.legend(['Runtime', 'Accuracy', 'Scalability', 'Capacity'], fontsize=12, loc='upper center', bbox_to_anchor=(0.49, 1.12), fancybox=True, shadow=True, ncol=2)
        
        #Add labels and score values
        ax.bar_label(speed_bar, fmt='%.2f',size= 10)     
        ax.bar_label(acc_bar, fmt='%.2f', size= 10) 
        ax.bar_label(scale_bar, fmt='%.2f', size= 10)
        ax.bar_label(cap_bar, fmt='%.2f', size= 10)
        ax.set_xlabel('Benchmark Score')
        if xlimit:
            ax.set_xlim(0, xlimit)
        
        #Save figure 
        if subdir:
            plt.savefig(f'plots/{subdir}/benchmark_{str(qpu_names[int(qpu, base=16)])}.pdf', bbox_inches='tight')
        else:
            plt.savefig(f'plots/benchmark_{str(qpu_names[int(qpu, base=16)])}.pdf', bbox_inches='tight')

        plt.close()     
        
def plotRadar(qpu_scores, qpus=[], reduce=False, subdir=None):
    '''
    @brief Creates plots for final benchmark scores on radar charts

    @param qpu_scores : Dictionary containing QPack scores for each QPU
    
    @param qpus : Optional subset of qpus
    '''      

    #Get all available QPU data if no subset is provided
    if not qpus:
        qpus = list(qpu_scores.keys())
    
    #Setup radar plot    
    categories = ['Runtime', 'Accuracy', 'Scalability', 'Capacity']  
    categories = [*categories, categories[0]]
    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(categories))
    
    #fig = plt.figure(figsize=(12, 7))
    #gs  = gridspec.GridSpec(ncols=17, nrows=10, figure=fig)
    #ax1 = fig.add_subplot(gs[0:9, 0:9], projection = 'polar')
    #ax2 = fig.add_subplot(gs[2:6, 11:16])    
    
    if not reduce:
        fig = plt.figure(figsize=(8, 4.5))
        gs  = gridspec.GridSpec(ncols=19, nrows=10, figure=fig)
        ax1 = fig.add_subplot(gs[0:9, 0:9], projection = 'polar')
        ax2 = fig.add_subplot(gs[2:6, 13:18])   

        #fig = plt.figure(figsize=(4.5, 7))
        #gs  = gridspec.GridSpec(ncols=10, nrows=17, figure=fig)
        #ax1 = fig.add_subplot(gs[0:9, 0:9], projection = 'polar')
        #ax2 = fig.add_subplot(gs[11:16, 1:9])    
    else:
        fig = plt.figure(figsize=(4.5, 4.5))
        gs  = gridspec.GridSpec(ncols=10, nrows=10, figure=fig)
        ax1 = fig.add_subplot(gs[0:9, 0:9], projection = 'polar')
        
    
    #Compute total QPU score
    max_score = 0
    max_subscore = 0
    for i, qpu in enumerate(qpus): 
        total_score = 0.5*(qpu_scores[qpu]['Combined']['runtime']+qpu_scores[qpu]['Combined']['scalability'])*(qpu_scores[qpu]['Combined']['accuracy']+qpu_scores[qpu]['Combined']['capacity'])
        ax1.plot(label_loc, [qpu_scores[qpu]['Combined']['runtime'], qpu_scores[qpu]['Combined']['accuracy'], qpu_scores[qpu]['Combined']['scalability'], qpu_scores[qpu]['Combined']['capacity'], qpu_scores[qpu]['Combined']['runtime']], label=str(qpu_names[int(qpu, base=16)]))
        ax1.fill(label_loc, [qpu_scores[qpu]['Combined']['runtime'], qpu_scores[qpu]['Combined']['accuracy'], qpu_scores[qpu]['Combined']['scalability'], qpu_scores[qpu]['Combined']['capacity'], qpu_scores[qpu]['Combined']['runtime']], alpha=0.25)
        if not reduce:
            ax2.bar_label(ax2.bar(i, total_score, 0.35), fmt='%.1f',size= 8)
        if total_score > max_score:
            max_score = total_score
        subs = [qpu_scores[qpu]['Combined']['runtime'], qpu_scores[qpu]['Combined']['scalability'], qpu_scores[qpu]['Combined']['accuracy'], qpu_scores[qpu]['Combined']['capacity']]    
      
        if max(subs) > max_subscore:
            max_subscore = max(subs)
    
    #Set figure titles and legends
    fig.suptitle('QPack benchmark results', size=13)
    ax1.set_thetagrids(np.degrees(label_loc), labels=categories)
    ax1.tick_params(axis='y', labelsize=8)
    #ax1.legend(prop={'size': 10}, loc='upper right',  bbox_to_anchor=(1.3, 1.07))
    ax1.legend(prop={'size': 9}, loc='lower left',  bbox_to_anchor=(0.6, 0.8))
    ax1.set_ylim(0, 1.1*max_subscore)
    
    
    if not reduce:
        ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax2.set_xticklabels([' ', *[qpu_names[int(qpu, base=16)] for qpu in qpus]], rotation=45, ha="right", rotation_mode="anchor")
        ax2.set_ylabel('Benchmark Score')
        ax2.set_xlabel('Quantum backend')
        ax2.set_ylim([0, 1.25*max_score])
    
    #Save figure
    #fig.tight_layout()    
    if subdir:
        plt.savefig(f'plots/{subdir}/benchmark_result.pdf', bbox_inches='tight')
    else:
        plt.savefig('plots/benchmark_result.pdf', bbox_inches='tight')

    plt.close()     

def main():
    '''
    @brief Main function of QPack data processing
    '''
    #Set path benchmark data
    #data_dir = "/home/huub/qpack/qpack_output/mock_data"
    data_dir = "/home/huub/qpack/qpack_output/benchmark_new" #str(Path.home())+"/qpack/qpack_output/benchmark_new"
        
    #Select subset of qpus
    #qpus = []
    qpus = [
            ['3010008', '7000000', '6010013', '9000001'],                        #local ideal simulators
            ['300001B', '3000010', '3000013', '3000017', '3000020', '300002E', '6000004'],  #local noisy simulators ['300001B', '3000010', '3000013', '3000017', '3000020', '300002E', '6000004']
            ['10000001', '4010001', '6010030'],                                  #remote ideal simulators
            #['402001B', '402002E', '4020013', '4020010'] #remote hardware 
            ['402001B','4020017', '4020020', '6020004']   #remote hardware 
            #['402001B', '402002E', '4020013', '4020010','4020017', '4020020', '6020004']   #remote hardware 
            ]
    #qpus = [['BBA', 'BBB', 'BBC'], ['AAA', 'BBA', 'CCA', 'DDA']]
    
    #Process benchmark data
    #sub_dirs = ['synthetic_const', 'synthetic_comb'] 
    sub_dirs = ['loc_sims', 'loc_sims_noisy', 'rem_sims', 'rem_qpu']
    limits = [30, 17, 30, 20]
    QPack_data = loadBenchmarkData(data_dir, max_size=5)#, #subset=['MCP', 'RH', 'IC'])

    selected = ['loc_sims', 'loc_sims_noisy', 'rem_sims', 'rem_qpu']

    for i, sub_dir in enumerate(sub_dirs): 
        if sub_dir not in selected:
            continue

        makedirs(f"plots/{sub_dir}",  exist_ok = True)       
        plotData(QPack_data, qpus=qpus[i], reduce=True, subdir=sub_dir)
        scores = computeScores(QPack_data, qpus=qpus[i])
        plotQPUScores(scores, subdir=sub_dir, xlimit=limits[i])
        plotRadar(scores, qpus=qpus[i], reduce=False, subdir=sub_dir)

    return 0


if __name__ == "__main__":
    main()