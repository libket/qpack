import json
from random import random
from statistics import mean
from os import makedirs

#Custom JSON encoder, lists have no line breaks
class MyJSONEncoder(json.JSONEncoder):

  def iterencode(self, o, _one_shot=False):
    list_lvl = 0
    for s in super(MyJSONEncoder, self).iterencode(o, _one_shot=_one_shot):
      if s.startswith('['):
        list_lvl += 1
        s = s.replace('\n', '').rstrip().replace(" ", "")
      elif 0 < list_lvl:
        s = s.replace('\n', '').rstrip().replace(" ", "")
        if len(s) > 1:
            s = s[0] + " " + s[1:]
        if s and s[-1] == ',':
          s = s[:-1] + self.item_separator
        elif s and s[-1] == ':':
          s = s[:-1] + self.key_separator
      if s.endswith(']'):
        list_lvl -= 1
      yield s
      

data_dir = "/home/huub/qpack/qpack_output/mock_data"

MCP_scores = [0, 0, 1, 2, 4, 6, 8, 10, 12, 12, 14, 16, 18, 18, 20, 22, 24, 24, 26, 28, 30, 30, 32, 34, 36, 36, 38, 40, 42, 42, 44, 46, 48, 48, 50, 52, 54, 54, 56, 58, 60, 60, 62, 64, 66, 66, 68, 70, 72, 72]
mcp_depth = [0, 0, 14, 32, 41] + [95+18*i for i in range(20)]

file = open("QuEST_Scores.json")
QuEST_data = json.load(file)['MCP']

noise = False
start = 2
end = 15

#Constant function 1     
for size in range(start, end):
    data = {}
    
    iters = 50 + 5*size
    q_job_times = [(20 + noise*random()-0.5)*1 for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]
    
    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/AAA/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/AAA/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)

#Constant function 2
for size in range(start, end):
    data = {}

    iters = 50 + 10*size
    q_job_times = [(20 + noise*random()-0.5)*10 for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random() + 1
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/AAB/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/AAB/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)	


#Constantfunction 3
for size in range(start, end):
    data = {}
    
    iters = 50 + 15*size
    q_job_times = [(20 + noise*random()-0.5)*100 for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)]*(1-0.4*(size/end)) + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/AAC/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/AAC/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)

#Linear function 1     
for size in range(start, end):
    data = {}
    
    iters = 50 + 5*size
    q_job_times = [(10 + noise*random()-0.5)*size for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/BBA/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/BBA/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)

#linear function 2
for size in range(start, end):
    data = {}
    
    iters = 50 + 10*size
    q_job_times = [(10 + noise*random()-0.5)*10*size for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random() + 1
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/BBB/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/BBB/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)	


#linear function 3
for size in range(start, end):
    data = {}
    
    iters = 50 + 15*size
    q_job_times = [(10 + noise*random()-0.5)*100*size for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)]*(1-0.4*(size/end)) + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/BBC/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/BBC/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)

#quadratic function 1
for size in range(start, end):
    data = {}
    
    iters = 50 + 5*size
    q_job_times = [(10 + noise*random()-0.5)*(size**1.5) for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/CCA/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/CCA/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)

#quadratric function 2
for size in range(start, end):
    data = {}
    
    iters = 50 + 10*size
    q_job_times = [(10 + noise*random()-0.5)*(size**2) for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random() + 1
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/CCB/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/CCB/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)	

#quadratic function 3
for size in range(start, end):
    data = {}
    
    iters = 50 + 15*size
    q_job_times = [(10 + noise*random()-0.5)*(size**3) for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)]*(1-0.4*(size/end)) + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/CCC/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/CCC/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)


#exponential function 1
for size in range(start, end):
    data = {}
    
    iters = 50 + 5*size
    q_job_times = [(10 + noise*random()-0.5)*(1.5**size) for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/DDA/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/DDA/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)

#exponential function 2
for size in range(start, end):
    data = {}
    
    iters = 50 + 10*size
    q_job_times = [(10 + noise*random()-0.5)*(2**size) for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)] + noise*random() + 1
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/DDB/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/DDB/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)

#exponential function 3
for size in range(start, end):
    data = {}
    
    iters = 50 + 15*size
    q_job_times = [(10 + noise*random()-0.5)*(3**size) for i in range(iters)]
    c_job_times = [q_time + 100 for q_time in q_job_times]

    data['Average job duration [ms]'] = mean(q_job_times)
    data['Depth'] = mcp_depth[size]
    data['P'] = 3
    data['Shots'] = 4096
    data['QJob durations [ms]'] = q_job_times
    data['Circuit execution durations [ms]'] = [q_time*0.1 for q_time in q_job_times]
    data['Optimal Expectation Value'] = -MCP_scores[size]
    data['Optimizer params'] = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    data['Optimizer durations [ms]'] = c_job_times
    data['Optimizer iterations'] = iters
    data['Qubits'] = size
    data['Expectation Value'] = QuEST_data['exp val'][QuEST_data['sizes'].index(size)]*(1-0.4*(size/end)) + noise*random()
    data['Total Algorithm duration [s]'] = sum(c_job_times) + sum(q_job_times)
    data['Total Classic duration [s]'] = sum(c_job_times)
    data['Total Quantum duration [s]'] = sum(q_job_times)
       
    #create directories if non-existant   
    makedirs(f'{data_dir}/DDC/MCP/N{size:02d}', exist_ok = True)   
    
    with open(f'{data_dir}/DDC/MCP/N{size:02d}/dataset00.json', 'w') as outfile:
        json.dump(data, outfile, indent=2, cls=MyJSONEncoder)
