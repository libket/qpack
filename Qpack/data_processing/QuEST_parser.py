import json
import matplotlib.pyplot as plt
from statistics import mean
from os import listdir, remove

#Custom JSON encoder, lists have no line breaks
class MyJSONEncoder(json.JSONEncoder):

  def iterencode(self, o, _one_shot=False):
    list_lvl = 0
    for s in super(MyJSONEncoder, self).iterencode(o, _one_shot=_one_shot):
      if s.startswith('['):
        list_lvl += 1
        s = s.replace('\n', '').rstrip().replace(" ", "")
      elif 0 < list_lvl:
        s = s.replace('\n', '').rstrip().replace(" ", "")
        if len(s) > 1:
            s = s[0] + " " + s[1:]
        if s and s[-1] == ',':
          s = s[:-1] + self.item_separator
        elif s and s[-1] == ':':
          s = s[:-1] + self.key_separator
      if s.endswith(']'):
        list_lvl -= 1
      yield s

#Find available qpu's and problems
data_dir = "/home/huub/qpack/qpack_output/baseline_new"

#Data file
QuEST_dict = {}
qpu = '7000000'
        
for problem in listdir(f'{data_dir}/{qpu}'):
    QuEST_dict[problem] = {}
    QuEST_dict[problem]['sizes'] = []
    QuEST_dict[problem]['exp val'] = []
    QuEST_dict[problem]['opt exp val'] = []
    
    for size in listdir(f'{data_dir}/{qpu}/{problem}'):
        QuEST_dict[problem]['sizes'].append(int(size[1:]))

        exp_val = float('inf')
        for rep in listdir(f'{data_dir}/{qpu}/{problem}/{size}'):
            file = open(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')
            data = json.load(file)
            if data['Expectation Value'] is None:
                print(f'Null... Removing {data_dir}/{qpu}/{problem}/{size}/{rep}')
                remove(f'{data_dir}/{qpu}/{problem}/{size}/{rep}')
                continue

            if data['Expectation Value'] < exp_val:
               exp_val = data['Expectation Value']

        QuEST_dict[problem]['exp val'].append(exp_val)      
        QuEST_dict[problem]['opt exp val'].append(data['Optimal Expectation Value'])  
        
#Store new result file
json.dump(QuEST_dict, open("QuEST_Scores.json", "w"), indent=2, cls=MyJSONEncoder)
                                   
#Plot baseline result
fig, axs = plt.subplots(2, 3)
fig.set_size_inches(8,8)

problems = ['MCP', 'DSP', 'MIS', 'TSP', 'RH', 'IC']

for i, r_ax in enumerate(axs):
    for j, ax in enumerate(r_ax):
        problem = problems[i*3+j]
        ax.set_title(f'{problem} score')
        ax.set_xlabel("Nodes")
        ax.set_ylabel("Score")
        zipped = sorted(zip(QuEST_dict[problem]['sizes'], QuEST_dict[problem]['exp val'], QuEST_dict[problem]['opt exp val']))
        
        ax.plot(list(item[0] for item in zipped), list(item[1] for item in zipped), marker = 'o')
        ax.plot(list(item[0] for item in zipped), list(item[2] for item in zipped), marker = 'o')

#Add legend
fig.suptitle("QuEST Simulator score results")
fig.legend(["VQA", "Optimal"], loc='upper center', bbox_to_anchor=(0.5, 0.01), fancybox=True, shadow=True, ncol=5)

fig.tight_layout()

plt.savefig('plots/QuEST_scores.png', bbox_inches='tight')
