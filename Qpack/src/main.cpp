/** @file Qpack/src/main.cpp

    @brief Qpack main source file

    @author Huub Donkers

    @defgroup qpack libket
 */

//#define OUTPUT_DIR  "/qpack/qpack_output/baseline_new/"
#define OUTPUT_DIR  "/qpack/qpack_output/benchmark_new/"
#define QUEST_ENABLE false

#include <iostream>
#include <LibKet.hpp>
#include "../include/Optimizers.hpp"
#include "../include/QAOA.hpp"
#include "../include/VQE.hpp"
#include "../include/BenchmarkLoops.hpp"

using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;


//Pack benchmark data
struct AlgData{

    static constexpr int reps = 5;      //Set number of repetions for VQA to run
    static constexpr int P = 3;         //Set QAOA iterations
    static constexpr int shots = 4096;  //Set QPU shots
    static constexpr vqa_problems problems[] = {MCP, RH, IC};     //Set problem sets to be evaluated
    static constexpr QDeviceType qpuIDs[] = {QDeviceType::ibmq_qasm_simulator        //Select units under test
                                             //QDeviceType::qiskit_aer_simulator,
                                             //QDeviceType::qi_26_simulator
                                             //QDeviceType::ionq_simulator
                                             //QDeviceType::qiskit_statevector_simulator
                                             //QDeviceType::quest
                                             //QDeviceType::cirq_simulator                                             
                                             //QDeviceType::rigetti_16q_simulator
                                             //QDeviceType::qiskit_nairobi_simulator,
                                             //QDeviceType::ibmq_quito
                                             //QDeviceType::ibmq_manila
                                             //QDeviceType::ibmq_jakarta
                                             //QDeviceType::ibmq_perth
                                             //QDeviceType::ibmq_lagos
                                             //QDeviceType::qiskit_jakarta_simulator,
                                             //QDeviceType::qiskit_perth_simulator,
                                             //QDeviceType::qiskit_lagos_simulator
                                             //QDeviceType::qiskit_nairobi_simulator
                                            };

    static constexpr std::array<int, 3> graph_loop_params(vqa_problems problem){   //Select problem sizes for each problem
        std::array<int, 3> MCPparams = {2, 5, 1}; //Start, End, Step   
        std::array<int, 3> MISparams = {7, 12, 1};  
        std::array<int, 3> DSPparams = {7, 10, 1};  
        std::array<int, 3> TSPparams = {3, 4, 1}; 
        std::array<int, 3> RHparams  = {1, 5, 1};  
        std::array<int, 3> ICparams  = {10, 10, 1};  
        std::array<int, 3> ones = {1, 1, 1}; 
        return problem == MCP ? MCPparams :
               problem == MIS ? MISparams :
               problem == DSP ? DSPparams :
               problem == TSP ? TSPparams : 
               problem == RH ? RHparams :
               problem == IC ? ICparams : ones; 
        };                                    
} algData;


int main(int argc, char *argv[]) {    
    
    
    //Determine problem sizes
    constexpr auto problemSize = sizeof(AlgData::problems)/sizeof(vqa_problems);
    
    //Run Qpack for selected QPUs
    int status = utils::static_for<0, problemSize-1, 1, benchmark_loops::problem_loop>(0, algData);

    //Finishing message
    std::cout << "=============================\n"
              << "  ┌───┐     ┌──────┐         \n"
              << " ─┤ Q ├──■──┤ Pack ├──────── \n"
              << "  └───┘  │  └──────┘         \n" 
              << "  ┌──────┴────────────────┐  \n"
              << " ─┤Benchmarking Completed!├─ \n" 
              << "  └───────────────────────┘  \n" 
              << "=============================\n"
              << std::flush;    
    
    /*
    // QAOA Debugging
    constexpr int graphSize = 3;
    constexpr int P = 3;
    constexpr int shots = 4096;       
    constexpr vqa_problems qaoa_problem = MCP;
    constexpr QDeviceType qpuID = QDeviceType::qiskit_nairobi_simulator;

    QAOA<graphSize, qaoa_problem, P, shots, qpuID> qaoa;
    Optimizer<QAOA<graphSize, qaoa_problem, P, shots, qpuID>> optimizer(&qaoa);
    
    auto graph = utils::make_regular_graph<graphSize>();
    //arma::vec opt_params = optimizer.COBYLA();
    arma::vec opt_params = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
    
    qaoa.run(opt_params, false);

    std::cout << "Circuit execution time: " << qaoa.getExecDuration() << std::endl;
    std::cout << "QJob execution time: " << qaoa.getJobDuration() << std::endl;
    std::cout << "Queue time: " << qaoa.getQueueDuration() << std::endl;
    std::cout << "Expectation value: " << qaoa.getExpectation()      << std::endl;
    //std::cout << "Circuit Depth: " << qaoa.getCircuitDepth()       << std::endl;

    //std::cout << qaoa.getHist() << std::endl;
    */
    /*
    std::string output_file = std::getenv("HOME");
    std::ofstream output(output_file + "/qpack/Qpack/data_processing/hist.py");
    output << "counts = [" << qaoa.getHist() << "]\n"
           << "P = " << P << "\n"
           << "problem = '" << vqa_names[qaoa_problem] << "'\n"
           << "shots = " << shots << std::endl;
    */

    /*
    // VQE Debugging
    constexpr int shots = 4096;       
    constexpr vqa_problems vqe_problem = IC;
    constexpr int problem_size = 3;
    constexpr QDeviceType qpuID = QDeviceType::ibmq_jakarta_simulator; 

    VQE<vqe_problem, problem_size, shots, qpuID> vqe;
    
    Optimizer<VQE<vqe_problem, problem_size, shots, qpuID>> optimizer(&vqe);
    //arma::vec opt_params = optimizer.COBYLA();
    arma::vec opt_params = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2};

    //std::cout << "Classical eigenvalue: \n" << vqe.classicalExpectation() << std::endl;
    vqe.run(opt_params, true);

    std::cout << "Duration: " << vqe.getJobDuration()       << std::endl;
    std::cout << "Exact: " << vqe.classicalExpectation()    << std::endl; 
    std::cout << "Expectation: " << vqe.getExpectation()    << std::endl;
    std::cout << "Circuit Depth: " << vqe.getCircuitDepth() << std::endl;
    */

    /*
    std::string output_file = std::getenv("HOME");
    std::ifstream myfile(output_file + "/LibKet/Qpack/data_processing/vqe.json");
    std::string line;
    std::string json_str = "";
    if (myfile.is_open()){
        while ( std::getline (myfile, line) ){
          json_str += line;
          json_str += "\n";
        }
        myfile.close();
    }

      else std::cout << "Unable to open file"; 
    
    auto data = nlohmann::json::parse(json_str);

    data[vqa_names[vqe_problem]][std::to_string(problem_size)]["Exp"] = vqe.getExpectation();
    data[vqa_names[vqe_problem]][std::to_string(problem_size)]["Exact"] = vqe.classicalExpectation();

    //Save result in file
    std::ofstream output(output_file + "/LibKet/Qpack/data_processing/vqe.json");
    output << std::setw(2) << data << std::endl;
    */  

    return 0; 
}
