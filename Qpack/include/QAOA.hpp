/** @file Qpack/include/CircuitsQAOA.hpp

    @brief Qpack networks class header file

    @author Huub Donkers

    @defgroup qpack libket
 */

#pragma once
#ifndef QAOA_HPP
#define QAOA_HPP

#include<cmath>
#include <LibKet.hpp>
#include "../include/QAOA_Loops.hpp"
#include "../include/VQA.hpp"

using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

/*
@brief: This class implements all necessary functionalities of the QAOA algorithm, such
        as determining the amount of qubits and statespace for a given problem. The class
        is able to generate QAOA circuits, execute these circuits and give an expectation
        value for each problem.
*/
template <int graphSize, vqa_problems problem, int P, int shots, QDeviceType qpuID>
class QAOA : public VQA{
public:

  //Return QPU ID
  static QDeviceType getQpuID(){
    return qpuID;
  }

  //Return problem
  static vqa_problems getProblem(){
    return problem;
  }

  //Return number of shots
  static int getNumShots(){
    return shots;
  }

  //Return P for QAOA
  static int getP(){
    return P;
  }

  //Return problem size
  static int getSize(){
    return graphSize;
  }


  //Returns the number of qubits based on selected problem
  static constexpr auto getNumQubits(){
  return problem == MCP ? graphSize   : 
         problem == DSP ? (graphSize == 2 ? 4 :
                           graphSize == 3 ? 6 :
                           graphSize == 4 ? 7 :
                           graphSize+5
                          ) : 
         problem == TSP ? graphSize*graphSize :
         problem == MIS ? (graphSize == 3 ? 4 :
                           graphSize == 4 ? 5 :
                           graphSize+3
                          ) :  0;
  }

private:

  //Returns the size of the histogram based on selected problem
  static constexpr auto getHistSize(){
  return problem == MCP ? 2 << (graphSize-1) : 
         problem == DSP ? 2 << (graphSize-1) : 
         problem == TSP ? 2 << (graphSize*graphSize-1) :
         problem == MIS ? 2 << (graphSize-1) : 1;
  }

  //Variable to store qpu result
  QArray<getHistSize(), unsigned long> _hist; 
  
  //Function to generate maxcut QAOA circuit. Returns quantum expression
  template <typename graphType, typename paramType>
  auto genCircuitMCP(graphType graph, paramType params){

    //Set inital state to superposition
    auto s0 = h(range<0,graphSize-1>(init()));

    //QAOA iterations
    auto step = utils::static_for<0, P-1, 1, QAOAGates::maxcut_step>(tag<0>(s0), graph, params);

    return step;
  }

  //Function to generate DSP QAOA circuit. Returns quantum expression
  template <typename graphType, typename paramType>
  auto genCircuitDSP(graphType graph, paramType params){

    //Set inital state to superposition
    auto s0 = all(h(range<0,graphSize-1>(init())));

    //QAOA iterations
    auto step = utils::static_for<0, P-1, 1, QAOAGates::dsp_step>(tag<1>(s0), graph, params);

    return step;
  }

  //Static for loop over graph edges for DMatrix construction.
  template<index_t start, index_t end, index_t step, index_t index>
  struct D_Matrix_Loop
  {  
    template<typename DMatrix, typename Graph>
    inline constexpr auto operator()(DMatrix&& dmatrix, Graph) noexcept
    { 
      //Get link
      int i_index = Graph::template from<index>();
      int j_index = Graph::template to<index>();

      //Copy matrix information
      std::array<std::array<int, graphSize>, graphSize> new_matrix = {0}; 
      for(int i = 0; i < graphSize; i++){
       for(int j = 0; j < graphSize; j++){
        new_matrix[i][j] = dmatrix[i][j];   
        }
      }

      //Set distance in matrix for connected links
      new_matrix[i_index][j_index] = 1;
      new_matrix[j_index][i_index] = 1;

      return new_matrix;         
    }
  };

  //Function to generate distance matrix for TSP. Returns 2D array
  template <typename graphType>
  auto genDMatrix(graphType graph){
    std::array<std::array<int, graphSize>, graphSize> init_matrix;
    for(int i = 0; i < graphSize; i++){
      for(int j = 0; j < graphSize; j++){
        if(i == j){
          init_matrix[i][j] = 20;    //Diagonal elements have higher penalty
        }else{
          init_matrix[i][j] = 10;    //All items have default penalty
        }
        
      }
    }        
    auto matrix = utils::static_for<0, graph.size()-1, 1, D_Matrix_Loop>(init_matrix, graph);

    return matrix;
  }

  //Function to generate TSP QAOA circuit. Returns quantum expression
  template <typename graphType, typename paramType>
  auto genCircuitTSP(graphType graph, paramType params){

    //Set inital state to superposition
    auto s0 = utils::static_for<0, graphSize*graphSize-1, graphSize, QAOAGates::tsp_init>(tag<2>(init()));

    //QAOA iterations
    auto fullGraph = utils::make_full_graph<graphSize>();
    std::array<std::array<int, graphSize>, graphSize> D_matrix = genDMatrix(graph);

    /*
    for(int i = 0; i < graphSize; i++){
      for(int j = 0; j < graphSize; j++){
        std::cout << D_matrix[i][j] << " ";
        }
        std::cout << std::endl;
      }
    */
    auto step = utils::static_for<0, P-1, 1, QAOAGates::tsp_step>(tag<2>(s0), graph, fullGraph, D_matrix, params);

    return step;
  }

  //Function to generate MIS QAOA circuit. Returns quantum expression
  template <typename graphType, typename paramType>
  auto genCircuitMIS(graphType graph, paramType params){

    //Set inital state to all zeros
    auto s0 = init();

    //QAOA iterations
    auto step = utils::static_for<0, P-1, 1, QAOAGates::mis_step>(tag<3>(s0), graph,  params);

    return step;
  }

  //Loop to determine number of shared edges
  template<index_t start, index_t end, index_t step, index_t index>
  struct shared_edge_loop
  {  
    template<typename Shared, typename Graph, typename BitString>
    inline constexpr auto operator()(Shared&& shared, Graph, BitString bitString) noexcept
    {
      if(bitString[Graph::template from<index>()] != bitString[Graph::template to<index>()]){
        shared -= 1;
      }

      return shared;         
    }
  };

  //Computes expectation value for MCP problem
  template <typename graphType, typename histType>
  float expectationMCP(graphType graph, histType hist) {

    static constexpr const std::size_t numEdges = graph.size();
    static constexpr const std::size_t numStates = hist.size();
    static constexpr const std::size_t numBits = graphSize;

    int average = 0;
    int total_count = 0;
    //Loop over all output states
    for(int i = 0; i < numStates; i++){
      
      //Convert state number to bitstring
      std::string bitString = std::bitset<numBits>(i).to_string();

      //Loop over all egdes
      int shared = 0;
      int shared_edges = utils::static_for<0, numEdges-1, 1, shared_edge_loop>(shared, graph, bitString);

      //Sum shared edges with histogram weight
      average += shared_edges * hist[i];
      total_count += hist[i];
    }

    //Return average result
    return (float)average / (float)total_count;
  }

  //Static for loop for expectation function
  template<index_t start, index_t end, index_t step, index_t index>
  struct T_loop
  {  
    template<typename TBits, typename Graph, typename Bits>
    inline constexpr auto operator()(TBits&& tBits, Graph, Bits bits) noexcept
    {
      int e0 = Graph::template from<index>();
      int e1 = Graph::template to<index>();
      int end_index = graphSize-1;

      //index reverse because of bitsting indexing
      if(bits[end_index - e0] == 1 or bits[end_index - e1] == 1){
        tBits[end_index-e0] = 1;
        tBits[end_index-e1] = 1;
      }

      return tBits;         
    }
  };

  //Computes expectation value for DSP problem
  template <typename graphType, typename histType>
  float expectationDSP(graphType graph, histType hist) {

    static constexpr const std::size_t numEdges = graph.size();
    static constexpr const std::size_t numStates = hist.size();

    int average = 0;
    int total_count = 0;
    //Loop over all output states
    for(int i = 0; i < numStates; i++){
       
      //Convert state number to bitstring
      std::bitset<graphSize> bits(i);
      
      //Cost of this state
      int cost = 0;
      std::bitset<graphSize> tBits(0);
      tBits = utils::static_for<0, numEdges-1, 1, T_loop>(tBits, graph, bits);

      //Check if solution monitors all nodes
      bool valid = true;
      for(int j = 0; j < graphSize; j++){   
        if(!tBits[j]){
          valid = false;
        }
      }

      //If solution is valid, compute solution cost
      if(valid){
        for(int j = 0; j < graphSize; j++){         
          cost -= !bits[j];  //Subtract to get best score be most negative
        }
      }

      //Debug message
      //std::cout << "State:" << bits << ": T=" << tBits << ", Score: " << cost << std::endl;

      //Sum cost with histogram weight
      average += cost * hist[i];
      total_count += hist[i];
    }

    //Return average result
    return (float)average / (float)total_count;
  }

  //Computes expectation value for TSP problem
  template <typename graphType, typename histType>
  float expectationTSP(graphType graph, histType hist) {

    int average = 0;
    int total_count = 0;
    static constexpr const std::size_t numStates = hist.size();
    std::array<std::array<int, graphSize>, graphSize> D_matrix = genDMatrix(graph);

    for(int state = 0; state < numStates; state++){

      //Create adjacency matrix from bitstring
      std::bitset<graphSize*graphSize> bits(state);
      auto A_matrix = [] (std::bitset<graphSize*graphSize> bits) {
        std::array<std::array<int, graphSize>, graphSize> matrix {0};
        for(int bit = 0; bit < graphSize*graphSize ; bit++){
          matrix[bit/graphSize][bit%graphSize] = bits[graphSize*graphSize-bit-1];
        }
        return matrix;
      }(bits);  

      /*
      //Only consider valid solutions to the travelling salesman problem
      bool valid = true;
      for (int i = 0; i < graphSize; i++){
        int count = 0;
        for (int j = 0; j < graphSize; ++j){
             count += A_matrix[i][j];
             if(i==j and A_matrix[i][j] != 0){valid = false;} //Check for zero diagonal
             if(A_matrix[i][j] != A_matrix[j][i]){valid = false;}  //Check for symmetry
        }
        if(count != 2){valid = false;}
      }
      */

      //Compute score for valid adjacency matrices
      float score = 0.0;
      //if(valid){
        for (int i = 0; i < graphSize; i++){
          for (int j = 0; j < graphSize; j++){
            if(A_matrix[i][j] == 1){
              //score -= 1.0/(float)D_matrix[i][j];
              score += D_matrix[i][j];
            }
            if((A_matrix[i][j]*A_matrix[j][i] == 1) && (i != j)){
              score += -5.0;
            }
          }
        }  
        score /= 2.0;                  
      //}

      //Debug message
        /*
      if(score < 0){
      for(int i = 0; i < graphSize; i++){
        for(int j = 0; j < graphSize; j++){
          std::cout << A_matrix[i][j] << " ";
          }
        std::cout << "   ";  
        for(int j = 0; j < graphSize; j++){
          std::cout << D_matrix[i][j] << " ";
        }
            
          std::cout << std::endl;
        }
        std::cout << "State:" << bits << ", Score: " << score << std::endl;
      }
      */

      //Sum cost with histogram weight
      average += score * hist[state];
      total_count += hist[state];     
    }

    //Return average result
    return (float)average / (float)total_count;
  }

  //Static for loop for expectation function
  template<index_t start, index_t end, index_t step, index_t index>
  struct check_IS
  {  
    template<typename Valid, typename Graph, typename Bits>
    inline constexpr auto operator()(Valid&& valid, Graph, Bits bits) noexcept
    {
      int e0 = Graph::template from<index>();
      int e1 = Graph::template to<index>();
      int end_index = graphSize-1;

      //index reverse because of bitsting indexing
      if(bits[end_index - e0] == 1 and bits[end_index - e1] == 1){
        valid = false;
      }

      return valid;         
    }
  };

  //Computes expectation value for MIS problem
  template <typename graphType, typename histType>
  float expectationMIS(graphType graph, histType hist) {

    static constexpr const std::size_t numEdges = graph.size();
    static constexpr const std::size_t numStates = hist.size();

    int average = 0;
    int total_count = 0;
    //Loop over all output states
    for(int i = 0; i < numStates; i++){
       
      //Convert state number to bitstring
      std::bitset<graphSize> bits(i);
      
      //Cost of this state
      int cost = 0;

      //Check if bitstring is an independent set
      bool valid = utils::static_for<0, numEdges-1, 1, check_IS>(true, graph, bits);

      //If solution is valid, compute solution cost
      if(valid){
        for(int j = 0; j < graphSize; j++){         
          cost -= bits[j];  //Subtract to get best score be most negative
        }
      }

      //Debug message
      //std::cout << "State:" << bits << ": valid=" << valid << ", Score: " << cost << std::endl;

      //Sum cost with histogram weight
      average += cost * hist[i];
      total_count += hist[i];
    }

    //Return average result
    return (float)average / (float)total_count;
  }

  //Functions to select which QAOA circuit to generate based on problem
  template<typename graphType, typename paramType>
  auto genCircuit(std::integral_constant<vqa_problems, MCP>, graphType graph, paramType params){
    return genCircuitMCP(graph, params);
  }

  template<typename graphType, typename paramType>
  auto genCircuit(std::integral_constant<vqa_problems, DSP>, graphType graph, paramType params){
    return genCircuitDSP(graph, params);
  }

  template<typename graphType, typename paramType>
  auto genCircuit(std::integral_constant<vqa_problems, TSP>, graphType graph, paramType params){
    return genCircuitTSP(graph, params);
  }

  template<typename graphType, typename paramType>
  auto genCircuit(std::integral_constant<vqa_problems, MIS>, graphType graph, paramType params){
    return genCircuitMIS(graph, params);
  }
  
  //Functions to select which expectation function to use based on problem
  template<typename graphType>
  auto expectation(std::integral_constant<vqa_problems, MCP>, graphType graph){
    return expectationMCP(graph, _hist);
  }

  template<typename graphType>
  auto expectation(std::integral_constant<vqa_problems, DSP>, graphType graph){
    return expectationDSP(graph, _hist);
  }

  template<typename graphType>
  auto expectation(std::integral_constant<vqa_problems, TSP>, graphType graph){
    return expectationTSP(graph, _hist);
  }

  //VQE problems TODO: FIND GENERAL DEFAULT
  template<typename graphType, typename paramType>
  auto genCircuit(std::integral_constant<vqa_problems, RH>, graphType graph, paramType params){
    return init();
  }

  template<typename graphType, typename paramType>
  auto genCircuit(std::integral_constant<vqa_problems, IC>, graphType graph, paramType params){
    return init();
  }

  template<typename graphType>
  auto expectation(std::integral_constant<vqa_problems, MIS>, graphType graph){
    return expectationMIS(graph, _hist);
  }

  template<typename graphType>
  auto expectation(std::integral_constant<vqa_problems, RH>, graphType graph){
    return 0.0;
  }

  template<typename graphType>
  auto expectation(std::integral_constant<vqa_problems, IC>, graphType graph){
    return 0.0;
  }

public:
  //Information constants
  const int id = 0; //QAOA
  const std::string name = "QAOA";

  //Constructor
  QAOA() {}

  //Run QAOA circuit
  template <typename paramType>
  void run(paramType params, bool printCircuit) {

    //Create Graph
    auto graph = utils::make_regular_graph<graphSize>();
    
    //Generate quantum circuit
    auto expr = genCircuit(std::integral_constant<vqa_problems, problem>{}, graph, params);
   
    //Execute circuit on quantum backend
    QDevice<qpuID, getNumQubits()> qpu;
    
    if(qpuID != QDeviceType::quest and
       qpuID != QDeviceType::qx){

      //Retrieve results
      qpu(measure(range<0, problem == TSP ? graphSize*graphSize -1 : graphSize-1>(expr)));
      auto job = qpu.execute(shots);
      utils::json result = job->get();

      //std::cout << result << std::endl;
      //std::cout << qpu.print_circuit() << std::endl;

      //Store results    
      _execDuration = qpu.template get<QResultType::duration>(result).count();  
      _jobDuration = qpu.template get<QResultType::jobDuration>(result).count();  
      _queueDuration = qpu.template get<QResultType::queueDuration>(result).count(); 
      auto full_hist = qpu.template get<QResultType::histogram>(result);
      for(int i=0; i < getHistSize(); i++){ //Store only qubit measurents in histogram
        _hist[i] = full_hist[i];
      }

    } else{
      
      qpu(expr);
      auto job = qpu.execute(shots);

    #if QUEST_ENABLE
      _execDuration = qpu.duration_s(); 
      _jobDuration = qpu.duration_s();    
      _queueDuration = 0.0;
      auto probs =  qpu.probabilities();
    
      
      for(int i=0; i < getHistSize(); i++){
        _hist[i] = 0;
      }

      for(int i=0; i < (1 << getNumQubits()); i++){

        _hist[i % getHistSize()] += shots*probs[i];
      }
    #else
      std::cout << "\nPlease enable QuEST macro. Exiting..." << std::endl; 
      std::exit(0);
    #endif

    }      
      //Print circuit if enabled (with Qiskit)
      if(printCircuit){
        QDevice<QDeviceType::qiskit_aer_simulator, getNumQubits()> qiskit;
        qiskit(expr);
        std::cout << qiskit.print_circuit() << std::endl;
      }

    _expectation = expectation(std::integral_constant<vqa_problems, problem>{}, graph);
  }

  //Return histogram results 
  auto getHist(){
    return _hist;
  }

  //Returns number of parameters needed for optimization
  int getNumParams(){
    return 2*P;
  }

  //Returns optimal score
  int getOptimalScore(){
    int MCP_scores[] = {0, 0, 1, 2, 4, 6, 8, 10, 12, 12, 14, 16, 18, 18, 20, 22, 24, 24, 26, 28, 30, 30, 32, 34, 36, 36, 38, 40, 42, 42, 44, 46, 48, 48, 50, 52, 54, 54, 56, 58, 60, 60, 62, 64, 66, 66, 68, 70, 72, 72};
    return problem == MCP ? -MCP_scores[graphSize] : 
           problem == DSP ? -(graphSize-((graphSize-1)/5 + 1)) : 
           problem == TSP ? -graphSize :
           problem == MIS ? -graphSize/3 :  0;
  }

  //Return circuit depth
  double getCircuitDepth(){
  
    //Get QPU
    QDevice<QDeviceType::qiskit_aer_simulator, getNumQubits()> qiskit;

    //Create Graph
    auto graph = utils::make_regular_graph<graphSize>();
    
    //Generate quantum circuit
    arma::vec params = arma::ones<arma::vec>(2*P);
    auto expr = genCircuit(std::integral_constant<vqa_problems, problem>{}, graph, params);

    //Load expression into qiskit backend
    qiskit(expr);

    return std::stoi(qiskit.circuit_depth()) + 1;
  }

};

#endif //QAOA_HPP