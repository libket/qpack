/** @file Qpack/include/Optimizers.hpp

	@brief Qpack Optimizers header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef OPTIMIZER_HPP
#define OPTIMIZER_HPP

#define OPTIM_ENABLE_ARMA_WRAPPERS
#include "../include/QAOA.hpp"
#include "../include/VQE.hpp"
#include <math.h>
#include <iomanip>
#include <iostream>
#include <vector>
#include <chrono>
#include <nlopt.hpp>

//Optimizer class which runs a VQA circuit and uses an optimizer to find the minimum score
template<typename vqaType>
class Optimizer{
private:
	double _minf;
	std::vector<double> _opt_params;
	double _totalDuration; //Total optimizer duration in seconds

	struct OptData{ //VQA
		vqaType* _algorithm_ptr;
		std::vector<double> _execDurations;
		std::vector<double> _queueDurations;
		std::vector<double> _jobDurations;
		std::vector<double> _optDurations;
		std::chrono::steady_clock::time_point _itTime;
	} optData;

//NLOpt optimization function
static double vqa_opt(const std::vector<double> &params, std::vector<double> &grad, void *func_data)
{
    if (!grad.empty()) {
        grad[0] = 0.0;
        grad[1] = 0.5 / sqrt(params[1]);
    }

	//Recast data
	OptData* optData = (OptData*) func_data;
	auto vqa = optData->_algorithm_ptr;
	
	//Execute quantum circuit
	vqa->run(params, false);

	//Retrieve expectation value
	double score = vqa->getExpectation();

	//Append Q-job time to data struct
	optData->_execDurations.push_back(vqa->getExecDuration()*1000); //Convert s to ms
	optData->_jobDurations.push_back(vqa->getJobDuration()*1000);   //Convert s to ms
	optData->_queueDurations.push_back(vqa->getQueueDuration()*1000);   //Convert s to ms

	//Append iteration ducatrion to data struct
	optData->_optDurations.push_back((double)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() -optData->_itTime).count()/1000); // us to ms
	optData->_itTime = std::chrono::steady_clock::now(); //Reset startime

	//std::cout << "Iteration score: " << score << std::endl;
	//std::cout << "Parameters: " << params[0] << " " << params[1] << std::endl;

	/*
	//Execution message
    std::string message = "\nIteration score: " + std::to_string(score);  	
    std::cout << message << std::flush;
    for(int i = 0; i < message.size(); i++){
    	std::cout << "\b";
    }
    */

	return score;
}

public:
	//Constructor
	template<typename algType>
	Optimizer(algType algorithm_ptr){
		optData._algorithm_ptr = algorithm_ptr;
	}

	//Reset data
	void resetData(){
		optData._jobDurations.clear();
		optData._optDurations.clear();
		optData._execDurations.clear();
		optData._queueDurations.clear();
		_totalDuration = 0.0;
		_minf = 0.0;
		_opt_params.clear();
	}

	//Return optimizer iterations
	unsigned long getIterations(){
		return optData._jobDurations.size();
	}

	//Return vector of circuit execution durations in ms
	std::vector<double> getQueueDurations(){
		return optData._queueDurations;
	}

	//Return vector of circuit execution durations in ms
	std::vector<double> getExecDurations(){
		return optData._execDurations;
	}

	//Return vector of qjob durations in ms
	std::vector<double> getJobDurations(){
		return optData._jobDurations;
	}

	//Return vector of optimizer durations in ms
	std::vector<double> getOptDurations(){
		return optData._optDurations;
	}

	//Return total job duration in ms
	double getTotalJobDuration(){
		double totalJobDuration = 0.0;
    	for(double i : optData._jobDurations){
            totalJobDuration += i;
    	}
		return totalJobDuration;
	}

	//Return optimal score
	double getOptimalScore(){
		return optData._algorithm_ptr->getOptimalScore();
	}

	//Return average job duration in ms
	double getAverageJobDuration(){
		return getTotalJobDuration()/optData._jobDurations.size();
	}

	//Return number of qubits
	int getNumQBits(){
		return optData._algorithm_ptr->getNumQubits();
	}

	//Return (average) circuit depth
	double getDepth(){
		return optData._algorithm_ptr->getCircuitDepth();
	}


	//Return all data
  	nlohmann::json getData(){
  		char qpu_buffer [16] = {};
	    std::sprintf(qpu_buffer, "%X", static_cast<int>(optData._algorithm_ptr->getQpuID()));

    	nlohmann::json data;
    	data["QPU"] = qpu_buffer;
    	data["Problem"] = vqa_names[optData._algorithm_ptr->getProblem()];
    	data["Size"] = optData._algorithm_ptr->getSize();
    	data["P"] = optData._algorithm_ptr->getP();
    	data["Shots"] = optData._algorithm_ptr->getNumShots();
    	data["Qubits"] = getNumQBits();
    	data["Depth"] = getDepth();
    	data["Optimizer iterations"] = getIterations();
    	data["QJob durations [ms]"] = getJobDurations();
    	data["Queue durations [ms]"] = getQueueDurations();	
    	data["Circuit execution durations [ms]"] = getExecDurations();
    	data["Expectation Value"] = _minf;
    	data["Optimal Expectation Value"] = getOptimalScore();
    	data["Optimizer params"] = _opt_params;
    	data["Optimizer durations [ms]"] = getOptDurations();
    	data["Total Algorithm duration [s]"] = _totalDuration;
    	data["Total Quantum duration [s]"] = getTotalJobDuration()/1000; // ms to s
    	data["Total Classic duration [s]"] = _totalDuration - getTotalJobDuration()/1000; //ms to s

    	return data;    
  	}

	//Run VQA with COBYLA	
	arma::vec COBYLA(){

		//NLopt optimizer initialisation 	
		int numParams;
		std::vector<double> lb;
		std::vector<double> ub;
		std::vector<double> params;
		float tol;
		
		//Fill in parameters based on algorithm	
		switch(optData._algorithm_ptr->id){
			case 0 : //QAOA			
			    numParams = optData._algorithm_ptr->getNumParams();
			    for(int i=0; i < numParams; i++){
			    	lb.push_back(0.0); 		//Set lower bounds to 0.0
			    	ub.push_back(1.0); 		//Set upper bounds to 1.0
			    	float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			    	params.push_back(r); 	//Set random initial parameters
			    	tol = 1e-4; 			//Set tolerance
			    }
				break;

			case 1 : //VQE
				numParams = optData._algorithm_ptr->getNumParams();
			    for(int i=0; i < numParams; i++){
			    	lb.push_back(0.0); 		//Set lower bounds to 0.0
			    	ub.push_back(1.0); 		//Set upper bounds to 1.0
			    	float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			    	params.push_back(r); 	//Set random initial parameters
			    	tol = 1e-3; 			//Set tolerance
			    }
				break;

			default : 
				std::cout << "Algorithm not recognized" << std::endl;
		}	

		//Set parameters
		nlopt::opt opt(nlopt::LN_COBYLA, numParams);
		opt.set_lower_bounds(lb);
		opt.set_upper_bounds(ub);
		opt.set_min_objective(vqa_opt, &optData);	
		opt.set_xtol_rel(tol);
		

		try{
			//Set timer and begin optimization
			std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();		 
			optData._itTime = startTime; 
		    nlopt::result result = opt.optimize(params, _minf);
		    _opt_params = params;
		    _totalDuration = (double)std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startTime).count()/1000; //ms to s
		}
		catch(std::exception &e) {
		    std::cout << "nlopt failed: " << e.what() << std::endl;
		}

	    return params;
	}
};

#endif //OPTIMIZER_HPP
