/** @file Qpack/include/VQE_Loops.hpp

	@brief Qpack VQE_Loops header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef VQE_LOOPS_HPP
#define VQE_LOOPS_HPP

#include <LibKet.hpp>
#include <math.h>

using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

namespace VQE_Loops{

	//Struct to pass constexpr as function parameter
	template<int I>
    struct static_int{
    	static constexpr const int _I = I;
    };

		//VQE Pauli measurement loop
    template<index_t start, index_t end, index_t step, index_t index>
    struct pauli_measurement_gates
    { 
       
		//Functions to select which expectation function to use based on problem
		template<typename Expr>
		auto pauli_measure(std::integral_constant<char, 'I'>, Expr&& expr){
		return all(expr);
		}

		template<typename Expr>
		auto pauli_measure(std::integral_constant<char, 'Z'>, Expr&& expr){
		return all(expr);
		}

		template<typename Expr>
		auto pauli_measure(std::integral_constant<char, 'Y'>, Expr&& expr){
		return all(h(sdag(expr)));
		}

		template<typename Expr>
		auto pauli_measure(std::integral_constant<char, 'X'>, Expr&& expr){
		return all(h(expr));
		}
		

      template<typename Expr, typename H_mol, typename TermIndex>
      inline constexpr auto operator()(Expr&& expr, H_mol, TermIndex) noexcept
      { 
      	constexpr char pauli = H_mol::_terms[TermIndex::_I*H_mol::_qubits + index];
        return pauli_measure(std::integral_constant<char, pauli>{}, sel<index>(expr));         
      }
    };

    //Convert Histogram output to expectation value
    template<typename HistType>
    float histToExp(HistType hist){

    	//Initialize expectation value an total number of shots to zero
    	long expectation = 0;
    	long shots = 0;
    	
    	//Loop over all states
    	for(int state = 0; state < hist.size(); state++){
    		std::bitset<ct_math::ct_sqrt(hist.size())> bits(state); //Get bitset based on hist index

    		//Add expectations bases on bitset parity
    		if(bits.count() % 2 == 0){ //Even
    			expectation += (long)hist[state];
    		}else{ //Odd
				expectation -= (long)hist[state];
    		}
    		shots += hist[state];
    	}
    	//Return expecation over all shots
    	return (float)expectation/(float)shots;
    }

    template<typename HistType, typename H_mol>
    auto reduced_hist(int index, HistType hist, H_mol h_mol){

    	//std::cout << "Terms: " << h_mol._numTerms << std::endl;
    	//std::cout << "Qubits: " << h_mol._qubits << std::endl;

    	std::vector<int> measure_base = {};
    	QArray<1 << h_mol._qubits, unsigned long> reducedHist;

    	for(int i=0; i < h_mol._qubits; i++){
    		char pauli = h_mol._terms[index*h_mol._qubits + i];
    		if(pauli != 'I'){
    			measure_base.push_back(i);
    		}
    	}

    	for(int i=0; i < hist.size(); i++){

	    	//Convert state number to bitstring
	      std::bitset<H_mol::_qubits> bits(i);
	      std::bitset<H_mol::_qubits> subset(0);

    		for(int j=0; j < measure_base.size(); j++){
    			std::bitset<H_mol::_qubits> mask(bits[measure_base[j]] << measure_base[j]); 
    			subset = subset | mask;
    		}    		

    		//std::cout << subset << std::endl;
    		reducedHist[subset.to_ulong()] += hist[i];
    	}

    	return reducedHist;
    }

    //VQE Pauli measurement loop
    template<index_t start, index_t end, index_t step, index_t index>
    struct pauli_measurements
    { 
      template<typename Expectation, typename Ansatz, typename H_mol, typename QPU_Info>
      inline constexpr auto operator()(Expectation&& expectation, 
      								   							 Ansatz&& ansatz, 
      								   							 H_mol& h_mol, 
      								   							 QPU_Info,
      								   							 std::vector<float>* execDurations,
      								   							 std::vector<float>* jobDurations,
      								   							 std::vector<float>* queueDurations,
      								   							 bool printCircuit) noexcept
      { 
        //Append pauli measurement gates based on Hamiltonian
        constexpr auto termIndex = static_int<index>();
  			auto expr = utils::static_for<0, H_mol::_qubits-1, 1, pauli_measurement_gates>(ansatz, H_mol{}, termIndex);

  			//Execute circuit on quantum backend
	    	QDevice<QPU_Info::_qpuID, QPU_Info::_qubits> qpu_z;
	    	QDevice<QPU_Info::_qpuID, QPU_Info::_qubits> qpu_x;
	    	QArray<1 << QPU_Info::_qubits, unsigned long> hist_z;
	    	QArray<1 << QPU_Info::_qubits, unsigned long> hist_x;
	    	

	    	if(QPU_Info::_qpuID != QDeviceType::quest and
		       QPU_Info::_qpuID != QDeviceType::qx){

	    		//Retrieve Z-basis results
		      qpu_z(measure_z(ansatz));
					auto job_z  = qpu_z.execute(QPU_Info::_shots);    	
	    	
		    	//Store results    
		    	execDurations->push_back(qpu_z.template get<QResultType::duration>(job_z->get()).count());
		    	jobDurations->push_back(qpu_z.template get<QResultType::jobDuration>(job_z->get()).count()); 
		    	queueDurations->push_back(qpu_z.template get<QResultType::queueDuration>(job_z->get()).count());  
		    	auto _hist_z = qpu_z.template get<QResultType::histogram>(job_z->get());

		      for(int i=0; i < (1 << QPU_Info::_qubits); i++){
		        hist_z[i] = _hist_z[i];
		      }

		      //Retrieve X-basis results
		      qpu_x(measure_x(ansatz));
					auto job_x  = qpu_x.execute(QPU_Info::_shots);    	
	    	
		    	//Store results    
		    	execDurations->push_back(qpu_x.template get<QResultType::duration>(job_x->get()).count()); 
		    	jobDurations->push_back(qpu_x.template get<QResultType::jobDuration>(job_x->get()).count()); 
		    	queueDurations->push_back(qpu_x.template get<QResultType::queueDuration>(job_x->get()).count());  
		    	auto _hist_x = qpu_x.template get<QResultType::histogram>(job_x->get());

		      for(int i=0; i < (1 << QPU_Info::_qubits); i++){
		        hist_x[i] = _hist_x[i];
		      }


		    } else{

		      qpu_z(ansatz);
		      auto job_z = qpu_z.execute(QPU_Info::_shots);

		      qpu_x(h(ansatz));
		      auto job_x = qpu_x.execute(QPU_Info::_shots);

		    #if QUEST_ENABLE
		      execDurations->push_back(qpu_z.duration_s());   
		      jobDurations->push_back(qpu_z.duration_s()); 
		      queueDurations->push_back(0.0);     
		      auto probs_z =  qpu_z.probabilities();

		      for(int i=0; i < (1 << QPU_Info::_qubits); i++){
		        hist_z[i] = 0;
		      }

		      for(int i=0; i < (1 << QPU_Info::_qubits); i++){

		        hist_z[i] += QPU_Info::_shots*probs_z[i];
		      } 

		      execDurations->push_back(qpu_x.duration_s());    
		      jobDurations->push_back(qpu_x.duration_s());   
		      queueDurations->push_back(0.0);   
		      auto probs_x =  qpu_x.probabilities();

		      for(int i=0; i < (1 << QPU_Info::_qubits); i++){
		        hist_x[i] = 0;
		      }

		      for(int i=0; i < (1 << QPU_Info::_qubits); i++){

		        hist_x[i] += QPU_Info::_shots*probs_x[i];
		      } 
		    #else
		      std::cout << "\nPlease enable QuEST macro. Exiting..." << std::endl; 
		      std::exit(0);
		    #endif       
		    }
   
		    //Print circuit if enabled (with Qiskit)
      	if(printCircuit){
      		QDevice<QDeviceType::qiskit_aer_simulator, QPU_Info::_qubits> qiskit;
        	qiskit(expr);
	        std::cout << qiskit.print_circuit() << std::endl;
	      }

	      //Loop over all Hamiltonian terms
	      for(int i = 0; i < H_mol::_numTerms; i++){

	      	bool z_flag = false;
	      	bool x_flag = false;

	      	for(int j = 0; j < H_mol::_qubits; j++){
	      		char term = H_mol::_terms[i*H_mol::_qubits + j];

	      		if(term == 'Z'){
	      			z_flag = true;
	      		}

	      		if(term == 'X'){
	      			x_flag = true;
	      		}
	      	}
	      	
	      	//Reduce histogram
	      	QArray<1 << QPU_Info::_qubits, unsigned long> reducedHist;
		      if(z_flag){ 
		      	reducedHist = reduced_hist(i, hist_z, h_mol); 
		      } else if(x_flag){
		      	reducedHist = reduced_hist(i, hist_x, h_mol);
		      }

		    	//Add partial expectation to total
		    	expectation += h_mol._coeffs[i]*histToExp(reducedHist); 
	      }

	    	//Debugging
	    	//std::cout <<"Histogram: " << hist << std::endl;
	    	//std::cout <<"Reduced histogram: " << reduced_hist<index>(hist, h_mol) << std::endl;
	    	//std::cout << "Partial expectation: " << h_mol._coeffs[index]*histToExp(hist) << std::endl;
				
	   		return expectation;     
      }
    };
}; //namespace VQE_LOOPS

#endif //VQE_LOOPS_HPP

