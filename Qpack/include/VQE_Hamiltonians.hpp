/** @file Qpack/include/VQE_Hamiltonians.hpp

	@brief Qpack VQE_Hamiltonians header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef VQE_HAMILTONIANS_HPP
#define VQE_HAMILTONIANS_HPP

namespace Hamiltonians{

  //Struct that containt Hamiltonian of Pauli matrices
  template<int numTerms, int numQubits, char... terms>
  struct Hamiltonian{
    static constexpr const int _numTerms = numTerms;
    static constexpr const int _qubits = numQubits;
    static constexpr const char _terms[] = {terms...};
    float _coeffs[numTerms] = {};

    // Creates a new Hamiltonian with additional pauli operator
  	template<char pauli>
  	using add_pauli = Hamiltonian<numTerms, numQubits, terms..., pauli>;

  };


  ////////////Random Hamiltonian//////////////////////////////////////

	//Loops over all qubits and assigns Z or I gate bases on qubit index and term index
  template<index_t start, index_t end, index_t step, index_t index>
  struct z_loop_inner
  {
    static constexpr auto pauli_mat(std::integral_constant<bool, false>){ return 'I';}
    static constexpr auto pauli_mat(std::integral_constant<bool, true>){  return 'Z';}

    template<typename H, typename TermIndex>
    inline constexpr auto operator()(H&& h, TermIndex) noexcept
    { 
      //Get pauli char based on index
      constexpr char pauli = pauli_mat(std::integral_constant<bool,index==TermIndex::_I>{});
      
      //Return new Hamiltonian with added pauli gate
      return typename std::decay<H>::type::template add_pauli<pauli> {};
    }
  };

  //Loops over all Hamiltonian terms
  template<index_t start, index_t end, index_t step, index_t index>
  struct z_loop_outer
  { 
  	template<typename H, typename size>
    inline constexpr auto operator()(H&& h, size) noexcept
    {
      constexpr auto termIndex = VQE_Loops::static_int<index>(); 	
      return utils::static_for<0, size::_I-1, 1, z_loop_inner>(H{}, termIndex);
    }
  };    

template<int size=1>
constexpr auto genDiagonalHamiltonian(){

  //Random constant floats (100)
	float random_floats[] = {-0.5744, 0.8371, -0.3355, 0.5271, 0.4706, -0.8478, 0.6888, 0.3292, -0.9607, 0.3222, -0.5105, -0.9897, -0.7715, 0.7951, 0.9434, -0.4727, 0.9197, 0.4264, -0.4883, 0.621, 0.4916, 0.1467, 0.0373, -0.28, -0.7621, 0.2613, 0.5845, -0.1643, -0.3134, -0.7457, -0.0752, 0.3431, 0.3516, -0.2646, -0.2114, -0.4217, 0.3649, -0.3971, 0.9196, 0.2383, -0.6071, -0.939, 0.6689, -0.2127, -0.3769, 0.1525, -0.088, 0.2868, 0.8808, -0.0309, -0.4611, 0.1063, -0.6852, 0.4813, -0.2738, 0.2184, 0.9295, 0.9794, -0.5543, 0.156, -0.9739, 0.6748, 0.5962, -0.2649, -0.3602, 0.4841, 0.3065, -0.2893, 0.1669, -0.2922, 0.6322, 0.9242, -0.7872, -0.0055, 0.4468, -0.3218, 0.9994, -0.4223, 0.3314, 0.1887, -0.242, -0.3767, -0.9138, 0.8556, -0.3891, -0.004, -0.1904, 0.3853, 0.1728, 0.8095, 0.4985, -0.5314, -0.0891, 0.5509, -0.2526, 0.1154, 0.1072, -0.3161, 0.2092, 0.8787};
	
  //Static constexpressions
  constexpr auto static_size = VQE_Loops::static_int<size>(); //Size of the size by size Hamiltonian
	constexpr auto neg_index = VQE_Loops::static_int<-1>();     //Negative index for only I loop
	
  //Append pauli Z operators on each diagonal;
  using Ham_Z = decltype(utils::static_for<0, size-1, 1, z_loop_outer>(Hamiltonian<size, size>(), static_size));

  //Declare Hamiltonian H
	Ham_Z H;

  //Load coeffients to hamiltonian (op to 100)
	for(int i = 0; i < size; i++){
        H._coeffs[i] = random_floats[i];
    } 
	
  return H;		
};

/////////////////Ising Chain Hamiltonian///////////////////////////////

  //Loops over all qubits and assigns ZZ or I gate bases on qubit index and term index for neighbouring qubits
  template<index_t start, index_t end, index_t step, index_t index>
  struct zz_loop_inner
  {
    static constexpr auto pauli_mat(std::integral_constant<bool, false>){ return 'I';}
    static constexpr auto pauli_mat(std::integral_constant<bool, true>){  return 'X';}

    template<typename H, typename TermIndex>
    inline constexpr auto operator()(H&& h, TermIndex) noexcept
    { 
      //Get pauli char based on index (neighbours)
      constexpr char pauli = pauli_mat(std::integral_constant<bool,index==TermIndex::_I or index==TermIndex::_I+1>{});
      
      //Return new Hamiltonian with added pauli gate
      return typename std::decay<H>::type::template add_pauli<pauli> {};
    }
  };

  //Loops over all Hamiltonian terms
  template<index_t start, index_t end, index_t step, index_t index>
  struct zz_loop_outer
  { 
    template<typename H, typename size>
    inline constexpr auto operator()(H&& h, size) noexcept
    {
      constexpr auto termIndex = VQE_Loops::static_int<index>();  
      return utils::static_for<0, size::_I-1, 1, zz_loop_inner>(H{}, termIndex);
    }
  };

  //Loops over all qubits and assigns X or I gate bases on qubit index and term index
  template<index_t start, index_t end, index_t step, index_t index>
  struct x_loop_inner
  {
    static constexpr auto pauli_mat(std::integral_constant<bool, false>){ return 'I';}
    static constexpr auto pauli_mat(std::integral_constant<bool, true>){  return 'Z';}

    template<typename H, typename TermIndex>
    inline constexpr auto operator()(H&& h, TermIndex) noexcept
    { 
      //Get pauli char based on index
      constexpr char pauli = pauli_mat(std::integral_constant<bool,index==TermIndex::_I>{});

      //Return new Hamiltonian with added pauli gate
      return typename std::decay<H>::type::template add_pauli<pauli> {};
    }
  };

  //Loops over all Hamiltonian terms
  template<index_t start, index_t end, index_t step, index_t index>
  struct x_loop_outer
  { 
    template<typename H, typename size>
    inline constexpr auto operator()(H&& h, size) noexcept
    {
    constexpr auto termIndex = VQE_Loops::static_int<index>();  
    return utils::static_for<0, size::_I-1, 1, x_loop_inner>(H{}, termIndex);
    }
  };  

  //Generate hamiltonian describing the energies of a 1-D Ising chain
  template<int size>
  constexpr auto genIsingHamiltonian(){

    //Constexpr terms based on chain size
    constexpr auto static_size = VQE_Loops::static_int<size>();   
    constexpr int numTerms = 2*size-1;
    constexpr int numQubits = size; 

    //Create hamiltonian with ZZ terms for neighbouring qubits
    using Ham_ZZ = decltype(utils::static_for<0, size-2, 1, zz_loop_outer>(Hamiltonian<numTerms, numQubits>(), static_size));

    //Append pauli X operators on each qubit
    using Ham_X = decltype(utils::static_for<0, size-1, 1, x_loop_outer>(Ham_ZZ(), static_size));

    //Declare Hamiltonian H
    Ham_X H;

    //Load coefficients in H
    float h = 1.0; //Tranverse field
    float J = 1.0; //Interaction strength
    for(int i = 0; i <= size-2; i++){ H._coeffs[i] = -J; } 
    for(int i = 0; i <= size-1; i++){ H._coeffs[i+size-1] = -h; } 

    return H;   
  };

}; //namespace Hamiltonians

#endif //VQE_HAMILTONIANS_HPP

