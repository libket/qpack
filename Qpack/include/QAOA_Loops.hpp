/** @file Qpack/include/QAOA_loops.hpp

	@brief Qpack QAOA_loops header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef QAOA_LOOPS_HPP
#define QAOA_LOOPS_HPP

#include <LibKet.hpp>
#include <math.h>

using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

#define PI  3.14159265358979323846  /* pi */

namespace ct_math{
  //Function to take a compile time square root (only for integer squares)
  static constexpr std::size_t ct_sqrt(std::size_t res, std::size_t l, std::size_t r){
      if(l == r){
          return r;
      } else {
          const auto mid = (r + l) / 2;

          if(mid * mid >= res){
              return ct_sqrt(res, l, mid);
          } else {
              return ct_sqrt(res, mid + 1, r);
          }
      }
  }

  static constexpr std::size_t ct_sqrt(std::size_t res){
      return ct_sqrt(res, 1, res);
  }
} //ct_math

namespace QAOAGates{
    
    //CNOT RZ CNOT Maxcut
    template<index_t start, index_t end, index_t step, index_t index>
    struct maxcut_cost_function
    {  
      template<typename  Expr, typename Graph, typename Gamma>
      inline constexpr auto operator()(Expr&& expr, Graph, Gamma) noexcept
      {
        auto cnot1 = cnot(sel<Graph::template from<index>()>(gototag<0>()),
                          sel<Graph::template to<index>()>(gototag<0>(expr))
                          );

        auto rot_z_gamma = rz(Gamma{}, sel<1>(cnot1));

        auto cnot2 = cnot(sel<Graph::template from<index>()>(gototag<0>()),
                          sel<Graph::template to<index>()>(gototag<0>(rot_z_gamma))
                          );

        return gototag<0>(cnot2);         
      }
    };

    //Maxcut p iteration
    template<index_t start, index_t end, index_t step, index_t index>
    struct maxcut_step
    {  
      template<typename  Expr, typename Graph, typename Params>
      inline constexpr auto operator()(Expr&& expr, Graph, Params&& params) noexcept
      { 
        
        //Set circuits parameters
        QVar_t<2*index,1> beta(2*PI*params[2*index]);
        QVar_t<2*index+1,1> gamma(2*PI*params[2*index+1]);

        //Cost unitaries
        auto cost = utils::static_for<0,Graph::size() -1,1,maxcut_cost_function>(tag<0>(expr), Graph{}, gamma);

        //Mixer unitaries
        auto mixed = rx(beta, gototag<0>(cost));  

        return gototag<0>(mixed);         
      }
    };
    
    //Inverted CRZ DSP
    template<index_t start, index_t end, index_t step, index_t index>
    struct dsp_inv_cphase
    {  
      template<typename Expr, typename A_filter, typename Gamma>
      inline constexpr auto operator()(Expr&& expr, A_filter&& a_filter, Gamma) noexcept
      {
        auto x1 = x(sel<index>(gototag<1>(expr)));
        auto crz_gate = crz(Gamma{}, 
                          sel<index>(),
                          a_filter(gototag<1>(x1)));
        auto x2 = x(sel<index>(gototag<1>(crz_gate)));

        return gototag<1>(x2);         
      }
    };

    template<int index>
    struct data{
      static constexpr const int _index = index;
    };

    //Find neigbours
    template<index_t start, index_t end, index_t step, index_t index>
    struct get_neighbour_filter
    { 
      template<int node, int e0, int e1>
      auto getNeighbour(std::false_type){
        return sel<>();
      }

      template<int node, int e0, int e1>
      auto getNeighbour(std::true_type){
        return sel<node == e0 ? e1 : e0>();
      }

      template<typename Q_filter, typename NodeIndex, typename Graph>
      inline constexpr auto operator()(Q_filter&& q_filter, NodeIndex, Graph) noexcept
      { 
        constexpr auto e0 = Graph::template from<index>();
        constexpr auto e1 = Graph::template to<index>();
        constexpr bool hasNeighbour = NodeIndex::_index == e0 || NodeIndex::_index == e1;

        return q_filter<<getNeighbour<NodeIndex::_index, e0, e1>(std::integral_constant<bool, hasNeighbour>{});
              
      }
    };

    //Arbitrary OR cphase for all connected nodes
    template<index_t start, index_t end, index_t step, index_t index>
    struct dsp_arb_OR_cphase
    {  
      template<typename Expr, typename A_filter, typename AA_filter, typename Graph, typename Gamma>
      inline constexpr auto operator()(Expr&& expr, A_filter&& a_filter, AA_filter&& aa_filter, Graph, Gamma) noexcept
      { 
        //Loop over graph to find connection for this node
        constexpr auto nodeIndex = data<index>();
        auto q_filter = utils::static_for<0, Graph::size()-1,1, get_neighbour_filter>(sel<index>(), nodeIndex, Graph{});

        //Reduce number of ancillas based on q_filter size
        constexpr auto size = q_filter.size();
        auto new_aa_filter = range<0,size-2>(aa_filter);

         //Apply OR cphase to selected qubits
        auto arb_OR_cphase = arb_OR<>(crz(Gamma{}),
                                      q_filter(),
                                      a_filter(),
                                      new_aa_filter(gototag<1>(expr))
                                      ); 

        return gototag<1>(arb_OR_cphase);         
      }
    };
    
    //DSP p iteration
    template<index_t start, index_t end, index_t step, index_t index>
    struct dsp_step
    {  
      template<typename  Expr, typename Graph, typename Params>
      inline constexpr auto operator()(Expr&& expr, Graph, Params&& params) noexcept
      { 
        
        //Set circuits parameters
        QVar_t<2*index,1> beta(2*PI*params[2*index]);
        QVar_t<2*index+1,1> gamma(2*PI*params[2*index+1]);
        constexpr auto numQubits = Graph::size() == 1 ? 2 :
                                   Graph::size() == 3 ? 3 :
                                   Graph::size() == 4 ? 4 : Graph::size()/2; //For regular graphs
        
        //Set ancilla filters
        auto cost_ancilla = QFilterSelect<numQubits>();
        auto ctrl_anchillas = QFilterSelectRange<numQubits+1, numQubits+4>();

        //Inverted CRZ for each qubit on ancilla
        auto inv_cphase = utils::static_for<0,numQubits-1,1, dsp_inv_cphase>(tag<1>(expr), cost_ancilla, gamma);

        //Logical OR for each qubit on ancilla
        auto cost = utils::static_for<0,numQubits-1,1, dsp_arb_OR_cphase>(tag<1>(inv_cphase), cost_ancilla, ctrl_anchillas, Graph{}, gamma);

        //Mixer unitaries
        auto mixed = rx(beta, range<0,numQubits-1>(gototag<1>(cost)));  

        return gototag<1>(mixed);         
      }
    };

    /* Implementation of CRY gate
    ─────■─────     ──────────────■─────────────────■──
    ┌────┴────┐     ┌──────────┐┌─┴─┐┌───────────┐┌─┴─┐
    ┤ Ry(0.5) ├  =  ┤ Ry(0.25) ├┤ X ├┤ Ry(-0.25) ├┤ X ├
    └─────────┘     └──────────┘└───┘└───────────┘└───┘
    */
    template<typename AnglePos, typename AngleNeg, typename CFilter, typename TFilter, typename Expr>
    auto cry(AnglePos, AngleNeg, CFilter&& cFilter, TFilter&& tFilter, Expr&& expr){

      auto RyPos = ry(AnglePos{}, tFilter(all(expr)));

      auto cnot1 = cnot(cFilter(),
                        tFilter(all(RyPos)));

      auto RyNeg = ry(AngleNeg{}, tFilter(all(cnot1)));

      auto cnot2 = cnot(cFilter(),
                        tFilter(all(RyNeg)));      

      return all(cnot2);
    }

    /* Implementation of CCRY gate
    ─────■─────   ──────────────■─────────────────■──
         │                      │                 │  
    ─────■───── = ──────────────■─────────────────■──
    ┌────┴────┐   ┌──────────┐┌─┴─┐┌───────────┐┌─┴─┐
    ┤ Ry(0.5) ├   ┤ Ry(0.25) ├┤ X ├┤ Ry(-0.25) ├┤ X ├
    └─────────┘   └──────────┘└───┘└───────────┘└───┘
    */
    template<typename AnglePos, typename AngleNeg, typename C0Filter, typename C1Filter, typename TFilter, typename Expr>
    auto ccry(AnglePos, AngleNeg, C0Filter&& c0Filter, C1Filter&& c1Filter, TFilter&& tFilter, Expr&& expr){

      auto RyPos = ry(AnglePos{}, tFilter(all(expr)));

      auto ccnot1 = ccnot(c0Filter(),
                          c1Filter(),
                          tFilter(all(RyPos)));

      auto RyNeg = ry(AngleNeg{}, tFilter(all(ccnot1)));

      auto ccnot2 = ccnot(c0Filter(),
                          c1Filter(),
                          tFilter(all(RyNeg)));      

      return all(ccnot2);
    }

    //TSP inital state encoding: Dicke state with Hamming weight 2
    //Implemenation by Mukherjee et. al. (2020)
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_scs
    {  
      template<typename  Expr, typename RowFilter>
      inline constexpr auto operator()(Expr&& expr, RowFilter&& rowFilter) noexcept
      { 
        //Setup filters for SCS iteration (three qubits for Hamming weight 2)     
        auto q0filter = QFilterSelect<end-index>()(rowFilter);
        auto q1filter = QFilterSelect<end-index+1>()(rowFilter);
        auto q2filter = QFilterSelect<end-index+2>()(rowFilter);

        //Select angles for SCS iteration
        double angle0 = 2*std::acos(std::sqrt(1.0/(end+3-index)));
        double angle1 = 2*std::acos(std::sqrt(2.0/(end+3-index)));

        //Set angles to QVars with unique index (divided by 2 for CRY and CCRY gates)
        QVar_t<300+index*4+0> angle0pos(angle0/2);
        QVar_t<300+index*4+1> angle0neg(-angle0/2);
        QVar_t<300+index*4+2> angle1pos(angle1/2);
        QVar_t<300+index*4+3> angle1neg(-angle1/2);

        //Add mu block
        auto mu = gototag<2>(cnot(q1filter(),
                             q2filter(gototag<2>(cry(angle0pos,
                                                     angle0neg,
                                                     q2filter(),
                                                     q1filter(),
                                                     gototag<2>(cnot(q1filter(),
                                                                     q2filter(expr)
                                                                    )
                                                               )
                                                    )
                                                )
                                      )
                                 )
                            );

        //Add M block
        auto M = gototag<2>(cnot(q0filter(),
                    q2filter(gototag<2>(ccry(angle1pos,
                                             angle1neg,
                                            q2filter(),
                                            q1filter(),
                                            q0filter(),
                                            gototag<2>(cnot(q0filter(),
                                                            q2filter(mu))
                                                      )
                                                 
                                           )
                                        )
                             )
                         )
                   );

        return gototag<2>(M);         
      }
    };

    //TSP inital state encoding
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_init
    {  
      template<typename  Expr>
      inline constexpr auto operator()(Expr&& expr) noexcept
      {         
        
        auto rowFilter = QFilterSelectRange<index, index+step-1>();

        auto flipFilter = QFilterSelect<step-2, step-1>()(rowFilter);

        //Dicke state of k = 2: Flip bottom two qubits
        auto flips = gototag<2>(x(flipFilter(expr)));

        //Execute SCS blocks
        auto scs = utils::static_for<0, step-3, 1, tsp_scs>(flips, rowFilter);

        //Add final SCS block
        double angle = 2*std::acos(std::sqrt(1.0/2));
        QVar_t<400+index*2+0> anglepos(angle/2);
        QVar_t<400+index*2+1> angleneg(-angle/2);
        auto scs_final = gototag<2>(cnot(sel<0>(rowFilter()),
                                         sel<1>(rowFilter(gototag<2>(cry(anglepos,
                                                                         angleneg,
                                                                         sel<1>(rowFilter()),
                                                                         sel<0>(rowFilter()),
                                                                         gototag<2>(cnot(sel<0>(rowFilter()),
                                                                                         sel<1>(rowFilter(scs))
                                                                                      )
                                                                       )
                                                                )
                                                            )
                                                  ))
                                             )
                                        );

        return gototag<2>(scs_final);         
      }
    };

    //Struct to pass constexpressions in function parameters (indices in this case)
    template<int index>
    struct IndexPass{
      static constexpr const int getIndex(){return index;}
    };

    //TSP: loop over all qubits
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_qubit_loop
    {  
      template<typename  Expr, typename D_Matrix, typename NumCities, typename Gamma>
      inline constexpr auto operator()(Expr&& expr, D_Matrix&& D_matrix, NumCities&& numCities, Gamma&& gamma) noexcept
      { 
        //Set Rz angle bases on D_matrix weights
        QVar_t<100+index,1> angle(gamma/(2*PI)*D_matrix[index/numCities][index%numCities]);

        return  gototag<2>(rz(angle, sel<index>(gototag<2>(expr))));
      }
    };

    //TSP: loop over all couplings
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_coupling_loop
    {  
      template<typename  Expr, typename FullGraph, typename Gamma>
      inline constexpr auto operator()(Expr&& expr, FullGraph, Gamma&& gamma) noexcept
      {        
        //RZZ angle is 5 gamma
        QVar_t<200,1> angle(5*gamma/PI);

        //Link all qubits that are symmetrically opposite of the matrix diagonal 
        constexpr int e0 = FullGraph::template from<index>();
        constexpr int e1 = FullGraph::template to<index>();
        constexpr int numCities = (1+ct_math::ct_sqrt(1+8*FullGraph::size()))/2;
        constexpr int q0 = e0*numCities + e1;
        constexpr int q1 = e1*numCities + e0;

        return gototag<2>(rzz(angle, sel<q0>(gototag<2>()), sel<q1>(gototag<2>(expr))));
      }
    };
    
    //TSP: rxx loop
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_rxx_loop
    {  
      template<typename  Expr, typename Beta>
      inline constexpr auto operator()(Expr&& expr, Beta&& beta) noexcept
      { 
        constexpr int N = end-start+1;
        constexpr int q0 = index;
        constexpr int q1 = index-start == N-1 ? start : index+1;
        return rxx(beta, 
                   sel<q0>(gototag<2>()), 
                   sel<q1>(gototag<2>(expr)));

      }
    };

    //TSP: ryy loop
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_ryy_loop
    {  
      template<typename  Expr, typename Beta>
      inline constexpr auto operator()(Expr&& expr, Beta&& beta) noexcept
      { 
        constexpr int N = end-start+1;
        constexpr int q0 = index;
        constexpr int q1 = index-start == N-1 ? start : index+1;
        return ryy(beta, 
                   sel<q0>(gototag<2>()), 
                   sel<q1>(gototag<2>(expr)));
      }
    };


    //TSP: loop over all nodes
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_node_loop
    {  
      template<typename  Expr, typename Beta>
      inline constexpr auto operator()(Expr&& expr, Beta&& beta) noexcept
      { 
        //Decomposed SWAP gate (SWAP_{ij} = e^{X_i X_j + Y_i Y_j})   
        /*
        auto rxx1 = rxx(beta, 
                        sel<index*(end+1)>(gototag<2>()), 
                        sel<index*(end+1)+1>(gototag<2>(expr)));

        auto rxx2 = rxx(beta, 
                        sel<index*(end+1)+1>(gototag<2>()), 
                        sel<index*(end+1)+2>(gototag<2>(rxx1)));

        auto ryy1 = ryy(beta, 
                        sel<index*(end+1)>(gototag<2>()), 
                        sel<index*(end+1)+1>(gototag<2>(rxx2)));

        auto ryy2 = ryy(beta, 
                        sel<index*(end+1)+1>(gototag<2>()), 
                        sel<index*(end+1)+2>(gototag<2>(ryy1)));
        
        return gototag<2>(ryy2);
        */
        constexpr int N = end+1;
        auto rxx1 = utils::static_for<N*index, N*index+N-1, 1, tsp_rxx_loop>(gototag<2>(expr), beta);
        auto ryy1 = utils::static_for<N*index, N*index+N-1, 1, tsp_ryy_loop>(gototag<2>(rxx1), beta);

        return gototag<2>(ryy1);
      }
    };

    //TSP p itersation
    template<index_t start, index_t end, index_t step, index_t index>
    struct tsp_step
    {  
      template<typename  Expr, typename Graph, typename FullGraph, typename D_Matrix, typename Params>
      inline constexpr auto operator()(Expr&& expr, Graph, FullGraph fullGraph, D_Matrix&& D_matrix, Params&& params) noexcept
      {   
        //Set circuits parameters
        QVar_t<2*index,1> beta(-(params[2*index]-0.5));///(end+1));
        auto gamma = (2*PI*(params[2*index+1]-0.5));///(end+1));   
        constexpr auto numCities = Graph::size() == 3 ? 3 : 
                                   Graph::size() == 4 ? 4 :
                                   Graph::size()/2; //For regular graphs
        
        //Cost hamiltonians
        //Soft constraint, enforcing minimal travel distance
        auto cost_soft = utils::static_for<0, 
                                           numCities*numCities-1, 
                                           1, 
                                           tsp_qubit_loop>(gototag<2>(expr), D_matrix, numCities, gamma);

        //Hard constraint, enforcing a symmetric adjacency matrix
        auto cost_hard = utils::static_for<0, FullGraph::size()-1, 1, tsp_coupling_loop>(gototag<2>(cost_soft), FullGraph{}, gamma);

        //Mixer hamiltonian: Mixes the solution space
        auto mixer = utils::static_for<0, numCities-1, 1, tsp_node_loop>(gototag<2>(cost_hard), beta);

        return gototag<2>(mixer);         
      }
    };

    //Arbitrary NOR X-rotation for all connected nodes
    template<index_t start, index_t end, index_t step, index_t index>
    struct mis_NOR_rotX
    {  
      template<typename Expr, typename Ancillas, typename Graph, typename Beta, typename P>
      inline constexpr auto operator()(Expr&& expr, Ancillas&& ancillas, Graph, Beta, P) noexcept
      { 
        //Loop over graph to find connection for this node
        constexpr size_t current_node = (index + P::_index) % (end+1);
        constexpr auto nodeIndex = data<current_node>();
        auto c_filter = utils::static_for<0,Graph::size()-1,1, get_neighbour_filter>(sel<>(), nodeIndex, Graph{});
        auto t_filter = sel<current_node>();

        //Reduce number of ancillas based on q_filter size
        constexpr auto size = c_filter.size();
        auto new_ancillas = range<0,size-2>(ancillas);
        auto c_last_filter = sel<size-2>(new_ancillas);

        //Apply OR cphase to selected qubits
        auto x1 = x(c_last_filter(gototag<3>(expr)));
        auto arb_OR_crz= arb_OR<>(crx(Beta{}),
                                  c_filter(),
                                  t_filter(),
                                  new_ancillas(gototag<3>(x1))
                                  );
        auto x2 = x(c_last_filter(gototag<3>(arb_OR_crz)));                               

        return gototag<3>(x2);         
      }
    };

    //MIS p iteration
    template<index_t start, index_t end, index_t step, index_t index>
    struct mis_step
    {  
      template<typename  Expr, typename Graph, typename Params>
      inline constexpr auto operator()(Expr&& expr, Graph, Params&& params) noexcept
      { 
        
        //Set circuits parameters
        QVar_t<2*index,1> beta(PI/2*params[2*index]);
        QVar_t<2*index+1,1> gamma(2*PI*params[2*index+1]);
        constexpr auto numQubits = Graph::size() == 1 ? 2 :
                                   Graph::size() == 3 ? 3 :
                                   Graph::size() == 4 ? 4 : Graph::size()/2; //For regular graphs
        
        //Set ancilla filters
        auto ancillas = QFilterSelectRange<numQubits, numQubits+3>();

        //Cost Hamiltonian
        auto cost = rz(gamma, range<0,numQubits-1>(gototag<3>(expr)));        

        //Mixer unitaries
        constexpr auto P = data<index>();
        auto mixed = utils::static_for<0,
                                       numQubits-1,
                                       1, 
                                       mis_NOR_rotX>(gototag<3>(cost), ancillas, Graph{}, beta, P); 

        return gototag<3>(mixed);         
      }
    };

}

#endif //QAOA_LOOPS_HPP

