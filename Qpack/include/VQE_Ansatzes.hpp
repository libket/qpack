/** @file Qpack/include/VQE_Ansatzes.hpp

	@brief Qpack VQE_Ansatzes header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef VQE_ANSATZES_HPP
#define VQE_ANSATZES_HPP

namespace Ansatzes{

///////Random Anzats//////////////////////////////

  //Applies Rx gate for all indices with indexed parameter values
  template<index_t start, index_t end, index_t step, index_t index>
  struct rotX_loop
  { 
      template<typename Expr, typename Params>
    inline constexpr auto operator()(Expr&& expr, Params&& params) noexcept
    {   
        QVar_t<100+index, 1> angle(2*PI*params[index]); 
          return all(rx(angle, sel<index>(expr)));    
    }

    template<typename Expr, typename Params, typename Offset> //Overload function that adds an offset
    inline constexpr auto operator()(Expr&& expr, Params&& params, Offset) noexcept
    { 	
    		QVar_t<200+index+Offset::_I, 1> angle(2*PI*params[index+Offset::_I]); 
    	  	return all(rx(angle, sel<index>(expr)));    
    }
  };

  //Generate ansatz for random diagonal hamiltonian
  template <int size, typename paramType>
  constexpr auto genRandomAnsatz(paramType params){

  	auto s0 = init();

    //RotX gates on all qubits for different parameters
  	auto RotX = utils::static_for<0, size-1, 1, rotX_loop>(s0, params);

  	return RotX;
  };

///////Ising chain Ansatz//////////////////////////////

  template<index_t start, index_t end, index_t step, index_t index>
  struct rotY_loop
  { 
    template<typename Expr, typename Params>
    inline constexpr auto operator()(Expr&& expr, Params&& params) noexcept
    {   
        QVar_t<300+index, 1> angle(2*PI*params[index]); 
          return all(ry(angle, sel<index>(expr)));    
    }

    template<typename Expr, typename Params, typename Offset>
    inline constexpr auto operator()(Expr&& expr, Params&& params, Offset) noexcept
    {   
        QVar_t<400+index+Offset::_I, 1> angle(2*PI*params[index+Offset::_I]); 
          return all(ry(angle, sel<index>(expr)));    
    }
  };

  template<index_t start, index_t end, index_t step, index_t index>
  struct rotZ_loop
  { 
    template<typename Expr, typename Params>
    inline constexpr auto operator()(Expr&& expr, Params&& params) noexcept
    {   
        QVar_t<500+index, 1> angle(2*PI*params[index]); 
          return all(rz(angle, sel<index>(expr)));    
    }

    template<typename Expr, typename Params, typename Offset>
    inline constexpr auto operator()(Expr&& expr, Params&& params, Offset) noexcept
    {   
        QVar_t<600+index+Offset::_I, 1> angle(2*PI*params[index+Offset::_I]); 
          return all(rz(angle, sel<index>(expr)));    
    }
  };

  template<index_t start, index_t end, index_t step, index_t index>
  struct CNOT_inner_loop
  { 
    template<typename Expr>
    inline constexpr auto operator()(Expr&& expr) noexcept
    {   
        return all(cnot(sel<start>(), sel<index+1>(expr)));    
    }

  };

  template<index_t start, index_t end, index_t step, index_t index>
  struct CNOT_outer_loop
  { 
    template<typename Expr>
    inline constexpr auto operator()(Expr&& expr) noexcept
    {   
        return utils::static_for<index, end, 1, CNOT_inner_loop>(expr);
    }

  };

  template <int size, typename paramType>
  constexpr auto genIsingAnsatz(paramType params){

    //Initialize zeros
    auto s0 = init();

    //Parameter offsets
    auto offset1 = VQE_Loops::static_int<size>();
    auto offset2 = VQE_Loops::static_int<2*size>();
    auto offset3 = VQE_Loops::static_int<3*size>();

    //Linear efficient anzats (Qiskit)
    auto RotY1 = utils::static_for<0, size-1, 1, rotY_loop>(s0, params);
    auto RotZ1 = utils::static_for<0, size-1, 1, rotZ_loop>(RotY1, params, offset1);
    auto cnots = utils::static_for<0, size-2, 1, CNOT_outer_loop>(RotZ1);
    auto RotY2 = utils::static_for<0, size-1, 1, rotY_loop>(cnots, params, offset2);
    auto RotZ2 = utils::static_for<0, size-1, 1, rotZ_loop>(RotY2, params, offset3);

    return RotZ2;
  };

}; //namespace Anzatses

#endif //VQE_ANSATZES_HPP

