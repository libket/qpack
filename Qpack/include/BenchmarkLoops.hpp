/** @file Qpack/include/BenchmarkLoops.hpp

	@brief Qpack BenchmarkLoops header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef BENCHMARKLOOPS_HPP
#define BENCHMARKLOOPS_HPP

#include "../include/Optimizers.hpp"
#include "../include/QAOA.hpp"
#include "../include/VQE.hpp"


namespace benchmark_loops{

	  //Struct to pass constexpressions in function parameters (indices in this case)
    template<int index>
    struct IndexPass{
    	static constexpr const int getIndex(){return index;}
    };

    //Function to create multiple from path
    void mkdirs(const char* path){

    	//Decompose path in a folder vector
    	int index = 0;
    	std::vector<int> dir_index = {};
    	while(path[index] != '\0'){
    		if(path[index] == '/'){
    			dir_index.push_back(index);
				}
				index++;
    	}

    	//Loop over path, add subfolder each loop
  		for(int i = 0; i < dir_index.size()-1; i++){  	
  			char output_path[256] = "";
  			char sub_path[256] = "";
  			
  			memcpy(sub_path, &path[0], dir_index[i+1]);

  			std::strcat(output_path, std::getenv("HOME"));
  			std::strcat(output_path, sub_path);
    	
	    	if(mkdir(output_path, 0777) != 0){
	        char buffer[ 256 ];
	        char * errorMsg = strerror_r( errno, buffer, 256 );    // GNU-specific version, Linux default
	        if(std::string(errorMsg).compare("File exists") != 0)
	            std::cout << "Error: " << errorMsg << std::endl;   //return value has to be used since buffer might not be modified
	    	}
	    }	
    }
		
		//Repeat QAOA measurements
    template<typename Optimizer> 
    void repeat_execution(Optimizer&& optimizer, int reps, const char* output_path){
    	
	    //Create ouput folder to save results
	    mkdirs(output_path);

	    //Execute VQA repeatedly
    	for(int index=0; index < reps; index++){

    		//Create JSON object to store results
    		nlohmann::json result;

    		//Generate output file path
    		char output_file[256] = "";
    		std::strcat(output_file, std::getenv("HOME"));
    		std::strcat(output_file, output_path);
    		std::strcat(output_file, "dataset");
    		std::strcat(output_file, index < 10 ? ("0"+std::to_string(index)).c_str() : std::to_string(index).c_str());
    		std::strcat(output_file, ".json");

    		//Execution message
			    std::string message = "(" + std::to_string(index + 1) + "/" + std::to_string(reps) + ")";  	
			    std::cout << message << std::flush;
			    for(int i = 0; i < message.size(); i++){
			    	std::cout << "\b";
			    }

	    	//Check if data is already available
	    	if(access(output_file, F_OK) != 0){

			    //Reset optimizer data
			    optimizer.resetData();
			    	
			    //Run optimizer	    
			    arma::vec opt_params = optimizer.COBYLA();

			    //Store optimizer results in JSON format
			    result = optimizer.getData();

					//Save result in file
		    	std::ofstream output(output_file);
			    output << std::setw(2) << result << std::endl;

	    	}
	    }		        
    }
 		

    //Loop over graph sizes
    template<index_t start, index_t end, index_t step, index_t index>
    struct graphSize_loop
    { 
      template<typename ProblemIndex, typename QpuIndex, typename AlgData> 
      inline constexpr auto operator()(int status, ProblemIndex, QpuIndex, AlgData) noexcept
      {	
			
      //Setup QAOA object
	    constexpr int problemSize = index;
	    constexpr int P = AlgData::P;
	    constexpr int shots = AlgData::shots;  	    
	    constexpr vqa_problems problem = AlgData::problems[ProblemIndex::getIndex()];
	    constexpr QDeviceType qpuID = AlgData::qpuIDs[QpuIndex::getIndex()]; 
	    constexpr int repetitions = AlgData::reps;
	   
	    //Execution message
	    std::cout << "\t\tProblem Size: " << std::to_string(problemSize) << " " << std::flush;

	    //Set name buffers for this problem, concatenate
	    char qpu_buffer [16] = {};
	    char size_buffer [8] = {};
	    char p_buffer [8] = {};
	    char shots_buffer [8] = {};
	    std::sprintf(qpu_buffer, "%X", static_cast<int>(qpuID));
	    std::sprintf(size_buffer, "%d", problemSize);
	    std::sprintf(p_buffer, "%d", P);
	    std::sprintf(shots_buffer, "%d", shots);

	    //Concat ouput path
    	char output_path[256] = "";
    	std::strcat(output_path, OUTPUT_DIR);			
    	std::strcat(output_path, qpu_buffer);	
    	std::strcat(output_path, "/");				   
    	std::strcat(output_path, vqa_names[problem].c_str()); 
    	std::strcat(output_path, "/N");	
    	if(problemSize < 10){
    		std::strcat(output_path, "0");
    	} 
    	std::strcat(output_path, size_buffer);		
    	std::strcat(output_path, "/");	
    	
    	//Select QAOA or VQE problem optimizer and repeat execution    			
	    switch(problem){
	    	//QAOA
	    	case MCP :
	    	case MIS :
	    	case DSP :
	    	case TSP :
	    		{
	    		QAOA<problemSize, problem, P, shots, qpuID> qaoa;
	    	  Optimizer<QAOA<problemSize, problem, P, shots, qpuID>> qaoa_optimizer(&qaoa);
	    	  
	    	  //Repeat execution
      		repeat_execution(qaoa_optimizer, repetitions, output_path);  
					
	        }    
	    		break;

	    	//VQE	
	    	case RH :
	    	case IC :
	    		{	
					VQE<problem, problemSize, shots, qpuID> vqe;
    			Optimizer<VQE<problem, problemSize, shots, qpuID>> vqe_optimizer(&vqe);

    			
    			//Repeat execution
    			repeat_execution(vqe_optimizer, repetitions, output_path); 
	        }   
	    		break;

	    	//Unknown problem selected
	    	default:
	    		std::cout << "QPack error: Unknown problem!" << std::endl;	
	    		
	    }
      	
  		//Message signalling all repetions have been completed
    	std::cout << "DONE         " << std::endl;
    	
	    //Return status
	    return 1;           
        
      }
    };		

    //Loop over graph sizes
    template<index_t start, index_t end, index_t step, index_t index>
    struct qpu_loop
    { 
      template<typename ProblemIndex, typename AlgData> 
      inline constexpr auto operator()(int status, ProblemIndex, AlgData) noexcept
      {	
      	//Set current qpu loop index
	    IndexPass<index> qpuIndex;

	    //Print execution message
	    std::cout << "\tQPU: " << AlgData::qpuIDs[index] << std::endl;
      	
      //Get graph loop parameters bases on problem set
      constexpr auto graph_loop_params = AlgData::graph_loop_params(AlgData::problems[ProblemIndex::getIndex()]);
    	
    	//Loop over graph sizes
	    int graph_status = utils::static_for<graph_loop_params[0], 
	    										  graph_loop_params[1], 
	    										  graph_loop_params[2], 
	    										  benchmark_loops::graphSize_loop>(0, ProblemIndex{}, qpuIndex, AlgData{});
			
	    //Return status
	    return 1;           
        
      }
    };		

    //Loop over graph sizes
    template<index_t start, index_t end, index_t step, index_t index>
    struct problem_loop
    { 
      template<typename AlgData> 
      inline constexpr auto operator()(int status, AlgData) noexcept
      {	
      
      //Set current problem loop index
	    IndexPass<index> problemIndex;

	    //Starting message
	    switch(AlgData::problems[index]){
	    	case MCP :
	    	case MIS :
	    	case DSP :
	    	case TSP :
	    		std::cout << "Running QAOA " << vqa_names[AlgData::problems[index]] << ":" << std::endl;
	    		break;

	    	case RH :
	    	case IC :
	    		std::cout << "Running VQE " << vqa_names[AlgData::problems[index]] << ":" << std::endl;
	    		break;

	    	default:
	    		std::cout << "Unknown Algorithm!" << std::endl;	
	    }
			
			
	    //Loop over problem set
	    constexpr auto qpuSize = sizeof(AlgData::qpuIDs)/sizeof(QDeviceType);
    	int qpu_status = utils::static_for<0, 
    											  qpuSize - 1, 
    											  1, 
    											  benchmark_loops::qpu_loop>(0, problemIndex, AlgData{});					  
	    
	    //Return status
	    return 1;           
        
      }
    };
}    

#endif //BENCHMARKLOOPS_HPP

