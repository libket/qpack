/** @file Qpack/include/VQA.hpp

	@brief Qpack VQA header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef VQA_HPP
#define VQA_HPP

/*
@brief: Base class for the QAOA and VQE classes. Holds shared variables and return fuctions
		for Variatonal Quantum Algorithms in Qpack
*/

/*
Declare VQA problems and their names:
	MaxCut problem (MCP): Find maximum cuts for bipartioned graph
	Dominating Set Problem (DSP): Find minium number of survailance nodes in a network
	Maximum Independent Set problem (MIS): Find maximum set of disconnected nodes
	Traveling Salesperson problem (TSP): Find shortest path in network
	Random Haltonian (RH): A diagonal hamiltonian with random coefficients
 	Ising Chain (IC): Simulations of the 1-D Ising chain
*/
enum vqa_problems {MCP, DSP, TSP, MIS, RH, IC};
std::vector<std::string> vqa_names = {"MCP", "DSP", "TSP", "MIS", "RH", "IC"};

class VQA
{
protected:  
	//Variables to store qpu results
	int _numParams;
	double _execDuration;
	double _queueDuration;
	double _jobDuration;
	double _expectation;

public:
	//Constants
	int id = 9999;
	std::string name = "Generic VQA"; 
	
	//Constructor
	VQA(){};
	
	//Placeholder functions
	void run(std::vector<double> params, bool printCircuit){};
	
	//Return number of parameters
	int getNumParams(){
		return _numParams;
	};

	auto getExecDuration(){
		return _execDuration;
	}

	//Return job duration
	auto getJobDuration(){
		return _jobDuration;
	}

	//Return job duration
	auto getQueueDuration(){
		return _queueDuration;
	}

	//Return expectation value for problem based on graph and qpu results
	auto getExpectation(){
	    return _expectation;
	}
};

#endif //VQA_HPP

