/** @file Qpack/include/VQE.hpp

	@brief Qpack VQE header file

	@author Huub Donkers

	@defgroup qpack libket
*/

#pragma once
#ifndef VQE_HPP
#define VQE_HPP

#include <LibKet.hpp>
#include "../include/VQA.hpp"
#include "../include/VQE_Loops.hpp"
#include "../include/VQE_Hamiltonians.hpp"
#include "../include/VQE_Ansatzes.hpp"

using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

#define PI  3.14159265358979323846  /* pi */


/*
@brief: This class implements all necessary functionalities of the VQE algorithm, such
        as determining the amount of qubits and statespace for a given problem. The class
        is able to generate VQE circuits, execute these circuits and give an expectation
        value for each problem.
*/
template <vqa_problems problem, int problem_size, int shots, QDeviceType qpuID>
class VQE : public VQA{

public:
  
  //Return QPU ID
  static QDeviceType getQpuID(){
    return qpuID;
  }

  //Return problem
  static vqa_problems getProblem(){
    return problem;
  }

  //Return number of shots
  static int getNumShots(){
    return shots;
  }

  //Return P for VQE (not applicable)
  static int getP(){
    return 0;
  }

  //Return problem size
  static int getSize(){
    return problem_size;
  }

  //Returns the number of qubits based on selected problem
  static constexpr auto getNumQubits(){
  return problem == RH ? problem_size   :
         problem == IC ? problem_size   :
         1;
  }

private:
  //Returns the size of the histogram based on selected problem
  static constexpr auto getHistSize(){
  return  2 << getNumQubits();
  }
  
  //Variable to store qpu result
  QArray<getHistSize(), unsigned long> _hist; 
  int _numParams = 1;

  //Struct containing QPU info to pass as constexpr function parameter
  struct QPU_Info{
    static constexpr const int _qubits = getNumQubits();
    static constexpr const int _shots = shots;
    static constexpr const QDeviceType _qpuID = qpuID;
  };

  //Static for loop to retreive Hamiltonian terms
  template<index_t start, index_t end, index_t step, index_t index>
  struct term_loop
  { 
    template<typename Vec, typename H_mol>
    inline constexpr auto operator()(Vec&& vec, H_mol) noexcept
    { 
      vec[index] = H_mol::_terms[index];
      return vec;    
    }
  }; 

  //Converts a Qpack Hamiltonian to an armadillo hamiltonian. 
  //Returns real value of Hamiltonian.
  template<typename H_mol>
  arma::cx_mat arma_hamiltonian(H_mol h_mol){

    //Complex contants (0, i, 1)
  	std::complex<double> c_0(0,0);
  	std::complex<double> c_i(0,1);
    std::complex<double> c_1(1,0);
  	
    //Pauli matrices
  	arma::cx_mat I = {{c_1,  c_0}, {c_0,  c_1}};
  	arma::cx_mat X = {{c_0,  c_1}, {c_1,  c_0}};
  	arma::cx_mat Y = {{c_0, -c_i}, {c_i,  c_0}};
  	arma::cx_mat Z = {{c_1,  c_0}, {c_0, -c_1}};

    //Create an char array that read the terms of the Qpack Hamiltonian
    char empty_vec[H_mol::_numTerms*H_mol::_qubits];
    auto P = utils::static_for<0, H_mol::_numTerms*H_mol::_qubits-1, 1, term_loop>(empty_vec, h_mol);

    //Loop oper all Hamiltonian terms and create armadillo hamiltonian with kronecker products
    int mat_size = 2<<(getNumQubits()-1);
    arma::cx_mat H(mat_size, mat_size);
    for(int t=0; t < H_mol::_numTerms; t++){
      
      arma::cx_mat kronecker;
      for(int q=0; q < H_mol::_qubits; q++){
        
        arma::cx_mat pauli;
        switch(P[t*H_mol::_qubits+q]){
          case 'I':
            pauli = I;
            break;
          case 'X':
            pauli = X;
            break;
          case 'Y':
            pauli = Y;
            break;
          case 'Z':
            pauli = Z;   
            break;
        }
        if(q == 0){ //First product is the first pauli matrix
          kronecker = pauli;
        }
        else{ //Tensor with previous pauli
          kronecker = arma::kron(kronecker, pauli);
        }
      }

      //Scale by term coefficient and sum to total Hamiltonian
      H += h_mol._coeffs[t]*kronecker;
    }

    //Debugging
    //std::cout << real(H) << std::endl;

  	return H;

  }

  //Functions to return ansatz and Hamiltonian based on problem set
  template< typename paramType>
  auto genAnsatz(std::integral_constant<vqa_problems, RH>, paramType params){
    return Ansatzes::genRandomAnsatz<problem_size>(params);
  }

  template< typename paramType>
  auto genAnsatz(std::integral_constant<vqa_problems, IC>, paramType params){
    return Ansatzes::genIsingAnsatz<problem_size>(params);
  }
  
  //Functions to select which Hamiltonian to use based on problem
  auto genHamiltonian(std::integral_constant<vqa_problems, RH>){
    return Hamiltonians::genDiagonalHamiltonian<problem_size>();
  }

  //Functions to select which Hamiltonian to use based on problem
  auto genHamiltonian(std::integral_constant<vqa_problems, IC>){
    return Hamiltonians::genIsingHamiltonian<problem_size>();
  }

  //QAOA functions (TODO: Fix general default)
  template< typename paramType>
  auto genAnsatz(std::integral_constant<vqa_problems, MCP>, paramType params){ return init(); } 

  template< typename paramType>
  auto genAnsatz(std::integral_constant<vqa_problems, MIS>, paramType params){ return init(); } 

  template< typename paramType>
  auto genAnsatz(std::integral_constant<vqa_problems, DSP>, paramType params){ return init(); } 

  template< typename paramType>
  auto genAnsatz(std::integral_constant<vqa_problems, TSP>, paramType params){ return init(); } 

  auto genHamiltonian(std::integral_constant<vqa_problems, MCP>){ return Hamiltonians::genDiagonalHamiltonian<1>();}
  auto genHamiltonian(std::integral_constant<vqa_problems, MIS>){ return Hamiltonians::genDiagonalHamiltonian<1>();}
  auto genHamiltonian(std::integral_constant<vqa_problems, DSP>){ return Hamiltonians::genDiagonalHamiltonian<1>();}
  auto genHamiltonian(std::integral_constant<vqa_problems, TSP>){ return Hamiltonians::genDiagonalHamiltonian<1>();}

public:
  	//Information constant
  	const int id = 1; //VQE ID
  	const std::string name = "VQE";

  	//Constructor
  	VQE(){}

  	//Run VQE circuits
  	template <typename paramType>
  	void run(paramType params, bool printCircuit) {
    
    	//Generate ansatz circuit
    	auto ansatz = genAnsatz(std::integral_constant<vqa_problems, problem>{}, params);

    	//H2 coefficients and Hamiltonian gates
      auto H_mol = genHamiltonian(std::integral_constant<vqa_problems, problem>{});

    	//Run VQE circuits and get expecation value
    	constexpr auto qpu_info = QPU_Info();
      std::vector<float> execDurations = {};
      std::vector<float> jobDurations = {};
      std::vector<float> queueDurations = {};
    	
      float expectation = utils::static_for<0, 0, 1, VQE_Loops::pauli_measurements>(0.0, ansatz, H_mol, qpu_info, &execDurations, &jobDurations, &queueDurations, printCircuit);

      //Store measurements (average of all circuits)
      _execDuration = 0.0;
      _jobDuration = 0.0;
      _queueDuration = 0.0;
      
      for(int i=0; i < execDurations.size(); i++){
        _execDuration += execDurations[i]; 
      }
      _execDuration /= execDurations.size(); 

      for(int i=0; i < jobDurations.size(); i++){
        _jobDuration += jobDurations[i]; 
      }
      _jobDuration /= jobDurations.size();

      for(int i=0; i < queueDurations.size(); i++){
        _queueDuration += queueDurations[i]; 
      }
      _queueDuration /= queueDurations.size();
      
      _expectation = expectation;
  	}

  //Compute classical solution
  float classicalExpectation(){

  	//Check classical solution
  	auto H_mol = genHamiltonian(std::integral_constant<vqa_problems, problem>{});
  	arma::cx_mat H_BK = arma_hamiltonian(H_mol);
  	arma::vec eigval = arma::sort(arma::eig_sym(real(H_BK)));

    //arma::cout << eigval << std::endl;

	return eigval[0];
  }	
   
  //Return histogram results 
  auto getHist(){
   	return _hist;
  }

  //Return number of optimizer parameters
  int getNumParams(){
  	return problem == RH ? problem_size   :
           problem == IC ? problem_size*4 :
           0;
  }

  float getOptimalScore(){
    return classicalExpectation();
  }

  //Return circuit depth
  double getCircuitDepth(){
    return problem == RH ? 2.0 :
           problem == IC ? 4.0 + 2*(problem_size-1) + 0.5:
           0;
  }
};

#endif //VQE_HPP  
