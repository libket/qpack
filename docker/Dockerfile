# This file creates docker containers of the LibKet project
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# The docker containers provide all prerequisite software tools,
# compilers and libraries and a complete build of the LibKet project.

# Set base image
FROM ubuntu:20.04

# This file accepts the following build-time arguments:

# CMAKE_BUILD_TYPE        : {Release,Debug,RelWithDebInfo}
ARG CMAKE_BUILD_TYPE=Release

# Set the variable DEBIAN_FRONTEND to noninteractive. Don't use ENV
# since this sets the variable DEBIAN_FRONTEND also in the container.
ARG DEBIAN_FRONTEND=noninteractive

# LIBKET_BUILD_C_API      : {ON,OFF}
ARG LIBKET_BUILD_C_API=OFF

# LIBKET_BUILD_DOC        : {ON,OFF}
ARG LIBKET_BUILD_DOC=OFF

# LIBKET_BUILD_PYTHON_API : {ON,OFF}
ARG LIBKET_BUILD_PYTHON_API=OFF

# LIBKET_BUILD_EXAMPLES   : {ON,OFF}
ARG LIBKET_BUILD_EXAMPLES=ON

# LIBKET_BUILD_UNITTESTS  : {ON,OFF}
ARG LIBKET_BUILD_UNITTESTS=OFF

# LIBKET_L2R_EVALUATION   : {ON,OFF}
ARG LIBKET_L2R_EVALUATION=OFF

# LIBKET_BUILD_OPTIMIZE_GATES : {ON,OFF}
ARG LIBKET_OPTIMIZE_GATES=ON

# LIBKET_WITH_OPENMP      : {ON,OFF}
ARG LIBKET_WITH_OPENMP=ON

# LIBKET_CIRQ_AQASM       : {ON,OFF}
ARG LIBKET_WITH_AQASM=OFF

# LIBKET_WITH_CIRQ        : {ON,OFF}
ARG LIBKET_WITH_CIRQ=OFF

# LIBKET_WITH_CQASM       : {ON,OFF}
ARG LIBKET_WITH_CQASM=OFF

# LIBKET_WITH_OPENQASM    : {ON,OFF}
ARG LIBKET_WITH_OPENQASM=OFF

# LIBKET_WITH_OPENQL      : {ON,OFF}
ARG LIBKET_WITH_OPENQL=OFF

# LIBKET_WITH_QASM        : {ON,OFF}
ARG LIBKET_WITH_QASM=OFF

# LIBKET_WITH_QUEST       : {ON,OFF}
ARG LIBKET_WITH_QUEST=OFF

# LIBKET_WITH_QUIL        : {ON,OFF}
ARG LIBKET_WITH_QUIL=OFF

# LIBKET_WITH_QX          : {ON,OFF}
ARG LIBKET_WITH_QX=OFF

# LIBKET_VERSION          : {HEAD}
ARG LIBKET_VERSION=HEAD

# Install required debian packages
RUN                               \
  DEBIAN_FRONTEND=noninteractive; \
  apt-get update -y &&            \
  apt-get install -y              \
    build-essential               \
    cmake-curses-gui              \
    cmake                         \
    curl                          \
    git                           \
    libblas-dev                   \
    liblapack-dev                 \
    python3-dev                   \
    sudo                          \
    vim                           \
  &&                              \
  apt-get clean &&                \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Doxygen and Sphinx (if required)
RUN if [ "$LIBKET_BUILD_DOC" = "ON" ] ;           \
    then                                          \
    apt-get update -y &&                          \
    apt-get install -y                            \
      doxygen                                     \
      python3-breathe                             \
      python3-pip                                 \
      python3-sphinx                              \
      texlive-font-utils                          \
    &&                                            \
    apt-get clean &&                              \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    &&                                            \
    pip3 install --upgrade                        \
      pip                                         \
      setuptools                                  \
    &&                                            \
    pip3 install --upgrade                        \
      sphinx_rtd_theme;                           \
    fi

# Install Cirq (if required)
RUN if [ "$LIBKET_WITH_CIRQ" = "ON" ] ;           \
    then                                          \
    apt-get update -y &&                          \
    apt-get install -y                            \
      python3-pip                                 \
    &&                                            \
    apt-get clean &&                              \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    &&                                            \
    pip3 install --upgrade                        \
      pip                                         \
      setuptools                                  \
    &&                                            \
    pip3 install --upgrade                        \
      cirq;                                       \
    fi

# Install cQASM (if required)
RUN if [ "$LIBKET_WITH_CQASM" = "ON" ] ;          \
    then                                          \
    apt-get update -y &&                          \
    apt-get install -y                            \
      python3-pip                                 \
    &&                                            \
    apt-get clean &&                              \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    &&                                            \
    pip3 install --upgrade                        \
      pip                                         \
      setuptools                                  \
    &&                                            \
    pip3 install --upgrade                        \
      quantuminspire;                             \
    fi

# Install OpenQASM (if required)
RUN if [ "$LIBKET_WITH_OPENQASM" = "ON" ] ;       \
    then                                          \
    apt-get update -y &&                          \
    apt-get install -y                            \
      python3-pip                                 \
    &&                                            \
    apt-get clean &&                              \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    &&                                            \
    pip3 install --upgrade                        \
      pip                                         \
      setuptools                                  \
    &&                                            \
    pip3 install --upgrade                        \
      qiskit;                                     \
    fi

# Install OpenQL (if required)
RUN if [ "$LIBKET_WITH_OPENQL" = "ON" ] ;         \
    then                                          \
    apt-get update -y &&                          \
    apt-get install -y                            \
      bison                                       \
      flex                                        \
      swig                                        \
    &&                                            \
    apt-get clean &&                              \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*;\
    fi

# Install Quil (if required)
RUN if [ "$LIBKET_WITH_QUIL" = "ON" ] ;           \
    then                                          \
    apt-get update -y &&                          \
    apt-get install -y                            \
      python3-pip                                 \
    &&                                            \
    apt-get clean &&                              \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    &&                                            \
    pip3 install --upgrade                        \
      pip                                         \
      setuptools                                  \
    &&                                            \
    pip3 install --upgrade                        \
      pyquil;                                     \
    fi

# Add and enable the default user
ARG USER=libket
RUN adduser --disabled-password --gecos '' $USER
RUN adduser $USER sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Make sure everything is in place
RUN chown -R $USER:$USER /home/$USER
USER $USER
ENV HOME /home/$USER
ENV USER $USER
WORKDIR $HOME

# Check out the LibKet project
RUN if [ "$LIBKET_VERSION" = "HEAD" ] ; \
    then \
    git clone https://gitlab.com/mmoelle1/LibKet.git LibKet && cd LibKet && git checkout matthias_branch; \
    else \
    git clone https://gitlab.com/mmoelle1/LibKet.git LibKet && cd LibKet && git fetch --tags && git checkout $LIBKET_VERSION; \
    fi

# Configure and build LibKet project
RUN cd LibKet   &&                                                \
    git submodule init &&                                         \
    git submodule update --recursive -- external/armadillo &&     \
    git submodule update --recursive -- external/optim &&         \
    git submodule update --recursive -- external/pegtl &&         \
    git submodule update --recursive -- external/unittest-cpp &&  \
    git submodule update --recursive -- external/universal &&     \
    if [ "$LIBKET_BUILD_PYTHON_API" = "ON" ] ;                    \
    then                                                          \
    git submodule update --recursive -- external/pybind11;        \
    fi &&                                                         \
    if [ "$LIBKET_WITH_OPENQL" = "ON" ] ;                         \
    then                                                          \
    git submodule update --recursive -- external/OpenQL;          \
    fi &&                                                         \
    if [ "$LIBKET_WITH_QUEST" = "ON" ] ;                          \
    then                                                          \
    git submodule update --recursive -- external/QuEST;           \
    fi &&                                                         \
    if [ "$LIBKET_WITH_QX" = "ON" ] ;                             \
    then                                                          \
    git submodule update --recursive -- external/qx-simulator;    \
    fi &&                                                         \
    mkdir build &&                                                \
    cd build    &&                                                \
    cmake ..                                                      \
        -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE                      \
        -DLIBKET_BUILD_C_API=$LIBKET_BUILD_C_API                  \
        -DLIBKET_BUILD_PYTHON_API=$LIBKET_BUILD_PYTHON_API        \
        -DLIBKET_BUILD_EXAMPLES=$LIBKET_BUILD_EXAMPLES            \
        -DLIBKET_BUILD_UNITTESTS=$LIBKET_BUILD_UNITTESTS          \
        -DLIBKET_L2R_EVALUATION=$LIBKET_L2R_EVALUATION            \
        -DLIBKET_OPTIMIZE_GATES=$LIBKET_OPTIMIZE_GATES            \
        -DLIBKET_WITH_OPENMP=$LIBKET_WITH_OPENMP                  \
        -DLIBKET_WITH_AQASM=$LIBKET_WITH_AQASM                    \
        -DLIBKET_WITH_CIRQ=$LIBKET_WITH_CIRQ                      \
        -DLIBKET_WITH_CQASM=$LIBKET_WITH_CQASM                    \
        -DLIBKET_WITH_OPENQASM=$LIBKET_WITH_OPENQASM              \
        -DLIBKET_WITH_OPENQL=$LIBKET_WITH_OPENQL                  \
        -DLIBKET_WITH_QASM=$LIBKET_WITH_QASM                      \
        -DLIBKET_WITH_QUEST=$LIBKET_WITH_QUEST                    \
        -DLIBKET_WITH_QUIL=$LIBKET_WITH_QUIL                      \
        -DLIBKET_WITH_QX=$LIBKET_WITH_QX &&                       \
    make &&                                                       \
    if [ "$LIBKET_BUILD_DOC" = "ON" ] ;                           \
    then                                                          \
    make docs;                                                    \
    fi

# Add directory of executables to search path
ENV PATH $PATH:$HOME/LibKet/build/examples:$HOME/LibKet/build/unittests/AQASM:$HOME/LibKet/build/unittests/Cirq:$HOME/LibKet/build/unittests/OpenQASM:$HOME/LibKet/build/unittests/OpenQL:$HOME/LibKet/build/unittests/QASM:$HOME/LibKet/build/unittests/QX:$HOME/LibKet/build/unittests/QuEST:$HOME/LibKet/build/unittests/Quil:$HOME/LibKet/build/unittests/cQASM

# Default command
CMD [ "/bin/bash" ]
