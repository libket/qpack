########################################################################
# QX.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# QX
########################################################################

include(FetchContent)
FetchContent_Declare(
  qx-simulator
  URL https://github.com/QE-Lab/qx-simulator/archive/refs/tags/v0.4.2.zip
  )

if (LIBKET_BUILTIN_QX)

  FetchContent_GetProperties(qx-simulator)
  if(NOT qx-simulator_POPULATED)
    message(STATUS "Fetching QX-simulator")
    FetchContent_Populate(qx-simulator)
    message(STATUS "Fetching QX-simulator -- done")
  endif()
  
  set(QX_BINARY_DIR ${qx-simulator_BINARY_DIR})
  set(QX_SOURCE_DIR ${qx-simulator_SOURCE_DIR})
  
  # Add include directory
  include_directories("${QX_SOURCE_DIR}/src" "${QX_SOURCE_DIR}/include" "${QX_SOURCE_DIR}/src/xpu-0.1.5")
  
else()

  # Add include directory
  if(QX_INCLUDE_PATH)
    include_directories(${QX_INCLUDE_PATH})
  else()
    message(WARNING "Variable QX_INCLUDE_PATH is not defined. LibKet might be unable to find QX include files.")
  endif()
    
endif()

# Enable QX simulator support
if(${CMAKE_VERSION} VERSION_LESS "3.12.0")
  add_definitions(-DCG_BC -DQX_SPARSE_MV_MUL -DXPU_TIMER -D__BUILTIN_LINALG__)
else()
  add_compile_definitions(CG_BC QX_SPARSE_MV_MUL XPU_TIMER __BUILTIN_LINALG__)
endif()

# QX needs SSE support
if(MSVC)
  add_compile_options(/arch:SSE3)
else()
  add_compile_options(-msse3)
endif()
