########################################################################
# QuEST.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# QuEST
########################################################################

include(FetchContent)
FetchContent_Declare(
  quest
  URL https://github.com/QuEST-Kit/QuEST/archive/refs/tags/v3.3.0.zip
  )

if (LIBKET_BUILTIN_QUEST)
  
  FetchContent_GetProperties(quest)
  if(NOT quest_POPULATED)
    message(STATUS "Fetching QuEST")
    FetchContent_Populate(quest)
    message(STATUS "Fetching QuEST -- done")
  endif()
  
  set(QUEST_BINARY_DIR ${quest_BINARY_DIR})
  set(QUEST_SOURCE_DIR ${quest_SOURCE_DIR})
  
  set(TESTING 0 CACHE BOOL "")
  
  # Add include directory
  include_directories("${QUEST_SOURCE_DIR}/QuEST/include")
  
else()

  # Add include directory
  if(QUEST_INCLUDE_PATH)
    include_directories(${QUEST_INCLUDE_PATH})
  else()
    message(WARNING "Variable QUEST_INCLUDE_PATH is not defined. LibKet might be unable to find QuEST include files.")
  endif()
    
endif()

# Process QuEST project
if (NOT TARGET QuEST)
  add_subdirectory(${QUEST_SOURCE_DIR} ${QUEST_BINARY_DIR})
endif()

# Get compile definitions
get_target_property(QUEST_COMPILE_DEFINITIONS QuEST COMPILE_DEFINITIONS)

# Add include directory
include_directories("${QUEST_SOURCE_DIR}/QuEST/include")

# Add library
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   QuEST)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES QuEST)
