########################################################################
# add_executables.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

#
# CMake macro: add executables for each source file
#
# Remark: The source files must be given with relative paths
#
macro(add_executables SRC)
  
  # Loop over enabled languages
  foreach(lang C CXX)

    # Determine file extensions
    if(${lang} STREQUAL C)
      set(SUFFIX "\\.c$")
    elseif(${lang} STREQUAL CXX)
      set(SUFFIX "\\.(C|c\\+\\+|cc|cpp|cxx)$")
    else()
      message(ERROR "Language ${lang} not recognized")
    endif()
    
    # Loop over source files
    foreach(src ${SRC})
      
      # Filter files according to file suffix
      string(REGEX MATCH "[^/|\\].*${SUFFIX}" src ${src})
      
      if(src)       
        # Remove file suffix     
        string(REGEX REPLACE ${SUFFIX} "" name ${src})
        
        # Add executable.
        add_executable(${name} ${src} ${LIBKET_HEADERS})

        # Add support for precompiled headers
        if(LIBKET_USE_PCH)
          if (NOT ${CMAKE_VERSION} VERSION_LESS "3.16.0")
            if(NOT ${name} STREQUAL "libket_${lang}_pch" AND
                NOT ${name} STREQUAL "libket_JIT_pch")
              target_precompile_headers(${name} REUSE_FROM "libket_${lang}_pch")
            endif()
          else()
            message(SEND_ERROR "Precompiled headers require CMake 3.16.0 or better")
          endif()
        endif()  
                
        # Set install rule
        string(REGEX REPLACE "^/.*/" "" appdir ${CMAKE_CURRENT_LIST_DIR})
        install(TARGETS ${name} DESTINATION ${appdir}/bin)

        # Set library dependencies
        if(LIBKET_${lang}_TARGET_DEPENDENCIES)
          add_dependencies(${name} ${LIBKET_${lang}_TARGET_DEPENDENCIES})
        endif()
        
        # Link to DL library
        target_link_libraries(${name} ${CMAKE_DL_LIBS})

        # Link to libraries
        if(LIBKET_${lang}_TARGET_LINK_LIBRARIES)
          target_link_libraries(${name} ${LIBKET_${lang}_TARGET_LINK_LIBRARIES})
        endif()

        # Link to C API
        if(${lang} STREQUAL C)
          target_link_libraries(${name} ${LIBKET_C_API_LIBRARY})
        endif()
        
      endif()
      
    endforeach()
  endforeach()
endmacro()
