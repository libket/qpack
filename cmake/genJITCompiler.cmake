########################################################################
# genJITCompier.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

#
# CMake macro: generate just-in-time compiler configuration
#
# Remark: The source files must be given with relative paths
#
macro(genJITCompiler)

  # Set JIT compiler command
  set(JIT_CXX_COMPILER ${CMAKE_CXX_COMPILER})


  # Get build-type as upper-case string
  string(TOUPPER ${CMAKE_BUILD_TYPE} JIT_BUILD_TYPE)

  # Set JIT compiler flags (build-type dependent)
  set(JIT_CXX_FLAGS ${CMAKE_CXX_FLAGS_${JIT_BUILD_TYPE}})
 
  # Set additional compile definitions
  get_directory_property(JIT_COMPILE_DEFINITIONS COMPILE_DEFINITIONS)
  if (JIT_COMPILE_DEFINITIONS)
    foreach (flag ${JIT_COMPILE_DEFINITIONS})
      set (JIT_CXX_FLAGS "${JIT_CXX_FLAGS} -D${flag}")
    endforeach()
  endif()

  if (QUEST_COMPILE_DEFINITIONS)
    foreach (flag ${QUEST_COMPILE_DEFINITIONS})
      set (JIT_CXX_FLAGS "${JIT_CXX_FLAGS} -D${flag}")
    endforeach()
  endif()
  
  # Set additional compile options
  if (TARGET libket_CXX_pch)
    get_target_property(JIT_COMPILE_OPTIONS libket_CXX_pch COMPILE_OPTIONS)
    if (JIT_COMPILE_OPTIONS)
      foreach (flag ${JIT_COMPILE_OPTIONS})
        set (JIT_CXX_FLAGS "${JIT_CXX_FLAGS} ${flag}")
      endforeach()
    endif()
  endif()

  if (QUEST_COMPILE_OPTIONS)
    foreach (flag ${QUEST_COMPILE_OPTIONS})
      set (JIT_CXX_FLAGS "${JIT_CXX_FLAGS} ${flag}")
    endforeach()
  endif()

  # Set SYSROOT on MacOS
  if (APPLE)
    set(JIT_CXX_FLAGS "${JIT_CXX_FLAGS} -isysroot ${CMAKE_OSX_SYSROOT}")
  endif()

  # Create a set of shared library variable specific to C++
  # For 90% of the systems, these are the same flags as the C versions
  # so if these are not set just copy the flags from the c version
  if(NOT DEFINED CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS)
    set(JIT_CXX_FLAGS "${JIT_CXX_FLAGS} ${CMAKE_CXX_FLAGS} ${CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS}")
  else()
    set(JIT_CXX_FLAGS "${JIT_CXX_FLAGS} ${CMAKE_CXX_FLAGS} ${CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS}")
  endif()

  # Add C++ standard and PIC (position independent code)
  if(NOT DEFINED CMAKE_CXX_EXTENSIONS OR NOT CMAKE_CXX_EXTENSIONS)
    set(JIT_CXX_FLAGS "${JIT_CXX_FLAGS} ${CMAKE_CXX${CMAKE_CXX_STANDARD}_STANDARD_COMPILE_OPTION} ${CMAKE_CXX_COMPILE_OPTIONS_PIC}")
  else()
    set(JIT_CXX_FLAGS "${JIT_CXX_FLAGS} ${CMAKE_CXX${CMAKE_CXX_STANDARD}_EXTENSION_COMPILE_OPTION} ${CMAKE_CXX_COMPILE_OPTIONS_PIC}")
  endif()

  # Fix visibility
  string(REPLACE "-fvisibility=hidden"         "" JIT_CXX_FLAGS ${JIT_CXX_FLAGS})
  string(REPLACE "-fvisibility-inlines-hidden" "" JIT_CXX_FLAGS ${JIT_CXX_FLAGS})

  # Generate list of include directories
  get_property(LIBKET_INCLUDE_DIRECTORIES DIRECTORY PROPERTY INCLUDE_DIRECTORIES)
  string(REPLACE ";" " -I" JIT_INCLUDE_DIRECTORIES "-I${LIBKET_INCLUDE_DIRECTORIES}")
  string(REPLACE ";" " /I" JIT_INCLUDE_DIRECTORIES_WIN32 "/I${LIBKET_INCLUDE_DIRECTORIES}")

  # Generate list of external libraries
  get_property(LIBKET_LINK_DIRECTORIES DIRECTORY PROPERTY LINK_DIRECTORIES)

  # Set precompiled headers
  if(LIBKET_USE_PCH)
    if (NOT ${CMAKE_VERSION} VERSION_LESS "3.16.0")       
      file(GENERATE 
        OUTPUT "${PROJECT_BINARY_DIR}/libket/QJITCompiler.pch"
        CONTENT "\#define PCH_FILE \"$<FILTER:$<TARGET_OBJECTS:libket_JIT_pch>,INCLUDE, *.${CMAKE_PCH_EXTENSION}>\"\n\#define PCH_EXTENSION \"${CMAKE_PCH_EXTENSION}\""
        )

      string(REPLACE ";" " " JIT_PCH_FLAGS "${CMAKE_CXX_COMPILE_OPTIONS_USE_PCH}")
      string(REPLACE "<PCH_FILE>" "\" + std::string(PCH_FILE).erase(std::string(PCH_FILE).rfind(\";\")) + \"" JIT_PCH_FLAGS "${JIT_PCH_FLAGS}")
      string(REPLACE "<PCH_HEADER>" "\" + std::string(PCH_FILE).erase(std::string(PCH_FILE).rfind(PCH_EXTENSION)) + \"" JIT_PCH_FLAGS "${JIT_PCH_FLAGS}")
      set(JIT_CXX_FLAGS "${JIT_CXX_FLAGS} ${JIT_PCH_FLAGS}")
    else()
      message(SEND_ERROR "Precompiled headers require CMake 3.16.0 or better")
    endif()
  endif()

  # Set (extra) library directories
  if(LIBKET_LINK_DIRECTORIES)
    string(REPLACE ";" " -L" JIT_LIBRARIES "-L${LIBKET_LINK_DIRECTORIES}")
    string(REPLACE ";" " /L" JIT_LIBRARIES_WIN32 "/L${LIBKET_LINK_DIRECTORIES}")
  endif(LIBKET_LINK_DIRECTORIES)


  # Set (extra) link libraries
  foreach(lib ${LIBKET_CXX_TARGET_LINK_LIBRARIES})
    continue()
    if(lib STREQUAL "Threads::Threads")
      # Fix imported target Threads::Threads
      continue()
    endif()

    if (NOT IS_ABSOLUTE ${lib})
      set(JIT_LIBRARIES "${JIT_LIBRARIES} -l${lib}")
      set(JIT_LIBRARIES_WIN32 "${JIT_LIBRARIES_WIN32} /l${lib}")
    else()
      set(JIT_LIBRARIES "${JIT_LIBRARIES} ${lib}")
      set(JIT_LIBRARIES_WIN32 "${JIT_LIBRARIES_WIN32} ${lib}")
    endif()
  endforeach()

  # Generate QJITCompiler source file
  configure_file(${PROJECT_SOURCE_DIR}/libket/QJITCompiler.hpp.in
    ${PROJECT_BINARY_DIR}/libket/QJITCompiler.hpp @ONLY)
endmacro()
