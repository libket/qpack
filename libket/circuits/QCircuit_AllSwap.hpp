/** @file libket/circuits/QCircuit_AllSwap.hpp

    @brief C++ API quantum AllSwap circuit class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup allswap AllSwap circuit

    @ingroup circuits
 */

#pragma once
#ifndef QCIRCUIT_ALLSWAP_HPP
#define QCIRCUIT_ALLSWAP_HPP

#include <QFilter.hpp>
#include <QGates.hpp>
#include <circuits/QCircuit.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {

/**
@brief AllSwap circuit class

The AllSwap circuit class implements an algorithm that swaps all
qubits in a given selection as it is done, for instance, as last part
of the quantum Fourier transform

@ingroup allswap
*/
class QCircuit_AllSwap : public QCircuit
{
private:
  
  /// AllSwap functor - recursion
  template<index_t start, index_t end, index_t step, index_t index>
  struct allswap_loop
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return swap(sel<start + (index >> 1)>(gototag<0>(expr0)),
                  sel<end - (index >> 1)>(gototag<0>(expr1)));
    }
  };

  /// AllSwap functor - terminal
  template<index_t start, index_t end, index_t step>
  struct allswap_loop<start, end, step, end>
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return expr0;
    }
  };

  /// AllSwap implementation - forward declaration
  template<std::size_t _qubits,
           typename _filter,
           QBackendType _qbackend,
           bool = (_filter::template size<_qubits>() >= 2)>
  struct allswap_impl;

  /// AllSwap implementation - recursion
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  struct allswap_impl<_qubits, _filter, _qbackend, true>
  {
    /// Apply function
    inline static auto apply(QExpression<_qubits, _qbackend>& expr) noexcept
    {
      auto e = gototag<0>(
        utils::static_for<0,
                          (index_t)_filter::template size<_qubits>() - 1,
                          2,
                          allswap_loop>(tag<0>(_filter{}), tag<0>(_filter{})));
      return e(expr);
    }
  };

  /// AllSwap implementation - terminal
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  struct allswap_impl<_qubits, _filter, _qbackend, false>
  {
    /// Apply function
    inline static auto apply(QExpression<_qubits, _qbackend>& expr) noexcept
    {
      return expr;
    }
  };

public:
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;
  
  /// Shows circuit
  template<std::size_t level = 1>
  std::string show() const noexcept
  {
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }
  
  /// Apply function
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static auto apply(QExpression<_qubits, _qbackend>& expr) noexcept
    -> QExpression<_qubits, _qbackend>&
  {
    expr = allswap_impl<_qubits, _filter, _qbackend>::apply(expr);
    return expr;
  }
};

///@ingroup allswap
///@{
  
/**
 @brief Serialize operation
*/
std::ostream&
operator<<(std::ostream& os, const QCircuit_AllSwap& circuit)
{
  os << "allswap(";
  return os;
}

/**
@brief AllSwap circuit creator

This overload of the LibKet::circuits::allswap() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::allswap();
\endcode
*/
inline constexpr auto
allswap() noexcept
{
  return UnaryQGate<filters::QFilter, QCircuit_AllSwap>(filters::QFilter{});
}

/**
@brief AllSwap circuit creator

This overload of the LibKet::circuits::allswap() function accepts
an expression as constant reference

\code
auto qcirc = circuits::allswap(expr);
\endcode
*/
template<typename _expr>
inline constexpr auto
allswap(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_AllSwap,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief AllSwap circuit creator

This overload of the LibKet::circuits::allswap() function accepts
an expression as universal reference

\code
auto qcirc = circuits::allswap(expr);
\endcode
*/
template<typename _expr>
inline constexpr auto
allswap(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_AllSwap,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief AllSwap circuit creator

Function alias for LibKet::circuits::allswap()

\code
auto qcirc = circuits::ALLSWAP(expr);
\endcode
*/
template<typename... Args>
inline constexpr auto
ALLSWAP(Args&&... args)
{
  return allswap(std::forward<Args>(args)...);
}

///@}
  
/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QCircuit_AllSwap::operator()(const T& t) const noexcept
{
  return allswap(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QCircuit_AllSwap::operator()(T&& t) const noexcept
{
  return allswap(std::forward<T>(t));
}

/**
   @brief Shows circuit - specialization for AllSwap class

   @ingroup allswap
*/
template<std::size_t level = 1>
inline static auto
show(const QCircuit_AllSwap& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "AllSwap\n";

  return circuit;
}

} // namespace circuits

} // namespace LibKet

#endif // QCIRCUIT_ALLSWAP_HPP
