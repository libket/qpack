/** @file libket/circuits/QCircuit_QFTdag.hpp

    @brief C++ API inverse quantum Fourier transform circuit class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers

    @defgroup qftdag Quantum Fourier transform

    @ingroup circuits
 */

#pragma once
#ifndef QCIRCUIT_QFTDAG_HPP
#define QCIRCUIT_QFTDAG_HPP

#include <QFilter.hpp>
#include <QGates.hpp>
#include <QUtils.hpp>
#include <circuits/QCircuit.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {

/**
 @brief Inverse quantum Fourier transform circuit versions

 The inverse quantum Fourier transform circuit version enumerator
 defines the different versions of the inverse QFT circuit

 @ingroup qftdag
*/
enum class QFTdagMode
{
  /**
     @brief Standard QFTdag (including all-swap)
  */
  standard,

  /**
     @brief QFTdag without all-swap
  */
  noswap
};

// Forward declaration
enum class QFTMode;
template<QFTMode _qft, typename _tol>
class QCircuit_QFT;

/**
@brief Inverse quantum Fourier transform circuit class

The QFT dagger circuit class implements the inverse quantum Fourier
transform for an arbitrary number of qubits

@ingroup qftdag
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t>
class QCircuit_QFTdag : public QCircuit
{
private:
  /// Inverse QFT inner-loop functor
  template<index_t start, index_t end, index_t step, index_t index>
  struct qftdag_loop_inner
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return crkdag<index - end + 2, _tol>(sel<index>(gototag<0>(expr0)),
                                           sel<end - 1>(gototag<0>(expr1)));
    }
  };

  /// Inverse QFT outer-loop functor
  template<index_t start, index_t end, index_t step, index_t index>
  struct qftdag_loop_outer
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return utils::static_for<start, index, -1, qftdag_loop_inner>(
        h(sel<index>(gototag<0>(expr0))), gototag<0>(expr1));
    }
  };

public:
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Shows circuit
  template<std::size_t level = 1>
  std::string show() const noexcept
  {
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }
  
  /// Apply function
  template<std::size_t _qubits,
           typename _filter,
           enum QFTdagMode __qftdag = _qftdag,
           QBackendType _qbackend>
  inline static auto apply(QExpression<_qubits, _qbackend>& expr) noexcept ->
    typename std::enable_if<__qftdag == QFTdagMode::standard,
                            QExpression<_qubits, _qbackend>>::type&
  {
    auto e = gototag<0>(h(sel<0>(gototag<0>(
      utils::static_for<(index_t)_filter::template size<_qubits>() - 1,
                        1,
                        -1,
                        qftdag_loop_outer>(
        gototag<0>(allswap(tag<0>(_filter{}))), tag<0>(_filter{}))))));
    return e(expr);
  }

  /// Apply function
  template<std::size_t _qubits,
           typename _filter,
           enum QFTdagMode __qftdag = _qftdag,
           QBackendType _qbackend>
  inline static auto apply(QExpression<_qubits, _qbackend>& expr) noexcept ->
    typename std::enable_if<__qftdag == QFTdagMode::noswap,
                            QExpression<_qubits, _qbackend>>::type&

  {
    auto e = gototag<0>(h(sel<0>(gototag<0>(
      utils::static_for<(index_t)_filter::template size<_qubits>() - 1,
                        1,
                        -1,
                        qftdag_loop_outer>(tag<0>(_filter{}),
                                           tag<0>(_filter{}))))));
    return e(expr);
  }
};

  ///@ingroup qftdag
  ///@{
  
  /**
     @brief Serialize operator
  */
template<QFTdagMode _qftdag, typename _tol>
std::ostream&
operator<<(std::ostream& os, const QCircuit_QFTdag<_qftdag, _tol>& circuit)
{
  os << "qftdag<"
     << (_qftdag == QFTdagMode::standard ? "standard" : "noswap")
     << ">(QConst("
     << _tol::to_string()
     << "),";
  return os;
}
  
#ifdef LIBKET_OPTIMIZE_GATES

/**
   @brief Inverse AFT circuit creator

   This overload of the LibKet::circuits::qftdag() function eliminates
   the application of the inverse QFT circuit to its adjoint, the QFT
   circuit, for the case that QFT and inverse QFT are either both of
   standard or noswap type
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(
  const UnaryQGate<_expr,
                   QCircuit_QFT<(enum QFTMode)_qftdag, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
 @brief Inverse QFT circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the inverse QFT circuit to its adjoint, the QFT
 circuit, for the case that QFT and inverse QFT are either both of
 standard or noswap type
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(UnaryQGate<_expr,
                  QCircuit_QFT<(enum QFTMode)_qftdag, _tol>,
                  typename filters::getFilter<_expr>::type>&& expr) noexcept
{
  return expr.expr;
}

/**
 @brief Inverse QFT circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the inverse QFT circuit to its adjoint, the QFT
 circuit, for the special case that inverse QFT is of standard type
 and QFT is of noswap type so that the optimized circuite is simple
 the swap from the inverse QFT
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(
  const UnaryQGate<_expr,
                   QCircuit_QFT<QFTMode::noswap, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::standard,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}

/**
 @brief Inverse QFT circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the inverse QFT circuit to its adjoint, the QFT
 circuit, for the special case that inverse QFT is of standard type
 and QFT is of noswap type so that the optimized circuite is simple
 the swap from the inverse QFT
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(UnaryQGate<_expr,
                  QCircuit_QFT<QFTMode::noswap, _tol>,
                  typename filters::getFilter<_expr>::type>&& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::standard,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}

/**
 @brief Inverse QFT circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the inverse QFT circuit to its adjoint, the QFT
 circuit, for the special case that inverse QFT is of noswap type
 and QFT is of standard type so that the optimized circuite is simple
 the swap from the inverse QFT
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(
  const UnaryQGate<_expr,
                   QCircuit_QFT<QFTMode::standard, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::noswap,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}

/**
 @brief Inverse QFT circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the inverse QFT circuit to its adjoint, the QFT
 circuit, for the special case that the inverse QFT is of noswap type
 and QFT is of standard type so that the optimized circuite is simple
 the swap from the inverse QFT
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(UnaryQGate<_expr,
                  QCircuit_QFT<QFTMode::standard, _tol>,
                  typename filters::getFilter<_expr>::type>&& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::noswap,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}
#endif // LIBKET_OPTIMIZE_GATES

/**
@brief Inverse QFT circuit creator

This overload of the LibKet::circuits::qftdag() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qftdag();
\endcode
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t>
inline constexpr auto
qftdag() noexcept
{
  return UnaryQGate<filters::QFilter, QCircuit_QFTdag<_qftdag, _tol>>(
    filters::QFilter{});
}

/**
@brief Inverse QFT circuit creator

This overload of the LibKet::circuits::qftdag() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qftdag(expr);
\endcode
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_QFTdag<_qftdag, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief Inverse QFT circuit creator

This overload of the LibKet::circuits::qftdag() function accepts
an expression as universal reference

\code
auto qcirc = circuits::qftdag(expr);
\endcode
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename _expr>
inline constexpr auto
qftdag(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_QFTdag<_qftdag, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief Inverse QFT circuit creator

Function alias for the LibKet::circuits::qftdag() function

\code
auto qcirc = circuits::QFTdag(expr);
\endcode
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_M_ZERO_t,
         typename... Args>
inline constexpr auto
QFTdag(Args&&... args)
{
  return qftdag<_qftdag, _tol>(std::forward<Args>(args)...);
}

///@}

/// Operator() - by constant reference
template<QFTdagMode _qftdag, typename _tol>
template<typename T>
inline constexpr auto
QCircuit_QFTdag<_qftdag, _tol>::operator()(const T& t) const noexcept
{
  return qftdag(std::forward<T>(t));
}

/// Operator() - by universal reference
template<QFTdagMode _qftdag, typename _tol>
template<typename T>
inline constexpr auto
QCircuit_QFTdag<_qftdag, _tol>::operator()(T&& t) const noexcept
{
  return qftdag(std::forward<T>(t));
}

/**
   @brief Shows circuit

   @note specialization for inverse QFT

   @ingroup qftdag
*/
template<std::size_t level = 1,
         enum QFTdagMode _qftdag,
         typename _tol = QConst_M_ZERO_t>
inline static auto
show(const QCircuit_QFTdag<_qftdag, _tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QFTdag\n";

  return circuit;
}

} // namespace circuits

} // namespace LibKet

#endif // QCIRCUIT_QFTdag_HPP
