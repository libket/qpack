/** @file libket/QJob.hpp

    @brief C++ API quantum job execution class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup cxx_api
*/

#pragma once
#ifndef QJOB_HPP
#define QJOB_HPP

#include <chrono>
#include <functional>
#include <future>
#include <thread>

#ifdef LIBKET_WITH_PYTHON

// Remove keyword 'register' which is deprecated in C++11
#if __cplusplus > 199711L
#define register
#endif

#include <Python.h>
#endif

namespace LibKet {

/** @namespace LibKet::detail

  @brief
  The LibKet::detail namespace, containing implementation details

  The LibKet::detail namespace contains implementation details that
  are not exposed to the end-user. Functionality in this namespace
  does not have a stable API over future LibKet releases and can
  change without notification.
*/
namespace detail {
#ifdef LIBKET_WITH_PYTHON

template<typename T>
T
py_cast(PyObject* obj);

template<>
short
py_cast<short>(PyObject* obj)
{
  return PyLong_AsLong(obj);
}

template<>
unsigned short
py_cast<unsigned short>(PyObject* obj)
{
  return PyLong_AsUnsignedLong(obj);
}

template<>
int
py_cast<int>(PyObject* obj)
{
  return PyLong_AsLong(obj);
}

template<>
unsigned int
py_cast<unsigned int>(PyObject* obj)
{
  return PyLong_AsUnsignedLong(obj);
}

template<>
long
py_cast<long>(PyObject* obj)
{
  return PyLong_AsLong(obj);
}

template<>
unsigned long
py_cast<unsigned long>(PyObject* obj)
{
  return PyLong_AsUnsignedLong(obj);
}

template<>
long long
py_cast<long long>(PyObject* obj)
{
  return PyLong_AsLongLong(obj);
}

template<>
unsigned long long
py_cast<unsigned long long>(PyObject* obj)
{
  return PyLong_AsUnsignedLongLong(obj);
}

template<>
void*
py_cast<void*>(PyObject* obj)
{
  return PyLong_AsVoidPtr(obj);
}

template<>
float
py_cast<float>(PyObject* obj)
{
  return PyFloat_AsDouble(obj);
}

template<>
double
py_cast<double>(PyObject* obj)
{
  return PyFloat_AsDouble(obj);
}

template<>
long double
py_cast<long double>(PyObject* obj)
{
  return PyFloat_AsDouble(obj);
}

template<>
std::string
py_cast<std::string>(PyObject* obj)
{
  return PyBytes_AsString(PyUnicode_AsUTF8String(obj));
}

template<>
nlohmann::json
py_cast<nlohmann::json>(PyObject* obj)
{
  return nlohmann::json::parse(PyBytes_AsString(PyUnicode_AsUTF8String(obj)));
}

template<typename T>
PyObject*
py_cast(T obj);

template<>
PyObject*
py_cast<short>(short val)
{
  return PyLong_FromLong(val);
}

template<>
PyObject*
py_cast<unsigned short>(unsigned short val)
{
  return PyLong_FromUnsignedLong(val);
}

template<>
PyObject*
py_cast<int>(int val)
{
  return PyLong_FromLong(val);
}

template<>
PyObject*
py_cast<unsigned int>(unsigned int val)
{
  return PyLong_FromUnsignedLong(val);
}

template<>
PyObject*
py_cast<long>(long val)
{
  return PyLong_FromLong(val);
}

template<>
PyObject*
py_cast<unsigned long>(unsigned long val)
{
  return PyLong_FromUnsignedLong(val);
}

template<>
PyObject*
py_cast<long long>(long long val)
{
  return PyLong_FromLongLong(val);
}

template<>
PyObject*
py_cast<unsigned long long>(unsigned long long val)
{
  return PyLong_FromUnsignedLongLong(val);
}

template<>
PyObject*
py_cast<void*>(void* val)
{
  return PyLong_FromVoidPtr(val);
}

template<>
PyObject*
py_cast<float>(float val)
{
  return PyLong_FromDouble(val);
}

template<>
PyObject*
py_cast<double>(double val)
{
  return PyLong_FromDouble(val);
}

template<>
PyObject*
py_cast<long double>(long double val)
{
  return PyLong_FromDouble(val);
}

template<>
PyObject*
py_cast<const std::string&>(const std::string& val)
{
  return PyBytes_FromString(val.c_str());
}

template<>
PyObject*
py_cast<std::string>(std::string val)
{
  return PyBytes_FromString(val.c_str());
}

template<>
PyObject*
py_cast<const char*>(const char* val)
{
  return PyBytes_FromString(val);
}

#else

template<typename T, typename U>
T py_cast(U)
{
  LIBKET_ERROR("This feature requires -DLIBKET_WITH_PYTHON");
}

#endif

} // namespace detail

/**
   @brief Quantum job base class

   The Quantum job base class is an abstract class that
   specifies the virtual member functions that need to be implemented
   by any quantum job class
*/
class QJobBase
{
public:
  /// (Re-)runs the quantum job
  virtual std::future<void> run(const QJobBase* depend = NULL) = 0;

  /// Waits for quantum job to complete
  virtual QJobBase* wait() const = 0;

  /// Returns true if quantum job has completed, or false if not
  virtual bool query() const = 0;
};

/**
   @brief Quantum job executor enumerator

   The Quantum job executor enumerator defines the supported job
   execution units
*/
enum class QJobType
{
  Python /**< Python job */,
  CXX /**< C++ job    */
};

/// Forward declaration
template<QJobType>
class QStream;

/**
   @brief Quantum job class

   The Quantum job class implements a quantum job
*/
template<QJobType>
class QJob;

/**
   @brief Quantum job class specialization for Python executor

   The Quantum job class implements a quantum job that can be executed
   within the Quantum stream class for the Python executor
*/
template<>
class QJob<QJobType::Python> : public QJobBase
{
private:
  // Timing information
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_start;
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_stop;

#ifdef LIBKET_WITH_PYTHON

private:
  // Python function and value objects
  PyObject* _value = NULL;

  // Python module and global/local dictionary objects
  PyObject* _module = NULL;
  PyObject* _global = NULL;
  PyObject* _local = NULL;

  // Handle to asynchronous operation
  std::future<void> _handle;

  // Python script
  const std::string _script;

  // Names of run, wait, and query methods
  const std::string _method_run;
  const std::string _method_wait;
  const std::string _method_query;

  // Launches the quantum job
  void launch(const std::string& method, const QJobBase* depend)
  {
    // Wait for dependent quantum job (if any)
    if (depend != NULL)
      depend->wait();

    // Ensure that the current thread is ready to call the Python C API
    // regardless of the current state of Python, or of the global interpreter
    // lock.
    PyGILState_STATE gilState;
    gilState = PyGILState_Ensure();

    // Define function in the newly created module
    _value = PyRun_String(_script.c_str(), Py_file_input, _global, _local);

    // _value would be null if the Python syntax is wrong
    if (_value == NULL) {
      if (PyErr_Occurred()) {
        PyErr_Print();
      }
      LIBKET_ERROR(
        "Embedded Python interpreter error: Python syntax is wrong!");
    }

    // _value is the result of the executing code,
    // chuck it away because we've only declared a function
    if (_value)
      Py_DECREF(_value);

    // Get a pointer to the function
    PyObject* _func = PyObject_GetAttrString(_module, method.c_str());

    // Double check we have actually found it and it is callable
    if (!_func || !PyCallable_Check(_func)) {
      if (PyErr_Occurred()) {
        PyErr_Print();
      }
      LIBKET_ERROR(
        "Embedded Python interpreter error: Cannot find function \"" + method +
        "\"!");
    }

    // Call the function, passing to it the arguments
    _timing_start = std::chrono::high_resolution_clock::now();
    _value = PyObject_CallObject(_func, NULL);
    _timing_stop = std::chrono::high_resolution_clock::now();

    PyErr_Print();

    // Decrease reference counter
    if (_func)
      Py_DECREF(_func);

    // Release any resources previously acquired
    PyGILState_Release(gilState);
  }

public:
  /// Default constructor deleted
  QJob() = delete;

  /// Explicit quantum job constructor
  explicit QJob(const std::string& script,
                const std::string& method_run,
                const std::string& method_wait,
                const std::string& method_query,
                PyObject* module,
                PyObject* global,
                PyObject* local,
                const QJobBase* depend = NULL)
    : _module(module)
    , _global(global)
    , _local(local)
    , _script(script)
    , _method_run(method_run)
    , _method_wait(method_wait)
    , _method_query(method_query)
  {
    // Start asynchronous execution
    _handle = run(depend);
  }

  /// Destructor
  virtual ~QJob()
  {
    this->wait();

    // Ensure that the current thread is ready to call the Python C API
    // regardless of the current state of Python, or of the global interpreter
    // lock.
    PyGILState_STATE gilState;
    gilState = PyGILState_Ensure();

    // Decrease reference counter
    if (_value)
      Py_DECREF(_value);

    // Release any resources previously acquired
    PyGILState_Release(gilState);
  }

  /// (Re-)runs the quantum job
  std::future<void> run(const QJobBase* depend = NULL) override
  {
    return std::async(
      std::launch::async, &QJob::launch, this, _method_run, depend);
  }

  /// Waits for quantum job to complete
  QJob* wait() const override
  {
    if (_method_wait.empty()) {
      _handle.wait();
    } else {
      QJob* qjob =
        new QJob(_script, _method_wait, "", "", _module, _global, _local, NULL);
      qjob->wait();
      delete qjob;
    }
    return const_cast<QJob*>(this);
  }

  /// Returns true if quantum job has completed, or false if not
  bool query() const override
  {

    if (_method_query.empty()) {
      return _handle.valid();

    } else {
      QJob* qjob = new QJob(
        _script, _method_query, "", "", _module, _global, _local, NULL);
      qjob->wait();
      bool result = qjob->get();
      delete qjob;
      return result;
    }
  }

  /// Waits for quantum job to complete and returns the result as JSON object
  nlohmann::json get() const
  {
    this->wait();
    if (_value)
      return LibKet::detail::py_cast<nlohmann::json>(_value);
    else
      LIBKET_ERROR("Embedded Python interpreter terminated with exception");
  }

#else // LIBKET_WITH_PYTHON

public:
  /// Default constructor
  QJob(const std::string& script,
       const std::string& method_run,
       const std::string& method_wait,
       const std::string& method_query,
       void* module,
       void* global,
       void* local,
       const QJobBase* depend = NULL)
  {
    LIBKET_WARN("This feature requires -DLIBKET_WITH_PYTHON");
  }

  /// (Re-)runs the quantum job
  std::future<void> run(const QJobBase* depend = NULL) override
  {
    LIBKET_ERROR("This feature requires -DLIBKET_WITH_PYTHON");
  }

  /// Waits for quantum job to complete
  QJob* wait() const override
  {
    LIBKET_ERROR("This feature requires -DLIBKET_WITH_PYTHON");
    return const_cast<QJob*>(this);
  }

  /// Returns true if quantum job has completed, or false if not
  bool query() const override
  {
    LIBKET_ERROR("This feature requires -DLIBKET_WITH_PYTHON");
    return false;
  }

  /// Waits for quantum job to complete and returns the result as JSON object
  nlohmann::json get() const
  {
    LIBKET_ERROR("This feature requires -DLIBKET_WITH_PYTHON");
    return nlohmann::json();
  }

#endif // LIBKET_WITH_PYTHON

  /// Returns time stamp of the start of the quantum job
  std::chrono::time_point<std::chrono::high_resolution_clock>
  timestamp_start () const
  {
    return _timing_start;
  }

  /// Returns time stamp of the finalization of the quantum job
  std::chrono::time_point<std::chrono::high_resolution_clock>
  timestamp_stop () const
  {
    return _timing_stop;
  }
  
  /// Returns duration of the quantum job
  template<class Rep = double, class Period = std::ratio<1>>
  const std::chrono::duration<Rep, Period> duration() const
  {
    return std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(
      _timing_stop - _timing_start);
  }
};

/**
   @brief Quantum job class specialization for C++ executor

   The Quantum job class implements a quantum job that can be executed
   within the Quantum stream class for the C++ executor
*/
template<>
class QJob<QJobType::CXX> : public QJobBase
{
private:
  // Timing information
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_start;
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_stop;

  // Handle to asynchronous operation
  std::future<void> _handle;

  // Callback method
  const std::function<void(void)>& _method;

  // Launches the quantum job
  void launch(const std::function<void(void)>& method, const QJobBase* depend)
  {
    // Wait for dependent quantum job (if any)
    if (depend != NULL)
      depend->wait();

    // Run method
    _timing_start = std::chrono::high_resolution_clock::now();
    method();
    _timing_stop = std::chrono::high_resolution_clock::now();
  }

public:
  /// Default constructor deleted
  QJob() = delete;

  /// Explicit quantum job constructur
  explicit QJob(const std::function<void(void)>& method,
                const QJobBase* depend = NULL)
    : _method(method)
  {
    // Start asynchronous execution
    _handle = run(depend);
  }

  /// Destructor
  virtual ~QJob() { this->wait(); }

  /// (Re-)runs the quantum job
  std::future<void> run(const QJobBase* depend = NULL) override
  {
    return std::async(std::launch::async, &QJob::launch, this, _method, depend);
  }

  /// Waits for quantum job to complete
  QJob* wait() const override
  {
    _handle.wait();
    return const_cast<QJob*>(this);
  }

  /// Returns true if quantum job has completed, or false if not
  bool query() const override { return _handle.valid(); }

  /// Waits for quantum job to complete and returns the result as JSON object
  nlohmann::json get() const
  {
    LIBKET_ERROR("This feature is only available for python based qpus");
    return nlohmann::json();
  }

  /// Returns time stamp of the start of the quantum job
  std::chrono::time_point<std::chrono::high_resolution_clock>
  timestamp_start () const
  {
    return _timing_start;
  }

  /// Returns time stamp of the finalization of the quantum job
  std::chrono::time_point<std::chrono::high_resolution_clock>
  timestamp_stop () const
  {
    return _timing_stop;
  }
  
  /// Returns duration of the quantum job
  template<class Rep = double, class Period = std::ratio<1>>
  const std::chrono::duration<Rep, Period> duration() const
  {
    return std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(
      _timing_stop - _timing_start);
  }
};

} // namespace LibKet

#endif // QJOB_HPP
