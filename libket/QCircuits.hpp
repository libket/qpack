/** @file libket/QCircuits.hpp

    @brief C++ API circuits header file

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup circuits Quantum circuits

    @ingroup cxx_api
 */
#pragma once
#ifndef CIRCUITS_HPP
#define CIRCUITS_HPP

#include <circuits/QCircuit.hpp>
#include <circuits/QCircuit_AllSwap.hpp>
#include <circuits/QCircuit_Arb_Ctrl.hpp>
#include <circuits/QCircuit_Arb_OR.hpp>
#include <circuits/QCircuit_Oracle.hpp>
#include <circuits/QCircuit_QFT.hpp>
#include <circuits/QCircuit_QFTdag.hpp>

#endif // CIRCUITS_HPP
