/** @file libket/utils/static_for.hpp

    @brief C++ API compile-time for-loop

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup utils
 */

#pragma once
#ifndef QUTILS_STATIC_FOR_HPP
#define QUTILS_STATIC_FOR_HPP

#include <string>
#include <type_traits>
#include <utility>

namespace LibKet {
namespace utils {

namespace detail {

/**
   @brief Compile-time for-loop (implementation)
*/
template<index_t for_start,
         index_t for_end,
         index_t for_step,
         index_t for_index,
         template<index_t start, index_t end, index_t step, index_t index>
         class functor,
         typename functor_return_type,
         typename... functor_types>
struct static_for_impl
{
  // Terminal
  template<bool is_terminal = ((for_step > 0) ? (for_index > for_end)
                                              : (for_index < for_end)),
           typename std::enable_if<is_terminal>::type* = nullptr>
  inline auto operator()(functor_return_type&& functor_return_arg,
                         functor_types&&... functor_args)
  {
#ifdef LIBKET_GEN_PROFILING
    QInfo << "static_for:" << std::to_string(for_start) << ","
          << std::to_string(for_end) << "," << std::to_string(for_step)
          << std::endl;
#endif

    return std::forward<functor_return_type>(functor_return_arg);
  }

  // Recursion
  template<bool is_terminal = ((for_step > 0) ? (for_index > for_end)
                                              : (for_index < for_end)),
           typename std::enable_if<!is_terminal>::type* = nullptr>
  inline auto operator()(functor_return_type&& functor_return_arg,
                         functor_types&&... functor_args)
  {
    auto arg = functor<for_start, for_end, for_step, for_index>()(
      std::forward<functor_return_type>(functor_return_arg),
      std::forward<functor_types>(functor_args)...);
    return static_for_impl<for_start,
                           for_end,
                           for_step,
                           for_index + for_step,
                           functor,
                           decltype(arg),
                           functor_types...>()(
      std::forward<decltype(arg)>(arg),
      std::forward<functor_types>(functor_args)...);
  }
};

#ifndef DOXYGEN
#include <specializations/static_for.hpp>
#endif

} // namespace detail

/**
   @brief Compile-time for-loop
*/
template<index_t for_start,
         index_t for_end,
         index_t for_step,
         template<index_t start, index_t end, index_t step, index_t index>
         class functor,
         typename functor_return_type,
         typename... functor_types>
inline auto
static_for(functor_return_type&& functor_return_arg,
           functor_types&&... functor_args)
{
  return LibKet::utils::detail::static_for_impl<for_start,
                                                for_end,
                                                for_step,
                                                for_start,
                                                functor,
                                                functor_return_type,
                                                functor_types...>()(
    std::forward<functor_return_type>(functor_return_arg),
    std::forward<functor_types>(functor_args)...);
}

} // namespace utils
} // namespace LibKet

#endif // QUTILS_STATIC_FOR_HPP
