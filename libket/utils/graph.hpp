/** @file libket/utils/graph.hpp

    @brief C++ API compile-time graph class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup utils
 */

#pragma once
#ifndef QUTILS_GRAPH_HPP
#define QUTILS_GRAPH_HPP

#include <utils/static_for.hpp>

namespace LibKet {
namespace utils {

/**
   @brief Compile-time variant of std::get for integer_sequence
*/
template<std::size_t index, class T, T... Is>
constexpr T get(std::integer_sequence<T, Is...>)
{
  constexpr T arr[] = { Is... };
  return arr[index];
}

namespace detail {
  template<index_t start, index_t end, index_t step, index_t index>
  struct full_graph_inner
  {  
    template<typename  Graph>
    inline constexpr auto operator()(Graph) noexcept
    {
      return typename std::decay<Graph>::type::template edge<start,index+1> {};
    }
  };
  
  template<index_t start, index_t end, index_t step, index_t index>
  struct full_graph_outer
  {  
    template<typename  Graph>
    inline constexpr auto operator()(Graph&& g) noexcept
    {
      return static_for<index,end-1,1,full_graph_inner>(g);
    }
  };

  template<index_t start, index_t end, index_t step, index_t index>
  struct regular_graph
  {  
    template<typename  Graph>
    inline constexpr auto operator()(Graph&& g) noexcept
    {
      return typename std::decay<Graph>::type::template edge<index,index+1>::template edge<index,index+2> {};
    }
  };    
}
  
/**
   @brief Compile-time graph
*/
template<std::size_t... Is>
struct graph
{
  // Creates a new graph with edge<Ifrom, Ito>
  template<std::size_t Ifrom, std::size_t Ito>
  using edge = graph<Is..., Ifrom, Ito>;

  // Returns the size of the graph (i.e. the number of edges)
  static constexpr const std::size_t size() { return sizeof...(Is) / 2; }

  // Returns the first node of the I-th edge
  template<std::size_t I>
  static constexpr const std::size_t from()
  {
    static_assert(2 * I < sizeof...(Is), "Index exceeds number of edges");
    return utils::get<2 * I>(seq);
  }

  // Returns the second node of the I-th edge
  template<std::size_t I>
  static constexpr const std::size_t to()
  {
    static_assert(2 * I < sizeof...(Is), "Index exceeds number of edges");
    return utils::get<2 * I + 1>(seq);
  }

  // Returns a collection of edges as a subgraph
  template<std::size_t... Es>
  static constexpr const auto subgraph()
  {
    return subgraph<graph<>, Es...>();
  }

  // Prints the graph
  static constexpr std::ostream& print(std::ostream& os)
  {
    os << "{ ";
    return print<Is..., 0>(os);
  }
private:
  static constexpr const std::index_sequence<Is...> seq{};

  // This is the termination of the recursive print function
  template<std::size_t eol>
  static constexpr std::ostream& print(std::ostream& os)
  {
    os << "}";
    return os;
  }

  // This is the recursive part of the print function
  template<std::size_t Jfrom, std::size_t Jto, std::size_t... Js>
  static constexpr std::ostream& print(std::ostream& os)
  {
    os << "{" << Jfrom << "," << Jto << "} ";
    return print<Js...>(os);
  }

  // This is the termination of the recursive subgraph function
  template<typename T, std::size_t E>
  static constexpr auto subgraph()
  {
    return typename std::decay<T>::type::template edge<from<E>(), to<E>()>{};
  }

  // This is the recursive part of the subgraph function
  template<typename T, std::size_t E0, std::size_t E1, std::size_t... Es>
  static constexpr auto subgraph()
  {
    return subgraph<
      typename std::decay<T>::type::template edge<from<E0>(), to<E0>()>,
      E1,
      Es...>();
  }
};

  // Creates a full graph of N vertices
  template<std::size_t N>
  auto make_full_graph()
  {
    return static_for<0,N-1,1,detail::full_graph_outer>(graph<>());
  }

  template<> auto make_full_graph<0>() { return graph<>(); }
  template<> auto make_full_graph<1>() { return graph<>(); }
  
  
  // Creates a regular graph of N vertices
  template<std::size_t N>
  auto make_regular_graph()
  {
    using g = decltype(static_for<0,N-3,1,detail::regular_graph>(graph<>()));
    return typename std::decay<g>::type
      ::template edge<N-2,N-1>
      ::template edge<N-2,0>
      ::template edge<N-1,0>
      ::template edge<N-1,1>();
  }

  template<> auto make_regular_graph<0>() { return graph<>(); }
  template<> auto make_regular_graph<1>() { return graph<>(); }
  template<> auto make_regular_graph<2>() { return graph<>::edge<0,1>(); }
  template<> auto make_regular_graph<3>() { return graph<>::edge<0,1>::edge<1,2>::edge<2,0>(); }
  template<> auto make_regular_graph<4>() { return utils::graph<>::edge<0,1>::edge<1,2>::edge<2,3>::edge<3,0>(); }
  
} // namespace utils

template<std::size_t... Is>
std::ostream&
operator<<(std::ostream& os, const utils::graph<Is...>&) noexcept
{
  utils::graph<Is...>::print(os);
  return os;
}

} // namespace LibKet

#endif // QUTILS_GRAPH_HPP
