/** @file libket/utils/sequence.hpp

    @brief C++ API integer sequence class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup utils
 */

#pragma once
#ifndef QUTILS_SEQUENCE_HPP
#define QUTILS_SEQUENCE_HPP

namespace LibKet {
namespace utils {

// This is the type which holds sequences
template<std::size_t... Ns>
struct sequence
{
  sequence()
    : _sequence{ Ns... }
  {}

  // Returns iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::iterator
    begin() noexcept
  {
    return _sequence.begin();
  }

  // Returns constant iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_iterator
    begin() const noexcept
  {
    return _sequence.begin();
  }

  // Returns iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::iterator
    end() noexcept
  {
    return _sequence.end();
  }

  // Returns constant iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_iterator
    end() const noexcept
  {
    return _sequence.end();
  }

  // Returns constant iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_iterator
    cbegin() const noexcept
  {
    return _sequence.cbegin();
  }

  // Returns constant iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_iterator
    cend() const noexcept
  {
    return _sequence.cend();
  }

  // Returns reverse iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::reverse_iterator
    rbegin() noexcept
  {
    return _sequence.rbegin();
  }

  // Returns constant reverse iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator
    rbegin() const noexcept
  {
    return _sequence.rbegin();
  }

  // Returns reverse iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::reverse_iterator
    rend() noexcept
  {
    return _sequence.rend();
  }

  // Returns constant reverse iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator
    rend() const noexcept
  {
    return _sequence.rend();
  }

  // Returns constant reverse iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator
    crbegin() const noexcept
  {
    return _sequence.crbegin();
  }

  // Returns constant reverse iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator
    crend() const noexcept
  {
    return _sequence.crend();
  }

private:
  std::array<std::size_t, sizeof...(Ns)> _sequence;
};

// First define the template signature
template<std::size_t... Ns>
struct seq_gen;

// Recursion case
template<std::size_t I0, std::size_t I, std::size_t... Ns>
struct seq_gen<I0, I, Ns...>
{
  // Take front most number of sequence,
  // decrement it, and prepend it twice.
  // First I - 1 goes into the counter,
  // Second I - 1 goes into the sequence.
  using type = typename seq_gen<I0, I - 1, I - 1, Ns...>::type;
};

// Recursion abort
template<std::size_t I0, std::size_t... Ns>
struct seq_gen<I0, I0, Ns...>
{
  using type = sequence<Ns...>;
};

template<std::size_t N, std::size_t M = 0>
using sequence_t = typename seq_gen<M >= N ? N : 0, M >= N ? M + 1 : N>::type;

template<std::size_t N, std::size_t _N, std::size_t... Ns>
std::ostream&
operator<<(std::ostream& os, const utils::sequence<N, _N, Ns...>&) noexcept
{
  os << N << ", " << utils::sequence<_N, Ns...>();
  return os;
}

template<std::size_t N>
std::ostream&
operator<<(std::ostream& os, const utils::sequence<N>&) noexcept
{
  os << N << "\n";
  return os;
}

} // namespace utils
} // namespace LibKet

#endif // QUTILS_SEQUENCE_HPP
