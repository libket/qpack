/** @file libket/QDevice.hpp

    @brief C++ API quantum device classes

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup devices Quantum devices

    @ingroup cxx_api
 */

#pragma once
#ifndef QDEVICE_HPP
#define QDEVICE_HPP

#include <QBase.hpp>
#include <QJob.hpp>
#include <QStream.hpp>

namespace LibKet {

/**
   @brief Quantum device types

   The Quantum device enumerator defines the supported quantum device
   types
*/
enum class QDeviceType
{
  // clang-format off
  generic                                   = 0x00000000 /**< Generic type                                      */,

  pyquil_visualizer                         = 0x00000001 /**< PyQuil circuit visualizer                         */,
  qasm2tex_visualizer                       = 0x00000002 /**< QASM2TEX circuit visualizer                       */,
  qiskit_visualizer                         = 0x00000003 /**< Qiskit circuit visualizer                         */,

  // Atos Quantum Learning Machine (QLM)
  atos_qlm_feynman_simulator                = 0x01000001 /**< Atos QLM Feynman integral path simulator          */,
  atos_qlm_linalg_simulator                 = 0x01000002 /**< Atos QLM Linear algebra-based  simulator          */,
  atos_qlm_stabs_simulator                  = 0x01000003 /**< Atos QLM Stabilizer-based simulator               */,
  atos_qlm_mps_simulator                    = 0x01000004 /**< Atos QLM Matrix product state-based simulator     */,

  // Quantum Inspire
  qi_26_simulator                           = 0x02000001 /**< Quantum Inspire 26-qubit simulator                */,
  qi_34_simulator                           = 0x02000002 /**< Quantum Inspire 34-qubit simulator                */,

  qi_spin2                                  = 0x02010001 /**< Quantum Inspire spin-2 processor (2 qubits)       */,
  qi_starmon5                               = 0x02010002 /**< Quantum Inspire starmon-5 processor (5 qubits)    */,

  // QisKit local simulators
  qiskit_almaden_simulator                  = 0x03000001 /**< Qiskit  20-qubit local simulator                  */,
  qiskit_armonk_simulator                   = 0x03000002 /**< Qiskit   1-qubit local simulator                  */,
  qiskit_athens_simulator                   = 0x03000003 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_belem_simulator                    = 0x03000004 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_boeblingen_simulator               = 0x03000005 /**< Qiskit  20-qubit local simulator                  */,
  qiskit_bogota_simulator                   = 0x03000006 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_brooklyn_simulator                 = 0x03000007 /**< Qiskit  65-qubit local simulator                  */,
  qiskit_burlington_simulator               = 0x03000008 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_cairo_simulator                    = 0x03000009 /**< Qiskit  27-qubit local simulator                  */,
  qiskit_cambridge_simulator                = 0x0300000A /**< Qiskit  28-qubit local simulator                  */,
  qiskit_casablanca_simulator               = 0x0300000B /**< Qiskit   7-qubit local simulator                  */,
  qiskit_dublin_simulator                   = 0x0300000C /**< Qiskit  27-qubit local simulator                  */,
  qiskit_essex_simulator                    = 0x0300000D /**< Qiskit   5-qubit local simulator                  */,
  qiskit_guadalupe_simulator                = 0x0300000E /**< Qiskit  16-qubit local simulator                  */,
  qiskit_hanoi_simulator                    = 0x0300000F /**< Qiskit  27-qubit local simulator                  */,
  qiskit_jakarta_simulator                  = 0x03000010 /**< Qiskit   7-qubit local simulator                  */,
  qiskit_johannesburg_simulator             = 0x03000011 /**< Qiskit  20-qubit local simulator                  */,
  qiskit_kolkata_simulator                  = 0x03000012 /**< Qiskit  27-qubit local simulator                  */,
  qiskit_lagos_simulator                    = 0x03000013 /**< Qiskit   7-qubit local simulator                  */,
  qiskit_lima_simulator                     = 0x03000014 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_london_simulator                   = 0x03000015 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_manhattan_simulator                = 0x03000016 /**< Qiskit  65-qubit local simulator                  */,
  qiskit_manila_simulator                   = 0x03000017 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_melbourne_simulator                = 0x03000018 /**< Qiskit  15-qubit local simulator                  */,    
  qiskit_montreal_simulator                 = 0x03000019 /**< Qiskit  27-qubit local simulator                  */,
  qiskit_mumbai_simulator                   = 0x0300001A /**< Qiskit  27-qubit local simulator                  */,
  qiskit_nairobi_simulator                  = 0x0300001B /**< Qiskit   7-qubit local simulator                  */,
  qiskit_ourense_simulator                  = 0x0300001C /**< Qiskit   5-qubit local simulator                  */,
  qiskit_paris_simulator                    = 0x0300001D /**< Qiskit  27-qubit local simulator                  */,
  qiskit_peekskill_simulator                = 0x0300001E /**< Qiskit  27-qubit local simulator                  */,
  qiskit_poughkeepsie_simulator             = 0x0300001F /**< Qiskit  20-qubit local simulator                  */,
  qiskit_quito_simulator                    = 0x03000020 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_rochester_simulator                = 0x03000021 /**< Qiskit  53-qubit local simulator                  */,
  qiskit_rome_simulator                     = 0x03000022 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_rueschlikon_simulator              = 0x03000023 /**< Qiskit  16-qubit local simulator                  */,
  qiskit_santiago_simulator                 = 0x03000024 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_singapore_simulator                = 0x03000025 /**< Qiskit  20-qubit local simulator                  */,
  qiskit_sydney_simulator                   = 0x03000026 /**< Qiskit  27-qubit local simulator                  */,
  qiskit_tenerife_simulator                 = 0x03000027 /**< Qiskit   5-qubit local simulator                  */,
  qiskit_tokyo_simulator                    = 0x03000028 /**< Qiskit  20-qubit local simulator                  */,
  qiskit_toronto_simulator                  = 0x03000029 /**< Qiskit  27-qubit local simulator                  */,
  qiskit_valencia_simulator                 = 0x0300002A /**< Qiskit   5-qubit local simulator                  */,
  qiskit_vigo_simulator                     = 0x0300002B /**< Qiskit   5-qubit local simulator                  */,
  qiskit_yorktown_simulator                 = 0x0300002C /**< Qiskit   5-qubit local simulator                  */,
  qiskit_washington_simulator               = 0x0300002D /**< Qiskit 127-qubit local simulator                  */,
  qiskit_perth_simulator                    = 0x0300002E /**< Qiskit   7-qubit local simulator                  */,

  qiskit_pulse_simulator                    = 0x03010001 /**< Qiskit pulse local simulator                      */,
  qiskit_qasm_simulator                     = 0x03010002 /**< Qiskit universal local simulator                  */,
  qiskit_statevector_simulator              = 0x03010003 /**< Qiskit statevector local simulator                */,
  qiskit_unitary_simulator                  = 0x03010004 /**< Qiskit density matrix local simulator             */,

  qiskit_aer_density_matrix_simulator       = 0x03010005 /**< Qiskit Aer density matrix local simulator         */,
  qiskit_aer_extended_stabilizer_simulator  = 0x03010006 /**< Qiskit Aer extended stabilizer local simulator    */,
  qiskit_aer_matrix_product_state_simulator = 0x03010007 /**< Qiskit Aer matrix product state local simulator   */,
  qiskit_aer_simulator                      = 0x03010008 /**< Qiskit Aer local simulator                        */,
  qiskit_aer_stabilizer_simulator           = 0x03010009 /**< Qiskit Aer stabilizer local simulator             */,
  qiskit_aer_statevector_simulator          = 0x0301000A /**< Qiskit Aer statevector local simulator            */,
  qiskit_aer_superop_simulator              = 0x0301000B /**< Qiskit Aer superop local simulator                */,
  qiskit_aer_unitary_simulator              = 0x0301000C /**< Qiskit Aer unitary local simulator                */,

  // IBM-Q Experience
  ibmq_almaden_simulator                    = 0x04000001 /**< IBM-Q  20-qubit remote simulator                  */,
  ibmq_armonk_simulator                     = 0x04000002 /**< IBM-Q   1-qubit remote simulator                  */,
  ibmq_athens_simulator                     = 0x04000003 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_belem_simulator                      = 0x04000004 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_boeblingen_simulator                 = 0x04000005 /**< IBM-Q  20-qubit remote simulator                  */,
  ibmq_bogota_simulator                     = 0x04000006 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_brooklyn_simulator                   = 0x04000007 /**< IBM-Q  65-qubit remote simulator                  */,
  ibmq_burlington_simulator                 = 0x04000008 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_cairo_simulator                      = 0x04000009 /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_cambridge_simulator                  = 0x0400000A /**< IBM-Q  28-qubit remote simulator                  */,
  ibmq_casablanca_simulator                 = 0x0400000B /**< IBM-Q   7-qubit remote simulator                  */,
  ibmq_dublin_simulator                     = 0x0400000C /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_essex_simulator                      = 0x0400000D /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_guadalupe_simulator                  = 0x0400000E /**< IBM-Q  16-qubit remote simulator                  */,
  ibmq_hanoi_simulator                      = 0x0400000F /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_jakarta_simulator                    = 0x04000010 /**< IBM-Q   7-qubit remote simulator                  */,
  ibmq_johannesburg_simulator               = 0x04000011 /**< IBM-Q  20-qubit remote simulator                  */,
  ibmq_kolkata_simulator                    = 0x04000012 /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_lagos_simulator                      = 0x04000013 /**< IBM-Q   7-qubit remote simulator                  */,
  ibmq_lima_simulator                       = 0x04000014 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_london_simulator                     = 0x04000015 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_manhattan_simulator                  = 0x04000016 /**< IBM-Q  65-qubit remote simulator                  */,
  ibmq_manila_simulator                     = 0x04000017 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_melbourne_simulator                  = 0x04000018 /**< IBM-Q  15-qubit remote simulator                  */,    
  ibmq_montreal_simulator                   = 0x04000019 /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_mumbai_simulator                     = 0x0400001A /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_nairobi_simulator                    = 0x0400001B /**< IBM-Q   7-qubit remote simulator                  */,
  ibmq_ourense_simulator                    = 0x0400001C /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_paris_simulator                      = 0x0400001D /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_peekskill_simulator                  = 0x0400001E /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_poughkeepsie_simulator               = 0x0400001F /**< IBM-Q  20-qubit remote simulator                  */,
  ibmq_quito_simulator                      = 0x04000020 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_rochester_simulator                  = 0x04000021 /**< IBM-Q  53-qubit remote simulator                  */,
  ibmq_rome_simulator                       = 0x04000022 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_rueschlikon_simulator                = 0x04000023 /**< IBM-Q  16-qubit remote simulator                  */,
  ibmq_santiago_simulator                   = 0x04000024 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_singapore_simulator                  = 0x04000025 /**< IBM-Q  20-qubit remote simulator                  */,
  ibmq_sydney_simulator                     = 0x04000026 /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_tenerife_simulator                   = 0x04000027 /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_tokyo_simulator                      = 0x04000028 /**< IBM-Q  20-qubit remote simulator                  */,
  ibmq_toronto_simulator                    = 0x04000029 /**< IBM-Q  27-qubit remote simulator                  */,
  ibmq_valencia_simulator                   = 0x0400002A /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_vigo_simulator                       = 0x0400002B /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_yorktown_simulator                   = 0x0400002C /**< IBM-Q   5-qubit remote simulator                  */,
  ibmq_washington_simulator                 = 0x0400002D /**< IBM-Q 127-qubit remote simulator                  */,
  ibmq_perth_simulator                      = 0x0400002E /**< IBM-Q   7-qubit remote simulator                  */,
  
  ibmq_qasm_simulator                       = 0x04010001 /**< IBM-Q universal remote simulator                  */,

  ibmq_almaden                              = 0x04020001 /**< IBM-Q  20-qubit processor                         */,
  ibmq_armonk                               = 0x04020002 /**< IBM-Q   1-qubit processor                         */,
  ibmq_athens                               = 0x04020003 /**< IBM-Q   5-qubit processor                         */,
  ibmq_belem                                = 0x04020004 /**< IBM-Q   5-qubit processor                         */,
  ibmq_boeblingen                           = 0x04020005 /**< IBM-Q  20-qubit processor                         */,
  ibmq_bogota                               = 0x04020006 /**< IBM-Q   5-qubit processor                         */,
  ibmq_brooklyn                             = 0x04020007 /**< IBM-Q  65-qubit processor                         */,
  ibmq_cairo                                = 0x04020008 /**< IBM-Q  27-qubit processor                         */,
  ibmq_burlington                           = 0x04020009 /**< IBM-Q   5-qubit processor                         */,
  ibmq_cambridge                            = 0x0402000A /**< IBM-Q  28-qubit processor                         */,
  ibmq_casablanca                           = 0x0402000B /**< IBM-Q   7-qubit processor                         */,
  ibmq_dublin                               = 0x0402000C /**< IBM-Q  27-qubit processor                         */,
  ibmq_essex                                = 0x0402000D /**< IBM-Q   5-qubit processor                         */,
  ibmq_guadalupe                            = 0x0402000E /**< IBM-Q  16-qubit processor                         */,
  ibmq_hanoi                                = 0x0402000F /**< IBM-Q  27-qubit processor                         */,
  ibmq_jakarta                              = 0x04020010 /**< IBM-Q   7-qubit processor                         */,
  ibmq_johannesburg                         = 0x04020011 /**< IBM-Q  20-qubit processor                         */,
  ibmq_kolkata                              = 0x04020012 /**< IBM-Q  27-qubit processor                         */,
  ibmq_lagos                                = 0x04020013 /**< IBM-Q   7-qubit processor                         */,
  ibmq_lima                                 = 0x04020014 /**< IBM-Q   5-qubit processor                         */,
  ibmq_london                               = 0x04020015 /**< IBM-Q   5-qubit processor                         */,
  ibmq_manhattan                            = 0x04020016 /**< IBM-Q  65-qubit processor                         */,
  ibmq_manila                               = 0x04020017 /**< IBM-Q   5-qubit processor                         */,
  ibmq_melbourne                            = 0x04020018 /**< IBM-Q  15-qubit processor                         */,    
  ibmq_montreal                             = 0x04020019 /**< IBM-Q  27-qubit processor                         */,
  ibmq_mumbai                               = 0x0402001A /**< IBM-Q  27-qubit processor                         */,
  ibmq_nairobi                              = 0x0402001B /**< IBM-Q   7-qubit processor                         */,
  ibmq_ourense                              = 0x0402001C /**< IBM-Q   5-qubit processor                         */,
  ibmq_paris                                = 0x0402001D /**< IBM-Q  27-qubit processor                         */,
  ibmq_peekskill                            = 0x0402001E /**< IBM-Q  27-qubit processor                         */,
  ibmq_poughkeepsie                         = 0x0402001F /**< IBM-Q  20-qubit processor                         */,
  ibmq_quito                                = 0x04020020 /**< IBM-Q   5-qubit processor                         */,
  ibmq_rochester                            = 0x04020021 /**< IBM-Q  53-qubit processor                         */,
  ibmq_rome                                 = 0x04020022 /**< IBM-Q   5-qubit processor                         */,
  ibmq_rueschlikon                          = 0x04020023 /**< IBM-Q  16-qubit processor                         */,
  ibmq_santiago                             = 0x04020024 /**< IBM-Q   5-qubit processor                         */,
  ibmq_singapore                            = 0x04020025 /**< IBM-Q  20-qubit processor                         */,
  ibmq_sydney                               = 0x04020026 /**< IBM-Q  27-qubit processor                         */,
  ibmq_tenerife                             = 0x04020027 /**< IBM-Q   5-qubit processor                         */,
  ibmq_tokyo                                = 0x04020028 /**< IBM-Q  20-qubit processor                         */,
  ibmq_toronto                              = 0x04020029 /**< IBM-Q  27-qubit processor                         */,
  ibmq_valencia                             = 0x0402002A /**< IBM-Q   5-qubit processor                         */,
  ibmq_vigo                                 = 0x0402002B /**< IBM-Q   5-qubit processor                         */,
  ibmq_yorktown                             = 0x0402002C /**< IBM-Q   5-qubit processor                         */,
  ibmq_washington                           = 0x0402002D /**< IBM-Q 127-qubit processor                         */,
  ibmq_perth                                = 0x0402002E /**< IBM-Q   7-qubit processor                         */,

  // OpenQL
  openql_cc_light_compiler                  = 0x05000001 /**< OpenQL compiler for CC-Light                      */,
  openql_cc_light17_compiler                = 0x05000002 /**< OpenQL compiler for CC-Light17                    */,
  openql_qx_compiler                        = 0x05000003 /**< OpenQL compiler for QX simulator                  */,

  // Rigetti
  rigetti_aspen_8_simulator                 = 0x06000001 /**< Rigetti Aspen-8 simulator                         */,
  rigetti_aspen_9_simulator                 = 0x06000002 /**< Rigetti Aspen-9 simulator                         */,
  rigetti_aspen_10_simulator                = 0x06000003 /**< Rigetti Aspen-10 simulator                        */,
  rigetti_aspen_M1_simulator                = 0x06000004 /**< Rigetti Aspen-M-1 simulator                       */,

  rigetti_9q_simulator                      = 0x06010001 /**< Rigetti 9Q simulator                              */,
  rigetti_9q_square_simulator               = 0x06010002 /**< Rigetti 9Q-square simulator                       */,

  rigetti_16q_simulator                     = 0x06010013 /**< Rigetti 16Q simulator                             */,
  rigetti_16q_square_simulator              = 0x06010004 /**< Rigetti 16Q-square simulator                      */,

  rigetti_aspen_8                           = 0x06020001 /**< Rigetti Aspen-8 processor                         */,
  rigetti_aspen_9                           = 0x06020002 /**< Rigetti Aspen-9 processor                         */,
  rigetti_aspen_10                          = 0x06020003 /**< Rigetti Aspen-10 processor                        */,
    
  // QuEST: Quantum Exact Simulation Toolkit
  quest                                     = 0x07000000 /**< QuEST simulator                                   */,

  // QX simulator
  qx                                        = 0x08000000 /**< QX simulator                                      */,

  // Cirq
  cirq_simulator                            = 0x09000001 /**< Cirq simulator                                    */,
  cirq_simulator_simulator                  = 0x09000001 /**< Cirq simulator (name demangling)                  */,
  cirq_bristlecone_simulator                = 0x09000002 /**< Cirq Bristlecone simulator                        */,
  cirq_foxtail_simulator                    = 0x09000003 /**< Cirq Foxtail simulator                            */,
  cirq_sycamore_simulator                   = 0x09000004 /**< Cirq Sycamore simulator                           */,
  cirq_sycamore23_simulator                 = 0x09000005 /**< Cirq Sycamore23 simulator                         */,

  //IonQ
  ionq_simulator                            = 0x10000001 /**< IonQ Simulator                                    */,
  ionq_qpu                                  = 0x10000002 /**< IonQ Trapped-Ion QPU                              */

  // clang-format on
};

std::ostream& operator << (std::ostream& os, const QDeviceType& obj)
{
   os << std::hex << static_cast<std::underlying_type<QDeviceType>::type>(obj);
   return os;
}

std::stringstream& operator << (std::stringstream& ss, const QDeviceType& obj)
{
   ss << std::hex << static_cast<std::underlying_type<QDeviceType>::type>(obj);
   return ss;
}

/**
   @brief Quantum result type

   The Quantum result enumerator defines the supported quantum result
   types
*/
enum class QResultType
{
  // clang-format off
  best        /**< Best state             */,
  duration    /**< Circuit execution time */,
  jobDuration /**< QJob execution time    */, 
  queueDuration /**< Queue time           */, 
  histogram   /**< Histogram              */,
  id          /**< Job ID                 */,
  status      /**< Job status             */,
  timestamp   /**< Time stamp             */
  // clang-format on
};

/**
 @namespace LibKet::device

 @brief
 The LibKet::device namespace, containing device-specific
 functionality of the LibKet projec.

 The LibKet::device namespace contains device-specific functionality
 of the LibKet project that is not exposed to the
 end-user. Functionality in this namespace can change without
 notice.
*/
namespace device {

/**
   @brief Type trait providing information about device properties
*/
template<QDeviceType>
struct QDeviceProperty;

/// Preprocessor macro for defining device properties
#define QDevicePropertyDefine(_type, _name, _qubits,                    \
                              _simulator, _endianness)                  \
  template<>                                                            \
  struct QDeviceProperty<_type>                                         \
  {                                                                     \
  public:                                                               \
  static const std::string type;                                        \
  static const std::string name;                                        \
  static const std::size_t qubits = _qubits;                            \
  static const bool simulator = _simulator;                             \
  static const QEndianness endianness = _endianness;                    \
  };                                                                    \
  const std::string QDeviceProperty<_type>::type = #_type;              \
  const std::string QDeviceProperty<_type>::name = _name;
  
QDevicePropertyDefine(QDeviceType::generic,
                      "generic",
                      0,
                      true,
                      QEndianness::lsb)

} // namespace device

/// Forward declaration
template<QDeviceType _type,
         std::size_t _qubits = device::QDeviceProperty<_type>::qubits,
         bool _simulator = device::QDeviceProperty<_type>::simulator,
         enum QEndianness _endianness = device::QDeviceProperty<_type>::endianness>
class QDevice;

/// Dummy device
class QDevice_Dummy
{
public:
  template<typename... Args>
  QDevice_Dummy(Args... args)
  {
    LIBKET_ERROR("Unsupported backend");
  }

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Dummy& operator()(const Expr& expr)
  {
    LIBKET_ERROR("Unsupported backend");
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Dummy& operator()(const std::string& expr)
  {
    LIBKET_ERROR("Unsupported backend");
    return *this;
  } 
  
  /// Execute quantum circuit asynchronously
  QJob<QJobType::Python>* execute_async(std::size_t = 0,
                                        QStream<QJobType::Python>* = NULL)
  {
    LIBKET_ERROR("Unsupported backend");
    return NULL;
  };

  /// Execute quantum circuit synchronously
  QJob<QJobType::Python>* execute(std::size_t = 0, QStream<QJobType::Python>* = NULL)
  {
    LIBKET_ERROR("Unsupported backend");
    return NULL;
  }

  /// Evaluate quantum circuit synchronously
  utils::json eval(std::size_t = 0, QStream<QJobType::Python>* = NULL)
  {
    LIBKET_ERROR("Unsupported backend");
    return NULL;
  }
};

} // namespace LibKet

#include <devices/QDevice_AQLM.hpp>
#include <devices/QDevice_Cirq.hpp>
#include <devices/QDevice_IBMQ.hpp>
#include <devices/QDevice_OpenQL.hpp>
#include <devices/QDevice_QX.hpp>
#include <devices/QDevice_Qiskit.hpp>
#include <devices/QDevice_QuEST.hpp>
#include <devices/QDevice_QuantumInspire.hpp>
#include <devices/QDevice_Rigetti.hpp>
#include <devices/QDevice_IonQ.hpp>

#include <devices/QDevice_visualizer.hpp>

#endif // QDEVICE_HPP
