/** @file libket/gates/QGate_Rotate_Xdag.hpp

    @brief C++ API quantum Rotate_Xdag class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moeller

    @defgroup rotate_xdag Rotate-X gate (conjugate transpose)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_ROTATE_XDAG_HPP
#define QGATE_ROTATE_XDAG_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QRotate_X;

/**
@brief Rotate_Xdag gate class

The Rotate_Xdag gate class implements the inverse quantum
Rotate_X gate for an arbitrary number of quantum bits.

The \f$R_{x}^{\dagger}(\theta)\f$ gate is defined as the
conjugate transpose of the Rotate_X gate.

The unitary matrix reads

\f[
R_{x}^{\dagger}(\theta) =
\begin{pmatrix}
\cos(\frac{\theta}{2}) & i\sin(\frac{\theta}{2})\\
i\sin(\frac{\theta}{2}) & \cos(\frac{\theta}{2})
\end{pmatrix}
\f]

@ingroup rotate_xdag
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QRotate_Xdag : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  UNARY_GATE_DEFAULT_DECL_AT(QRotate_Xdag, QRotate_X);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RX[" + (-_angle{}).to_string() + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.rx(rads=" + (-_angle{}).to_string() +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "rx q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += (-_angle{}).to_string() + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("rx(" + (-_angle{}).to_string(true) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().rx(i, (-_angle{}).value()); });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("\trxdag q" + utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RX (" + (-_angle{}).to_string(true) + ") " +
                           utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateX(expr.reg(), i, (-_angle{}).value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::rx(i, (-_angle{}).value()));
    }
    return expr;
  }
#endif
  /// @}
};

/**
   @brief Rotate_Xdag gate creator
   @ingroup rotate_xdag
   
   This overload of the LibKet::gates::rotate_xdag() function can be
   used as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::rotate_xdag();
   \endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto rotate_xdag(_angle) noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_Xdag<_angle, _tol>>(
    filters::QFilter{});
}

/// @brief Rotate_Xdag gate default implementation
/// @ingroup rotate_xdag
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_AT_NEGATIVE_IDENTITY(QRotate_Xdag, rotate_xdag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SAME_IDENTITY(QRotate_X, rotate_xdag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_ADD_SINGLE(QRotate_Xdag, rotate_xdag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SUB_SINGLE(QRotate_X, rotate_xdag);    
UNARY_GATE_DEFAULT_CREATOR_AT(QRotate_Xdag, rotate_xdag);
GATE_ALIAS_T(rotate_xdag, ROTATE_Xdag);
GATE_ALIAS_T(rotate_xdag, rxdag);
GATE_ALIAS_T(rotate_xdag, RXdag);
GATE_ALIAS_T(rotate_xdag, Rxdag);
UNARY_GATE_DEFAULT_IMPL_AT(QRotate_Xdag, rotate_xdag);
/// @}

/// @brief Rotate_X gate default implementation
/// @ingroup rotate_x
/// @{
UNARY_GATE_DEFAULT_IMPL_AT(QRotate_X, rotate_x);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_XDAG_HPP
