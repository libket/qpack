/** @file libket/gates/QGate_Measure_Y.hpp

    @brief C++ API quantum Measure_Y class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup measure_y Measurement gate (in Y-basis)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_MEASURE_Y_HPP
#define QGATE_MEASURE_Y_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief Measure_Y class

The Measure_Y class implements the measurement of an arbitrary
number of quantum bits in the Z-basis. Striktly speaking, Measure_y is
not a quantum gate.

@ingroup measure_y
*/
class QMeasure_Y : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QMeasure_Y, QMeasure_X);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("S q[" + utils::to_string(i) + "]\n");
      expr.append_kernel("MEAS q[" + utils::to_string(i) + "]\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ////
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("cirq.S.on(q[" + utils::to_string(i) + "])\n");
      expr.append_kernel("cirq.measure(q[" + utils::to_string(i) + "])\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    std::string _expr = "measure_y q[";
    for (auto i : _filter::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("s q[" + utils::to_string(i) + "];\n");
      expr.append_kernel("measure q[" + utils::to_string(i) + "] -> c[" +
                         utils::to_string(i) + "];\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel([&]() {
        expr.kernel().s(i);
        expr.kernel().measure(i);
      });
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("\ts q" + utils::to_string(i) + "\n");
      expr.append_kernel("\tmeasure q" + utils::to_string(i) + "\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("S " + utils::to_string(i) + "\n");
      expr.append_kernel("MEASURE " + utils::to_string(i) + " ro[" +
                         utils::to_string(i) + "]\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      quest::sGate(expr.reg(), i);
      expr.creg()[i] = quest::measure(expr.reg(), i);
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel(new qx::measure_y(i));

    return expr;
  }
#endif
  /// @}
};

/**
   @brief Measure_Y gate creator
   @ingroup measure_y

   This overload of the LibKet::gates::measure_y() function can be used as
   terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::measure_y();
   \endcode
*/
inline constexpr auto
measure_y() noexcept
{
  return UnaryQGate<filters::QFilter, QMeasure_Y>(filters::QFilter{});
}

/// @brief Measure_Y gate default implementation
/// @ingroup measure_y
/// @{
  UNARY_GATE_OPTIMIZE_CREATOR_SINGLE(QMeasure_Y, measure_y);
UNARY_GATE_DEFAULT_CREATOR(QMeasure_Y, measure_y);
GATE_ALIAS(measure_y, MEASURE_Y);
UNARY_GATE_DEFAULT_IMPL(QMeasure_Y, measure_y);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_MEASURE_Y_HPP
