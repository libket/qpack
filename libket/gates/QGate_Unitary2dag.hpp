/** @file libket/gates/QGate_Unitary2dag.hpp

    @brief C++ API \f$2\times 2\f$ unitary gate (conjugate transpose) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup u2dag U2 gate (conjugate transpose)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_UNITARY2DAG_HPP
#define QGATE_UNITARY2DAG_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _functor, typename _tol>
class QUnitary2;
  
/**
@brief \f$2\times 2\f$ unitary gate (conjugate transpose) class

The \f$2\times2\f$ unitary gate (conjugate transpose) accepts an
arbitrary \f$2\times 2\f$ unitary matrix \f$U\f$ as input and performs
the ZYZ decomposition of the conjugate transpose of \f$U\f$

\f[
U^\dagger = \exp(i\Phi) R_z(-\gamma)R_y(-\beta)R_z(-\alpha)
\f]

with \f$\Phi,\alpha,\beta,\gamma\f$ are rotation angles.

@ingroup u2dag
*/
template<typename _functor, typename _tol = QConst_M_ZERO_t>
class QUnitary2dag : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL_FTOR_T(QUnitary2dag, QUnitary2);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    auto x = decompose();
    if (tolerance(x(3), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RZ[" + std::to_string(-x(3)) + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RY[" + std::to_string(-x(2)) + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RZ[" + std::to_string(-x(1)) + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    auto x = decompose();
    if (tolerance(x(3), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.ZPowGate(exponent=" + std::to_string(-x(3)) +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.YPowGate(exponent=" + std::to_string(-x(2)) +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.ZPowGate(exponent=" + std::to_string(-x(1)) +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    auto x = decompose();
    std::string _expr = "rz q[";
    if (tolerance(x(3), _tol{})) {
      _expr += "rz q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
          (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += std::to_string(-x(3)) + "\n";
    }
    if (tolerance(x(2), _tol{})) {
      _expr += "ry q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
          (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += std::to_string(-x(2)) + "\n";
    }
    if (tolerance(x(1), _tol{})) {
      _expr += "rz q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
          (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += std::to_string(-x(1)) + "\n";
    }
    expr.append_kernel(_expr);
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    auto x = decompose();
    if (tolerance(x(3), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("rz(" + std::to_string(-x(3)) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("ry(" + std::to_string(-x(2)) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("rz(" + std::to_string(-x(1)) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    if (tolerance(x(3), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().rz(i, -x(3)); });
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().ry(i, -x(2)); });
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().rz(i, -x(1)); });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("\tu2dag q" + utils::to_string(i) + "\n");
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    auto x = decompose();
    if (tolerance(x(3), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RZ (" + std::to_string(-x(3)) + ") " +
                           utils::to_string(i) + "\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RY (" + std::to_string(-x(2)) + ") " +
                           utils::to_string(i) + "\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RZ (" + std::to_string(-x(1)) + ") " +
                           utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    auto x = decompose();
    if (tolerance(x(3), _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateZ(expr.reg(), i, -x(3));
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateY(expr.reg(), i, -x(2));
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateZ(expr.reg(), i, -x(1));
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    auto x = decompose();
    if (tolerance(x(3), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::rz(i, -x(3)));
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::ry(i, -x(2)));
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::rz(i, -x(1)));
    }
    return expr;
  }
#endif
  /// @}
  
private:
  inline static arma::vec decompose()
  {
    arma::vec u = _functor{}();
    arma::vec x = { 0.0, 1.0, 1.0, 0.0 };
    
    if (!optim::de(
          x,
          QUnitary2dag<_functor, _tol>::decompose_fn,
          &u))
      throw std::runtime_error(
        "QUnitary2dag: An error occured in the decomposition");

    return x;
  }
  
  static double decompose_fn(const arma::vec& vals_inp,
                             arma::vec* grad_out,
                             void* opt_expr)
  {
    // Get the matrix values from the input vector
    arma::vec u_expr = *(arma::vec*)(opt_expr);

    // Name the seperate components of the input and output vectors to
    // make the calculation more readable and a one-on-one copy of the
    // original way of writing it out.
    double u_1 = u_expr(0);
    double u_2 = u_expr(1);
    double u_3 = u_expr(2);
    double u_4 = u_expr(3);

    double x_1 = vals_inp(0);
    double x_2 = vals_inp(1);
    double x_3 = vals_inp(2);
    double x_4 = vals_inp(3);

    // Define the necessary imaginary number
    std::complex<double> mycomplex((double)0.0, (double)1.0);

    using namespace std;
    double obj_val = abs(exp(imag(mycomplex) * (x_1 - x_2 / 2.0 - x_4 / 2.0)) *
                         cos(x_3 / 2.0) -
                         u_1) +
                     abs(-exp(imag(mycomplex) * (x_1 - x_2 / 2.0 + x_4 / 2.0)) *
                         sin(x_3 / 2.0) -
                         u_2) +
                     abs(exp(imag(mycomplex) * (x_1 + x_2 / 2.0 - x_4 / 2.0)) *
                         sin(x_3 / 2.0) -
                         u_3) +
                     abs(exp(imag(mycomplex) * (x_1 + x_2 / 2.0 + x_4 / 2.0)) *
                         cos(x_3 / 2.0) -
                         u_4);

    return obj_val;
  }
};
  
/**
   @brief \f$2\times 2\f$ unitary gate (conjugate transpose) creator

   This overload of the LibKet::gates::unitary2dag() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression

   \code
   struct U2_ftor {
   inline auto operator()() const noexcept
   { return arma::vec{ 1.0/sqrt(2.0), -1.0/sqrt(2.0),                               
                       1.0/sqrt(2.0),  1.0/sqrt(2.0) }
   };

   auto expr = gates::unitary2dag<U2_ftor>();
   \endcode
*/
template<typename _functor, typename _tol = QConst_M_ZERO_t>
inline constexpr auto unitary2dag() noexcept
{
  return UnaryQGate<filters::QFilter, QUnitary2dag<_functor, _tol>, filters::QFilter>(
    filters::QFilter{});
}

/// @brief \f$2\times 2\f$ unitary gate (conjugate transpose) default implementation
/// @ingroup u2dag
/// @{
UNARY_GATE_DEFAULT_CREATOR_FTOR_T(QUnitary2dag, unitary2dag);
GATE_ALIAS_FTOR_T(unitary2dag, u2dag);
GATE_ALIAS_FTOR_T(unitary2dag, UNITARY2dag);
GATE_ALIAS_FTOR_T(unitary2dag, U2dag);
UNARY_GATE_DEFAULT_IMPL_FTOR_T(QUnitary2dag, unitary2dag);
///@}

/// @brief \f$2\times 2\f$ unitary gate default implementation
/// @ingroup u2
/// @{
UNARY_GATE_DEFAULT_IMPL_FTOR_T(QUnitary2, unitary2);
///@}
} // namespace gates

} // namespace LibKet

#endif // QGATE_UNITARY2DAG_HPP
