/** @file libket/gates/QGate_CZ.hpp

    @brief C++ API quantum CZ (controlled-Z gate) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup cz CZ gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_CZ_HPP
#define QGATE_CZ_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>
#include <gates/QGate_CNOT.hpp>
#include <gates/QGate_Hadamard.hpp>

namespace LibKet {

namespace gates {

/**
@brief CZ (controlled-Z) gate class

The CZ (controlled-Z) gate class implements the quantum
controlled-Z gate for an arbitrary number of quantum bits

The CZ (controlled-Z) gate is a two-qubit operation, where the first
qubit is usually referred to as the control qubit and the second qubit
as the target qubit. It maps the basis state \f$\left|00\right>\f$ to
\f$\left|00\right>\f$, \f$\left|01\right>\f$ to \f$\left|01\right>\f$,
\f$\left|10\right>\f$ to \f$\left|10\right>\f$ and
\f$\left|11\right>\f$ to \f$-\left|11\right>\f$.

The CZ (controlled-Z) gates leaves the control qubit unchanged and
performs a Pauli-Z gate on the target qubit only when the control
qubit is in state \f$\left|1\right>\f$.

The unitary matrix reads

\f[
\text{CZ} =
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & 1 & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & -1
\end{pmatrix}
\f]

@ingroup cz
*/
class QCZ : public QGate
{
public:
  BINARY_GATE_DEFAULT_DECL(QCZ, QCZ);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("CTRL(Z) q[" + utils::to_string(std::get<0>(i)) +
                         "],q[" + utils::to_string(std::get<1>(i)) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("cirq.CZ.on(q[" + utils::to_string(std::get<0>(i)) +
                         "],q[" + utils::to_string(std::get<1>(i)) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    std::string _expr = "cz q[";
    for (auto i : _filter0::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("cz q[" + utils::to_string(std::get<0>(i)) + "], q[" +
                         utils::to_string(std::get<1>(i)) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel(
        [&]() { expr.kernel().cz(std::get<0>(i), std::get<1>(i)); });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("\tcz q" + utils::to_string(std::get<0>(i)) + ",q" +
                         utils::to_string(std::get<1>(i)) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("CZ " + utils::to_string(std::get<0>(i)) + " " +
                         utils::to_string(std::get<1>(i)) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    auto e = H(filters::gototag<1>(
      CNOT(filters::tag<0>(_filter0{}), H(filters::tag<1>(_filter1{})))));
    return e(expr);
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel(
        new qx::bin_ctrl(std::get<0>(i), new qx::pauli_z(std::get<1>(i))));

    return expr;
  }
#endif
  /// @}
};

/**
   @brief CZ (controlled-Z) gate creator
   @ingroup cz
   
   This overload of the LibKet::gates::cz() function can be used as
   terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::cz();
   \endcode
*/
inline constexpr auto
cz() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCZ>(
    filters::QFilter{}, filters::QFilter{});
}

BINARY_GATE_OPTIMIZE_CREATOR_CTRL(QCZ, cz);
BINARY_GATE_DEFAULT_CREATOR(QCZ, cz);
GATE_ALIAS(cz, CZ);
GATE_ALIAS(cz, czdag);
GATE_ALIAS(cz, CZdag);
BINARY_GATE_DEFAULT_IMPL(QCZ, cz);

} // namespace gates

} // namespace LibKet

#endif // QGATE_CZ_HPP
