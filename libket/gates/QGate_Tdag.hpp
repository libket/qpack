/** @file libket/gates/QGate_Tdag.hpp

    @brief C++ API quantum \f$T\f$-dagger gate class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup tdag T gate (conjugate transpose)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_TDAG_HPP
#define QGATE_TDAG_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
class QT;

/**
@brief \f$T\f$-dagger gate class

The \f$T\f$-dagger gate class implements the quantum
\f$T\f$-dagger gate for an arbitrary number of quantum bits.


The \f$T\f$-dagger is a single-qubit operation that maps the basis
state \f$\left|0\right>\f$ to \f$\left|0\right>\f$ and
\f$\left|1\right>\f$ to \f$\exp(-i\frac{\pi}{4})\left|1\right>\f$.

The \f$T^{\dagger}\f$ gate is also defined as the conjugate transpose
of the \f$T\f$ gate

The unitary matrix reads

\f[
T^{\dagger} =
\begin{pmatrix}
1 & 0 \\
0 & \exp(-i\frac{\pi}{4})
\end{pmatrix}
\f]

@ingroup tdag
*/
class QTdag : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QTdag, QT);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("DAG(T) q[" + utils::to_string(i) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("(cirq.T**-1).on(q[" + utils::to_string(i) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    std::string _expr = "tdag q[";
    for (auto i : _filter::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("tdg q[" + utils::to_string(i) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel([&]() { expr.kernel().tdag(i); });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("\ttdag q" + utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("DAGGER T " + utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    quest::ComplexMatrix2 tdag = {
      .real = { { 1.0, 0.0 }, { 0.0, 1.0 / std::sqrt(2.0) } },
      .imag = { { 0.0, 0.0 }, { 0.0, -1.0 / std::sqrt(2.0) } }
    };
    for (auto i : _filter::range(expr))
      quest::unitary(expr.reg(), i, tdag);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel(new qx::t_dag_gate(i));

    return expr;
  }
#endif
  /// @}
};

/**
   @brief \f$T\f$-dagger gate creator
   @ingroup tdag
   
   This overload of the LibKet::gate:gate_tdag() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::tdag();
   \endcode
*/
inline constexpr auto
tdag() noexcept
{
  return UnaryQGate<filters::QFilter, QTdag>(filters::QFilter{});
}

/// @brief \f$T\f$-dagger gate default implementation
/// @ingroup tdag
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_IDENTITY(QT, tdag);
UNARY_GATE_DEFAULT_CREATOR(QTdag, tdag);
GATE_ALIAS(tdag, Tdag);
UNARY_GATE_DEFAULT_IMPL(QTdag, tdag);
///@}

/// @brief \f$T\f$ gate default implementation
/// @ingroup t
/// @{
UNARY_GATE_DEFAULT_IMPL(QT, t);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_TDAG_HPP
