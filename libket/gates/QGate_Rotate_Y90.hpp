/** @file libket/gates/QGate_Rotate_Y90.hpp

    @brief C++ API quantum Rotate_Y90 class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup rotate_y90 Rotate-Y gate (+90 degree)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_ROTATE_Y90_HPP
#define QGATE_ROTATE_Y90_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
class QRotate_MY90;

/**
@brief Rotate_Y90 gate class

The Rotate_Y90 gate class implements the quantum Rotate_Y90
gate for an arbitrary number of quantum bits.

The Rotate_Y90 gate is a single-qubit rotation through angle
\f$\frac{\pi}{2}\f$ (radians) around the y-axis.

The unitary matrix reads

\f[
R_{y}(\frac{\pi}{2}) =
\begin{pmatrix}
\frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}\\
\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}
\end{pmatrix}
\f]

@ingroup rotate_y90
*/
class QRotate_Y90 : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QRotate_Y90, QRotate_MY90);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("RY[PI/2.0] q[" + utils::to_string(i) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("cirq.YPowGate(exponent=" + utils::to_string(M_PI_2) +
                         ").on(q[" + utils::to_string(i) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    std::string _expr = "y90 q[";
    for (auto i : _filter::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("ry(" + utils::to_string(M_PI_2) + ") q[" +
                         utils::to_string(i) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel([&]() { expr.kernel().ry90(i); });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("\ty90 q" + utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("RY(" + utils::to_string(M_PI_2) + ") " +
                         utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      quest::rotateY(expr.reg(), i, M_PI_2);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel(new qx::ry(i, M_PI_2));

    return expr;
  }
#endif
  /// @}
};

/**
   @brief Rotate_Y90 gate creator
   @ingroup rotate_y90
   
   This overload of the LibKet::gate:rotate_y90() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::rotate_y90();
   \endcode
*/
inline constexpr auto
rotate_y90() noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_Y90>(filters::QFilter{});
}

/// @brief Rotate_Y90 gate default implementation
/// @ingroup rotate_y90
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_IDENTITY(QRotate_MY90, rotate_y90);
UNARY_GATE_DEFAULT_CREATOR(QRotate_Y90, rotate_y90);
GATE_ALIAS(rotate_y90, ROTATE_Y90);
GATE_ALIAS(rotate_y90, ry90);
GATE_ALIAS(rotate_y90, RY90);
GATE_ALIAS(rotate_y90, Ry90);
GATE_ALIAS(rotate_y90, y90);
GATE_ALIAS(rotate_y90, Y90);
GATE_ALIAS(rotate_my90, rotate_y90dag);
GATE_ALIAS(rotate_my90, ROTATE_Y90dag);
GATE_ALIAS(rotate_my90, ry90dag);
GATE_ALIAS(rotate_my90, RY90dag);
GATE_ALIAS(rotate_my90, Ry90dag);
GATE_ALIAS(rotate_my90, y90dag);
GATE_ALIAS(rotate_my90, Y90dag);
UNARY_GATE_DEFAULT_IMPL(QRotate_Y90, rotate_y90);
/// @}

/// @brief Rotate_MY90 gate default implementation
/// @ingroup rotate_my90
/// @{
GATE_ALIAS(rotate_y90, rotate_my90dag);
GATE_ALIAS(rotate_y90, ROTATE_MY90dag);
GATE_ALIAS(rotate_y90, rmy90dag);
GATE_ALIAS(rotate_y90, RMY90dag);
GATE_ALIAS(rotate_y90, Rmy90dag);
GATE_ALIAS(rotate_y90, my90dag);
GATE_ALIAS(rotate_y90, MY90dag);
UNARY_GATE_DEFAULT_IMPL(QRotate_MY90, rotate_my90);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_Y90_HPP
