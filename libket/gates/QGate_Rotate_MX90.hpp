/** @file libket/gates/QGate_Rotate_MX90.hpp

    @brief C++ API quantum Rotate_MX90 class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup rotate_mx90 Rotate-X gate (-90 degree)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_ROTATE_MX90_HPP
#define QGATE_ROTATE_MX90_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
class QRotate_X90;
  
/**
@brief Rotate_MX90 gate class

The Rotate_MX90 gate class implements the quantum Rotate_MX90
gate for an arbitrary number of quantum bits.

The Rotate_MX90 gate is a single-qubit rotation through angle
\f$-\frac{\pi}{2}\f$ (radians) around the x-axis.

The unitary matrix reads

\f[
R_{x}\left(-\frac{\pi}{2}\right) =
\begin{pmatrix}
\frac{1}{\sqrt{2}} & i\frac{1}{\sqrt{2}}\\
i\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}
\end{pmatrix}
\f]

@ingroup rotate_mx90
*/
class QRotate_MX90 : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QRotate_MX90, QRotate_X90);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("RX[-PI/2.0] q[" + utils::to_string(i) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel(
        "cirq.XPowGate(exponent=" + utils::to_string(M_PI_2 + M_PI) +
        ").on(q[" + utils::to_string(i) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    std::string _expr = "mx90 q[";
    for (auto i : _filter::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("rx(" + utils::to_string(M_PI_2 + M_PI) + ") q[" +
                         utils::to_string(i) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel([&]() { expr.kernel().mrx90(i); });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @note Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("\tmx90 q" + utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("RX(" + utils::to_string(M_PI_2 + M_PI) + ") " +
                         utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      quest::rotateX(expr.reg(), i, M_PI_2 + M_PI);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel(new qx::rx(i, M_PI_2 + M_PI));

    return expr;
  }
#endif
  /// @}
};

/**
   @brief Rotate_MX90 gate creator
   @ingroup rotate_mx90
   
   This overload of the LibKet::gate:rotate_mx90() function can be
   used as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::rotate_mx90();
   \endcode
*/
inline constexpr auto
rotate_mx90() noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_MX90>(filters::QFilter{});
}

/// @brief Rotate_MX90 gate default implementation
/// @ingroup rotate_mx90
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_IDENTITY(QRotate_X90, rotate_mx90);
UNARY_GATE_DEFAULT_CREATOR(QRotate_MX90, rotate_mx90);
GATE_ALIAS(rotate_mx90, ROTATE_MX90);
GATE_ALIAS(rotate_mx90, rmx90);
GATE_ALIAS(rotate_mx90, RMX90);
GATE_ALIAS(rotate_mx90, Rmx90);
GATE_ALIAS(rotate_mx90, mx90);
GATE_ALIAS(rotate_mx90, MX90);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_MX90_HPP
