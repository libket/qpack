/** @file libket/gates/QGate_Sqrt_Not.hpp

    @brief C++ API quantum square-root-of-NOT class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup sqrtnot Sqrt-NOT gate
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_SQRT_NOT_HPP
#define QGATE_SQRT_NOT_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>
#include <gates/QGate_Rotate_Y.hpp>
#include <gates/QGate_Rotate_Z.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace gates {

/**
@brief Square-root-of-NOT gate class

The Quantum square root of NOT gate class implements the square
root of NOT gate for an arbitrary number of qubits.

The square root of NOT gate (or square root of Pauli_X,
\f$\sqrt{X}\f$) acts on a single qubit. It maps the basis state
\f$\left|0\right>\f$ to \f$\frac{(1+i)\left|0\right> +
(1-i)\left|1\right>}{2}\f$ and \f$\left|1\right>\f$ to
\f$\frac{(1-i)\left|0\right> + (1+i)\left|1\right>}{2}\f$.

The unitary matrix reads

\f[
\sqrt{X} = \frac{1}{2}
\begin{pmatrix}
1+i & 1-i\\                                          \
1-i & 1+i
\end{pmatrix}
\f]

@ingroup sqrtnot
*/
template<typename _tol = QConst_M_ZERO_t>
class QSqrt_Not : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL_T(QSqrt_Not, QSqrt_Not);
  
  /// @brief Apply function
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static QExpression<_qubits, _qbackend>& apply(
    QExpression<_qubits, _qbackend>& expr) noexcept
  {
    auto e = Rz(-QConst_M_PI_2,
                Ry(QConst_M_PI_2,
                   Rz(QConst_M_PI_2,
                      _filter{})));
    return e(expr);
  }
};

/**
   @brief Square-root-of-NOT gate creator
   @ingroup sqrtnot
   
   This overload of the LibKet::gates::sqrt_not() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto qcirc = gates::sqrt_not();
   \endcode
*/
template<typename _tol = QConst_M_ZERO_t>
inline constexpr auto
sqrt_not() noexcept
{
  return UnaryQGate<filters::QFilter, QSqrt_Not<_tol>>(filters::QFilter{});
}
  
/// @brief Square-root-of-NOT gate default implementation
/// @ingroup sqrtnot
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_T_IDENTITY(QSqrt_Not, sqrt_not);
UNARY_GATE_DEFAULT_CREATOR_T(QSqrt_Not, sqrt_not);
GATE_ALIAS_T(sqrt_not, SQRT_NOT);
GATE_ALIAS_T(sqrt_not, snot);
GATE_ALIAS_T(sqrt_not, SNOT);
GATE_ALIAS_T(sqrt_not, sqrt_notdag);
GATE_ALIAS_T(sqrt_not, SQRT_NOTdag);
GATE_ALIAS_T(sqrt_not, snotdag);
GATE_ALIAS_T(sqrt_not, SNOTdag);
UNARY_GATE_DEFAULT_IMPL_T(QSqrt_Not, sqrt_not);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_SQRT_NOT_HPP
