/** @file libket/gates/QGate_CPhaseK.hpp

    @brief C++ API quantum CPHASEK (controlled phase shift with
    \f$\pi/2^k\f$ angle) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup cphasek CPhase-k gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_CPHASEK_HPP
#define QGATE_CPHASEK_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<std::size_t k, typename _tol>
class QCPhaseKdag;
      
/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) gate
class

The CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
gate class implements the quantum controlled phase shift with
\f$\pi/2^k\f$ angle gate for an arbitrary number of quantum bits

The CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) gate is a
two-qubit operation, where the first qubit is usually referred to as
the control qubit and the second qubit as the target qubit. It maps
the basis state \f$\left|00\right>\f$ to \f$\left|00\right>\f$,
\f$\left|01\right>\f$ to \f$\left|01\right>\f$, \f$\left|10\right>\f$
to \f$\left|10\right>\f$ and \f$\left|11\right>\f$ to \f$\exp(\frac{2
\pi i}{2^{k}})\left|11\right>\f$.

The CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) gates
leaves the control qubit unchanged and performs a Phase gate on the
target qubit only when the control qubit is in state
\f$\left|1\right>\f$.


The unitary matrix reads

\f[
\text{CPHASEK} =
\begin{pmatrix}
1 & 0 & 0 & 0\\                                          \
0 & 1 & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & \exp(\frac{2 \pi i}{2^{k}})
\end{pmatrix}
\f]

@ingroup cphasek
*/
template<std::size_t k, typename _tol = QConst_M_ZERO_t>
class QCPhaseK : public QGate
{
public:
  /// Rotation angle 2*PI/2^k
  real_t static constexpr angle = 2 * M_PI / (1 << k);

  BINARY_GATE_DEFAULT_DECL_KT(QCPhaseK, QCPhaseKdag);
  
  ///@{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CTRL(PH[" + utils::to_string(angle) + "]) q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel(
          "cirq.CZPowGate(exponent=" + utils::to_string(angle) + ").on(q[" +
          utils::to_string(std::get<0>(i)) + "],q[" +
          utils::to_string(std::get<1>(i)) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      std::string _expr = "cr q[";
      for (auto i : _filter0::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(expr).end() - 1) ? "," : "], ");
      _expr += utils::to_string(angle) + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cu1(" + utils::to_string(angle) + ") q[" +
                           utils::to_string(std::get<0>(i)) + "], q[" +
                           utils::to_string(std::get<1>(i)) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    std::cerr << "The CRR/CPHASEK gate is not implemented for OpenQL!!!\n";
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel([&]() {
          expr.kernel().controlled_rz(std::get<0>(i), std::get<1>(i), angle);
        });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("\tcphase" + utils::to_string(k) + " q" +
                           utils::to_string(std::get<0>(i)) + ",q" +
                           utils::to_string(std::get<1>(i)) + " # 2pi/2^" +
                           utils::to_string(k) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CPHASE (" + utils::to_string(angle) + ") " +
                           utils::to_string(std::get<0>(i)) + " " +
                           utils::to_string(std::get<1>(i)) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        quest::controlledPhaseShift(
          expr.reg(), std::get<0>(i), std::get<1>(i), (qreal)angle);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel(new qx::ctrl_phase_shift(
          std::get<0>(i), std::get<1>(i), (double)angle));
    }
    return expr;
  }
#endif
  ///@}
};

// Complete type definition
template<std::size_t k, typename _tol>
real_t constexpr QCPhaseK<k, _tol>::angle;

/// @ingroup cphasek
///@{

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) gate
creator

This overload of the LibKet::gates::cphasek() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cphasek();
\endcode
*/
template<std::size_t k, typename _tol = QConst_M_ZERO_t>
inline constexpr auto
cphasek() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCPhaseK<k, _tol>>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename _expr1,
         typename = void>
struct cphasek_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QCPhaseK<k, _tol>,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0_0, __expr0_1, QCPhaseK<k, _tol>, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QCPhaseK<k, _tol>, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QCPhaseK<k, _tol>, __filter0>&
      expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QCPhaseK<k, _tol>, __filter1>&
      expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return cphasek_impl<
      _tol,
      k,
      decltype(typename filters::getFilter<__filter0>::type{}(all(
        expr1.expr1(all(expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
      typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return cphasek_impl<
      _tol,
      k,
      typename filters::getFilter<__filter0>::type,
      decltype(typename filters::getFilter<__filter1>::type{}(all(
        expr0.expr0(all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphasek_impl<
  _tol,
  k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphasek_impl<
  _tol,
  k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphasek_impl<
  _tol,
  k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphasek_impl<
  _tol,
  k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphasek_impl<
  _tol,
  k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCPhaseK here */
    !std::is_same<typename gates::getGate<_expr0>::type,
                  QCPhaseK<k,_tol> >::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphasek_impl<
  _tol,
  k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphasek_impl<
  _tol,
  k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CPhaseK gate is handled explicitly */
    (!std::is_base_of<
      QCPhaseK<k, _tol>,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCPhaseK here */
    !std::is_same<typename gates::getGate<_expr1>::type,
                  QCPhaseK<k, _tol>>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle)
   creator

   This overload of the LibKet::gates::cphasek() function
   eliminates the double-application of the CPhaseK gate
*/
template<typename _tol,
         std::size_t k,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphasek_impl<
  _tol,
  k,
  BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CPhaseK gate is handled explicitly */
    (!std::is_base_of<
      QCPhaseK<k, _tol>,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseK<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseK gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) creator

This overload of the LibKet::gates::cphasek() function accepts
two expressions as constant reference
*/
template<std::size_t k,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::cphasek_impl<_tol, k, _expr0, _expr1>::apply(
    expr0, expr1);
}

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) creator

This overload of the LibKet::gates::cphasek() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<std::size_t k,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::cphasek_impl<_tol, k, _expr0, _expr1>::apply(
    expr0, expr1);
}

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) creator

This overload of the LibKet::gates::cphasek() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<std::size_t k,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::cphasek_impl<_tol, k, _expr0, _expr1>::apply(
    expr0, expr1);
}

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) creator

This overload of the LibKet::gates::cphasek() function accepts
two expression as universal reference
*/
template<std::size_t k,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::cphasek_impl<_tol, k, _expr0, _expr1>::apply(
    expr0, expr1);
}

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) gate
creator

Function alias for LibKet::gates::cphasek()
*/
template<std::size_t k, typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
CPHASEK(Args&&... args)
{
  return cphasek<k, _tol>(std::forward<Args>(args)...);
}

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) gate
creator

Function alias for LibKet::gates::cphasek()
*/
template<std::size_t k, typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
crk(Args&&... args)
{
  return cphasek<k, _tol>(std::forward<Args>(args)...);
}

/**
@brief CPHASEK (controlled phase shift with \f$\pi/2^k\f$ angle) gate
creator

Function alias for LibKet::gates::cphasek()
*/
template<std::size_t k, typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
CRK(Args&&... args)
{
  return cphasek<k, _tol>(std::forward<Args>(args)...);
}

///BINARY_GATE_DEFAULT_IMPL_KT(QCPhaseK, cphasek);
///@}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CPHASEK_HPP
