/** @file libket/gates/QGate_Prep_X.hpp

    @brief C++ API quantum Prep_X class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup prep_x Prep-X gate
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_PREP_X_HPP
#define QGATE_PREP_X_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief Prep_X class

The Prep_X class implements the initialization of an arbitrary
number of quantum bits in the X-basis. Striktly speaking, Prep_X is
not a quantum gate.

By using Prep_X, qubits will be initialized in the x-basis in the
\f$\left|+\right> = \frac{\left|0\right> +
\left|1\right>}{\sqrt{2}}\f$ state

@ingroup prep_x
*/
class QPrep_X : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QPrep_X, QPrep_X);

  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("RESET q[" + utils::to_string(i) + "]\n");
      expr.append_kernel("H q[" + utils::to_string(i) + "]\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ////
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("cirq.reset(q[" + utils::to_string(i) + "])\n");
      expr.append_kernel("cirq.H.on(q[" + utils::to_string(i) + "])\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    std::string _expr = "prep_x q[";
    for (auto i : _filter::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("reset q[" + utils::to_string(i) + "];\n");
      expr.append_kernel("h q[" + utils::to_string(i) + "];\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel([&]() {
        expr.kernel().prepz(i);
        expr.kernel().h(i);
      });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("\tprep_x q" + utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    for (auto i : _filter::range(expr)) {
      expr.append_kernel("RESET " + utils::to_string(i) + "\n");
      expr.append_kernel("H " + utils::to_string(i) + "\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    quest::initPlusState(expr.reg());

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel(new qx::prepx(i));

    return expr;
  }
#endif
  /// @}
};

/**
   @brief Prep_X gate creator
   @ingroup prep_x
   
   This overload of the LibKet::gates::prep_x() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::prep_x();
   \endcode
*/
inline constexpr auto
prep_x() noexcept
{
  return UnaryQGate<filters::QFilter, QPrep_X>(filters::QFilter{});
}

/// @brief Prep_X gate default implementation
/// @ingroup prep_x
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_SINGLE(QPrep_X, prep_x);
UNARY_GATE_DEFAULT_CREATOR(QPrep_X, prep_x);
GATE_ALIAS(prep_x, PREP_X);
UNARY_GATE_DEFAULT_IMPL(QPrep_X, prep_x);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_PREP_X_HPP
