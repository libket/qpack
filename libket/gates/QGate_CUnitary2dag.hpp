/** @file libket/gates/QGate_CUnitary2dag.hpp

    @brief C++ API \f$2\times 2\f$ controlled unitary gate (conjugate transpose) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup cu2dag CU2 gate (conjugate transpose)
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_CUNITARY2DAG_HPP
#define QGATE_CUNITARY2DAG_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _functor, typename _tol>
class QCUnitary2;
  
/**
@brief \f$2\times 2\f$ controlled unitary gate (conjugate transpose) class

The \f$2\times2\f$ controlled unitary gate (conjugate transpose)
accepts an arbitrary \f$2\times 2\f$ unitary matrix \f$U\f$ as input
and performs the ZYZ decomposition of the conjugate transpose of
\f$U\f$

\f[
U^\dagger = \exp(i\Phi) R_z(-\gamma)R_y(-\beta)R_z(-\alpha)
\f]

with \f$\Phi,\alpha,\beta,\gamma\f$ are rotation angles.

@ingroup cu2dag
*/
template<typename _functor, typename _tol = QConst_M_ZERO_t>
class QCUnitary2dag : public QGate
{
public:
  //BINARY_GATE_DEFAULT_DECL_FTOR_T(QCUnitary2dag, QCUnitary2);
  
  /// Apply function
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           QBackendType _qbackend>
  inline static QExpression<_qubits, _qbackend>& apply(
    QExpression<_qubits, _qbackend>& expr)
  {
    return expr;
  }
  
private:
  inline static arma::vec decompose()
  {
    arma::vec u = _functor{}();
    arma::vec x = { 0.0, 1.0, 1.0, 0.0,
                    0.0, 1.0, 1.0, 0.0,
                    0.0, 1.0, 1.0, 0.0, 1.0 };

    if (!optim::de(x,
                   QCUnitary2<_functor, _tol>::decompose_fn,
                   &u))
      throw std::runtime_error("QCUnitary2: An error occured in the decomposition");

    return x;
  }

  static double decompose_fn(const arma::vec& vals_inp,
                             arma::vec* grad_out,
                             void* opt_expr)
  {
    // Get the matrix values from the input vector
    arma::vec u_expr = *(arma::vec*)(opt_expr);

    // Name the seperate components of the input and output vectors to
    // make the calculation more readable and a one-on-one copy of the
    // original way of writing it out.
    double u_1 = u_expr(0);
    double u_2 = u_expr(1);
    double u_3 = u_expr(2);
    double u_4 = u_expr(3);

    double a_1 = vals_inp(0);
    double a_2 = vals_inp(1);
    double a_3 = vals_inp(2);
    double a_4 = vals_inp(3);

    double b_1 = vals_inp(4);
    double b_2 = vals_inp(5);
    double b_3 = vals_inp(6);
    double b_4 = vals_inp(7);

    double c_1 = vals_inp(8);
    double c_2 = vals_inp(9);
    double c_3 = vals_inp(10);
    double c_4 = vals_inp(11);

    double alpha = vals_inp(12);

    // Define the necessary imaginary number
    std::complex<double> mycomplex((double)0.0, (double)1.0);

    using namespace std;
    double obj_val = abs(exp(imag(mycomplex) * alpha) *
                         ((a_2 * b_2 * c_1) + (a_1 * b_4 * c_1) +
                          (a_2 * b_1 * c_3) + (a_1 * b_3 * c_3)) -
                         u_1) +
                     abs(exp(imag(mycomplex) * alpha) *
                         ((a_2 * b_2 * c_2) + (a_1 * b_3 * c_2) +
                          (a_2 * b_1 * c_4) + (a_1 * b_3 * c_4)) -
                         u_2) +
                     abs(exp(imag(mycomplex) * alpha) *
                         ((a_4 * b_2 * c_1) + (a_3 * b_4 * c_1) +
                          (a_4 * b_1 * c_3) + (a_3 * b_3 * c_3)) -
                         u_3) +
                     abs(exp(imag(mycomplex) * alpha) *
                         ((a_4 * b_2 * c_2) + (a_3 * b_4 * c_2) +
                          (a_4 * b_1 * c_4) + (a_3 * b_3 * c_4)) -
                         u_4) +
                     abs(a_1 * b_1 * c_1 + a_2 * b_3 * c_1 + a_1 * b_2 * c_3 +
                         a_2 * b_4 * c_3 - 1.0) +
                     abs(a_1 * b_1 * c_2 + a_2 * b_3 * c_2 + a_1 * b_2 * c_4 +
                         a_2 * b_4 * c_4) +
                     abs(a_3 * b_1 * c_1 + a_4 * b_3 * c_1 + a_3 * b_2 * c_3 +
                         a_4 * b_4 * c_3) +
                     abs(a_3 * b_1 * c_2 + a_4 * b_3 * c_2 + a_3 * b_2 * c_4 +
                         a_4 * b_4 * c_4 - 1.0);

    return obj_val;
  }
};
  
/**
   @brief \f$2\times 2\f$ controlled unitary gate (conjugate transpose) creator

   This overload of the LibKet::gates::cunitary2dag() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression

   \code
   struct CU2_ftor {
   inline auto operator()() const noexcept
    return arma::vec{ 1.0/sqrt(2.0), -1.0/sqrt(2.0),                               
                      1.0/sqrt(2.0),  1.0/sqrt(2.0)};
   };

   auto expr = gates::cunitary2dag<CU2_ftor>();
   \endcode
*/
template<typename _functor, typename _tol = QConst_M_ZERO_t>
inline constexpr auto cunitary2dag() noexcept
{
  return BinaryQGate<filters::QFilter, QCUnitary2dag<_functor, _tol>, filters::QFilter>(
                                                                                        filters::QFilter{}, filters::QFilter{});
}

/// @brief \f$2\times 2\f$ controlled unitary gate (conjugate transpose) default implementation
/// @ingroup cu2dag
/// @{
//BINARY_GATE_DEFAULT_CREATOR_FTOR_T(QCUnitary2dag, cunitary2dag);
//GATE_ALIAS_FTOR_T(cunitary2dag, cu2dag);
//GATE_ALIAS_FTOR_T(cunitary2dag, CUNITARY2dag);
//GATE_ALIAS_FTOR_T(cunitary2dag, CU2dag);
//BINARY_GATE_DEFAULT_IMPL_FTOR_T(QCUnitary2dag, cunitary2dag);
///@}

/// @brief \f$2\times 2\f$ controlled unitary gate default implementation
/// @ingroup cu2
/// @{
//BINARY_GATE_DEFAULT_IMPL_FTOR_T(QCUnitary2, cunitary2);
///@}
} // namespace gates

} // namespace LibKet

#endif // QGATE_CUNITARY2DAG_HPP
