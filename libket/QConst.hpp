/** @file libket/QConst.hpp

    @brief C++ API quantum constant class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup cxx_api
*/

#pragma once
#ifndef QCONST_HPP
#define QCONST_HPP

#include <iomanip>
#include <sstream>
#include <type_traits>

#include <QConfig.h>

// Useful preprocessor macros
#define VARGS_(_10, _9, _8, _7, _6, _5, _4, _3, _2, _1, N, ...) N
#define VARGS(...) VARGS_(__VA_ARGS__, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)

#define CONCAT_(a, b) a##b
#define CONCAT(a, b) CONCAT_(a, b)

namespace LibKet {

  /**
     @brief
     Compile-time constant string type

     The compile-time constant string type makes it possible to
     encode objects of type std::string into types so that the
     content of the string can be extracted from the type and does
     not need an instantiated object.

     The constvalue_t type is be used to store, e.g., parameter values
     of quantum gates in the LibKet::QGate classes
  */
  template<char... Cs>
  struct constvalue_t
  {
  private:
    /// Capture leading character
    template<char C_, char... Cs_>
    struct lchar_t
    {
      static constexpr char value = C_;
    };

  public:
    /// Default constructor
    constexpr constvalue_t() = default;

    /// Returns value as string
    inline static std::string to_string(bool symbol=false)
    {
      std::string str;
      int unpack[]{ 0, (str += Cs, 0)... };
      static_cast<void>(unpack);
      
      // Remove trailing '\x00' symbols
      str.erase(std::remove(str.begin(), str.end(), '\x00'), str.end());
      
      return eval(str); 
    }

    /// Returns type as string
    inline static std::string to_type()
    {
      if (sizeof...(Cs) <= 1)
        return "QConst1_t(" + to_string() + ")";
      else if (sizeof...(Cs) <= 2)
        return "QConst2_t(" + to_string() + ")";
      else if (sizeof...(Cs) <= 4)
        return "QConst4_t(" + to_string() + ")";
      else if (sizeof...(Cs) <= 8)
        return "QConst8_t(" + to_string() + ")";
      else if (sizeof...(Cs) <= 16)
        return "QConst16_t(" + to_string() + ")";
      else if (sizeof...(Cs) <= 32)
        return "QConst32_t(" + to_string() + ")";
      else if (sizeof...(Cs) <= 64)
        return "QConst64_t(" + to_string() + ")";
      else if (sizeof...(Cs) <= 128)
        return "QConst128_t(" + to_string() + ")";
      else
        return "QConst256_t(" + to_string() + ")";
    }

    /// Returns object as string
    inline static std::string to_obj()
    {
      if (sizeof...(Cs) <= 1)
        return "QConst1(" + to_string() + ")";
      else if (sizeof...(Cs) <= 2)
        return "QConst2(" + to_string() + ")";
      else if (sizeof...(Cs) <= 4)
        return "QConst4(" + to_string() + ")";
      else if (sizeof...(Cs) <= 8)
        return "QConst8(" + to_string() + ")";
      else if (sizeof...(Cs) <= 16)
        return "QConst16(" + to_string() + ")";
      else if (sizeof...(Cs) <= 32)
        return "QConst32(" + to_string() + ")";
      else if (sizeof...(Cs) <= 64)
        return "QConst64(" + to_string() + ")";
      else if (sizeof...(Cs) <= 128)
        return "QConst128(" + to_string() + ")";
      else
        return "QConst256(" + to_string() + ")";
    }

    /// Returns value as float
    inline static float to_float()
    {
      return std::stof(to_string());
    }

    /// Returns value as double
    inline static double to_double()
    {
      return std::stod(to_string());
    }

    /// Returns value as long double
    inline static long double to_long_double()
    {
      return std::stold(to_string());
    }

    /// Returns value as real-valued number
    inline static real_t value()
    {
      return (std::is_same<real_t, float>::value
              ? std::stof(to_string())
              : (std::is_same<real_t, double>::value
                 ? std::stod(to_string())
                 : std::stold(to_string())));
    }

    /// Operator+()
    template<char D, char... Ds>
    inline auto
    operator+(const constvalue_t<D, Ds...>&) const
    {
      return constvalue_t<Cs...,'+',D, Ds...>{};
    }

    /// Operator-()
    template<char D, char... Ds>
    inline auto
    operator-(const constvalue_t<D, Ds...>&) const
    {
      return constvalue_t<Cs...,'-',D, Ds...>{};
    }

    /// Operator*()
    template<char D, char... Ds>
    inline auto
    operator*(const constvalue_t<D, Ds...>&) const
    {
      return constvalue_t<Cs...,'*',D, Ds...>{};
    }

    /// Operator/()
    template<char D, char... Ds>
    inline auto
    operator/(const constvalue_t<D, Ds...>&) const
    {
      return constvalue_t<Cs...,'/',D, Ds...>{};
    }
    
  private:
    /// Evaluates the string expression
    inline static std::string eval(std::string str)
    {
      std::size_t ipos;      
      ipos = str.rfind("+");
      if (ipos != std::string::npos && ipos > 0)
        return to_string_precision(std::stold(eval(str.substr(0,ipos)))
                                   +
                                   std::stold(eval(str.substr(ipos+1))));
      
      ipos = str.rfind("-");
      if (ipos != std::string::npos && ipos > 0)
        return to_string_precision(std::stold(eval(str.substr(0,ipos)))
                                   -
                                   std::stold(eval(str.substr(ipos+1))));
      
      ipos = str.rfind("*");
      if (ipos != std::string::npos && ipos > 0)
        return to_string_precision(std::stold(eval(str.substr(0,ipos)))
                                   *
                                   std::stold(eval(str.substr(ipos+1))));
      
      ipos = str.rfind("/");
      if (ipos != std::string::npos && ipos > 0)
        return to_string_precision(std::stold(eval(str.substr(0,ipos)))
                                   /
                                   std::stold(eval(str.substr(ipos+1))));
      
      return str;      
    }

    /// Formats the real-valued number to a string
    inline static std::string to_string_precision(long double value)
    {
      std::ostringstream ss;
      ss << std::fixed;
      ss << std::setprecision(std::numeric_limits<long double>::digits10);
      ss << value;

      std::string str = ss.str();
      str.erase (str.find_last_not_of('0') + 1, std::string::npos);
      
      return str;
    }
  };

  namespace detail {
    // Strip last character in constvalue_t type
    template<char... Cs>
    struct strip_constvalue_t;

    template<char C>
    struct strip_constvalue_t<C>
    {
      using type = constvalue_t<>;
    };

    template<typename, typename>
    struct concat_constvalue_t
    {};

    template<char... Cs, char... Ds>
    struct concat_constvalue_t<constvalue_t<Cs...>, constvalue_t<Ds...>>
    {
      using type = constvalue_t<Cs..., Ds...>;
    };

    template<char C, char... Cs>
    struct strip_constvalue_t<C, Cs...>
    {
      using type =
        typename concat_constvalue_t<constvalue_t<C>,
                                     typename strip_constvalue_t<Cs...>::type>::type;
    };

  } // namespace detail

  /// Operator+()
  template<char C, char... Cs>
  inline static auto
  operator+(const constvalue_t<C, Cs...>&)
  {
    return constvalue_t<C, Cs...>{};
  }

  /// Operator-()
  template<char C, char... Cs>
  inline static auto
  operator-(const constvalue_t<C, Cs...>&)
  {
    return typename LibKet::detail::strip_constvalue_t<'-', C, Cs...>::type{};
  }

  /// Operator-()
  template<char... Cs>
  inline static auto
  operator-(const constvalue_t<'-', Cs...>&)
  {
    return constvalue_t<Cs..., '\000'>{};
  }

  /// Operator==()
  template<char... Cs, char... Ds>
  inline static bool
  operator==(const constvalue_t<Cs...>&, const constvalue_t<Ds...>&)
  {
    return (constvalue_t<Cs...>::value() == constvalue_t<Ds...>::value());
  }

  /// Operator!=()
  template<char... Cs, char... Ds>
  inline static bool
  operator!=(const constvalue_t<Cs...>&, const constvalue_t<Ds...>&)
  {
    return (constvalue_t<Cs...>::value() != constvalue_t<Ds...>::value());
  }

  /// Operator<=()
  template<char... Cs, char... Ds>
  inline static bool
  operator<=(const constvalue_t<Cs...>&, const constvalue_t<Ds...>&)
  {
    return (constvalue_t<Cs...>::value() <= constvalue_t<Ds...>::value());
  }

  /// Operator>=()
  template<char... Cs, char... Ds>
  inline static bool
  operator>=(const constvalue_t<Cs...>&, const constvalue_t<Ds...>&)
  {
    return (constvalue_t<Cs...>::value() >= constvalue_t<Ds...>::value());
  }

  /// Operator<()
  template<char... Cs, char... Ds>
  inline static bool
  operator<(const constvalue_t<Cs...>&, const constvalue_t<Ds...>&)
  {
    return (constvalue_t<Cs...>::value() < constvalue_t<Ds...>::value());
  }

  /// Operator>()
  template<char... Cs, char... Ds>
  inline static bool
  operator>(const constvalue_t<Cs...>&, const constvalue_t<Ds...>&)
  {
    return (constvalue_t<Cs...>::value() > constvalue_t<Ds...>::value());
  }

  /// Tolerance function: true if abs(first argument) > abs(second argument)
  template<char... Cs, char... Ds>
  inline static bool
  tolerance(const constvalue_t<Cs...>&, const constvalue_t<Ds...>&)
  {
    return (std::abs(constvalue_t<Cs...>::value()) >
            std::abs(constvalue_t<Ds...>::value()));
  }

  /// Tolerance function: true if abs(value) > abs(second argument)
  template<char... Ds>
  inline static bool
  tolerance(const real_t& value, const constvalue_t<Ds...>&)
  {
    return (std::abs(value) > std::abs(constvalue_t<Ds...>::value()));
  }

  /// Serialization operat
  template<char... Cs>
  std::ostream&
  operator<<(std::ostream& os, const constvalue_t<Cs...>& obj)
  {
    os << obj.to_obj();
    return os;
  }

  namespace detail {

    /**
       @brief
       Converts strings into types

       @note
       This is a simplified version of the 'typestring' header file
       developed by George Makrydakis <george@irrequietus.eu>.

       The header of the original 'typestring' header file reads as follows:

       @copyright Copyright (C) 2015, 2016 George Makrydakis
       <george@irrequietus.eu>

       The 'typestring' header is a single header C++ library for
       creating types to use as type parameters in template
       instantiations, repository available at
       https://github.com/irrequietus/typestring. Conceptually
       stemming from own implementation of the same thing (but in a
       more complicated manner to be revised) in 'clause':
       https://github.com/irrequietus/clause.

       File subject to the terms and conditions of the Mozilla
       Public License v 2.0. If a copy of the MPLv2 license text
       was not distributed with this file, you can obtain it at:
       http://mozilla.org/MPL/2.0/.
    */
    template<int N, int M>
    constexpr char
    tygrab(char const (&c)[M]) noexcept
    {
      return c[N < M ? N : M - 1];
    }

  } // namespace detail
} // namespace LibKet

// clang-format off

///@{
/** @name Macros to encode std::string into LibKet::detail::tygrab */
#define TYPESTRING1(n, x) \
  LibKet::detail::tygrab<0x##n##0>(x)

#define TYPESTRING2(n, x)                                                   \
  LibKet::detail::tygrab<0x##n##0>(x), LibKet::detail::tygrab<0x##n##1>(x)

#define TYPESTRING4(n, x)                                                   \
  LibKet::detail::tygrab<0x##n##0>(x), LibKet::detail::tygrab<0x##n##1>(x), \
  LibKet::detail::tygrab<0x##n##2>(x), LibKet::detail::tygrab<0x##n##3>(x)

#define TYPESTRING8(n, x)                                                   \
  LibKet::detail::tygrab<0x##n##0>(x), LibKet::detail::tygrab<0x##n##1>(x), \
  LibKet::detail::tygrab<0x##n##2>(x), LibKet::detail::tygrab<0x##n##3>(x), \
  LibKet::detail::tygrab<0x##n##4>(x), LibKet::detail::tygrab<0x##n##5>(x), \
  LibKet::detail::tygrab<0x##n##6>(x), LibKet::detail::tygrab<0x##n##7>(x)

#define TYPESTRING16(n, x)                                                  \
  LibKet::detail::tygrab<0x##n##0>(x), LibKet::detail::tygrab<0x##n##1>(x), \
  LibKet::detail::tygrab<0x##n##2>(x), LibKet::detail::tygrab<0x##n##3>(x), \
  LibKet::detail::tygrab<0x##n##4>(x), LibKet::detail::tygrab<0x##n##5>(x), \
  LibKet::detail::tygrab<0x##n##6>(x), LibKet::detail::tygrab<0x##n##7>(x), \
  LibKet::detail::tygrab<0x##n##8>(x), LibKet::detail::tygrab<0x##n##9>(x), \
  LibKet::detail::tygrab<0x##n##A>(x), LibKet::detail::tygrab<0x##n##B>(x), \
  LibKet::detail::tygrab<0x##n##C>(x), LibKet::detail::tygrab<0x##n##D>(x), \
  LibKet::detail::tygrab<0x##n##E>(x), LibKet::detail::tygrab<0x##n##F>(x)
///@}

///@{
/** @name Macros to create LibKet QConst_t types */
#define QConst1_t(value)                        \
  LibKet::constvalue_t<TYPESTRING1(0, #value)>
#define QConst2_t(value)                        \
  LibKet::constvalue_t<TYPESTRING2(0, #value)>
#define QConst4_t(value)                        \
  LibKet::constvalue_t<TYPESTRING4(0, #value)>
#define QConst8_t(value)                        \
  LibKet::constvalue_t<TYPESTRING8(0, #value)>
#define QConst16_t(value)                       \
  LibKet::constvalue_t<TYPESTRING16(0, #value)>
#define QConst32_t(value)                       \
  LibKet::constvalue_t<TYPESTRING16(0, #value), \
                       TYPESTRING16(1, #value)>
#define QConst64_t(value)                       \
  LibKet::constvalue_t<TYPESTRING16(0, #value), \
                       TYPESTRING16(1, #value), \
                       TYPESTRING16(2, #value), \
                       TYPESTRING16(3, #value)>
#define QConst128_t(value)                      \
  LibKet::constvalue_t<TYPESTRING16(0, #value), \
                       TYPESTRING16(1, #value), \
                       TYPESTRING16(2, #value), \
                       TYPESTRING16(3, #value), \
                       TYPESTRING16(4, #value), \
                       TYPESTRING16(5, #value), \
                       TYPESTRING16(6, #value), \
                       TYPESTRING16(7, #value)>
#define QConst256_t(value)                       \
  LibKet::constvalue_t<TYPESTRING16(0, #value),  \
                       TYPESTRING16(1, #value),  \
                       TYPESTRING16(2, #value),  \
                       TYPESTRING16(3, #value),  \
                       TYPESTRING16(4, #value),  \
                       TYPESTRING16(5, #value),  \
                       TYPESTRING16(6, #value),  \
                       TYPESTRING16(7, #value),  \
                       TYPESTRING16(8, #value),  \
                       TYPESTRING16(9, #value),  \
                       TYPESTRING16(10, #value), \
                       TYPESTRING16(11, #value), \
                       TYPESTRING16(12, #value), \
                       TYPESTRING16(13, #value), \
                       TYPESTRING16(14, #value), \
                       TYPESTRING16(15, #value)>
///@}

///@{
/** @name Macros to create LibKet QConst instances */
#define QConst1(value)   QConst1_t(value) ()
#define QConst2(value)   QConst2_t(value) ()
#define QConst4(value)   QConst4_t(value) ()
#define QConst8(value)   QConst8_t(value) ()
#define QConst16(value)  QConst16_t(value) ()
#define QConst32(value)  QConst32_t(value) ()
#define QConst64(value)  QConst64_t(value) ()
#define QConst128(value) QConst128_t(value) ()
#define QConst256(value) QConst256_t(value) ()
///@}

#define QConst(value) QConst_t(value)()

///@{
/** @name Predefined LibKet constants **/
#define QConst_M_1_PI      QConst32(0.31830988618379067154)
#define QConst_M_1_PIl     QConst64(0.318309886183790671537767526745028724)
#define QConst_M_2_PI      QConst32(0.63661977236758134308)
#define QConst_M_2_PIl     QConst64(0.636619772367581343075535053490057448)
#define QConst_M_2_SQRTPI  QConst32(1.12837916709551257390)
#define QConst_M_2_SQRTPIl QConst64(1.128379167095512573896158903121545172)
#define QConst_M_E         QConst32(2.7182818284590452354)
#define QConst_M_El        QConst64(2.718281828459045235360287471352662498)
#define QConst_M_LN10      QConst32(2.30258509299404568402)
#define QConst_M_LN10l     QConst64(2.302585092994045684017991454684364208)
#define QConst_M_LN2       QConst32(0.69314718055994530942)
#define QConst_M_LN2l      QConst64(0.693147180559945309417232121458176568)
#define QConst_M_LOG10E    QConst32(0.43429448190325182765)
#define QConst_M_LOG10El   QConst64(0.434294481903251827651128918916605082)
#define QConst_M_LOG2E     QConst32(1.4426950408889634074)
#define QConst_M_LOG2El    QConst64(1.442695040888963407359924681001892137)
#define QConst_M_PI        QConst32(3.14159265358979323846)
#define QConst_M_PI_2      QConst32(1.57079632679489661923)
#define QConst_M_PI_2l     QConst64(1.570796326794896619231321691639751442)
#define QConst_M_PI_4      QConst32(0.78539816339744830962)
#define QConst_M_PI_4l     QConst64(0.785398163397448309615660845819875721)
#define QConst_M_PIl       QConst64(3.141592653589793238462643383279502884)
#define QConst_M_SQRT1_2   QConst32(0.70710678118654752440)
#define QConst_M_SQRT1_2l  QConst64(0.707106781186547524400844362104849039)
#define QConst_M_SQRT2     QConst32(1.41421356237309504880)
#define QConst_M_SQRT2l    QConst64(1.414213562373095048801688724209698079)

#define QConst_M_ZERO      QConst1(0)
#define QConst_M_ONE       QConst1(1)
#define QConst_M_TWO       QConst1(2)
#define QConst_M_HALF      QConst4(0.5)
///@}

///@{
/** @name Predefined LibKet constant types */
#define QConst_M_1_PI_t      QConst32_t(0.31830988618379067154)
#define QConst_M_1_PIl_t     QConst64_t(0.318309886183790671537767526745028724)
#define QConst_M_2_PI_t      QConst32_t(0.63661977236758134308)
#define QConst_M_2_PIl_t     QConst64_t(0.636619772367581343075535053490057448)
#define QConst_M_2_SQRTPI_t  QConst32_t(1.12837916709551257390)
#define QConst_M_2_SQRTPIl_t QConst64_t(1.128379167095512573896158903121545172)
#define QConst_M_E_t         QConst32_t(2.7182818284590452354)
#define QConst_M_El_t        QConst64_t(2.718281828459045235360287471352662498)
#define QConst_M_LN10_t      QConst32_t(2.30258509299404568402)
#define QConst_M_LN10l_t     QConst64_t(2.302585092994045684017991454684364208)
#define QConst_M_LN2_t       QConst32_t(0.69314718055994530942)
#define QConst_M_LN2l_t      QConst64_t(0.693147180559945309417232121458176568)
#define QConst_M_LOG10E_t    QConst32_t(0.43429448190325182765)
#define QConst_M_LOG10El_t   QConst64_t(0.434294481903251827651128918916605082)
#define QConst_M_LOG2E_t     QConst32_t(1.4426950408889634074)
#define QConst_M_LOG2El_t    QConst64_t(1.442695040888963407359924681001892137)
#define QConst_M_PI_t        QConst32_t(3.14159265358979323846)
#define QConst_M_PI_2_t      QConst32_t(1.57079632679489661923)
#define QConst_M_PI_2l_t     QConst64_t(1.570796326794896619231321691639751442)
#define QConst_M_PI_4_t      QConst32_t(0.78539816339744830962)
#define QConst_M_PI_4l_t     QConst64_t(0.785398163397448309615660845819875721)
#define QConst_M_PIl_t       QConst64_t(3.141592653589793238462643383279502884)
#define QConst_M_SQRT1_2_t   QConst32_t(0.70710678118654752440)
#define QConst_M_SQRT1_2l_t  QConst64_t(0.707106781186547524400844362104849039)
#define QConst_M_SQRT2_t     QConst32_t(1.41421356237309504880)
#define QConst_M_SQRT2l_t    QConst64_t(1.414213562373095048801688724209698079)

#define QConst_M_ZERO_t      QConst1_t(0)
#define QConst_M_ONE_t       QConst1_t(1)
#define QConst_M_TWO_t       QConst1_t(2)
#define QConst_M_HALF_t      QConst4_t(0.5)
///@}

// clang-format on

#endif // QCONST_HPP
