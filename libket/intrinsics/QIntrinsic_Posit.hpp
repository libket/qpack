/** @file libket/intrinsics/QIntrinsic_Posit.hpp

    @brief C++ API quantum intrinsic posit data type class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup intrinsics
 */

#pragma once
#ifndef QINTRINSIC_POSIT_HPP
#define QINTRINSIC_POSIT_HPP

#include <intrinsics/QIntrinsic.hpp>

#ifndef NDEBUG
#define POSIT_THROW_ARITHMETIC_EXCEPTION 1
#endif

//#define POSIT_FAST_SPECIALIZATION
//#include <posit/posit>

namespace LibKet {

namespace intrinsics {

/**
@brief Quantum intrinsic posit data type class

The Quantum intrinsic posit data type implements posit data type
*/
template<std::size_t bits, std::size_t es>
class QPosit : public QIntrinsic
{};

} // namespace intrinsics

} // namespace LibKet

#endif // QINTRINSIC_POSIT_HPP
