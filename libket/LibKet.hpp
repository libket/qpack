/** @file libket/LibKet.hpp

    @brief C++ API main header file

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup cxx_api LibKet C++ API
 */
#pragma once
#ifndef LIBKET_HPP
#define LIBKET_HPP

#include <QBase.hpp>
#include <QConst.hpp>
#include <QCircuits.hpp>
#include <QDevice.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QFunctor.hpp>
#include <QGates.hpp>
#include <QGrammar.hpp>
#include <QIntrinsics.hpp>
#include <QProgram.hpp>
#include <QUtils.hpp>
#include <QVar.hpp>

#include <QJITCompiler.hpp>

#include <gates/QGate_CUnitary2.hpp>


#endif // LIBKET_HPP
