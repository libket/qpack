/** @file libket/devices/QDevice_IBMQ.hpp

    @brief C++ API IBM Quantum Experience device class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_IBMQ_HPP
#define QDEVICE_IBMQ_HPP

#include <ctime>
#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_OPENQASM
/**
   @brief IBM Quantum Experience physical device class

   This class executes quantum circuits remotely on physical quantum
   devices made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_IBMQ : public QExpression<_qubits, QBackendType::OpenQASMv2>
{
private:
  /// User access token
  const std::string API_token;

  /// Name of the provider's hub
  const std::string hub;

  /// Name of the provider's group
  const std::string group;

  /// Name of the provider's project
  const std::string project;

  /// Name of the backend
  const std::string backend;

  /// Number of shots to run the quantum kernel
  const std::size_t shots;

  /// Base type
  using Base = QExpression<_qubits, QBackendType::OpenQASMv2>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_IBMQ(
    const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),
    const std::string& hub = LibKet::getenv("IBMQ_HUB", "ibm-q"),
    const std::string& group = LibKet::getenv("IBMQ_GROUP", "open"),
    const std::string& project = LibKet::getenv("IBMQ_PROJECT", "main"),
    const std::string& backend = LibKet::getenv("IBMQ_BACKEND", "ibmq_armonk"),
    std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
    : API_token(API_token)
    , hub(hub)
    , group(group)
    , project(project)
    , backend(backend)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_IBMQ(const utils::json& config)
    : QDevice_IBMQ(config.find("API_token") != config.end()
                     ? config["API_token"]
                     : LibKet::getenv("IBMQ_API_TOKEN"),
                   config.find("hub") != config.end()
                     ? config["hub"]
                     : LibKet::getenv("IBMQ_HUB", "ibm-q"),
                   config.find("group") != config.end()
                     ? config["group"]
                     : LibKet::getenv("IBMQ_GROUP", "open"),
                   config.find("project") != config.end()
                     ? config["project"]
                     : LibKet::getenv("IBMQ_PROJECT", "main"),
                   config.find("backend") != config.end()
                     ? config["backend"]
                     : LibKet::getenv("IBMQ_BACKEND", "ibmq_armonk"),
                   config.find("shots") != config.end()
                     ? config["shots"].get<size_t>()
                     : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_IBMQ& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_IBMQ& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tdef default(o):\n"
       << "\t\timport numpy\n"
       << "\t\timport datetime\n"
       << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
       << "\t\tif isinstance(o, (datetime.date, datetime.datetime)): return "
          "o.isoformat()\n"
       << "\t\traise TypeError\n"
      
       << "\tdef get_provider():\n"
       << "\t\tif IBMQ.active_account(): return IBMQ.get_provider(hub='" << hub << "', group='" << group << "', project='" <<  project << "')\n"
       << "\t\telse: return IBMQ.enable_account(token='" << API_token
       << "', hub='" << hub << "', group='" << group << "', project='" << project << "')\n"
      
       << "\timport json\n"
       << "\timport time\n"
       << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
      "QuantumCircuit, execute\n";

    // Authentication to IBMQ
    if (API_token.empty())
      ss << "\tif IBMQ.active_account():\n"
         << "\t\tIBMQ.disable_account()\n"
         << "\tprovider = IBMQ.load_account()\n";
    else
      ss << "\tprovider = get_provider()\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('"  << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Execute circuit remotely
    ss << "\tstarttime = time.time()\n"
       << "\tjob = execute(qc, backend=backend, shots="
       << utils::to_string(shots > 0 ? shots : this->shots) << ")\n"
       << "\tresult = job.result()\n"
       << "\ttimedelta = time.time() - starttime\n"
       << "\ttimes = job.time_per_step()\n"
       << "\tif times.get('QUEUED') is None or times.get('RUNNING') is None:\n"
       << "\t\tprint('No queue time. Inserting -1')\n"
       << "\t\tt_queue = -1\n"
       << "\telse:\n"
       << "\t\tt_queue_datetime = times.get('RUNNING') - times.get('QUEUED')\n"
       << "\t\tt_queue = t_queue_datetime.total_seconds()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\toutput = result.to_dict()\n"
       << "\toutput['qjob_time'] = timedelta\n"
       << "\toutput['queue_time'] = t_queue\n"
       << "\treturn json.dumps(output, default=default)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

  /// Export circuit to ASCII string
  std::string print_circuit(const std::string& script_init = "",
                            const std::string& script_before = "",
                            const std::string& script_after = "",
                            QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\timport json\n"
       << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, QuantumCircuit\n"
       << "\tfrom qiskit.compiler import transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    // Authentication to IBMQ
    ss << "\tif IBMQ.active_account():\n\t\tIBMQ.disable_account()\n";
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_account()\n";
    else
      ss << "\tprovider = IBMQ.enable_account(token='" << API_token
         << "', hub='" << hub << "', group='" << group << "', project='"
         << project << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Transpile circuit
    ss << "\tqc = transpile(qc, backend=backend)\n";

    // Draw circuit to ASCII console output
    ss << "\tresult = circuit_drawer(qc, output='text').single_string()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Export circuit to LaTeX using the XYZ package
  std::string to_latex(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\timport json\n"
       << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, QuantumCircuit\n"
       << "\tfrom qiskit.compiler import transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    // Authentication to IBMQ
    ss << "\tif IBMQ.active_account():\n\t\tIBMQ.disable_account()\n";
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_account()\n";
    else
      ss << "\tprovider = IBMQ.enable_account(token='" << API_token
         << "', hub='" << hub << "', group='" << group << "', project='"
         << project << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Transpile circuit
    ss << "\tqc = transpile(qc, backend=backend)\n";

    // Draw circuit to latex source
    ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    std::size_t _value = 0;
    std::string _key = "0x0";

    for (auto& _item : _counts.items()) {
      if (_item.value() > _value) {
        _value = _item.value();
        _key = _item.key();
      }
    }

    return stoi(_key, 0, 16);
  }

  /// Get qpu execution duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["time_taken"].get<Rep>());
  }

   /// Get qjob duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::jobDuration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["qjob_time"].get<Rep>());
  }

  /// Get queue duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::queueDuration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["queue_time"].get<Rep>());
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (auto& _item : _counts.items()) {
      _histogram[stoi(_item.key(), 0, 16)] = (T)_item.value();
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return result["job_id"].get<std::string>();
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return result["success"].get<bool>();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    struct tm tm;
    strptime(
      result["date"].get<std::string>().c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
    return mktime(&tm);
  }
};

/**
   @brief IBM Quantum Experience simulator device class

   This class executes quantum circuits remotely on IBM's quantum
   simulator made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_IBMQ_simulator : public QExpression<_qubits, QBackendType::OpenQASMv2>
{
private:
  /// User access token
  const std::string API_token;

  /// Name of the provider's hub
  const std::string hub;

  /// Name of the provider's group
  const std::string group;

  /// Name of the provider's project
  const std::string project;

  /// Name of the backend
  const std::string backend;

  /// Number of shots to run the quantum kernel
  const std::size_t shots;

  /// Base type
  using Base = QExpression<_qubits, QBackendType::OpenQASMv2>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_IBMQ_simulator(
    const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),
    const std::string& hub = LibKet::getenv("IBMQ_HUB", "ibm-q"),
    const std::string& group = LibKet::getenv("IBMQ_GROUP", "open"),
    const std::string& project = LibKet::getenv("IBMQ_PROJECT", "main"),
    const std::string& backend = LibKet::getenv("IBMQ_BACKEND",
                                                "ibmq_qasm_simulator"),
    std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
    : API_token(API_token)
    , hub(hub)
    , group(group)
    , project(project)
    , backend(backend)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_IBMQ_simulator(const utils::json& config)
    : QDevice_IBMQ_simulator(
        config.find("API_token") != config.end()
          ? config["API_token"]
          : LibKet::getenv("IBMQ_API_TOKEN"),
        config.find("hub") != config.end()
          ? config["hub"]
          : LibKet::getenv("IBMQ_HUB", "ibm-q"),
        config.find("group") != config.end()
          ? config["group"]
          : LibKet::getenv("IBMQ_GROUP", "open"),
        config.find("project") != config.end()
          ? config["project"]
          : LibKet::getenv("IBMQ_PROJECT", "main"),
        config.find("backend") != config.end()
          ? config["backend"]
          : LibKet::getenv("IBMQ_BACKEND", "ibmq_qasm_simulator"),
        config.find("shots") != config.end()
          ? config["shots"].get<size_t>()
          : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_IBMQ_simulator& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_IBMQ_simulator& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tdef default(o):\n"
       << "\t\timport numpy\n"
       << "\t\timport datetime\n"
       << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
       << "\t\tif isinstance(o, (datetime.date, datetime.datetime)): return "
          "o.isoformat()\n"
       << "\t\traise TypeError\n"

       << "\tdef get_provider():\n"
       << "\t\tif IBMQ.active_account(): return IBMQ.get_provider(hub='" << hub << "', group='" << group << "', project='" <<  project << "')\n"
       << "\t\telse: return IBMQ.enable_account(token='" << API_token
       << "', hub='" << hub << "', group='" << group << "', project='" << project << "')\n"
      
       << "\timport json\n"
       << "\timport time\n"
       << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
      "QuantumCircuit, execute\n";

    // Import noise model for hardware simulators
    if (backend.compare("ibmq_qasm_simulator"))
      ss << "\tfrom qiskit.providers.aer import noise\n";

    // Authentication to IBMQ
    if (API_token.empty())
      ss << "\tif IBMQ.active_account():\n"
         << "\t\tIBMQ.disable_account()\n"
         << "\tprovider = IBMQ.load_account()\n";
    else
      ss << "\tprovider = get_provider()\n";          

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // Prepare noise model for hardware simulator
    if (backend.compare("ibmq_qasm_simulator")) {
      ss << "\tproperties = backend.properties()\n"
         << "\tcoupling_map = backend.configuration().coupling_map\n"
         << "\tnoise_model = noise.NoiseModel.from_backend(backend)\n"
         << "\tbasis_gates = noise_model.basis_gates\n"
         << "\tbackend_simulator = "
            "provider.get_backend('ibmq_qasm_simulator')\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute noisy circuit remotely
      ss << "\tstarttime = time.time()\n"
         << "\tjob = execute(qc, backend_simulator, coupling_map=coupling_map, "
            "noise_model=noise_model, basis_gates=basis_gates, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
         
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute circuit remotely
      ss << "\tstarttime = time.time()\n"
         << "\tjob = execute(qc, backend=backend, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    }
    ss << "\tresult = job.result()\n"
       << "\ttimedelta = time.time() - starttime\n"
       << "\ttimes = job.time_per_step()\n"
       << "\tif times.get('QUEUED') is None or times.get('RUNNING') is None:\n"
       << "\t\tprint('No queue time. Inserting -1')\n"
       << "\t\tt_queue = -1\n"
       << "\telse:\n"
       << "\t\tt_queue_datetime = times.get('RUNNING') - times.get('QUEUED')\n"
       << "\t\tt_queue = t_queue_datetime.total_seconds()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\toutput = result.to_dict()\n"
       << "\toutput['qjob_time'] = timedelta\n"
       << "\toutput['queue_time'] = t_queue\n"
       << "\treturn json.dumps(output, default=default)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return pointer to jbo
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

  /// Export circuit to ASCII string
  std::string print_circuit(const std::string& script_init = "",
                            const std::string& script_before = "",
                            const std::string& script_after = "",
                            QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    // Import noise model for hardware simulators
    if (backend.compare("ibmq_qasm_simulator"))
      ss << "\tfrom qiskit.providers.aer import noise\n";

    // Authentication to IBMQ
    ss << "\tif IBMQ.active_account():\n\t\tIBMQ.disable_account()\n";
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_account()\n";
    else
      ss << "\tprovider = IBMQ.enable_account(token='" << API_token
         << "', hub='" << hub << "', group='" << group << "', project='"
         << project << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // Prepare noise model for hardware simulator
    if (backend.compare("ibmq_qasm_simulator")) {
      ss << "\tproperties = backend.properties()\n"
         << "\tcoupling_map = backend.configuration().coupling_map\n"
         << "\tnoise_model = noise.NoiseModel.from_backend(backend)\n"
         << "\tbasis_gates = noise_model.basis_gates\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Transpile circuit
      ss << "\tqc = transpile(qc, backend, coupling_map=coupling_map, "
            "basis_gates=basis_gates)\n";

      // Draw circuit to latex source
      ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Draw circuit to ASCII console output
      ss << "\tresult = circuit_drawer(qc, output='text').single_string()\n";
    }

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Export circuit to LaTeX using the XYZ package
  std::string to_latex(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    // Import noise model for hardware simulators
    if (backend.compare("ibmq_qasm_simulator"))
      ss << "\tfrom qiskit.providers.aer import noise\n";

    // Authentication to IBMQ
    ss << "\tif IBMQ.active_account():\n\t\tIBMQ.disable_account()\n";
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_account()\n";
    else
      ss << "\tprovider = IBMQ.enable_account(token='" << API_token
         << "', hub='" << hub << "', group='" << group << "', project='"
         << project << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // Prepare noise model for hardware simulator
    if (backend.compare("ibmq_qasm_simulator")) {
      ss << "\tproperties = backend.properties()\n"
         << "\tcoupling_map = backend.configuration().coupling_map\n"
         << "\tnoise_model = noise.NoiseModel.from_backend(backend)\n"
         << "\tbasis_gates = noise_model.basis_gates\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Transpile circuit
      ss << "\tqc = transpile(qc, backend, coupling_map=coupling_map, "
            "basis_gates=basis_gates)\n";

      // Draw circuit to latex source
      ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Draw circuit to latex source
      ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";
    }

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    std::size_t _value = 0;
    std::string _key = "0x0";

    for (auto& _item : _counts.items()) {
      if (_item.value() > _value) {
        _value = _item.value();
        _key = _item.key();
      }
    }

    return stoi(_key, 0, 16);
  }

  /// Get qpu execution duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["time_taken"].get<Rep>());
  }

  /// Get qjob duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::jobDuration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["qjob_time"].get<Rep>());
  }

  /// Get queue duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::queueDuration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["queue_time"].get<Rep>());
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (auto& _item : _counts.items()) {
      _histogram[stoi(_item.key(), 0, 16)] = (T)_item.value();
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return result["job_id"].get<std::string>();
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return result["success"].get<bool>();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    struct tm tm;
    strptime(
      result["date"].get<std::string>().c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
    return mktime(&tm);
  }
};

#else

/**
   @brief IBM Quantum Experience physical device class

   This class executes quantum circuits remotely on physical quantum
   devices made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_IBMQ : public QDevice_Dummy
{};

/**
   @brief IBM Quantum Experience simulator device class

   This class executes quantum circuits remotely on IBM's quantum
   simulator made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_IBMQ_simulator : public QDevice_Dummy
{};
#endif

#define _QDeviceDefineIBMQ_simulator(_device, _name, _qubits)           \
  namespace device {                                                    \
    QDevicePropertyDefine(_device, _name, _qubits,                      \
                          true,                                         \
                          QEndianness::lsb);                            \
  }                                                                     \
                                                                        \
  template<std::size_t __qubits>                                        \
  class QDevice<_device,                                                \
                __qubits,                                               \
                device::QDeviceProperty<_device>::simulator,            \
                device::QDeviceProperty<_device>::endianness>           \
    : public QDevice_IBMQ_simulator<__qubits>                           \
  {                                                                            \
  public:                                                                      \
    QDevice(                                                                   \
      const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),         \
      const std::string& hub = LibKet::getenv("IBMQ_HUB", "ibm-q"),            \
      const std::string& group = LibKet::getenv("IBMQ_GROUP", "open"),         \
      const std::string& project = LibKet::getenv("IBMQ_PROJECT", "main"),     \
      std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))     \
      : QDevice_IBMQ_simulator<__qubits>(API_token,                     \
                                         hub,                           \
                                         group,                         \
                                         project,                       \
                                         device::QDeviceProperty<_device>::name, \
                                         shots)                         \
      {                                                                 \
        static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
                                                                        \
    QDevice(const utils::json& config)                                  \
      : QDevice(config.find("API_token") != config.end()                \
                ? config["API_token"]                                   \
                : LibKet::getenv("IBMQ_API_TOKEN"),                     \
                config.find("hub") != config.end()                      \
                ? config["hub"]                                         \
                : LibKet::getenv("IBMQ_GROUP", "ibm-q"),                \
                config.find("group") != config.end()                    \
                ? config["group"]                                       \
                : LibKet::getenv("IBMQ_GROUP", "open"),                 \
                config.find("project") != config.end()                  \
                ? config["project"]                                     \
                : LibKet::getenv("IBMQ_PROJECT", "main"),               \
                config.find("shots") != config.end()                    \
                ? config["shots"].get<size_t>()                         \
                : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))      \
      {                                                                 \
        static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
  };

#define _QDeviceDefineIBMQ(_device, _name, _qubits)                     \
  namespace device {                                                    \
    QDevicePropertyDefine(_device, _name, _qubits,                      \
                          false,                                        \
                          QEndianness::lsb);                            \
  }                                                                     \
                                                                        \
  template<std::size_t __qubits>                                        \
  class QDevice<_device,                                                \
                __qubits,                                               \
                device::QDeviceProperty<_device>::simulator,            \
                device::QDeviceProperty<_device>::endianness>           \
    : public QDevice_IBMQ<__qubits>                                     \
  {                                                                            \
  public:                                                                      \
    QDevice(                                                                   \
      const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),         \
      const std::string& hub = LibKet::getenv("IBMQ_HUB", "ibm-q"),            \
      const std::string& group = LibKet::getenv("IBMQ_GROUP", "open"),         \
      const std::string& project = LibKet::getenv("IBMQ_PROJECT", "main"),     \
      std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))     \
      : QDevice_IBMQ<__qubits>(API_token,                               \
                               hub,                                     \
                               group,                                   \
                               project,                                 \
                               device::QDeviceProperty<_device>::name,  \
                               shots)                                   \
      {                                                                 \
        static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
                                                                        \
    QDevice(const utils::json& config)                                  \
      : QDevice(config.find("API_token") != config.end()                       \
                  ? config["API_token"]                                        \
                  : LibKet::getenv("IBMQ_API_TOKEN"),                          \
                config.find("hub") != config.end()                             \
                  ? config["hub"]                                              \
                  : LibKet::getenv("IBMQ_GROUP", "ibm-q"),                     \
                config.find("group") != config.end()                           \
                  ? config["group"]                                            \
                  : LibKet::getenv("IBMQ_GROUP", "open"),                      \
                config.find("project") != config.end()                         \
                  ? config["project"]                                          \
                  : LibKet::getenv("IBMQ_PROJECT", "main"),                    \
                config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))           \
    {                                                                          \
      static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                    "#qubits exceeds device capacity");                 \
    }                                                                   \
  };

#define QDeviceDefineIBMQ_simulator(_name, _qubits)                     \
  _QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_##_name##_simulator,   \
                               "ibmq_"#_name, _qubits)

#define QDeviceDefineIBMQ(_name, _qubits)                               \
  _QDeviceDefineIBMQ(QDeviceType::ibmq_##_name,                         \
                     "ibmq_"#_name, _qubits)                            \
  _QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_##_name##_simulator,   \
                               "ibmq_"#_name, _qubits)

#define QDeviceDefineIBM(_name, _qubits)                               \
  _QDeviceDefineIBMQ(QDeviceType::ibmq_##_name,                         \
                     "ibm_"#_name, _qubits)                            \
  _QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_##_name##_simulator,   \
                               "ibm_"#_name, _qubits)  

// IBM Quantum Experience devices
QDeviceDefineIBMQ(almaden, 20);
QDeviceDefineIBMQ(armonk, 1);
QDeviceDefineIBMQ(athens, 5);
QDeviceDefineIBMQ(belem, 5);
QDeviceDefineIBMQ(boeblingen, 20);
QDeviceDefineIBMQ(bogota, 5);
QDeviceDefineIBMQ(brooklyn, 65);
QDeviceDefineIBMQ(burlington, 5);
QDeviceDefineIBMQ(cairo, 27);
QDeviceDefineIBMQ(cambridge, 28);
QDeviceDefineIBMQ(casablanca, 5);
QDeviceDefineIBMQ(dublin, 27);
QDeviceDefineIBMQ(essex, 5);
QDeviceDefineIBMQ(guadalupe, 16);
QDeviceDefineIBMQ(hanoi, 27);
QDeviceDefineIBMQ(jakarta, 7);
QDeviceDefineIBMQ(johannesburg, 20);
QDeviceDefineIBMQ(kolkata, 27);
QDeviceDefineIBM(lagos, 7);
QDeviceDefineIBMQ(lima, 5);
QDeviceDefineIBMQ(london, 5);
QDeviceDefineIBMQ(manhattan, 65);
QDeviceDefineIBMQ(manila, 5);
QDeviceDefineIBMQ(melbourne, 15);
QDeviceDefineIBMQ(montreal, 27);
QDeviceDefineIBMQ(mumbai, 27);
QDeviceDefineIBM(nairobi, 7);
QDeviceDefineIBMQ(ourense, 5);
QDeviceDefineIBMQ(paris, 27);
QDeviceDefineIBMQ(peekskill, 27);
QDeviceDefineIBMQ(poughkeepsie, 20);
QDeviceDefineIBMQ(quito, 5);
QDeviceDefineIBMQ(rochester, 53);
QDeviceDefineIBMQ(rome, 5);
QDeviceDefineIBMQ(rueschlikon, 16);
QDeviceDefineIBMQ(santiago, 5);
QDeviceDefineIBMQ(singapore, 20);
QDeviceDefineIBMQ(sydney, 27);
QDeviceDefineIBMQ(tenerife, 5);
QDeviceDefineIBMQ(tokyo, 20);
QDeviceDefineIBMQ(toronto, 27);
QDeviceDefineIBMQ(valencia, 5);
QDeviceDefineIBMQ(vigo, 5);
QDeviceDefineIBMQ(yorktown, 5);
QDeviceDefineIBMQ(washington, 127);
QDeviceDefineIBM(perth, 7);

_QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_qasm_simulator, "ibmq_qasm_simulator", 32);

} // namespace LibKet

#endif // QDEVICE_IBMQ_HPP
