/** @file libket/QProgram.hpp

    @brief C++ API quantum program class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup cxx_api
 */

#pragma once
#ifndef QPROGRAM_HPP
#define QPROGRAM_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QGates.hpp>

namespace LibKet {

  /**
     @brief Program class
  */

  class QProgram
  {
  public:    
    QProgram()
      : expr(new decltype(init()))
    {}      
    
  private:    
    QGate* expr = nullptr;
  };
  
} // namespace LibKet

#endif // QPROGRAM_HPP
