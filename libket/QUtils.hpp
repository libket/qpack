/** @file libket/QUtils.hpp

    @brief C++ API utility classes

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup utils Utility functions
    
    @ingroup cxx_api
 */

#pragma once
#ifndef QUTILS_HPP
#define QUTILS_HPP

#include <sys/stat.h>

#include <complex>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <utility>

#if defined _WIN32
#include <ShlObj.h>
#include <direct.h>
#include <windows.h>
#else
#include <dirent.h>
#include <dlfcn.h>
#include <pwd.h>
#include <unistd.h>
#endif

#include <QConfig.h>

#include <utils/counter.hpp>
#include <utils/graph.hpp>
#include <utils/sequence.hpp>
#include <utils/static_for.hpp>
#include <utils/zip.hpp>

namespace LibKet {

/**
 @namespace LibKet::utils

 @brief
 The LibKet::utils namespace, containing utility functionality of the LibKet
 project

 The LibKet::utils namespace contains internal utility
 functionality of the LibKet project that is not exposed to the
 end-user. Functionality in this namespace can change without
 notice.
*/

namespace utils {

/// @ingroup utils
///@{
  
  
/**
     Make JSON available
 */
using json = nlohmann::json;

/**
   @brief Compile-time variant of std::forward_as_tuple

   The std::forward_as_tuple function cannot be used within constant
   expressions. This variant can be used within constant expressions.

   @param args zero or more arguments to construct the tuple from

   @result A \b std::tuple object created as if by \b
   std::tuple<Types&&...>(std::forward<Types>(args)...)
  */
template<typename... Types>
constexpr std::tuple<Types...>
forward_as_tuple(Types&&... args) noexcept
{
  return std::tuple<Types...>{ std::forward<Types>(args)... };
}

/**
   @brief Compile-time hash function

   @param str character array to compute the hash for

   @result An \b unsigned \b long holding the hash value
*/
constexpr inline unsigned long
hash(const char* str)
{
  unsigned long hash = 5381;
  int c = 0;
  while ((c = *str++))
    hash = ((hash << 5) + hash) + c;
  // same as: hash = hash * 33 + c ;
  // but faster
  return hash;
}

/**
   @brief Convers a given integer to binary representation
*/
inline std::string
to_binary(uint64_t state, uint64_t numQubits)
{
  std::string s(numQubits, '0');
  uint64_t k = 0;
  while (numQubits--) {
    s[k] = (((state >> numQubits) & 1) ? '1' : '0');
    k++;
  }
  return s;
}

/**
   @brief Converts a given object to a string using \b std::to_string

   @param obj object to convert to string

   @result String representation of object \b obj
*/
template<typename T>
inline std::string
to_string(const T& obj)
{
  return std::to_string(obj);
}

/**
 @brief Converts a given \b float number to a string

 @param obj object to convert to string

 @result String representation of object \b obj
*/
template<>
std::string inline to_string(const float& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<float>::digits10)
     << obj;
  return ss.str();
}

/**
@brief Converts a given \b double number to a string

@param obj object to convert to string

@result String representation of object \b obj
*/
template<>
std::string inline to_string(const double& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<double>::digits10)
     << obj;
  return ss.str();
}

/**
@brief Converts a given \b long \b double number to a string

@param obj object to convert to string

@result String representation of object \b obj
*/
template<>
std::string inline to_string(const long double& obj)
{
  std::stringstream ss;
  ss << std::fixed
     << std::setprecision(std::numeric_limits<long double>::digits10) << obj;
  return ss.str();
}

/**
 @brief Converts a given \b std::complex<float> number to a string

 @param obj object to convert to string

 @result String representation of object \b obj
*/
template<>
std::string inline to_string(const std::complex<float>& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<float>::digits10)
     << "(" << obj.real() << "," << obj.imag() << ")";
  return ss.str();
}

/**
 @brief Converts a given \b std::complex<double> number to a string

 @param obj object to convert to string

 @result String representation of object \b obj
*/
template<>
std::string inline to_string(const std::complex<double>& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<double>::digits10)
     << "(" << obj.real() << "," << obj.imag() << ")";
  return ss.str();
}

/**
 @brief Converts a given \b std::complex<long double> number to a string

 @param obj object to convert to string

 @result String representation of object \b obj
*/
template<>
std::string inline to_string(const std::complex<long double>& obj)
{
  std::stringstream ss;
  ss << std::fixed
     << std::setprecision(std::numeric_limits<long double>::digits10) << "("
     << obj.real() << "," << obj.imag() << ")";
  return ss.str();
}

#ifdef LIBKET_WITH_QUEST
/**
 @brief Converts a given \b qreal number to a string

 @param obj object to convert to string

 @result String representation of object \b obj
  */
template<>
std::string inline to_string(const quest::Complex& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<qreal>::digits10)
     << "(" << obj.real << "," << obj.imag << ")";
  return ss.str();
}
#endif

/**
   @brief Idents each line of a multiline string by a given identer

   @param str multiline string

   @param identer identer string

   @result String with each line being idented by the given identer
 */
inline static std::string&
string_ident(std::string& str, const std::string& identer)
{
  if (str.empty())
    return str;

  str.insert(0, identer);
  std::size_t pos = str.find("\n", 1);
  std::size_t next = str.find("\n", pos + 1);

  while (next != std::string::npos) {
    str.insert(pos + 1, identer);
    pos = next + 1;
    next = str.find("\n", pos + 1);
  }

  return str;
}

inline static std::string
string_ident(const std::string& str, const std::string& identer)
{
  std::string s(str);
  return string_ident(s, identer);
}

/**
   @brief Returns all valid path separators

   @result String containing all valid path separators
*/
const std::string&
getValidPathSeparators()
{
#if defined _WIN32 || defined __CYGWIN__
  static const std::string ps("\\/");
#else
  static const std::string ps("/");
#endif
  return ps;
}

/**
   @brief Returns the native path separator

   @result Character containing the native path separator
*/
char
getNativePathSeparator()
{
  return getValidPathSeparators()[0];
}

/**
   @brief Sets last character to the native path seperator

   @note special case "" gets "./"

   @param str String whose last character is set to the native path separator
*/
void
makePath(std::string& str)
{
  if (str.length() == 0)
    str.push_back('.');
  if (str[str.length() - 1] != getNativePathSeparator())
    str.push_back(getNativePathSeparator());
}

/**
   @brief: Returns the native TEMP directory path

   @note This is a simplified version of the <a
   href="https://github.com/gismo/gismo/blob/09660940cd9c44800c55bf1fe66673abae7e668a/src/gsIO/gsFileManager.cpp#L349-L393">
   getTempPath</a> function from the 'gsFileManager.cpp' file
   taken from the <a href="https://github.com/gismo/gismo">G+So</a>
   library

   @copyright Copyright (C) Stefan Takacs and Angelos Mantzaflaris

   @result String containing the native TEMP directory path
 */
std::string
getTempPath()
{
#if defined(_WIN32)
  TCHAR _temp[MAX_PATH];
  (void)GetTempPath(MAX_PATH, // length of the buffer
                    _temp);   // buffer for path
  return std::string(_temp);
#else

  // Typically, we should consider TMPDIR
  //   http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap08.html#tag_08_03
  //   https://en.wikipedia.org/wiki/TMPDIR&oldid=728654758
  char* _temp = std::getenv("TMPDIR");
  // getenv returns NULL ptr if the variable is unknown
  // (http://en.cppreference.com/w/cpp/utility/program/getenv). If it is an
  // empty string, we should also exclude it.
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    makePath(_path);
    return _path;
  }

  // Okey, if first choice did not work, try this:
  _temp = std::getenv("TEMP");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    makePath(_path);
    return _path;
  }

  // And as third choice, use just current directory
  // http://man7.org/linux/man-pages/man2/getcwd.2.html
  _temp = getcwd(NULL, 0);
  if (NULL == _temp)
    LIBKET_ERROR("getcwd returned NULL.");
  std::string _path(_temp);
  // The string is allocated using malloc, see the reference above
  std::free(_temp);
  makePath(_path);
  return _path;
#endif
}

/**
   @brief: Returns the native HOME directory path

   @note This is a simplified version of the <a
   href="https://github.com/gismo/gismo/blob/09660940cd9c44800c55bf1fe66673abae7e668a/src/gsIO/gsFileManager.cpp#454-L476">
   getHomePath</a> function from the 'gsFileManager.cpp' file
   taken from the <a href="https://github.com/gismo/gismo">G+So</a>
   library

   @copyright Copyright (C) Stefan Takacs and Angelos Mantzaflaris

   @result String containing the native HOME directory path
 */
std::string
getLibKetPath()
{
  // Check if LIBKET_DIR is defined
  char* _temp = std::getenv("LIBKET_DIR");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    makePath(_path);
    return _path;
  }

#if defined(_WIN32)
  // Okey, if first choice did not work, try this:
  _temp = std::getenv("USERPROFILE");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    _path.append("\\.libket");
    makePath(_path);
    return _path;
  }

  // Okey, if second coice did not work, try this:
  char* _drive = std::getenv("HOMEDRIVE");
  char* _home = std::getenv("HOMEPATH");
  if (_drive != NULL && _drive[0] != '\0' && _home != NULL &&
      _home[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_drive);
    _path.append(std::string(_home));
    _path.append("\\.libket");
    makePath(_path);
    return _path;
  }
#else
  // Okey, if first choice did not work, try this:
  _temp = std::getenv("HOME");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    std::cout << _path << std::endl;
    _path.append("/.libket");
    makePath(_path);
    return _path;
  }
#endif

  // And as last choice, use just current directory
  // http://man7.org/linux/man-pages/man2/getcwd.2.html
  _temp = getcwd(NULL, 0);
  if (NULL == _temp)
    LIBKET_ERROR("getcwd returned NULL.");
  std::string _path(_temp);
  // The string is allocated using malloc, see the reference above
  std::free(_temp);
  makePath(_path);
  return _path;
}

/**
   @brief Returns true if the given directory path exists and false otherwise

   @param path character array containing the directory path

   @result True of the given directory path exists and false otherwise
*/
int
dirExists(const char* path)
{
  struct stat info;

  if (stat(path, &info) != 0)
    return 0; // does not exist
  else if (info.st_mode & S_IFDIR)
    return 1; // is directory
  else
    return 2; // is file
}

///@}

} // namespace utils

} // namespace LibKet

#endif // QUTILS_HPP
